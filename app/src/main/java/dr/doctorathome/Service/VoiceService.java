package dr.doctorathome.Service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.SeekBar;

import java.io.IOException;

import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;

public class VoiceService extends Service {
    MediaPlayer mp;
    private Boolean isPlay = false;
    Runnable run;
    Handler seekHandler = new Handler();


    public Boolean getPlay() {
        return isPlay;
    }

    public VoiceService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //getting systems default ringtone
        mp = new MediaPlayer();
        Bundle extras = intent.getExtras();
        //playVoice(extras.getString("url",""));
        //we have some options for service
        //start sticky means service will be explicity started and stopped
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //stopping the player when service is destroyed
    }

    public void playVoice(String url, SeekBar seekBar) {
        if (mp.isPlaying()) {
            mp.pause();
    //        viewHolder.play.setImageResource(R.drawable.ic_audio_player);
            isPlay = false;
            return;
        }

        try {
            mp.reset();
            mp.setDataSource(url);
            Log.d("TTTTT", url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mp.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        seekBar.setMax(mp.getDuration());
        if (!mp.isPlaying()) {
//            viewHolder.play.setImageResource(R.drawable.ic_puase_player);
            mp.start();
            run = new Runnable() {
                @Override
                public void run() {
                    // Updateing SeekBar every 100 miliseconds
                    seekBar.setProgress(mp.getCurrentPosition());
                    seekHandler.postDelayed(run, 100);
                }
            };
            run.run();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
               //     viewHolder.play.setImageResource(R.drawable.ic_audio_player);
                    mp.pause();

                }
            });
        } else {
            mp.pause();
           // viewHolder.play.setImageResource(R.drawable.ic_audio_player);

        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
               // viewHolder.play.setImageResource(R.drawable.ic_audio_player);

            }
        });


    }

}
