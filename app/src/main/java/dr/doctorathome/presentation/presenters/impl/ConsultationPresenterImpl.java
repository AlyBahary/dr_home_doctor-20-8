package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangeConsultationStatusInteractor;
import dr.doctorathome.domain.interactors.GetDoctorOnlineConsultationRequestsInteractor;
import dr.doctorathome.domain.interactors.GetOnlineConsultationDetailsInteractor;
import dr.doctorathome.domain.interactors.impl.ChangeConsultationStatusInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetDoctorOnlineConsultationRequestsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetOnlineConsultationDetailsInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.ChangeOnlineConsultationStatuesBody;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.OnlineConsultationsBody;
import dr.doctorathome.network.model.RestOnlineConsultationRequestDetailsResponse;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.presentation.presenters.ConsultationPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class ConsultationPresenterImpl extends AbstractPresenter implements ConsultationPresenter,
        GetDoctorOnlineConsultationRequestsInteractor.Callback
        , GetOnlineConsultationDetailsInteractor.Callback
, ChangeConsultationStatusInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;

    private String wentToGet;

    public ConsultationPresenterImpl(Executor executor, MainThread mainThread,
                                     View mView, DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }


    @Override
    public void onGettingDoctorOnlineConsultationRequestsSuccess(
            RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse) {
        mView.hideProgress();
        mView.doctorConsultationsRetrievedSuccessfully(restOnlineConsultationsRequestsResponse);
    }

    @Override
    public void onGettingDoctorOnlineConsultationRequestsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void getDoctorConsultationRequests(OnlineConsultationsBody onlineConsultationsBody) {
        mView.showProgress("");
        GetDoctorOnlineConsultationRequestsInteractor getDoctorOnlineConsultationRequestsInteractor
                = new GetDoctorOnlineConsultationRequestsInteractorImpl(
                mExecutor, mMainThread, this,
                mDoctorRepository, onlineConsultationsBody
                , CommonUtil.commonSharedPref().getString("token", ""));
        getDoctorOnlineConsultationRequestsInteractor.execute();
    }

    @Override
    public void getDoctorConsultationRequestDetails(int id) {
        mView.showProgress("");
        GetOnlineConsultationDetailsInteractor getOnlineConsultationDetailsInteractor
                = new GetOnlineConsultationDetailsInteractorImpl(
                mExecutor, mMainThread, this,
                mDoctorRepository, id
                , CommonUtil.commonSharedPref().getString("token", ""));
        getOnlineConsultationDetailsInteractor.execute();
    }

    @Override
    public void acceptRequest(ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.accepting_request));
        ChangeConsultationStatusInteractor changeVisitStatuesInteractor = new ChangeConsultationStatusInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeOnlineConsultationStatuesBody,
                CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "ACCEPT_REQUEST";
    }

    @Override
    public void rejectRequest(ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.rejecting_request));
        ChangeConsultationStatusInteractor changeVisitStatuesInteractor = new ChangeConsultationStatusInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeOnlineConsultationStatuesBody,
                CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "REJECT_REQUEST";
    }

    @Override
    public void cancelRequest(ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.cancelling_request));
        ChangeConsultationStatusInteractor changeVisitStatuesInteractor = new ChangeConsultationStatusInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeOnlineConsultationStatuesBody,
                CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "CANCEL_REQUEST";
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onGettingOnlineConsultationDetailsSuccess(RestOnlineConsultationRequestDetailsResponse
                                                                  restOnlineConsultationRequestDetailsResponse) {
        mView.hideProgress();
        mView.doctorConsultationDetailsRetrievedSuccessfully(restOnlineConsultationRequestDetailsResponse);
    }

    @Override
    public void onGettingOnlineConsultationDetailsFailed(String failedMessage) {

        mView.showError(failedMessage, false);
    }

    @Override
    public void onChangingConsultationStatusSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        switch (wentToGet) {
            case "ACCEPT_REQUEST":
                mView.requestAcceptedSuccessfully();
                break;
            case "REJECT_REQUEST":
                mView.requestRejectedSuccessfully();
                break;
            case "CANCEL_REQUEST":
                mView.requestCanceledSuccessfully();
                break;
        }
    }

    @Override
    public void onChangingConsultationStatusFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }
}
