package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;

public interface UpdateLocationPresenter {
    void updateDoctorLocationData(RestChangeDoctorStatuesBody changeDoctorStatuesBody);

}
