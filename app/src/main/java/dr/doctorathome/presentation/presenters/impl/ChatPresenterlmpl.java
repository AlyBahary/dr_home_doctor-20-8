package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.EndSessionInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.DoctorNewLocationBody;
import dr.doctorathome.network.model.RatePatientBody;
import dr.doctorathome.network.model.StartEndSessionBody;
import dr.doctorathome.presentation.presenters.VisitPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;

public class ChatPresenterlmpl extends AbstractPresenter implements VisitPresenter,  EndSessionInteractor.Callback {

    private View mView;
    private DoctorRepository mDoctorRepository;

    public ChatPresenterlmpl(Executor executor, MainThread mainThread) {
        super(executor, mainThread);
    }

    @Override
    public void onEndingSessionSuccess(CommonResponse commonResponse) {

    }

    @Override
    public void onEndingSessionFailed(String failedMessage) {

    }

    @Override
    public void getVisitDetails(int visitId) {

    }

    @Override
    public void acceptVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {

    }

    @Override
    public void rejectVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {

    }

    @Override
    public void cancelVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {

    }

    @Override
    public void startSession(StartEndSessionBody startEndSessionBody) {

    }

    @Override
    public void endSession(StartEndSessionBody startEndSessionBody) {

    }

    @Override
    public void ratePatient(Integer visitId, RatePatientBody ratePatientBody) {

    }

    @Override
    public void updateDoctorLocation(DoctorNewLocationBody doctorNewLocationBody) {

    }

    @Override
    public void isArrived(DoctorNewLocationBody doctorNewLocationBody) {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }
}
