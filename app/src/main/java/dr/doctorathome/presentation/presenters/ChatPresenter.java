package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.StartEndSessionBody;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface ChatPresenter extends BasePresenter {
    interface View extends BaseView {
        void sessionEndedSuccessfully();
    }

    void endSession(StartEndSessionBody startEndSessionBody);

}
