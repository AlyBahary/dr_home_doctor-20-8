package dr.doctorathome.presentation.presenters.impl;

import android.content.SharedPreferences;

import com.auth0.android.jwt.JWT;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetLookupsInteractor;
import dr.doctorathome.domain.interactors.LoginWithMailInteractor;
import dr.doctorathome.domain.interactors.RequestForgetPasswordCodeInteractor;
import dr.doctorathome.domain.interactors.RequestSignupCodeInteractor;
import dr.doctorathome.domain.interactors.ResetPasswordInteractor;
import dr.doctorathome.domain.interactors.SignupInteractor;
import dr.doctorathome.domain.interactors.ValidateSignUpDataInteractor;
import dr.doctorathome.domain.interactors.impl.GetLookupsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.LoginWithMailInteractorImpl;
import dr.doctorathome.domain.interactors.impl.RequestForgetPasswordCodeInteractorImpl;
import dr.doctorathome.domain.interactors.impl.RequestSignupCodeInteractorImpl;
import dr.doctorathome.domain.interactors.impl.ResetPasswordInteractorImpl;
import dr.doctorathome.domain.interactors.impl.SignupInteractorImpl;
import dr.doctorathome.domain.interactors.impl.ValidateSignUpDataInteractorImpl;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ResetPasswordModel;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestLoginWithMailDataModel;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestRequestForgetPasswordCodeResponse;
import dr.doctorathome.network.model.RestRequestSignupResponse;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.network.model.ValidateSignUpDataBody;
import dr.doctorathome.presentation.presenters.SignupPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class SignupPresenterImpl extends AbstractPresenter implements SignupPresenter,
        GetLookupsInteractor.Callback, RequestSignupCodeInteractor.Callback
        , RequestForgetPasswordCodeInteractor.Callback, SignupInteractor.Callback, ResetPasswordInteractor.Callback,
        ValidateSignUpDataInteractor.Callback {
    private View mView;
    private LoginRepository mLoginRepository;
    private CommonDataRepository mCommonDataRepository;
    private SharedPreferences mSharedPreferences;

    public SignupPresenterImpl(Executor executor, MainThread mainThread, View mView
            , LoginRepository mLoginRepository, CommonDataRepository mCommonDataRepository, SharedPreferences mSharedPreferences) {
        super(executor, mainThread);
        this.mView = mView;
        this.mLoginRepository = mLoginRepository;
        this.mCommonDataRepository = mCommonDataRepository;
        this.mSharedPreferences = mSharedPreferences;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onGettingLookupsSuccess(RestLookupsResponse restLookupsResponse) {
        mView.hideProgress();
        mView.lookupsRetrievedSuccessfully(restLookupsResponse);
    }

    @Override
    public void onGettingLookupsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void getLookups() {
        GetLookupsInteractor getLookupsInteractor = new GetLookupsInteractorImpl(mExecutor,
                mMainThread, this, mCommonDataRepository);
        getLookupsInteractor.execute();
    }

    @Override
    public void requestSignupCode(String phoneNumber) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.progress_dialog_get_verification_code));
        RequestSignupCodeInteractor requestSignupCodeInteractor = new RequestSignupCodeInteractorImpl(mExecutor,
                mMainThread, this, mLoginRepository, phoneNumber);
        requestSignupCodeInteractor.execute();
    }

    @Override
    public void requestForgetPasswordCode(String email) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.progress_dialog_get_verification_code));
        RequestForgetPasswordCodeInteractor requestForgetPasswordCodeInteractor = new RequestForgetPasswordCodeInteractorImpl(mExecutor,
                mMainThread, this, mLoginRepository, email);
        requestForgetPasswordCodeInteractor.execute();
    }

    @Override
    public void signup(UserModel userModel) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.progress_dialog_sign_up));
        SignupInteractor signupInteractor = new SignupInteractorImpl(mExecutor,
                mMainThread, this, mLoginRepository, userModel);
        signupInteractor.execute();
    }

    @Override
    public void resetPassword(String email, String newPassword, String code) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.progress_dialog_resset_password));
        ResetPasswordInteractor resetPasswordInteractor = new ResetPasswordInteractorImpl(mExecutor,
                mMainThread, this, mLoginRepository, new ResetPasswordModel(email, newPassword, code));
        resetPasswordInteractor.execute();
    }

    @Override
    public void validateSignUpData(ValidateSignUpDataBody validateSignUpDataBody) {
        mView.showProgress("");
        ValidateSignUpDataInteractor validateSignUpDataInteractor = new ValidateSignUpDataInteractorImpl(mExecutor,
                mMainThread, this, mLoginRepository, validateSignUpDataBody);
        validateSignUpDataInteractor.execute();
    }

    @Override
    public void onRequestingSignupCodeSuccess(RestRequestSignupResponse restRequestSignupResponse) {
        mView.hideProgress();
        mView.signupCodeRetrievedSuccessfully(restRequestSignupResponse.getCode());
    }

    @Override
    public void onRequestingSignupCodeFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onSignupSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.signupRequestSentSuccessfully();
    }

    @Override
    public void onSignupFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onRequestingForgetPasswordCodeSuccess(RestRequestForgetPasswordCodeResponse restRequestForgetPasswordCodeResponse) {
        mView.hideProgress();
        mView.forgetPasswordCodeRetrievedSuccessfully(restRequestForgetPasswordCodeResponse.getCode());
    }

    @Override
    public void onRequestingForgetPasswordCodeFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onResetPasswordSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.passwordResetSuccessfully();
    }

    @Override
    public void onResetPasswordFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onDataValidateSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.dataValidatedSuccessfully();
    }

    @Override
    public void onDataValidateFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }
}
