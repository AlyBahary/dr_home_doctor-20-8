package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.BlockUnblockPatientInteractor;
import dr.doctorathome.domain.interactors.ChangeVisitStatuesInteractor;
import dr.doctorathome.domain.interactors.EnableDisableDoctorServiceInteractor;
import dr.doctorathome.domain.interactors.GetDoctorServisesInteractor;
import dr.doctorathome.domain.interactors.GetDoctorVisitRequestsInteractor;
import dr.doctorathome.domain.interactors.impl.BlockUnblockPatientInteractorImpl;
import dr.doctorathome.domain.interactors.impl.ChangeVisitStatuesInteractorImpl;
import dr.doctorathome.domain.interactors.impl.EnableDisableDoctorServiceInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetDoctorServicesInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetDoctorVisitRequestsInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.BlockPatientBody;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.EnableDisableServiceBody;
import dr.doctorathome.network.model.RestDoctorServicesResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.VisitRequestsBody;
import dr.doctorathome.presentation.presenters.ServicesPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class ServicesPresenterImpl extends AbstractPresenter implements ServicesPresenter,
        GetDoctorServisesInteractor.Callback, EnableDisableDoctorServiceInteractor.Callback,
        GetDoctorVisitRequestsInteractor.Callback, ChangeVisitStatuesInteractor.Callback, BlockUnblockPatientInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;

    private String goToWhich;

    private String wentToGet;

    public ServicesPresenterImpl(Executor executor, MainThread mainThread,
                                 View mView, DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }

    @Override
    public void onGettingDoctorServicesSuccess(RestDoctorServicesResponse restDoctorServicesResponse) {
        mView.hideProgress();
        mView.doctorServicesRetrievedSuccessfully(restDoctorServicesResponse);
    }

    @Override
    public void onGettingDoctorServicesFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void getDoctorServices() {
        mView.showProgress("");
        GetDoctorServisesInteractor getDoctorServicesInteractor = new GetDoctorServicesInteractorImpl(mExecutor, mMainThread, this,
                mDoctorRepository, CommonUtil.commonSharedPref().getInt("doctorId", -1)
                , CommonUtil.commonSharedPref().getString("token", ""));
        getDoctorServicesInteractor.execute();
    }

    @Override
    public void enableDisableDoctorService(EnableDisableServiceBody enableDisableServiceBody, int position) {
        EnableDisableDoctorServiceInteractor enableDisableDoctorServiceInteractor = new EnableDisableDoctorServiceInteractorImpl(
                mExecutor, mMainThread, this,
                mDoctorRepository, enableDisableServiceBody
                , CommonUtil.commonSharedPref().getString("token", ""), position);
        enableDisableDoctorServiceInteractor.execute();
    }

    @Override
    public void getDoctorVisitRequests(VisitRequestsBody visitRequestsBody) {
        mView.showProgress("");
        GetDoctorVisitRequestsInteractor getDoctorVisitRequestsInteractor = new GetDoctorVisitRequestsInteractorImpl(
                mExecutor, mMainThread, this,
                mDoctorRepository, visitRequestsBody
                , CommonUtil.commonSharedPref().getString("token", ""));
        getDoctorVisitRequestsInteractor.execute();
    }

    @Override
    public void blockPatient(Integer patientId) {
        mView.showProgress("");

        goToWhich = "Block";

        BlockUnblockPatientInteractor blockUnblockPatientInteractor = new BlockUnblockPatientInteractorImpl(
                mExecutor, mMainThread, this,
                mDoctorRepository, new BlockPatientBody("BLOCK",
                CommonUtil.commonSharedPref().getInt("doctorId", -1), patientId)
                , CommonUtil.commonSharedPref().getString("token", ""));
        blockUnblockPatientInteractor.execute();
    }

    @Override
    public void unblockPatient(Integer patientId) {
        mView.showProgress("");

        goToWhich = "Unblock";

        BlockUnblockPatientInteractor blockUnblockPatientInteractor = new BlockUnblockPatientInteractorImpl(
                mExecutor, mMainThread, this,
                mDoctorRepository, new BlockPatientBody("UNBLOCK",
                CommonUtil.commonSharedPref().getInt("doctorId", -1), patientId)
                , CommonUtil.commonSharedPref().getString("token", ""));
        blockUnblockPatientInteractor.execute();
    }

    @Override
    public void acceptVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.accepting_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "ACCEPT_VISIT";
    }

    @Override
    public void rejectVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.rejecting_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "REJECT_VISIT";
    }

    @Override
    public void cancelVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.cancelling_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "CANCEL_VISIT";
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onEnablingDisablingDoctorServiceSuccess(CommonResponse commonResponse, int position) {
        mView.doctorServiceEnabledDisabledSuccessfully(commonResponse, position);
    }

    @Override
    public void onEnablingDisablingDoctorServiceFailed(String failedMessage, int position) {
        mView.doctorServiceEnabledDisableFailed(failedMessage, position);
    }

    @Override
    public void onGettingDoctorVisitRequestsSuccess(RestVisitRequestsResponse restVisitRequestsResponse) {
        mView.hideProgress();
        mView.doctorVisitRequestRetrievedSuccessfully(restVisitRequestsResponse);
    }

    @Override
    public void onGettingDoctorVisitRequestsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onBlockUnblockPatientSuccess(CommonResponse commonResponse) {
        mView.hideProgress();

        if (goToWhich.contains("Block")) {
            mView.patientBlockedSuccessfully(commonResponse);
        } else if (goToWhich.contains("Unblock")) {
            mView.patientUnblockedSuccessfully(commonResponse);
        }
    }

    @Override
    public void onBlockUnblockPatientFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onChangingVisitStatuesSuccess(CommonResponse commonResponse) {
        switch (wentToGet) {
            case "ACCEPT_VISIT":
                mView.hideProgress();
                mView.visitAcceptedSuccessfully();
                break;
            case "REJECT_VISIT":
                mView.hideProgress();
                mView.visitRejectedSuccessfully();
                break;
            case "CANCEL_VISIT":
                mView.hideProgress();
                mView.visitCancelledSuccessfully();
                break;
        }
    }

    @Override
    public void onChangingVisitStatuesFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }
}
