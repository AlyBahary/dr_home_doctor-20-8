package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestNotificationsResponse;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface NotificationPresenter extends BasePresenter {
    interface View extends BaseView {
        void userNotificationsRetrievedSuccessfully(RestNotificationsResponse restNotificationsResponse);
        void notificationMarkedAsReadSuccessfully(CommonResponse commonResponse);
    }

    void requestUserNotifications(int pageNumber);
    void markNotificationAsRead(int notificationId);
}
