package dr.doctorathome.presentation.presenters.impl;

import android.content.SharedPreferences;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangeDoctorStatuesInteractor;
import dr.doctorathome.domain.interactors.ChangeVisitStatuesInteractor;
import dr.doctorathome.domain.interactors.GetAppSettingsInteractor;
import dr.doctorathome.domain.interactors.GetDoctorHomePageDataInteractor;
import dr.doctorathome.domain.interactors.GetLookupsInteractor;
import dr.doctorathome.domain.interactors.GetUserDataInteractor;
import dr.doctorathome.domain.interactors.SaveOneSignalUserIdInteractor;
import dr.doctorathome.domain.interactors.UpdateDoctorInteractor;
import dr.doctorathome.domain.interactors.impl.ChangeDoctorStatuesInteractorImpl;
import dr.doctorathome.domain.interactors.impl.ChangeVisitStatuesInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetAppSettingsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetDoctorHomePageDataInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetLookupsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetUserDataInteractorImpl;
import dr.doctorathome.domain.interactors.impl.SaveOneSignalUserIdInteractorImpl;
import dr.doctorathome.domain.interactors.impl.UpdateDoctorInteractorImpl;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.SaveOneSignalBody;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.presentation.presenters.UpdateLocationPresenter;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class UserDataPresenterImpl extends AbstractPresenter implements UserDataPresenter, UpdateLocationPresenter,
        GetDoctorHomePageDataInteractor.Callback, ChangeDoctorStatuesInteractor.Callback, GetLookupsInteractor.Callback
        , GetUserDataInteractor.Callback, ChangeVisitStatuesInteractor.Callback, UpdateDoctorInteractor.Callback,
        SaveOneSignalUserIdInteractor.Callback{
    private View mView;
    private LoginRepository mLoginRepository;
    private CommonDataRepository mCommonDataRepository;
    private DoctorRepository mDoctorRepository;
    private SharedPreferences mSharedPreferences;

    private String wentToGet;

    public UserDataPresenterImpl(Executor executor, MainThread mainThread, View mView
            , CommonDataRepository mCommonDataRepository, LoginRepository mLoginRepository
            , DoctorRepository mDoctorRepository, SharedPreferences mSharedPreferences) {
        super(executor, mainThread);
        this.mView = mView;
        this.mLoginRepository = mLoginRepository;
        this.mCommonDataRepository = mCommonDataRepository;
        this.mDoctorRepository = mDoctorRepository;
        this.mSharedPreferences = mSharedPreferences;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onGettingUserDataSuccess(RestUserDataResponse restUserDataResponse) {
        mView.hideProgress();
        mView.userDataRetrievedSuccessfully(restUserDataResponse);
    }

    @Override
    public void onGettingUserDataFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void getUserData() {
        mView.showProgress("");
        GetUserDataInteractor getUserDataInteractor = new GetUserDataInteractorImpl(mExecutor, mMainThread, this,
                mLoginRepository, mSharedPreferences.getInt("doctorId", -1)
                , mSharedPreferences.getString("token", ""));
        getUserDataInteractor.execute();
    }

    @Override
    public void getLookups() {
        mView.showProgress("");
        GetLookupsInteractor getLookupsInteractor = new GetLookupsInteractorImpl(mExecutor,
                mMainThread, this, mCommonDataRepository);
        getLookupsInteractor.execute();
    }

    @Override
    public void updateDoctorData(UserDataForUpdateBody userData) {
        mView.showProgress("");
        UpdateDoctorInteractor updateDoctorInteractor = new UpdateDoctorInteractorImpl(mExecutor, mMainThread, this,
                mLoginRepository, userData
                , mSharedPreferences.getString("token", ""));
        updateDoctorInteractor.execute();
    }

    @Override
    public void changeDoctorStatuesData(RestChangeDoctorStatuesBody changeDoctorStatuesBody) {
        mView.showProgress("");
        ChangeDoctorStatuesInteractor changeDoctorStatuesInteractor = new ChangeDoctorStatuesInteractorImpl(mExecutor, mMainThread, this,
                mDoctorRepository, changeDoctorStatuesBody
                , mSharedPreferences.getString("token", ""));
        changeDoctorStatuesInteractor.execute();
    }

    @Override
    public void getDoctorHomePageData() {
        mView.showProgress("");
        GetDoctorHomePageDataInteractor getDoctorHomePageDataInteractor = new GetDoctorHomePageDataInteractorImpl(mExecutor, mMainThread, this,
                mDoctorRepository, mSharedPreferences.getInt("doctorId", -1)
                , mSharedPreferences.getString("token", ""));
        getDoctorHomePageDataInteractor.execute();
    }

    @Override
    public void acceptVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.accepting_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "ACCEPT_VISIT";
    }

    @Override
    public void rejectVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.rejecting_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "REJECT_VISIT";
    }

    @Override
    public void cancelVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.cancelling_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "CANCEL_VISIT";
    }

    @Override
    public void saveOneSignalUserId(String supportMessageId) {
        SaveOneSignalUserIdInteractor saveOneSignalUserIdInteractor = new SaveOneSignalUserIdInteractorImpl(mExecutor,
                mMainThread, this,
                mCommonDataRepository, new SaveOneSignalBody(CommonUtil.commonSharedPref().getInt("userId", -1),
                supportMessageId, CommonUtil.commonSharedPref().getString("appLanguage", "ar")),
                CommonUtil.commonSharedPref().getString("token", ""));
        saveOneSignalUserIdInteractor.execute();
    }

    @Override
    public void onUpdateDoctorSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.doctorDataUpdatedSuccessfully(commonResponse);
    }

    @Override
    public void onUpdateDoctorFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onGettingLookupsSuccess(RestLookupsResponse restLookupsResponse) {
        mView.hideProgress();
        mView.lookupsRetrievedSuccessfully(restLookupsResponse);
    }

    @Override
    public void onGettingLookupsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onChangeDoctorStatuesSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.doctorStatuesSuccessfully(commonResponse);
    }

    @Override
    public void onChangeDoctorStatuesFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onGettingDoctorHomePageDataSuccess(RestHomePageResponse restHomePageResponse) {
        mView.hideProgress();
        mView.doctorHomePageDataRetrievedSuccessfully(restHomePageResponse);
    }

    @Override
    public void onGettingDoctorHomePageDataFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onChangingVisitStatuesSuccess(CommonResponse commonResponse) {
        switch (wentToGet) {
            case "ACCEPT_VISIT":
                mView.hideProgress();
                mView.visitAcceptedSuccessfully();
                break;
            case "REJECT_VISIT":
                mView.hideProgress();
                mView.visitRejectedSuccessfully();
                break;
            case "CANCEL_VISIT":
                mView.hideProgress();
                mView.visitCancelledSuccessfully();
                break;
        }
    }

    @Override
    public void onChangingVisitStatuesFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onSavingOneSignalUserIdSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.oneSignalUserIdSavedSuccessfully(commonResponse);
    }

    @Override
    public void onSavingOneSignalUserIdFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void updateDoctorLocationData(RestChangeDoctorStatuesBody changeDoctorStatuesBody) {
        ChangeDoctorStatuesInteractor changeDoctorStatuesInteractor = new ChangeDoctorStatuesInteractorImpl(mExecutor, mMainThread, this,
                mDoctorRepository, changeDoctorStatuesBody
                , mSharedPreferences.getString("token", ""));
        changeDoctorStatuesInteractor.execute();
    }
}
