package dr.doctorathome.presentation.presenters;

import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface LoginPresenter extends BasePresenter {
    interface View extends BaseView {
        void loginSuccessAndSaved();
    }
    void onLoginWithMail(String username, String password);
}
