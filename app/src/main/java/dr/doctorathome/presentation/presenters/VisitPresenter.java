package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.DoctorNewLocationBody;
import dr.doctorathome.network.model.RatePatientBody;
import dr.doctorathome.network.model.RestVisitDetailsResponse;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.network.model.StartEndSessionBody;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface VisitPresenter extends BasePresenter {
    interface View extends BaseView {
        void visitDetailsRetrievedSuccessfully(RestVisitDetailsResponse restVisitDetailsResponse);
        void visitAcceptedSuccessfully();
        void visitRejectedSuccessfully();
        void visitCanceledSuccessfully();
        void sessionStartedSuccessfully();
        void sessionEndedSuccessfully();
        void ratePatientDoneSuccessfully();
        void doctorNewLocationSavedSuccessfully(ServerCommonResponse serverCommonResponse);
        void isDoctorArrivedSuccessfully(ServerCommonResponse serverCommonResponse);
    }

    void getVisitDetails(int visitId);
    void acceptVisit(ChangeVisitStatuesBody changeVisitStatuesBody);
    void rejectVisit(ChangeVisitStatuesBody changeVisitStatuesBody);
    void cancelVisit(ChangeVisitStatuesBody changeVisitStatuesBody);
    void startSession(StartEndSessionBody startEndSessionBody);
    void endSession(StartEndSessionBody startEndSessionBody);
    void ratePatient(Integer visitId, RatePatientBody ratePatientBody);
    void updateDoctorLocation(DoctorNewLocationBody doctorNewLocationBody);
    void isArrived(DoctorNewLocationBody doctorNewLocationBody);
}
