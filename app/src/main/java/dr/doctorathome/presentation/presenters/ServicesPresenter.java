package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.EnableDisableServiceBody;
import dr.doctorathome.network.model.RestDoctorServicesResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.VisitRequestsBody;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface ServicesPresenter extends BasePresenter {
    interface View extends BaseView {
        void doctorServicesRetrievedSuccessfully(RestDoctorServicesResponse restDoctorServicesResponse);

        void doctorServiceEnabledDisabledSuccessfully(CommonResponse commonResponse, int position);

        void doctorServiceEnabledDisableFailed(String message, int position);

        void doctorVisitRequestRetrievedSuccessfully(RestVisitRequestsResponse restVisitRequestsResponse);

        void patientBlockedSuccessfully(CommonResponse commonResponse);

        void patientUnblockedSuccessfully(CommonResponse commonResponse);

        void visitAcceptedSuccessfully();

        void visitRejectedSuccessfully();

        void visitCancelledSuccessfully();
    }

    void getDoctorServices();

    void enableDisableDoctorService(EnableDisableServiceBody enableDisableServiceBody, int position);

    void getDoctorVisitRequests(VisitRequestsBody visitRequestsBody);

    void blockPatient(Integer patientId);

    void unblockPatient(Integer patientId);

    void acceptVisit(ChangeVisitStatuesBody changeVisitStatuesBody);

    void rejectVisit(ChangeVisitStatuesBody changeVisitStatuesBody);

    void cancelVisit(ChangeVisitStatuesBody changeVisitStatuesBody);
}
