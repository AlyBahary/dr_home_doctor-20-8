package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface RenewCardPresenter extends BasePresenter {
    interface View extends BaseView {
        void renewCardSuccess(CommonResponse commonResponse);
    }

    void onRenewingCard(Integer  doctorId,String date, String newCard, String token);
}
