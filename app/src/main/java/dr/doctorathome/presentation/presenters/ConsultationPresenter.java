package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.ChangeOnlineConsultationStatuesBody;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.OnlineConsultationsBody;
import dr.doctorathome.network.model.RestOnlineConsultationRequestDetailsResponse;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface ConsultationPresenter extends BasePresenter {
    interface View extends BaseView {
        void doctorConsultationsRetrievedSuccessfully(
                RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse);

        void doctorConsultationDetailsRetrievedSuccessfully(
                RestOnlineConsultationRequestDetailsResponse restOnlineConsultationRequestDetailsResponse);
        void requestAcceptedSuccessfully();
        void requestRejectedSuccessfully();
        void requestCanceledSuccessfully();
    }

    void getDoctorConsultationRequests(OnlineConsultationsBody onlineConsultationsBody);
    void getDoctorConsultationRequestDetails(int id);
    void acceptRequest(ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody);
    void rejectRequest(ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody);
    void cancelRequest(ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody);
}
