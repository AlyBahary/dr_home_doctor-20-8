package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.ChangePasswordBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RequestClearanceBody;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface SettingsPresenter extends BasePresenter {
    interface View extends BaseView {
        void clearanceRequestSubmittedSuccessfully(CommonResponse commonResponse);

        void passwordChangedSuccessfully(CommonResponse commonResponse);

        void appSettingsRetrievedSuccessfully(RestAppSettingsResponse restAppSettingsResponse);

        void supportMessageSentSuccessfully(CommonResponse commonResponse);

        void supportMessagesHistoryRetrievedSuccessfully(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse);

        void supportMessageDetailsRetrievedSuccessfully(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse);

        void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse);
    }

    void requestClearance(RequestClearanceBody requestClearanceBody);

    void changePassword(ChangePasswordBody changePasswordBody);

    void getAppSettings();

    void sendSupportMessage(String subject, String message);

    void getSupportMessagesHistory();

    void getSupportMessageDetails(int supportMessageId);

    void saveOneSignalUserId(String supportMessageId);
}
