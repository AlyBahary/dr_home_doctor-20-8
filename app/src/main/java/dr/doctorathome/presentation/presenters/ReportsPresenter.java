package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.RestReportDetailsResponse;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface ReportsPresenter extends BasePresenter {
    interface View extends BaseView {
        void reportDetailsRetrieved(RestReportDetailsResponse restReportDetailsResponse);
    }

    void onRequestingReport(String fromDate, String toDate);
}
