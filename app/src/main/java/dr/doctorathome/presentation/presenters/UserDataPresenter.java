package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface UserDataPresenter extends BasePresenter {
    interface View extends BaseView {

        void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse);

        void doctorDataUpdatedSuccessfully(CommonResponse commonResponse);
        void doctorStatuesSuccessfully(CommonResponse commonResponse);
        void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse);
        void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse);

        void visitAcceptedSuccessfully();

        void visitRejectedSuccessfully();

        void visitCancelledSuccessfully();

        void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse);
    }
    void getUserData();
    void getLookups();
    void updateDoctorData(UserDataForUpdateBody userData);
    void changeDoctorStatuesData(RestChangeDoctorStatuesBody changeDoctorStatuesBody);
    void getDoctorHomePageData();

    void acceptVisit(ChangeVisitStatuesBody changeVisitStatuesBody);

    void rejectVisit(ChangeVisitStatuesBody changeVisitStatuesBody);

    void cancelVisit(ChangeVisitStatuesBody changeVisitStatuesBody);

    void saveOneSignalUserId(String supportMessageId);
}
