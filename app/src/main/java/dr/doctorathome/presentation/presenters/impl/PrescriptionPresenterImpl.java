package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetAllLabTestsInteractor;
import dr.doctorathome.domain.interactors.GetAllSystemMedicinesInteractor;
import dr.doctorathome.domain.interactors.GetDoctorMedicineBoxesInteractor;
import dr.doctorathome.domain.interactors.GetPrescriptionDetailsInteractor;
import dr.doctorathome.domain.interactors.SavePrescriptionInteractor;
import dr.doctorathome.domain.interactors.impl.GetAllLabTestsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetAllSystemMedicinesInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetDoctorMedicineBoxesInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetPrescriptionDetailsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.SavePrescriptionInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.DoctorMedicineBoxesBody;
import dr.doctorathome.network.model.RestAllLabTestsResponse;
import dr.doctorathome.network.model.RestAllMedicinesResponse;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;
import dr.doctorathome.network.model.SavingPrescreptionBody;
import dr.doctorathome.presentation.presenters.PrescriptionPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class PrescriptionPresenterImpl extends AbstractPresenter implements PrescriptionPresenter,
        GetAllSystemMedicinesInteractor.Callback, GetAllLabTestsInteractor.Callback,
        GetDoctorMedicineBoxesInteractor.Callback,
        SavePrescriptionInteractor.Callback,
        GetPrescriptionDetailsInteractor.Callback {

    private View mView;
    private DoctorRepository mDoctorRepository;

    public PrescriptionPresenterImpl(Executor executor, MainThread mainThread, View mView,
                                     DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }

    @Override
    public void onGettingAllSystemMedicinesSuccess(RestAllMedicinesResponse restAllMedicinesResponse) {
        mView.hideProgress();
        mView.allSystemMedicinesRetrievedSuccessfully(restAllMedicinesResponse);

        getAllLabTests();
    }

    @Override
    public void onGettingAllSystemMedicinesFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void getAllSystemMedicines() {
        mView.showProgress("");
        GetAllSystemMedicinesInteractor getAllSystemMedicinesInteractor = new GetAllSystemMedicinesInteractorImpl(
                mExecutor, mMainThread,
                this, mDoctorRepository, CommonUtil.commonSharedPref().getString("token", ""));
        getAllSystemMedicinesInteractor.execute();
    }

    @Override
    public void getAllLabTests() {
        mView.showProgress("");
        GetAllLabTestsInteractor getAllLabTestsInteractor = new GetAllLabTestsInteractorImpl(
                mExecutor, mMainThread,
                this, mDoctorRepository, CommonUtil.commonSharedPref().getString("token", ""));
        getAllLabTestsInteractor.execute();
    }

    @Override
    public void getDoctorMedicineBoxes(int medicineId) {
        mView.showProgress("");
        GetDoctorMedicineBoxesInteractor getDoctorMedicineBoxesInteractor = new GetDoctorMedicineBoxesInteractorImpl(
                mExecutor, mMainThread,
                this, mDoctorRepository,
                new DoctorMedicineBoxesBody(CommonUtil.commonSharedPref().getInt("doctorId", -1),
                        medicineId)
                ,CommonUtil.commonSharedPref().getString("token", ""));
        getDoctorMedicineBoxesInteractor.execute();
    }

    @Override
    public void savePrescription(SavingPrescreptionBody savingPrescreptionBody) {
        mView.showProgress("");
        SavePrescriptionInteractor savePrescriptionInteractor = new SavePrescriptionInteractorImpl(
                mExecutor, mMainThread,
                this, mDoctorRepository,
                savingPrescreptionBody
                ,CommonUtil.commonSharedPref().getString("token", ""));
        savePrescriptionInteractor.execute();
    }

    @Override
    public void getPrescriptionDetails(int visitId) {
        mView.showProgress("");
        GetPrescriptionDetailsInteractor getPrescriptionDetailsInteractor = new GetPrescriptionDetailsInteractorImpl(
                mExecutor, mMainThread,
                this, mDoctorRepository,
                visitId
                ,CommonUtil.commonSharedPref().getString("token", ""));
        getPrescriptionDetailsInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onGettingAllLabTestsSuccess(RestAllLabTestsResponse restAllLabTestsResponse) {
        mView.hideProgress();
        mView.allLabTestsRetrievedSuccessfully(restAllLabTestsResponse);
    }

    @Override
    public void onGettingAllLabTestsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onGettingDoctorMedicineBoxesSuccess(RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse) {
        mView.hideProgress();
        mView.allDoctorMedicineBoxesRetrievedSuccessfully(restDoctorMedicineBoxesResponse);
    }

    @Override
    public void onGettingDoctorMedicineBoxesFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onSavePrescriptionSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.prescriptionSavedSuccessfully(commonResponse);
    }

    @Override
    public void onSavePrescriptionFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onGettingPrescriptionDetailsSuccess(RestPrescriptionDetailsResponse restPrescriptionDetailsResponse) {
        mView.hideProgress();
        mView.prescriptionDetailsRetrievedSuccessfully(restPrescriptionDetailsResponse);
    }

    @Override
    public void onGettingPrescriptionDetailsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }
}
