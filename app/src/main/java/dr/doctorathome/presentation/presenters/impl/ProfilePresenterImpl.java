package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetProfileStatisticsInteractor;
import dr.doctorathome.domain.interactors.impl.GetProfileStatisticsInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestProfileStatistcsResponse;
import dr.doctorathome.presentation.presenters.ProfilePresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class ProfilePresenterImpl extends AbstractPresenter implements ProfilePresenter,
        GetProfileStatisticsInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;

    public ProfilePresenterImpl(Executor executor, MainThread mainThread, View mView
            , DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onGettingProfileStatisticsSuccess(RestProfileStatistcsResponse restProfileStatistcsResponse) {
        mView.hideProgress();
        mView.profileStatisticsRetrieved(restProfileStatistcsResponse);
    }

    @Override
    public void onGettingProfileStatisticsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onRequestingProfileStatistics() {
        mView.showProgress("");

        GetProfileStatisticsInteractor getProfileStatisticsInteractor = new GetProfileStatisticsInteractorImpl(mExecutor,
                mMainThread, this, mDoctorRepository, CommonUtil.commonSharedPref()
                .getInt("doctorId", -1), CommonUtil.commonSharedPref().getString("token", ""));
        getProfileStatisticsInteractor.execute();
    }
}
