package dr.doctorathome.presentation.presenters.impl;

import android.content.SharedPreferences;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangePasswordInteractor;
import dr.doctorathome.domain.interactors.GetAppSettingsInteractor;
import dr.doctorathome.domain.interactors.GetSupportMessageDetailsInteractor;
import dr.doctorathome.domain.interactors.GetSupportMessagesHistoryInteractor;
import dr.doctorathome.domain.interactors.RequestClearanceInteractor;
import dr.doctorathome.domain.interactors.SaveOneSignalUserIdInteractor;
import dr.doctorathome.domain.interactors.SendSupportMessageInteractor;
import dr.doctorathome.domain.interactors.impl.ChangePasswordInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetAppSettingsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetSupportMessageDetailsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetSupportMessagesHistoryInteractorImpl;
import dr.doctorathome.domain.interactors.impl.RequestClearanceInteractorImpl;
import dr.doctorathome.domain.interactors.impl.SaveOneSignalUserIdInteractorImpl;
import dr.doctorathome.domain.interactors.impl.SendSupportMessageInteractorImpl;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.ChangePasswordBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RequestClearanceBody;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.model.SaveOneSignalBody;
import dr.doctorathome.network.model.SendSupportMessageBody;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.presentation.presenters.SettingsPresenter;
import dr.doctorathome.utils.CommonUtil;

public class SettingsPresenterImpl extends AbstractPresenter implements SettingsPresenter,
        RequestClearanceInteractor.Callback, ChangePasswordInteractor.Callback,
        GetAppSettingsInteractor.Callback, SendSupportMessageInteractor.Callback,
        GetSupportMessagesHistoryInteractor.Callback,
        GetSupportMessageDetailsInteractor.Callback,
        SaveOneSignalUserIdInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;
    private CommonDataRepository mCommonDataRepository;

    public SettingsPresenterImpl(Executor executor, MainThread mainThread,
                                 View mView, DoctorRepository mDoctorRepository, CommonDataRepository mCommonDataRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
        this.mCommonDataRepository = mCommonDataRepository;
    }


    @Override
    public void requestClearance(RequestClearanceBody requestClearanceBody) {
        mView.showProgress("");
        RequestClearanceInteractor requestClearanceInteractor = new RequestClearanceInteractorImpl(mExecutor, mMainThread, this,
                mDoctorRepository, requestClearanceBody
                , CommonUtil.commonSharedPref().getString("token", ""));
        requestClearanceInteractor.execute();
    }

    @Override
    public void changePassword(ChangePasswordBody changePasswordBody) {
        mView.showProgress("");
        ChangePasswordInteractor changePasswordInteractor = new ChangePasswordInteractorImpl(mExecutor,
                mMainThread, this,
                mDoctorRepository, changePasswordBody
                , CommonUtil.commonSharedPref().getString("token", ""));
        changePasswordInteractor.execute();
    }

    @Override
    public void getAppSettings() {
        mView.showProgress("");
        GetAppSettingsInteractor getAppSettingsInteractor = new GetAppSettingsInteractorImpl(mExecutor,
                mMainThread, this,
                mCommonDataRepository);
        getAppSettingsInteractor.execute();
    }

    @Override
    public void sendSupportMessage(String subject, String message) {
        mView.showProgress("");
        SendSupportMessageInteractor sendSupportMessageInteractor = new SendSupportMessageInteractorImpl(mExecutor,
                mMainThread, this,
                mCommonDataRepository, new SendSupportMessageBody(CommonUtil.commonSharedPref().getInt("userId", -1),
                subject, message), CommonUtil.commonSharedPref().getString("token", ""));
        sendSupportMessageInteractor.execute();
    }

    @Override
    public void getSupportMessagesHistory() {
        mView.showProgress("");
        GetSupportMessagesHistoryInteractor getSupportMessagesHistoryInteractor = new GetSupportMessagesHistoryInteractorImpl(mExecutor,
                mMainThread, this,
                mCommonDataRepository, CommonUtil.commonSharedPref().getInt("userId", -1),
                CommonUtil.commonSharedPref().getString("token", ""));
        getSupportMessagesHistoryInteractor.execute();
    }

    @Override
    public void getSupportMessageDetails(int supportMessageId) {
        mView.showProgress("");
        GetSupportMessageDetailsInteractor getSupportMessageDetailsInteractor = new GetSupportMessageDetailsInteractorImpl(mExecutor,
                mMainThread, this,
                mCommonDataRepository, supportMessageId,
                CommonUtil.commonSharedPref().getString("token", ""));
        getSupportMessageDetailsInteractor.execute();
    }

    @Override
    public void saveOneSignalUserId(String supportMessageId) {
        SaveOneSignalUserIdInteractor saveOneSignalUserIdInteractor = new SaveOneSignalUserIdInteractorImpl(mExecutor,
                mMainThread, this,
                mCommonDataRepository, new SaveOneSignalBody(CommonUtil.commonSharedPref().getInt("userId", -1),
                supportMessageId, CommonUtil.commonSharedPref().getString("appLanguage", "ar")),
                CommonUtil.commonSharedPref().getString("token", ""));
        saveOneSignalUserIdInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onRequestClearanceSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.clearanceRequestSubmittedSuccessfully(commonResponse);
    }

    @Override
    public void onRequestClearanceFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onChangingPasswordSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.passwordChangedSuccessfully(commonResponse);
    }

    @Override
    public void onChangingPasswordFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onGettingAppSettingsSuccess(RestAppSettingsResponse restAppSettingsResponse) {
        SharedPreferences.Editor editor = CommonUtil.commonSharedPref().edit();
        editor.putBoolean("appSettings", true);
        editor.putInt("id", restAppSettingsResponse.getId());
        editor.putString("adminEmail", restAppSettingsResponse.getAdminEmail());
        editor.putString("facebook", restAppSettingsResponse.getFacebook());
        editor.putString("twitter", restAppSettingsResponse.getTwitter());
        editor.putString("instagram", restAppSettingsResponse.getInstagram());
        editor.putString("linkedIn", restAppSettingsResponse.getLinkedIn());
        editor.putString("youtube", restAppSettingsResponse.getYoutube());
        editor.putString("mobile", restAppSettingsResponse.getMobile());
        editor.putString("phone", restAppSettingsResponse.getPhone());
        editor.putString("address", restAppSettingsResponse.getAddress());
        editor.putString("aboutUS", restAppSettingsResponse.getAboutUS());
        editor.putString("termsAndConditions", restAppSettingsResponse.getTermsAndConditions());
        editor.apply();
        editor.commit();

        mView.hideProgress();
        mView.appSettingsRetrievedSuccessfully(restAppSettingsResponse);
    }

    @Override
    public void onGettingAppSettingsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onGettingSupportMessagesHistorySuccess(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse) {
        mView.hideProgress();
        mView.supportMessagesHistoryRetrievedSuccessfully(restSupportMessagesHistoryResponse);
    }

    @Override
    public void onGettingSupportMessagesHistoryFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onSendingSupportMessageSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.supportMessageSentSuccessfully(commonResponse);
    }

    @Override
    public void onSendingSupportMessageFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onGettingSupportMessageDetailsSuccess(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse) {
        mView.hideProgress();
        mView.supportMessageDetailsRetrievedSuccessfully(restSupportMessageDetailsResponse);
    }

    @Override
    public void onGettingSupportMessageDetailsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onSavingOneSignalUserIdSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.oneSignalUserIdSavedSuccessfully(commonResponse);
    }

    @Override
    public void onSavingOneSignalUserIdFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }
}
