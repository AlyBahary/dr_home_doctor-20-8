package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAllLabTestsResponse;
import dr.doctorathome.network.model.RestAllMedicinesResponse;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;
import dr.doctorathome.network.model.SavingPrescreptionBody;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface PrescriptionPresenter extends BasePresenter {
    interface View extends BaseView {
        void allSystemMedicinesRetrievedSuccessfully(RestAllMedicinesResponse restAllMedicinesResponse);
        void allLabTestsRetrievedSuccessfully(RestAllLabTestsResponse restAllLabTestsResponse);
        void allDoctorMedicineBoxesRetrievedSuccessfully(RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse);
        void prescriptionSavedSuccessfully(CommonResponse commonResponse);
        void prescriptionDetailsRetrievedSuccessfully(RestPrescriptionDetailsResponse restPrescriptionDetailsResponse);
    }

    void getAllSystemMedicines();
    void getAllLabTests();
    void getDoctorMedicineBoxes(int medicineId);
    void savePrescription(SavingPrescreptionBody savingPrescreptionBody);
    void getPrescriptionDetails(int visitId);
}
