package dr.doctorathome.presentation.presenters.impl;

import android.util.Log;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetUserNotificationsInteractor;
import dr.doctorathome.domain.interactors.MarkNotificationAsReadInteractor;
import dr.doctorathome.domain.interactors.impl.GetUserNotificationsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.MarkNotificationAsReadInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestNotificationsResponse;
import dr.doctorathome.presentation.presenters.NotificationPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class NotificationPresenterImpl extends AbstractPresenter implements NotificationPresenter,
        GetUserNotificationsInteractor.Callback, MarkNotificationAsReadInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;

    public NotificationPresenterImpl(Executor executor, MainThread mainThread, View mView
            , DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onGettingUserNotificationsSuccess(RestNotificationsResponse restNotificationsResponse) {
        mView.hideProgress();
        mView.userNotificationsRetrievedSuccessfully(restNotificationsResponse);
    }

    @Override
    public void onGettingUserNotificationsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onMarkingNotificationAsReadSuccess(CommonResponse commonResponse) {

    }

    @Override
    public void onMarkingNotificationAsReadFailed(String failedMessage) {

    }

    @Override
    public void requestUserNotifications(int pageNumber) {
        mView.showProgress("");
        GetUserNotificationsInteractor getUserNotificationsInteractor = new GetUserNotificationsInteractorImpl(mExecutor,
                mMainThread, this, mDoctorRepository, CommonUtil.commonSharedPref().getInt("userId", -1),
                CommonUtil.commonSharedPref().getString("token", ""), pageNumber);
        getUserNotificationsInteractor.execute();
        Log.d("TTTT", "requestUserNotifications: "+getUserNotificationsInteractor);
    }

    @Override
    public void markNotificationAsRead(int notificationId) {
        MarkNotificationAsReadInteractor markNotificationAsReadInteractor = new MarkNotificationAsReadInteractorImpl(mExecutor,
                mMainThread, this, mDoctorRepository, notificationId,
                CommonUtil.commonSharedPref().getString("token", ""));
        markNotificationAsReadInteractor.execute();
    }
}
