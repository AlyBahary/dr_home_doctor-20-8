package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.RenewCardInteractor;
import dr.doctorathome.domain.interactors.impl.RenewCardInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RenewCardBody;
import dr.doctorathome.presentation.presenters.RenewCardPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class RenewCardPresenterImpl extends AbstractPresenter implements RenewCardPresenter,
        RenewCardInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;

    public RenewCardPresenterImpl(Executor executor, MainThread mainThread, View mView
            , DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onRenewCardSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.renewCardSuccess(commonResponse);
    }

    @Override
    public void onRenewCardFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onRenewingCard(Integer doctorId, String date, String newCard, String token) {
        mView.showProgress("");

        RenewCardInteractor renewCardInteractor = new RenewCardInteractorImpl(mExecutor,
                mMainThread, this, mDoctorRepository, new RenewCardBody(doctorId, date, newCard), token);
        renewCardInteractor.execute();
    }
}
