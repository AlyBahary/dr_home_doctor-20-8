package dr.doctorathome.presentation.presenters.impl;

import android.content.SharedPreferences;
import android.util.Log;

import com.auth0.android.jwt.JWT;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.LoginWithMailInteractor;
import dr.doctorathome.domain.interactors.impl.LoginWithMailInteractorImpl;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestLoginWithMailDataModel;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.presentation.presenters.LoginPresenter;
import timber.log.Timber;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter,
        LoginWithMailInteractor.Callback {
    private LoginPresenter.View mView;
    private LoginRepository mLoginRepository;
    private SharedPreferences mSharedPreferences;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, View mView
            , LoginRepository mLoginRepository, SharedPreferences mSharedPreferences) {
        super(executor, mainThread);
        this.mView = mView;
        this.mLoginRepository = mLoginRepository;
        this.mSharedPreferences = mSharedPreferences;
    }

    @Override
    public void onLoginSuccess(RestLoginResponse loginResponse) {
        JWT jwt = new JWT(loginResponse.getAccessToken());
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("token", "Bearer " + loginResponse.getAccessToken());
        editor.putString("refresh_token", loginResponse.getRefresh_token());

        editor.putInt("doctorId", jwt.getClaim("doctorId").asInt());
        editor.putInt("userId", jwt.getClaim("userId").asInt());
        editor.putString("firstName", jwt.getClaim("firstName").asString());
        editor.putString("lastName", jwt.getClaim("lastName").asString());
        editor.putString("email", jwt.getClaim("email").asString());
        editor.putString("specialist", jwt.getClaim("specialist").asString());
        editor.putString("profilePicUrl", jwt.getClaim("profilePicUrl").asString());
        editor.putString("uploadedExpiredCardStatus", jwt.getClaim("uploadedExpiredCardStatus").asString());
        editor.putBoolean("isExpired", jwt.getClaim("isExpired").asBoolean());
        editor.putString("accountStatus", jwt.getClaim("accountStatus").asString());

        editor.apply();
        editor.commit();
        mView.hideProgress();
        mView.loginSuccessAndSaved();
    }

    @Override
    public void onLoginFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onLoginWithMail(String username, String password) {
        Timber.e("The message is :: " + AndroidApplication.getAppContext().getString(R.string.progress_dialog_logging_in));
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.progress_dialog_logging_in));

        LoginWithMailInteractor loginWithMailInteractor = new LoginWithMailInteractorImpl(mExecutor,
                mMainThread, this, mLoginRepository, new RestLoginWithMailDataModel(username, password, "password"));
        loginWithMailInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }
}
