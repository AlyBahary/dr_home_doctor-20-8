package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetReportInteractor;
import dr.doctorathome.domain.interactors.impl.GetReportInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.GetReportBody;
import dr.doctorathome.network.model.RestReportDetailsResponse;
import dr.doctorathome.presentation.presenters.ReportsPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class ReportsPresenterImpl extends AbstractPresenter implements ReportsPresenter,
        GetReportInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;

    public ReportsPresenterImpl(Executor executor, MainThread mainThread, View mView
            , DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onGettingReportSuccess(RestReportDetailsResponse restReportDetailsResponse) {
        mView.hideProgress();
        mView.reportDetailsRetrieved(restReportDetailsResponse);
    }

    @Override
    public void onGettingReportFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onRequestingReport(String fromDate, String toDate) {
        mView.showProgress("");

        GetReportInteractor getReportInteractor = new GetReportInteractorImpl(mExecutor,
                mMainThread, this, mDoctorRepository, new GetReportBody(CommonUtil.commonSharedPref()
                .getInt("doctorId", -1), fromDate, toDate), CommonUtil.commonSharedPref().getString("token", ""));
        getReportInteractor.execute();
    }
}
