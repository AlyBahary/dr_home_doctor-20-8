package dr.doctorathome.presentation.presenters.impl;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangeVisitStatuesInteractor;
import dr.doctorathome.domain.interactors.EndSessionInteractor;
import dr.doctorathome.domain.interactors.GetVisitDetailsInteractor;
import dr.doctorathome.domain.interactors.IsArrivedInteractor;
import dr.doctorathome.domain.interactors.RatePatientInteractor;
import dr.doctorathome.domain.interactors.StartSessionInteractor;
import dr.doctorathome.domain.interactors.UpdateDoctorLocationInteractor;
import dr.doctorathome.domain.interactors.impl.ChangeVisitStatuesInteractorImpl;
import dr.doctorathome.domain.interactors.impl.EndSessionInteractorImpl;
import dr.doctorathome.domain.interactors.impl.GetVisitDetailsInteractorImpl;
import dr.doctorathome.domain.interactors.impl.IsArrivedInteractorImpl;
import dr.doctorathome.domain.interactors.impl.RatePatientInteractorImpl;
import dr.doctorathome.domain.interactors.impl.StartSessionInteractorImpl;
import dr.doctorathome.domain.interactors.impl.UpdateDoctorLocationInteractorImpl;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.DoctorNewLocationBody;
import dr.doctorathome.network.model.RatePatientBody;
import dr.doctorathome.network.model.RestVisitDetailsResponse;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.network.model.StartEndSessionBody;
import dr.doctorathome.presentation.presenters.VisitPresenter;
import dr.doctorathome.presentation.presenters.base.AbstractPresenter;
import dr.doctorathome.utils.CommonUtil;

public class VisitPresenterImpl extends AbstractPresenter implements VisitPresenter,
        GetVisitDetailsInteractor.Callback, ChangeVisitStatuesInteractor.Callback,
        StartSessionInteractor.Callback, EndSessionInteractor.Callback,
        RatePatientInteractor.Callback, UpdateDoctorLocationInteractor.Callback,
        IsArrivedInteractor.Callback {
    private View mView;
    private DoctorRepository mDoctorRepository;

    private String wentToGet;

    public VisitPresenterImpl(Executor executor, MainThread mainThread, View mView,
                              DoctorRepository mDoctorRepository) {
        super(executor, mainThread);
        this.mView = mView;
        this.mDoctorRepository = mDoctorRepository;
    }

    @Override
    public void onGettingVisitDetailsSuccess(RestVisitDetailsResponse restVisitDetailsResponse) {
        mView.hideProgress();
        mView.visitDetailsRetrievedSuccessfully(restVisitDetailsResponse);
    }

    @Override
    public void onGettingVisitDetailsFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void getVisitDetails(int visitId) {
        mView.showProgress("");
        GetVisitDetailsInteractor getVisitDetailsInteractor = new GetVisitDetailsInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, visitId, CommonUtil.commonSharedPref().getString("token", ""));
        getVisitDetailsInteractor.execute();
    }

    @Override
    public void acceptVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.accepting_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "ACCEPT_VISIT";
    }

    @Override
    public void rejectVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.rejecting_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "REJECT_VISIT";
    }

    @Override
    public void cancelVisit(ChangeVisitStatuesBody changeVisitStatuesBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.cancelling_visit));
        ChangeVisitStatuesInteractor changeVisitStatuesInteractor = new ChangeVisitStatuesInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, changeVisitStatuesBody, CommonUtil.commonSharedPref().getString("token", ""));
        changeVisitStatuesInteractor.execute();
        wentToGet = "CANCEL_VISIT";
    }

    @Override
    public void startSession(StartEndSessionBody startEndSessionBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.starting_session));
        StartSessionInteractor startSessionInteractor = new StartSessionInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, startEndSessionBody, CommonUtil.commonSharedPref().getString("token", ""));
        startSessionInteractor.execute();
    }

    @Override
    public void endSession(StartEndSessionBody startEndSessionBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.ending_session));
        EndSessionInteractor endSessionInteractor = new EndSessionInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, startEndSessionBody, CommonUtil.commonSharedPref().getString("token", ""));
        endSessionInteractor.execute();
    }

    @Override
    public void ratePatient(Integer visitId, RatePatientBody ratePatientBody) {
        mView.showProgress(AndroidApplication.getAppContext().getString(R.string.rating_patient));
        RatePatientInteractor ratePatientInteractor = new RatePatientInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, visitId, ratePatientBody, CommonUtil.commonSharedPref().getString("token", ""));
        ratePatientInteractor.execute();
    }

    @Override
    public void updateDoctorLocation(DoctorNewLocationBody doctorNewLocationBody) {
        UpdateDoctorLocationInteractor updateDoctorLocationInteractor = new UpdateDoctorLocationInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, doctorNewLocationBody, CommonUtil.commonSharedPref().getString("token", ""));
        updateDoctorLocationInteractor.execute();
    }

    @Override
    public void isArrived(DoctorNewLocationBody doctorNewLocationBody) {
        IsArrivedInteractor isArrivedInteractor = new IsArrivedInteractorImpl(mExecutor, mMainThread,
                this, mDoctorRepository, doctorNewLocationBody, CommonUtil.commonSharedPref().getString("token", ""));
        isArrivedInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onChangingVisitStatuesSuccess(CommonResponse commonResponse) {
        switch (wentToGet) {
            case "ACCEPT_VISIT":
                mView.hideProgress();
                mView.visitAcceptedSuccessfully();
                break;
            case "REJECT_VISIT":
                mView.hideProgress();
                mView.visitRejectedSuccessfully();
                break;
            case "CANCEL_VISIT":
                mView.hideProgress();
                mView.visitCanceledSuccessfully();
                break;
        }
    }

    @Override
    public void onChangingVisitStatuesFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onEndingSessionSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.sessionEndedSuccessfully();
    }

    @Override
    public void onEndingSessionFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onStartingSessionSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.sessionStartedSuccessfully();
    }

    @Override
    public void onStartingSessionFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onRatingPatientSuccess(CommonResponse commonResponse) {
        mView.hideProgress();
        mView.ratePatientDoneSuccessfully();
    }

    @Override
    public void onRatingPatientFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onUpdatingDoctorLocationSuccess(ServerCommonResponse serverCommonResponse) {
        mView.hideProgress();
        mView.doctorNewLocationSavedSuccessfully(serverCommonResponse);
    }

    @Override
    public void onUpdatingDoctorLocationFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }

    @Override
    public void onDoctorArrivedSuccess(ServerCommonResponse serverCommonResponse) {
        mView.hideProgress();
        mView.isDoctorArrivedSuccessfully(serverCommonResponse);
    }

    @Override
    public void onDoctorArrivedFailed(String failedMessage) {
        mView.hideProgress();
        mView.showError(failedMessage, false);
    }
}
