package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.network.model.ValidateSignUpDataBody;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface SignupPresenter extends BasePresenter {
    interface View extends BaseView {
        void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse);
        void signupCodeRetrievedSuccessfully(String code);
        void forgetPasswordCodeRetrievedSuccessfully(String code);
        void signupRequestSentSuccessfully();
        void passwordResetSuccessfully();
        void dataValidatedSuccessfully();
    }
    void getLookups();
    void requestSignupCode(String phoneNumber);
    void requestForgetPasswordCode(String email);
    void signup(UserModel userModel);
    void resetPassword(String email, String newPassword, String code);
    void validateSignUpData(ValidateSignUpDataBody validateSignUpDataBody);
}
