package dr.doctorathome.presentation.presenters;

import dr.doctorathome.network.model.RestProfileStatistcsResponse;
import dr.doctorathome.presentation.presenters.base.BasePresenter;
import dr.doctorathome.presentation.ui.BaseView;

public interface ProfilePresenter extends BasePresenter {
    interface View extends BaseView {
        void profileStatisticsRetrieved(RestProfileStatistcsResponse restProfileStatistcsResponse);
    }

    void onRequestingProfileStatistics();
}
