package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.TestModel;
import dr.doctorathome.presentation.ui.activities.ImageZoomingActivity;

public class labItemInFollowUpAdapter extends RecyclerView.Adapter<labItemInFollowUpAdapter.ViewHolder> {

    List<TestModel> testModels;
    Context context;

    Fragment callerFragment;

    public labItemInFollowUpAdapter(Context context) {
        this.context = context;
    }

    public List<TestModel> getTestModels() {
        return testModels;
    }

    public void setTestModels(List<TestModel> testModels) {
        this.testModels = testModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.follow_up_item, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (getTestModels() == null)
            return;
        else {
            holder.tvLabTestName.setText(testModels.get(position).getTest().getName());
            holder.download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getTestModels().get(position).getTestResultUrl() != null) {

                        context.startActivity(new Intent(context, ImageZoomingActivity.class).putExtra("Image", getTestModels().get(position).getTestResultUrl()));
                    } else {

                        Toast.makeText(context, "" + context.getString(R.string.canntFindImage), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

//
    }

    @Override
    public int getItemCount() {
        if (testModels == null)
            return 0;
        else
            return testModels.size();
    }

//    public List<DoctorRequest> getDoctorRequests() {
//        return doctorRequests;
//    }

    public void setCallerFragment(Fragment callerFragment) {
        this.callerFragment = callerFragment;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvLabTestName)
        TextView tvLabTestName;
        @BindView(R.id.download)
        TextView download;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
