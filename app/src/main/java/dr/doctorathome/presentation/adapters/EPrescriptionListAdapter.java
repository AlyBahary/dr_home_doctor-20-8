package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.network.model.SupportMessage;
import dr.doctorathome.presentation.ui.activities.SupportMessageReplyActivity;
import dr.doctorathome.utils.CommonUtil;

public class EPrescriptionListAdapter extends RecyclerView.Adapter<EPrescriptionListAdapter.ViewHolder> {

    List<MedicineModel> medicineModels;
    Context context;
    private OnItemClick mOnItemClick;

    public List<MedicineModel> getMedicineModels() {
        return medicineModels;
    }

    public void setMedicineModels(List<MedicineModel> medicineModels) {
        this.medicineModels = medicineModels;
    }

    public EPrescriptionListAdapter(Context context, OnItemClick mOnItemClick) {
        this.medicineModels = medicineModels;
        this.context = context;
        this.mOnItemClick = mOnItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.list_item, parent, false);
        return new EPrescriptionListAdapter.ViewHolder(serviceItemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.text.setText(medicineModels.get(position).getName());

    }

    @Override
    public int getItemCount() {
        if (medicineModels == null)
            return 0;
        return medicineModels.size();
    }


      public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.text)
        TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }

    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}
