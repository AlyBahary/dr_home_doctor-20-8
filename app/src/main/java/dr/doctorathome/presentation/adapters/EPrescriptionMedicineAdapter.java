package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.MedicineModel;

public class EPrescriptionMedicineAdapter extends RecyclerView.Adapter<EPrescriptionMedicineAdapter.ViewHolder> {

    List<MedicineModel> medicineModels = new ArrayList<>();
    Context context;

    public List<MedicineModel> getMedicineModels() {
        return medicineModels;
    }

    public void setMedicineModels(List<MedicineModel> medicineModels) {
        this.medicineModels = medicineModels;
    }

    public void addMedicineModels(MedicineModel medicineModel) {
        for (int i = 0; i < medicineModels.size(); i++) {
            if (medicineModels.get(i).getId().equals(medicineModel.getId())) {
                Toast.makeText(context, "" + medicineModel.getName() + context.getString(R.string.is_already_exist), Toast.LENGTH_SHORT).show();
                return;
            }
        }
        medicineModel.setQuantity(1);
        this.medicineModels.add(medicineModel);
    }

    public EPrescriptionMedicineAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.add_medicine_eprescription_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.text.setText(medicineModels.get(position).getName());
        holder.quantityTv.setText(medicineModels.get(position).getQuantity()+"");
        holder.delete.setOnClickListener(v -> {
            medicineModels.remove(position);
            notifyDataSetChanged();
        });

        holder.icUp.setOnClickListener(v -> {
            holder.quantityTv.setText((Integer.parseInt(holder.quantityTv.getText().toString()) + 1)+"");
            medicineModels.get(position).setQuantity(Integer.parseInt(holder.quantityTv.getText().toString()));

        });
        holder.icDown.setOnClickListener(v -> {
            if (Integer.parseInt(holder.quantityTv.getText().toString()) == 1)
                return;
            holder.quantityTv.setText((Integer.parseInt(holder.quantityTv.getText().toString()) - 1)+"");
            medicineModels.get(position).setQuantity(Integer.parseInt(holder.quantityTv.getText().toString()));

        });

    }

    @Override
    public int getItemCount() {
        if (medicineModels == null)
            return 0;
        return medicineModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.ic_up)
        ImageView icUp;
        @BindView(R.id.ic_down)
        ImageView icDown;
        @BindView(R.id.quantity_tv)
        TextView quantityTv;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
