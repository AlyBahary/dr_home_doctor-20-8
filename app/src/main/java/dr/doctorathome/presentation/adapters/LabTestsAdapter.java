package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.LabTestInPrescription;
import dr.doctorathome.network.model.MedicineInPrescription;
import dr.doctorathome.presentation.ui.activities.PrescriptionActivity;

public class LabTestsAdapter extends RecyclerView.Adapter<LabTestsAdapter.ViewHolder> {

    List<LabTestInPrescription> labTestsInPrescription;
    Context context;

    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public LabTestsAdapter(List<LabTestInPrescription> labTestsInPrescription, Context context) {
        this.labTestsInPrescription = labTestsInPrescription;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_lab_test, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        viewBinderHelper.bind(holder.swipeRevealLayout, String.valueOf(labTestsInPrescription.get(position).getId()));

        holder.deleteLayout.setOnClickListener(view -> {
            labTestsInPrescription.remove(position);
            notifyDataSetChanged();
        });

        holder.tvLabTestName.setText(labTestsInPrescription.get(position).getName());
        holder.tvPrice.setText(labTestsInPrescription.get(position).getPrice());

        if(((PrescriptionActivity) context).isViewOnly()) {
            holder.swipeRevealLayout.setLockDrag(true);
        }
    }

    @Override
    public int getItemCount() {
        return labTestsInPrescription.size();
    }

    public List<LabTestInPrescription> getLabTestsInPrescription() {
        return labTestsInPrescription;
    }

    public void setLabTestsInPrescription(List<LabTestInPrescription> labTestsInPrescription) {
        this.labTestsInPrescription = labTestsInPrescription;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.swipeRevealLayout)
        SwipeRevealLayout swipeRevealLayout;

        @BindView(R.id.tvLabTestName)
        TextView tvLabTestName;

        @BindView(R.id.deleteLayout)
        RelativeLayout deleteLayout;

        @BindView(R.id.tvPrice)
        TextView tvPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void saveStates(Bundle outState) {
        viewBinderHelper.saveStates(outState);
    }

    public void restoreStates(Bundle inState) {
        viewBinderHelper.restoreStates(inState);
    }
}
