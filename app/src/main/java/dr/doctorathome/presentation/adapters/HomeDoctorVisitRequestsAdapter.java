package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.DoctorRequest;
import dr.doctorathome.network.model.Service;
import dr.doctorathome.presentation.ui.activities.MainActivity;
import dr.doctorathome.presentation.ui.activities.VisitDetailsActivity;
import dr.doctorathome.presentation.ui.fragments.HistoryFragment;
import dr.doctorathome.presentation.ui.fragments.HomeFragment;
import dr.doctorathome.presentation.ui.fragments.ServicesFragment;
import dr.doctorathome.utils.CommonUtil;

public class HomeDoctorVisitRequestsAdapter extends RecyclerView.Adapter<HomeDoctorVisitRequestsAdapter.ViewHolder> {

    List<DoctorRequest> doctorRequests;
    Context context;

    Fragment callerFragment;

    public HomeDoctorVisitRequestsAdapter(List<DoctorRequest> doctorRequests, Context context) {
        this.doctorRequests = doctorRequests;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_visit, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvPatientName.setText(doctorRequests.get(position).getPatient().getUser().getFirstName() + " "
                + doctorRequests.get(position).getPatient().getUser().getLastName());
        holder.tvPatientLocation.setText(
                doctorRequests.get(position).getPatient().getAddress() );
        holder.tvTime.setText(CommonUtil.dateFullFromServerWithTime(doctorRequests.get(position).getRequestdVisitDate()));

        if(doctorRequests.get(position).getVisitType() != null){
            holder.tvVisitType.setVisibility(View.VISIBLE);
            holder.tvVisitType.setText(doctorRequests.get(position).getVisitType());
            if(!doctorRequests.get(position).getVisitType().equals("IMMEDIATE")){
                holder.tvVisitType.setTextColor(context.getResources().getColor(R.color.colorAccentNotOfficial));
            }
        }else {
            holder.tvVisitType.setVisibility(View.GONE);
        }

        if (doctorRequests.get(position).getVisitStatus() != null && doctorRequests.get(position).getVisitStatus()
                .getName().equals(RequestStatues.PENDING)) {
            holder.actionsLayout.setVisibility(View.VISIBLE);
            holder.tvApprove.setVisibility(View.VISIBLE);
            holder.tvReject.setVisibility(View.VISIBLE);
            holder.blockBtn.setVisibility(View.GONE);
            holder.unblockBtn.setVisibility(View.GONE);
            holder.tvCancel.setVisibility(View.GONE);
            holder.tvRunning.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.GONE);

            holder.tvApprove.setOnClickListener(view -> {
                if (callerFragment instanceof HomeFragment) {
                    ((HomeFragment) callerFragment).acceptRequest(doctorRequests.get(position).getId());
                } else if (callerFragment instanceof HistoryFragment) {
                    ((HistoryFragment) callerFragment).acceptRequest(doctorRequests.get(position).getId());
                }
            });

            holder.tvReject.setOnClickListener(view -> {
                if (callerFragment instanceof HomeFragment) {
                    ((HomeFragment) callerFragment).rejectRequest(doctorRequests.get(position).getId());
                } else if (callerFragment instanceof HistoryFragment) {
                    ((HistoryFragment) callerFragment).rejectRequest(doctorRequests.get(position).getId());
                }
            });
        } else if (doctorRequests.get(position).getVisitStatus() != null && doctorRequests.get(position).getVisitStatus()
                .getName().equals(RequestStatues.COMPLETED)) {
            holder.actionsLayout.setVisibility(View.VISIBLE);
            holder.tvApprove.setVisibility(View.GONE);
            holder.tvReject.setVisibility(View.GONE);
            holder.tvCancel.setVisibility(View.GONE);
            holder.tvRunning.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.VISIBLE);
            holder.tvPaid.setText( doctorRequests.get(position).getVisitStatus()
                    .getName());
            if (doctorRequests.get(position).getBlockedPatient() != null && doctorRequests.get(position).getBlockedPatient()) {
                holder.unblockBtn.setVisibility(View.VISIBLE);
                holder.blockBtn.setVisibility(View.GONE);
            } else {
                holder.unblockBtn.setVisibility(View.GONE);
                holder.blockBtn.setVisibility(View.VISIBLE);
            }

            holder.blockBtn.setOnClickListener(view -> {
                if (callerFragment instanceof HomeFragment) {

                } else if (callerFragment instanceof HistoryFragment) {
                    ((HistoryFragment) callerFragment).blockPatient(doctorRequests.get(position).getPatient().getId());
                }
            });

            holder.unblockBtn.setOnClickListener(view -> {
                if (callerFragment instanceof HomeFragment) {

                } else if (callerFragment instanceof HistoryFragment) {
                    ((HistoryFragment) callerFragment).unblockPatient(doctorRequests.get(position).getPatient().getId());
                }
            });

        }else if (doctorRequests.get(position).getVisitStatus() != null && doctorRequests.get(position).getVisitStatus()
                .getName().equals(RequestStatues.CONFIRMED)) {
            holder.actionsLayout.setVisibility(View.VISIBLE);
            holder.tvApprove.setVisibility(View.GONE);
            holder.tvReject.setVisibility(View.GONE);
            holder.blockBtn.setVisibility(View.GONE);
            holder.unblockBtn.setVisibility(View.GONE);
            holder.tvCancel.setVisibility(View.VISIBLE);
            holder.tvRunning.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.GONE);

            holder.tvCancel.setOnClickListener(view -> {
                if (callerFragment instanceof HomeFragment) {
                    ((HomeFragment) callerFragment).cancelRequest(doctorRequests.get(position).getId());
                } else if (callerFragment instanceof HistoryFragment) {
                    ((HistoryFragment) callerFragment).cancelRequest(doctorRequests.get(position).getId());
                }
            });

        }else if (doctorRequests.get(position).getVisitStatus() != null && doctorRequests.get(position).getVisitStatus()
                .getName().equals(RequestStatues.RUNNING)) {
            holder.actionsLayout.setVisibility(View.VISIBLE);
            holder.tvApprove.setVisibility(View.GONE);
            holder.tvReject.setVisibility(View.GONE);
            holder.blockBtn.setVisibility(View.GONE);
            holder.unblockBtn.setVisibility(View.GONE);
            holder.tvCancel.setVisibility(View.GONE);
            holder.tvRunning.setVisibility(View.VISIBLE);
            holder.tvPaid.setVisibility(View.GONE);
        }else if (doctorRequests.get(position).getVisitStatus() != null && doctorRequests.get(position).getVisitStatus()
                .getName().equals(RequestStatues.PAID)) {
            holder.actionsLayout.setVisibility(View.VISIBLE);
            holder.tvApprove.setVisibility(View.GONE);
            holder.tvReject.setVisibility(View.GONE);
            holder.blockBtn.setVisibility(View.GONE);
            holder.unblockBtn.setVisibility(View.GONE);
            holder.tvCancel.setVisibility(View.GONE);
            holder.tvRunning.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.VISIBLE);
        }
        else if (doctorRequests.get(position).getVisitStatus() != null && doctorRequests.get(position).getVisitStatus()
                .getName().equals(RequestStatues.CANCELLED)) {
            holder.actionsLayout.setVisibility(View.VISIBLE);
            holder.tvApprove.setVisibility(View.GONE);
            holder.tvReject.setVisibility(View.GONE);
            holder.blockBtn.setVisibility(View.GONE);
            holder.unblockBtn.setVisibility(View.GONE);
            holder.tvCancel.setVisibility(View.GONE);
            holder.tvRunning.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.VISIBLE);
            holder.tvPaid.setText(doctorRequests.get(position).getVisitStatus()
                    .getName());
            holder.tvPaid.setTextColor(context.getResources().getColor(R.color.colorRed));

        }else if (doctorRequests.get(position).getVisitStatus() != null && doctorRequests.get(position).getVisitStatus()
                .getName().equals(RequestStatues.REJECTED)) {
            holder.actionsLayout.setVisibility(View.VISIBLE);
            holder.tvApprove.setVisibility(View.GONE);
            holder.tvReject.setVisibility(View.GONE);
            holder.blockBtn.setVisibility(View.GONE);
            holder.unblockBtn.setVisibility(View.GONE);
            holder.tvCancel.setVisibility(View.GONE);
            holder.tvRunning.setVisibility(View.GONE);
            holder.tvPaid.setVisibility(View.VISIBLE);
            holder.tvPaid.setText(doctorRequests.get(position).getVisitStatus()
                    .getName());
            holder.tvPaid.setTextColor(context.getResources().getColor(R.color.colorRed));

        }
//        else {
//            holder.actionsLayout.setVisibility(View.GONE);
//        }
        try {
            Glide.with(context).load(RestClient.REST_API_URL
                    + doctorRequests.get(position).getPatient().getProfilePicUrl().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.profile_placeholder_rounded).into(holder.ivPatientImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(context, VisitDetailsActivity.class);
        intent.putExtra("visitId", doctorRequests.get(position).getId());
        holder.itemView.setOnClickListener(view -> ((MainActivity) context).startActivityForResult(intent, 2));
    }

    @Override
    public int getItemCount() {
        return doctorRequests.size();
    }

    public List<DoctorRequest> getDoctorRequests() {
        return doctorRequests;
    }

    public void setCallerFragment(Fragment callerFragment) {
        this.callerFragment = callerFragment;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivPatientImage)
        AppCompatImageView ivPatientImage;

        @BindView(R.id.tvPatientName)
        AppCompatTextView tvPatientName;

        @BindView(R.id.tvPatientLocation)
        AppCompatTextView tvPatientLocation;

        @BindView(R.id.tvTime)
        AppCompatTextView tvTime;

        @BindView(R.id.rbRatingVisit)
        RatingBar rbRatingVisit;

        @BindView(R.id.actionsLayout)
        RelativeLayout actionsLayout;

        @BindView(R.id.blockBtn)
        Button blockBtn;

        @BindView(R.id.unblockBtn)
        Button unblockBtn;

        @BindView(R.id.tvApprove)
        TextView tvApprove;

        @BindView(R.id.tvReject)
        TextView tvReject;

        @BindView(R.id.tvCancel)
        TextView tvCancel;

        @BindView(R.id.tvRunning)
        TextView tvRunning;

        @BindView(R.id.tvPaid)
        TextView tvPaid;

        @BindView(R.id.tvVisitType)
        TextView tvVisitType;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
