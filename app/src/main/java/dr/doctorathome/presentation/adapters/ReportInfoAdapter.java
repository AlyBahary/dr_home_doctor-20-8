package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.LabTestInPrescription;
import dr.doctorathome.network.model.ReportModel;
import dr.doctorathome.presentation.ui.activities.PrescriptionActivity;

public class ReportInfoAdapter extends RecyclerView.Adapter<ReportInfoAdapter.ViewHolder> {

    List<ReportModel> reportModels;
    Context context;


    public ReportInfoAdapter(Context context) {
        this.context = context;
    }

    public List<ReportModel> getReportModels() {
        return reportModels;
    }

    public void setReportModels(List<ReportModel> reportModels) {
        this.reportModels = reportModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.report_item, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (reportModels != null) {
            holder.tvTotalPremVisitsLabel.setText(reportModels.get(position).getFieldName());
            holder.tvTotalPremVisitsValue.setText(reportModels.get(position).getFieldValue());
        }

    }

    @Override
    public int getItemCount() {
        if (reportModels == null)
            return 0;
        return reportModels.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tvTotalPremVisitsLabel)
        TextView tvTotalPremVisitsLabel;

        @BindView(R.id.tvTotalPremVisitsValue)
        TextView tvTotalPremVisitsValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
