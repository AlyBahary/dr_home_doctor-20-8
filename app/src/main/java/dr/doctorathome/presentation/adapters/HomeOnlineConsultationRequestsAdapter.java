package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ConsultRequest;
import dr.doctorathome.network.model.ConsultationRequest;
import dr.doctorathome.network.model.DoctorRequest;
import dr.doctorathome.presentation.ui.activities.ConsultationChatActivity;
import dr.doctorathome.presentation.ui.activities.OnlineConsultationDetailsActivity;
import dr.doctorathome.utils.CommonUtil;

public class HomeOnlineConsultationRequestsAdapter extends RecyclerView.Adapter<HomeOnlineConsultationRequestsAdapter.ViewHolder> {

    List<ConsultRequest> consultationRequests;
    Context context;

    public HomeOnlineConsultationRequestsAdapter(List<ConsultRequest> consultationRequests, Context context) {
        this.consultationRequests = consultationRequests;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_online, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            Glide.with(context).load(RestClient.REST_API_URL
                    + consultationRequests.get(position).getPatient().getProfilePicUrl().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.profile_placeholder_rounded).into(holder.ivUserImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.name.setText(consultationRequests.get(position).getPatient().getUser().getFirstName());
        holder.itemView.setOnClickListener(view -> {
            if (consultationRequests.get(position).getRequestStatus() != null && (consultationRequests.get(position).getRequestStatus()
                    .getName().equals(RequestStatues.RUNNING) || consultationRequests.get(position).getRequestStatus()
                    .getName().equals(RequestStatues.COMPLETED))) {
                Intent intent = new Intent(context, ConsultationChatActivity.class);
                intent.putExtra("session_id", consultationRequests.get(position).getId());
                intent.putExtra("patient_id", consultationRequests.get(position).getPatient().getId());
                intent.putExtra("patient_user_id", consultationRequests.get(position).getPatient().getUser().getId());
                intent.putExtra("patient_avatar", consultationRequests.get(position).getPatient().getProfilePicUrl());
                try {
                    intent.putExtra("name", consultationRequests.get(position).getPatient().getUser().getFirstName() + " "
                            + consultationRequests.get(position).getPatient().getUser().getLastName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                context.startActivity(intent);
            } else {
                Intent intent = new Intent(context, OnlineConsultationDetailsActivity.class);
                intent.putExtra("id", consultationRequests.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return consultationRequests.size();
    }

    public List<ConsultRequest> getConsultationRequests() {
        return consultationRequests;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivUserImage)
        AppCompatImageView ivUserImage;
        @BindView(R.id.name)
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
