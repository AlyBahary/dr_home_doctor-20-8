package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ConsultRequest;
import dr.doctorathome.presentation.ui.activities.ConsultationChatActivity;
import dr.doctorathome.presentation.ui.activities.OnlineConsultationDetailsActivity;
import dr.doctorathome.utils.CommonUtil;

public class OnlineConsultationsAdapter extends RecyclerView.Adapter<OnlineConsultationsAdapter.ViewHolder> {

    List<ConsultRequest> consultRequests;
    Context context;

    public OnlineConsultationsAdapter(List<ConsultRequest> consultRequests, Context context) {
        this.consultRequests = consultRequests;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_online_consultation, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvPatientName.setText(consultRequests.get(position).getPatient().getUser().getFirstName() + " "
                + consultRequests.get(position).getPatient().getUser().getLastName());

        try {
            Glide.with(context).load(RestClient.REST_API_URL
                    + consultRequests.get(position).getPatient().getProfilePicUrl().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.profile_placeholder_rounded).into(holder.ivPatientImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tvTime.setText(CommonUtil.dateFullFromServerWithTime(consultRequests.get(position).getRequestdDate()));

        if (consultRequests.get(position).getCode() != null) {
            holder.tvCode.setVisibility(View.VISIBLE);
            holder.tvCode.setText("#" + consultRequests.get(position).getCode());
        } else {
            holder.tvCode.setVisibility(View.GONE);
        }

        if (consultRequests.get(position).getRequestStatus() != null && consultRequests.get(position).getRequestStatus()
                .getName().equals(RequestStatues.PENDING)) {
            holder.newPadge.setVisibility(View.VISIBLE);
        } else {
            holder.newPadge.setVisibility(View.GONE);
        }


        holder.itemView.setOnClickListener(view -> {

            Intent intent = new Intent(context, OnlineConsultationDetailsActivity.class);
            intent.putExtra("id", consultRequests.get(position).getId());
            context.startActivity(intent);
//
//
//            if(consultRequests.get(position).getRequestStatus() != null && (consultRequests.get(position).getRequestStatus()
//                    .getName().equals(RequestStatues.RUNNING) || consultRequests.get(position).getRequestStatus()
//                    .getName().equals(RequestStatues.COMPLETED))){
//                Intent intent = new Intent(context, ConsultationChatActivity.class);
//                intent.putExtra("session_id", consultRequests.get(position).getId());
//                intent.putExtra("patient_id", consultRequests.get(position).getPatient().getId());
//                intent.putExtra("patient_user_id", consultRequests.get(position).getPatient().getUser().getId());
//                intent.putExtra("patient_avatar", consultRequests.get(position).getPatient().getProfilePicUrl());
//                try {
//                    intent.putExtra("name", consultRequests.get(position).getPatient().getUser().getFirstName() + " "
//                            + consultRequests.get(position).getPatient().getUser().getLastName());
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//                context.startActivity(intent);
//            }else{
//                Intent intent = new Intent(context, OnlineConsultationDetailsActivity.class);
//                intent.putExtra("id", consultRequests.get(position).getId());
//                context.startActivity(intent);
//            }
        });
    }

    @Override
    public int getItemCount() {
        return consultRequests.size();
    }

    public List<ConsultRequest> getDoctorRequests() {
        return consultRequests;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivPatientImage)
        AppCompatImageView ivPatientImage;

        @BindView(R.id.tvPatientName)
        AppCompatTextView tvPatientName;

        @BindView(R.id.tvTime)
        AppCompatTextView tvTime;

        @BindView(R.id.tvCode)
        TextView tvCode;

        @BindView(R.id.newPadge)
        TextView newPadge;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
