package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zcw.togglebutton.ToggleButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.LabTestInPrescription;
import dr.doctorathome.network.model.Service;
import dr.doctorathome.presentation.ui.fragments.ServicesFragment;

public class AllLabTestsAdapter extends RecyclerView.Adapter<AllLabTestsAdapter.ViewHolder> {

    List<LabTestInPrescription> allLabTests;
    Context context;

    public AllLabTestsAdapter(List<LabTestInPrescription> allLabTests, Context context) {
        this.allLabTests = allLabTests;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_checkbox_lab, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.checkBox.setText(allLabTests.get(position).getName());
        holder.checkBox.setChecked(allLabTests.get(position).isChecked());

        holder.checkBox.setOnCheckedChangeListener((compoundButton, b) ->
                allLabTests.get(position).setChecked(b));

        holder.switchBtn.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on)
                    allLabTests.get(position).setPayable(true);
                else
                    allLabTests.get(position).setPayable(false);

                Log.d("TTTTTT", "onToggle: )"+on);
            }
        });

    }


    @Override
    public int getItemCount() {
        return allLabTests.size();
    }

    public List<LabTestInPrescription> getAllLabTests() {
        return allLabTests;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkBox)
        CheckBox checkBox;
        @BindView(R.id.switchBtn)
        com.zcw.togglebutton.ToggleButton switchBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
