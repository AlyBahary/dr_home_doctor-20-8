package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.Service;
import dr.doctorathome.presentation.ui.fragments.ServicesFragment;
import timber.log.Timber;

public class AllServicesAdapter extends RecyclerView.Adapter<AllServicesAdapter.ViewHolder>{

    List<Service> allServices;
    Context context;
    Fragment callerFragment;

    public AllServicesAdapter(List<Service> allServices, Context context, Fragment callerFragment) {
        this.allServices = allServices;
        this.context = context;
        this.callerFragment = callerFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_service, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvServiceName.setText(allServices.get(position).getName());
        holder.tvServiceStatus.setText(allServices.get(position).isSelected() ? R.string.on : R.string.off);
        holder.swServiceSwitch.setChecked(allServices.get(position).isSelected());
        holder.swServiceSwitch.setOnClickListener(view -> {
//            Timber.i("Selected is :: " + holder.swServiceSwitch.isChecked());
            allServices.get(position).setSelected(holder.swServiceSwitch.isChecked());
            notifyItemChanged(position);
            ((ServicesFragment) callerFragment).changeServiceStatues(position);
        });
        if(allServices.get(position).isSubmittingToServer()){
            holder.swServiceSwitch.setVisibility(View.GONE);
            holder.serviceChangeProgressLayout.setVisibility(View.VISIBLE);
        }else {
            holder.swServiceSwitch.setVisibility(View.VISIBLE);
            holder.serviceChangeProgressLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return allServices.size();
    }

    public List<Service> getAllServices() {
        return allServices;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.swServiceSwitch)
        SwitchCompat swServiceSwitch;

        @BindView(R.id.tvServiceName)
        TextView tvServiceName;

        @BindView(R.id.tvServiceStatus)
        TextView tvServiceStatus;

        @BindView(R.id.serviceChangeProgressLayout)
        RelativeLayout serviceChangeProgressLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
