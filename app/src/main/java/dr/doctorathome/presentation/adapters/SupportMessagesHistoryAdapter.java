package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.SupportMessage;
import dr.doctorathome.presentation.ui.activities.SupportMessageReplyActivity;
import dr.doctorathome.utils.CommonUtil;

public class SupportMessagesHistoryAdapter extends RecyclerView.Adapter<SupportMessagesHistoryAdapter.ViewHolder> {

    List<SupportMessage> supportMessages;
    Context context;

    public SupportMessagesHistoryAdapter(List<SupportMessage> supportMessages, Context context) {
        this.supportMessages = supportMessages;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_contact_us_history, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvItemHeader.setText(supportMessages.get(position).getSubject());
        holder.tvItemBody.setText(supportMessages.get(position).getMessage());

//        holder.tvTime.setVisibility(View.GONE);
        holder.tvTime.setText(CommonUtil.dateDaysAndTimeFromServerWithTimeFull(supportMessages.get(position).getCreationDate()));

        if (supportMessages.get(position).getStatus() != null) {
            holder.ivItemType.setVisibility(View.VISIBLE);
            if(supportMessages.get(position).getStatus().equals("OPEN")){
                holder.ivItemType.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_outcome));
            }else{
                holder.ivItemType.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_income));
            }
        } else {
            holder.ivItemType.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(view -> {
            Intent goToDetailsIntent = new Intent(context, SupportMessageReplyActivity.class);
            goToDetailsIntent.putExtra("supportMessageId", supportMessages.get(position).getId());
            context.startActivity(goToDetailsIntent);
        });
    }

    @Override
    public int getItemCount() {
        return supportMessages.size();
    }

    public List<SupportMessage> getDoctorRequests() {
        return supportMessages;
    }

    public void setSupportMessages(List<SupportMessage> supportMessages) {
        this.supportMessages = supportMessages;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvItemHeader)
        TextView tvItemHeader;

        @BindView(R.id.tvItemBody)
        TextView tvItemBody;

        @BindView(R.id.tvTime)
        TextView tvTime;

        @BindView(R.id.ivItemType)
        ImageView ivItemType;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
