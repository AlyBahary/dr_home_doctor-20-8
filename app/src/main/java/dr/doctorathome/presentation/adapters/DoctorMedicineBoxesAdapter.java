package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.MedicineBox;
import dr.doctorathome.network.model.MedicineInPrescription;
import dr.doctorathome.presentation.ui.fragments.BottomSheetAddMedicine;

public class DoctorMedicineBoxesAdapter extends RecyclerView.Adapter<DoctorMedicineBoxesAdapter.ViewHolder> {

    List<MedicineBox> medicineBoxes;
    Context context;
    BottomSheetAddMedicine callerFragment;

    public DoctorMedicineBoxesAdapter(List<MedicineBox> medicineBoxes, Context context, BottomSheetAddMedicine callerFragment) {
        this.medicineBoxes = medicineBoxes;
        this.context = context;
        this.callerFragment = callerFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_checkbox, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.checkBox.setText(medicineBoxes.get(position).getCode());
        holder.checkBox.setChecked(medicineBoxes.get(position).isChecked());

        holder.checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            medicineBoxes.get(position).setChecked(b);
            callerFragment.checkedItemsChanged();
        });

    }

    @Override
    public int getItemCount() {
        return medicineBoxes.size();
    }

    public void setMedicineBoxes(List<MedicineBox> medicineBoxes) {
        this.medicineBoxes = medicineBoxes;
    }

    public List<MedicineBox> getDoctorMedicineBoxes() {
        return medicineBoxes;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkBox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
