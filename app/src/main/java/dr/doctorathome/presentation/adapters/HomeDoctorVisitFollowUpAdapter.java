package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.FollowUpModel;
import dr.doctorathome.presentation.ui.activities.FollowUpActivity;

public class HomeDoctorVisitFollowUpAdapter extends RecyclerView.Adapter<HomeDoctorVisitFollowUpAdapter.ViewHolder> {

    ArrayList<FollowUpModel> followUpModels;
    Context context;

    Fragment callerFragment;

    public HomeDoctorVisitFollowUpAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_visit, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (followUpModels == null)
            return;
        else {
            holder.actionsLayout.setVisibility(View.GONE);
            holder.consContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, FollowUpActivity.class).putExtra("id",followUpModels.get(position).getId()+""));
                }
            });

            holder.tvPatientName.setText(followUpModels.get(position).getPatient().getUser().getFirstName() + " " + followUpModels.get(position).getPatient().getUser().getLastName());
            holder.tvPatientLocation.setText(followUpModels.get(position).getPatient().getAddress());
            holder.tvTime.setText(followUpModels.get(position).getVisitDate());
            holder.tvVisitType.setText(followUpModels.get(position).getStatus().getName());
            holder.tvReject.setVisibility(View.GONE);
            holder.tvApprove.setVisibility(View.GONE);
            holder.rbRatingVisit.setVisibility(View.GONE);
            holder.seprator.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        if (followUpModels == null)
            return 0;
        else
            return followUpModels.size();
    }

    public List<FollowUpModel> getDoctorRequests() {
        return followUpModels;
    }

    public void setFollowUpModels(ArrayList<FollowUpModel> followUpModels) {
        this.followUpModels = followUpModels;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.consContainer)
        ConstraintLayout consContainer;

        @BindView(R.id.ivPatientImage)
        AppCompatImageView ivPatientImage;

        @BindView(R.id.tvPatientName)
        AppCompatTextView tvPatientName;

        @BindView(R.id.tvPatientLocation)
        AppCompatTextView tvPatientLocation;

        @BindView(R.id.tvTime)
        AppCompatTextView tvTime;

        @BindView(R.id.rbRatingVisit)
        RatingBar rbRatingVisit;

        @BindView(R.id.actionsLayout)
        RelativeLayout actionsLayout;

        @BindView(R.id.blockBtn)
        Button blockBtn;

        @BindView(R.id.unblockBtn)
        Button unblockBtn;

        @BindView(R.id.tvApprove)
        TextView tvApprove;

        @BindView(R.id.tvReject)
        TextView tvReject;

        @BindView(R.id.tvCancel)
        TextView tvCancel;

        @BindView(R.id.tvRunning)
        TextView tvRunning;

        @BindView(R.id.tvPaid)
        TextView tvPaid;

        @BindView(R.id.tvVisitType)
        TextView tvVisitType;

        @BindView(R.id.seprator)
        RelativeLayout seprator;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
