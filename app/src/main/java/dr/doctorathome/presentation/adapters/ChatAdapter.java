package dr.doctorathome.presentation.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import android.os.Handler;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ChatModel;
import dr.doctorathome.presentation.ui.activities.ConsultationChatActivity;
import dr.doctorathome.utils.CommonUtil;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private ArrayList<ChatModel> chatModels;
    private OnItemClick mOnItemClick;
    private Context context;
    String doctorID, patientavatar, doctorAvatar;
    Activity activity;
    //Thread thread;
    int Adapter_position = -1;
    MediaPlayer mp;
    ChatAdapter instance = this;
    Handler seekHandler = new Handler();
    boolean isOld = false;
    int playerNum = -1;
    Runnable run;


    public ChatAdapter(Activity activity, ArrayList<ChatModel> chatModels, Context context, OnItemClick mOnItemClick, String doctorID
            , String patientavatar
            , String doctorAvatar
    ) {
        this.chatModels = chatModels;
        this.mOnItemClick = mOnItemClick;
        this.context = context;
        this.doctorID = doctorID;
        this.patientavatar = patientavatar;
        this.doctorAvatar = doctorAvatar;
        this.activity = activity;

    }

    public void isOld(Boolean isOld) {
        this.isOld = isOld;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.chat_item, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ChatModel itemMode = chatModels.get(i);
        viewHolder.seekbar.setEnabled(false);
        //media
        mp = new MediaPlayer();

        if (isOld) {
            viewHolder.tvDate.setText(CommonUtil.dateFullFromServerWithTime(itemMode.getSentAt()));
        } else {
            viewHolder.tvDate.setVisibility(View.GONE);
        }

        // if doctor
        if (itemMode.getSenderId() == ((Integer.parseInt(doctorID)))) {
            Log.d("TTTT", "onBindViewHolder: DoctorID" + doctorID);
            Log.d("TTTT", "onBindViewHolder: DoctorID" + itemMode.getSenderId());


            viewHolder.MsG.setText(itemMode.getContent() + "");
            viewHolder.MsG.setGravity(Gravity.START);
            viewHolder.tvDate.setGravity(Gravity.START);
            viewHolder.MsG.setTextColor(Color.parseColor("#ffffff"));
            viewHolder.bg_crdViewOfText.setCardBackgroundColor(Color.parseColor("#0BACBC"));
            viewHolder.bg_crdViewOfImage.setCardBackgroundColor(Color.parseColor("#0BACBC"));
            viewHolder.bg_crdViewOfAudio.setCardBackgroundColor(Color.parseColor("#0BACBC"));
            ViewCompat.setLayoutDirection(viewHolder.linearLayout, ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(viewHolder.linearLayout_date, ViewCompat.LAYOUT_DIRECTION_RTL);
            if (!doctorAvatar.equals("")) {
                Glide.with(context).load(RestClient.REST_API_URL + doctorAvatar)
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile_placeholder_rounded).into(viewHolder.ivUserImage);
            } else {
                Glide.with(context).load(R.drawable.profile_placeholder_rounded)
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile_placeholder_rounded).into(viewHolder.ivUserImage);
            }

        }
        //if patient
        else {
            Log.d("TTTT", "onBindViewHolder: DoctorID" + doctorID);
            Log.d("TTTT", "onBindViewHolder: DoctorID" + itemMode.getSenderId());
            viewHolder.MsG.setText(itemMode.getContent() + "");
            viewHolder.MsG.setGravity(Gravity.END);
            viewHolder.tvDate.setGravity(Gravity.END);
            viewHolder.bg_crdViewOfText.setCardBackgroundColor(Color.parseColor("#ffffff"));
            viewHolder.bg_crdViewOfImage.setCardBackgroundColor(Color.parseColor("#ffffff"));
            viewHolder.bg_crdViewOfAudio.setCardBackgroundColor(Color.parseColor("#ffffff"));
            viewHolder.MsG.setTextColor(Color.parseColor("#000000"));
            ViewCompat.setLayoutDirection(viewHolder.linearLayout, ViewCompat.LAYOUT_DIRECTION_LTR);
            ViewCompat.setLayoutDirection(viewHolder.linearLayout_date, ViewCompat.LAYOUT_DIRECTION_LTR);

            if (!patientavatar.equals("")) {
                Glide.with(context).load(RestClient.REST_API_URL + patientavatar)
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile_placeholder_rounded).into(viewHolder.ivUserImage);
            } else {
                Glide.with(context).load(R.drawable.profile_placeholder_rounded)
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile_placeholder_rounded).into(viewHolder.ivUserImage);
            }
        }

        if (itemMode.getMediaType() != null) {
            if (itemMode.getMediaType().equals("IMAGE")) {
                viewHolder.bg_crdViewOfText.setVisibility(View.GONE);
                viewHolder.bg_crdViewOfImage.setVisibility(View.VISIBLE);
                viewHolder.bg_crdViewOfAudio.setVisibility(View.GONE);
                viewHolder.MsG.setVisibility(View.GONE);
                viewHolder.image.setVisibility(View.VISIBLE);
                viewHolder.seekbar.setVisibility(View.GONE);
                viewHolder.play.setVisibility(View.GONE);
                viewHolder.audioBackgroundLinearLayout.setVisibility(View.GONE);
                Glide.with(context).load(RestClient.REST_API_URL + itemMode.getContent())
                        .apply(RequestOptions.circleCropTransform()).fitCenter()
                        .into(viewHolder.image);
            } else if (itemMode.getMediaType().equals("VOICE")) {
                viewHolder.bg_crdViewOfText.setVisibility(View.GONE);
                viewHolder.bg_crdViewOfImage.setVisibility(View.GONE);
                viewHolder.bg_crdViewOfAudio.setVisibility(View.VISIBLE);
                viewHolder.MsG.setVisibility(View.GONE);
                viewHolder.image.setVisibility(View.GONE);
                viewHolder.seekbar.setVisibility(View.VISIBLE);
                viewHolder.play.setVisibility(View.VISIBLE);
                viewHolder.audioBackgroundLinearLayout.setVisibility(View.VISIBLE);
                final int[] play_flag = {0};
                viewHolder.play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (mp.isPlaying() && playerNum == i) {
                            mp.pause();
                            viewHolder.play.setImageResource(R.drawable.ic_audio_player);
                            return;
                        } else if (mp.isPlaying()) {
                            mp.pause();
                            viewHolder.play.setImageResource(R.drawable.ic_audio_player);
                        }

                        //
                        try {
                            mp.reset();
                            mp.setDataSource("" + RestClient.REST_API_URL + itemMode.getContent().substring(1));
                            Log.d("TTTTT", RestClient.REST_API_URL + itemMode.getContent().substring(1));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            mp.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        viewHolder.seekbar.setMax(mp.getDuration());

                        //


                        if (!mp.isPlaying()) {
                            playerNum = i;
                            Adapter_position = i;
                            viewHolder.play.setImageResource(R.drawable.ic_puase_player);
                            mp.start();
                            run = new Runnable() {
                                @Override
                                public void run() {
                                    // Updateing SeekBar every 100 miliseconds
                                    viewHolder.seekbar.setProgress(mp.getCurrentPosition());
                                    seekHandler.postDelayed(run, 100);
                                }
                            };
                            run.run();
                            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    viewHolder.play.setImageResource(R.drawable.ic_audio_player);
                                    mp.pause();

                                }
                            });
                        } else {
                            mp.pause();
                            viewHolder.play.setImageResource(R.drawable.ic_audio_player);

                        }

                    }
                });
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        viewHolder.play.setImageResource(R.drawable.ic_audio_player);

                    }
                });

            } else {
                viewHolder.bg_crdViewOfText.setVisibility(View.VISIBLE);
                viewHolder.bg_crdViewOfImage.setVisibility(View.GONE);
                viewHolder.bg_crdViewOfAudio.setVisibility(View.GONE);
                viewHolder.MsG.setVisibility(View.VISIBLE);
                viewHolder.image.setVisibility(View.GONE);
                viewHolder.seekbar.setVisibility(View.GONE);
                viewHolder.play.setVisibility(View.GONE);
                viewHolder.audioBackgroundLinearLayout.setVisibility(View.GONE);
            }
        }


    }

    @Override
    public int getItemCount() {
        return chatModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout linearLayout, audioBackgroundLinearLayout, linearLayout_date;
        TextView MsG, tvDate;
        AppCompatImageView ivUserImage;
        CardView bg_crdViewOfText, bg_crdViewOfImage, bg_crdViewOfAudio;
        SeekBar seekbar;
        ImageView play, image;

        public ViewHolder(View itemView) {
            super(itemView);
            audioBackgroundLinearLayout = itemView.findViewById(R.id.audioBackgroundLinearLayout);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            linearLayout_date = itemView.findViewById(R.id.linearLayout_date);
            MsG = itemView.findViewById(R.id.MsG);
            tvDate = itemView.findViewById(R.id.tvDate);
            ivUserImage = itemView.findViewById(R.id.ivUserImage);
            image = itemView.findViewById(R.id.image);
            bg_crdViewOfText = itemView.findViewById(R.id.bg_crdViewOfText);
            bg_crdViewOfImage = itemView.findViewById(R.id.bg_crdViewOfImage);
            bg_crdViewOfAudio = itemView.findViewById(R.id.bg_crdViewOfAudio);
            play = itemView.findViewById(R.id.play);
            seekbar = itemView.findViewById(R.id.progressBar);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}
