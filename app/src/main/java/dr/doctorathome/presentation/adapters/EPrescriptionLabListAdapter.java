package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.LabTestModel;
import dr.doctorathome.network.model.MedicineModel;

public class EPrescriptionLabListAdapter extends RecyclerView.Adapter<EPrescriptionLabListAdapter.ViewHolder> {

    List<LabTestModel> labTestModels;
    Context context;
    private OnItemClick mOnItemClick;

    public List<LabTestModel> getLabTestModels() {
        return labTestModels;
    }

    public void setLabTestModels(List<LabTestModel> labTestModels) {
        this.labTestModels = labTestModels;
    }

    public EPrescriptionLabListAdapter(Context context, OnItemClick mOnItemClick) {
        this.context = context;
        this.mOnItemClick = mOnItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.list_item, parent, false);
        return new EPrescriptionLabListAdapter.ViewHolder(serviceItemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.text.setText(labTestModels.get(position).getName());

    }

    @Override
    public int getItemCount() {
        if (labTestModels == null)
            return 0;
        return labTestModels.size();
    }


      public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.text)
        TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }

    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}
