package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.DashboardModel;
import dr.doctorathome.network.model.ReportModel;

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.ViewHolder> {

    List<DashboardModel> dashboardModels;
    Context context;


    public DashBoardAdapter(Context context) {
        this.context = context;
    }

    public List<DashboardModel> getDashboardModels() {
        return dashboardModels;
    }

    public void setDashboardModels(List<DashboardModel> dashboardModels) {
        this.dashboardModels = dashboardModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.home_dashboard_card_item, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (dashboardModels != null) {
            holder.tvType.setText(dashboardModels.get(position).getType());
            holder.tvNumber.setText(dashboardModels.get(position).getNumber());
            holder.tvStatus.setText(dashboardModels.get(position).getStatus());

            switch (position % 4) {
                case 0:
                    holder.card.setCardBackgroundColor(Color.parseColor("#0BACBC"));
                    Log.d("TAG", "onBindViewHolder: 000");
                    break;
                case 1:
                    holder.card.setCardBackgroundColor(Color.parseColor("#8E24AA"));
                    Log.d("TAG", "onBindViewHolder: 111");
                    break;

                case 2:
                    holder.card.setCardBackgroundColor(Color.parseColor("#C91709"));
                    Log.d("TAG", "onBindViewHolder: 222");
                    break;

                case 3:
                    holder.card.setCardBackgroundColor(Color.parseColor("#2BAA56"));
                    Log.d("TAG", "onBindViewHolder: 333");
                    break;

            }

        }
    }

    @Override
    public int getItemCount() {
        if (dashboardModels == null)
            return 0;
        return dashboardModels.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvType)
        AppCompatTextView tvType;
        @BindView(R.id.tvNumber)
        AppCompatTextView tvNumber;
        @BindView(R.id.tvStatus)
        AppCompatTextView tvStatus;
        @BindView(R.id.card)
        CardView card;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
