package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.network.model.NewMedicinModel;

public class EPrescriptionNewMedicineAdapter extends RecyclerView.Adapter<EPrescriptionNewMedicineAdapter.ViewHolder> {

    ArrayList<NewMedicinModel> medicineModels = new ArrayList<>();
    Context context;

    public ArrayList<NewMedicinModel> getMedicineModels() {
        return medicineModels;
    }

    public void setMedicineModels(ArrayList<NewMedicinModel> medicineModels) {
        this.medicineModels = medicineModels;
    }

    public void addMedicineModels(NewMedicinModel medicineModels) {
        this.medicineModels.add(medicineModels);
    }

    public EPrescriptionNewMedicineAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.consultation_medicine_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.text.setText(medicineModels.get(position).getName());
        holder.delete.setOnClickListener(v -> {
            medicineModels.remove(position);
            notifyDataSetChanged();
        });

    }

    @Override
    public int getItemCount() {
        if (medicineModels == null)
            return 0;
        return medicineModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.delete)
        ImageView delete;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
