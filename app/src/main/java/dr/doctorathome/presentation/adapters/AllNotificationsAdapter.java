package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.NotificationModel;
import dr.doctorathome.presentation.ui.activities.NotificationsActivity;
import dr.doctorathome.presentation.ui.activities.OnlineConsultationDetailsActivity;
import dr.doctorathome.presentation.ui.activities.SupportMessageReplyActivity;
import dr.doctorathome.presentation.ui.activities.VisitDetailsActivity;
import dr.doctorathome.utils.CommonUtil;

public class AllNotificationsAdapter extends RecyclerView.Adapter<AllNotificationsAdapter.ViewHolder> {

    List<NotificationModel> allNotifications;
    Context context;

    public AllNotificationsAdapter(List<NotificationModel> allNotifications, Context context) {
        this.allNotifications = allNotifications;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_notification, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvPatientName.setText(allNotifications.get(position).getSenderName());
        holder.tvNotificationBody.setText(allNotifications.get(position).getMessage());
        holder.tvTime.setText(CommonUtil.dateFullFromServerWithTime(allNotifications.get(position).getSentAt()));
        try {
            Glide.with(context).load(RestClient.REST_API_URL
                    + allNotifications.get(position).getSenderImageUrl().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.profile_placeholder_rounded).into(holder.ivPatientImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (allNotifications.get(position).getRead() != null && !allNotifications.get(position).getRead()) {
            holder.notificationContainer.setBackgroundColor(context.getResources().getColor(R.color.grayTooLight));
        } else {
            holder.notificationContainer.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
        }

        holder.itemView.setOnClickListener(view -> {
            if(allNotifications.get(position).getData() != null){
                if(allNotifications.get(position).getActionType().equals("VIEW_VISIT_ACTION")){
                    Intent goToVisitDetails = new Intent(context, VisitDetailsActivity.class);
                    goToVisitDetails.putExtra("visitId", Integer.parseInt(allNotifications.get(position).getData()));
                    context.startActivity(goToVisitDetails);
                }else if(allNotifications.get(position).getActionType().equals("VIEW_CONTACTUS_ACTION")){
                    Intent goToSupportMessageDetails = new Intent(context, SupportMessageReplyActivity.class);
                    goToSupportMessageDetails.putExtra("supportMessageId", Integer.parseInt(allNotifications.get(position).getData()));
                    context.startActivity(goToSupportMessageDetails);
                }else if(allNotifications.get(position).getActionType().equals("VIEW_ONLINE_CONSULTATION_REQUEST_ACTION")){
                    Intent goToOnlineConsultationDetails = new Intent(context, OnlineConsultationDetailsActivity.class);
                    goToOnlineConsultationDetails.putExtra("id", Integer.parseInt(allNotifications.get(position).getData()));
                    context.startActivity(goToOnlineConsultationDetails);
                }

                ((NotificationsActivity) context).markNotificationAsRead(allNotifications.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return allNotifications.size();
    }

    public List<NotificationModel> getAllNotifications() {
        return allNotifications;
    }

    public void setAllNotifications(List<NotificationModel> allNotifications) {
        this.allNotifications = allNotifications;
    }
 public void addToAllNotifications(List<NotificationModel> allNotifications) {
        this.allNotifications.addAll(allNotifications);
    }

    public void addToFirstIndexAllNotifications(List<NotificationModel> allNotifications) {
        this.allNotifications.addAll(0,allNotifications);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.notificationContainer)
        RelativeLayout notificationContainer;

        @BindView(R.id.ivPatientImage)
        AppCompatImageView ivPatientImage;

        @BindView(R.id.tvPatientName)
        TextView tvPatientName;

        @BindView(R.id.tvNotificationBody)
        TextView tvNotificationBody;

        @BindView(R.id.tvTime)
        TextView tvTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
