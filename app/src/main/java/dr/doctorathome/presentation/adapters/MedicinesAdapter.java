package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.MedicineTypes;
import dr.doctorathome.network.model.CommonDataObject;
import dr.doctorathome.network.model.MedicineInPrescription;
import dr.doctorathome.network.model.Service;
import dr.doctorathome.presentation.ui.activities.PrescriptionActivity;
import dr.doctorathome.presentation.ui.fragments.ServicesFragment;

public class MedicinesAdapter extends RecyclerView.Adapter<MedicinesAdapter.ViewHolder> {

    List<MedicineInPrescription> allMedicines;
    Context context;

    // This object helps you save/restore the open/close state of each view
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public MedicinesAdapter(List<MedicineInPrescription> allMedicines, Context context) {
        this.allMedicines = allMedicines;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.item_medicine, parent, false);

        return new ViewHolder(serviceItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        viewBinderHelper.bind(holder.swipeRevealLayout, String.valueOf(allMedicines.get(position).getUniqueViewId()));

        if (allMedicines.get(position).getMedicineType() == MedicineTypes.RECOMENDED_MEDICINE) {
            holder.doseAndPriceLayout.setVisibility(View.VISIBLE);
            holder.countLayout.setVisibility(View.VISIBLE);
            holder.ivIncreaseAmount.setVisibility(View.VISIBLE);
            holder.ivDecreaseAmount.setVisibility(View.VISIBLE);
        } else if (allMedicines.get(position).getMedicineType() == MedicineTypes.MEDICINE_BOX) {
            holder.doseAndPriceLayout.setVisibility(View.VISIBLE);
            holder.countLayout.setVisibility(View.VISIBLE);
            holder.ivIncreaseAmount.setVisibility(View.GONE);
            holder.ivDecreaseAmount.setVisibility(View.GONE);
        } else if (allMedicines.get(position).getMedicineType() == MedicineTypes.OUT_OF_THE_SYSTEM) {
            holder.doseAndPriceLayout.setVisibility(View.INVISIBLE);
            holder.countLayout.setVisibility(View.GONE);
        }

        holder.amountTV.setText(allMedicines.get(position).getCount() + "");
        holder.ivIncreaseAmount.setOnClickListener(view -> {
            allMedicines.get(position).setCount(allMedicines.get(position).getCount() + 1);
            notifyItemChanged(position);
        });

        holder.ivDecreaseAmount.setOnClickListener(view -> {
            if (allMedicines.get(position).getCount() != 1) {
                allMedicines.get(position).setCount(allMedicines.get(position).getCount() - 1);
                notifyItemChanged(position);
            }
        });

        holder.deleteLayout.setOnClickListener(view -> {
            allMedicines.remove(position);
            notifyDataSetChanged();
        });

        holder.medicineName.setText(allMedicines.get(position).getName());
        holder.tvDose.setText(allMedicines.get(position).getDose());
        holder.tvPrice.setText(allMedicines.get(position).getPrice());

        if(((PrescriptionActivity) context).isViewOnly()) {
            holder.swipeRevealLayout.setLockDrag(true);
            holder.ivIncreaseAmount.setVisibility(View.GONE);
            holder.ivDecreaseAmount.setVisibility(View.GONE);
        }
        holder.itemCard.setOnClickListener(view -> {
        });
    }

    @Override
    public int getItemCount() {
        return allMedicines.size();
    }

    public List<MedicineInPrescription> getAllMedicines() {
        return allMedicines;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.swipeRevealLayout)
        SwipeRevealLayout swipeRevealLayout;

        @BindView(R.id.amountTV)
        TextView amountTV;

        @BindView(R.id.ivIncreaseAmount)
        ImageView ivIncreaseAmount;

        @BindView(R.id.ivDecreaseAmount)
        ImageView ivDecreaseAmount;

        @BindView(R.id.deleteLayout)
        RelativeLayout deleteLayout;

        @BindView(R.id.medicineName)
        TextView medicineName;

        @BindView(R.id.tvDose)
        TextView tvDose;

        @BindView(R.id.tvPrice)
        TextView tvPrice;

        @BindView(R.id.doseAndPriceLayout)
        LinearLayout doseAndPriceLayout;

        @BindView(R.id.countLayout)
        LinearLayout countLayout;

        @BindView(R.id.itemCard)
        CardView itemCard;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void saveStates(Bundle outState) {
        viewBinderHelper.saveStates(outState);
    }

    public void restoreStates(Bundle inState) {
        viewBinderHelper.restoreStates(inState);
    }
}
