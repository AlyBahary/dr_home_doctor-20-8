package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.LabTestModel;

public class EPrescriptionLabAdapter extends RecyclerView.Adapter<EPrescriptionLabAdapter.ViewHolder> {

    List<LabTestModel> labTestModels = new ArrayList<>();
    Context context;

    public List<LabTestModel> getMedicineModels() {
        return labTestModels;
    }

    public void addTestModels(LabTestModel labTestModel) {
        for (int i = 0; i <labTestModels.size() ; i++) {
            if (labTestModels.get(i).getId().equals(labTestModel.getId())) {
                Toast.makeText(context, ""+labTestModel.getName()+context.getString(R.string.is_already_exist), Toast.LENGTH_SHORT).show();
                return;
            }
        }
        this.labTestModels.add(labTestModel);
        notifyDataSetChanged();
    }

    public void setMedicineModels(List<LabTestModel> medicineModels) {
        this.labTestModels = medicineModels;
    }

    public EPrescriptionLabAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.consultation_medicine_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.text.setText(labTestModels.get(position).getName());
        holder.delete.setOnClickListener(v -> {
            labTestModels.remove(position);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        if (labTestModels == null)
            return 0;
        return labTestModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.delete)
        ImageView delete;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
