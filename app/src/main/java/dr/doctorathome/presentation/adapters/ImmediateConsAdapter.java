package dr.doctorathome.presentation.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ImmediateRequestModel;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.utils.CommonUtil;

public class ImmediateConsAdapter extends RecyclerView.Adapter<ImmediateConsAdapter.ViewHolder> {

    List<ImmediateRequestModel> immediateRequestModels;
    Context context;
    private OnItemClick mOnItemClick;

    public ImmediateConsAdapter(List<ImmediateRequestModel> immediateRequestModels, Context context, OnItemClick mOnItemClick) {
        this.immediateRequestModels = immediateRequestModels;
        this.context = context;
        this.mOnItemClick = mOnItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceItemView = inflater.inflate(R.layout.immediate_cons_request_item, parent, false);
        return new ImmediateConsAdapter.ViewHolder(serviceItemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvUserName.setText(immediateRequestModels.get(position).getPatient().getUser().getFirstName() + " " + immediateRequestModels.get(position).getPatient().getUser().getLastName());

        try {
            Glide.with(context).load(RestClient.REST_API_URL
                    + immediateRequestModels.get(position).getPatient().getProfilePicUrl().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .error(R.drawable.test)
                    .placeholder(R.drawable.profile_placeholder_rounded).into(holder.ivPatientImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (immediateRequestModels == null)
            return 0;
        return immediateRequestModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvApprove)
        TextView tvApprove;
        @BindView(R.id.ivPatientImage)
        ImageView ivPatientImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvApprove.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }

    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}
