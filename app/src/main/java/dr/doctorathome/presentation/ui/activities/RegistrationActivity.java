package dr.doctorathome.presentation.ui.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.shuhart.stepview.StepView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.network.model.ValidateSignUpDataBody;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.SignupPresenter;
import dr.doctorathome.presentation.presenters.impl.SignupPresenterImpl;
import dr.doctorathome.presentation.ui.fragments.Step1Fragment;
import dr.doctorathome.presentation.ui.fragments.Step2Fragment;
import dr.doctorathome.presentation.ui.fragments.Step3Fragment;
import dr.doctorathome.presentation.ui.fragments.Step4Fragment;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class RegistrationActivity extends BasicActivity implements SignupPresenter.View {

    private Step1Fragment step1Fragment;
    private Step2Fragment step2Fragment;
    private Step3Fragment step3Fragment;
    private Step4Fragment step4Fragment;

    @BindView(R.id.stepView)
    StepView stepView;

    @BindView(R.id.ivNext)
    ImageView ivNext;

    @BindView(R.id.btnSignup)
    Button btnSignup;

    @BindView(R.id.progressbarLoading)
    ProgressBar progressbarLoading;

    private SignupPresenter signupPresenter;

    private ProgressDialog progressDialog;

    private UserModel userModel;

    private boolean isValidatingData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        userModel = new UserModel();

        isValidatingData = false;

        signupPresenter = new SignupPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new LoginRepositoryImpl(),
                new CommonDataRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        signupPresenter.getLookups();

        if (savedInstanceState == null) {
            step1Fragment = Step1Fragment.newInstance();
            step2Fragment = Step2Fragment.newInstance();
            step3Fragment = Step3Fragment.newInstance();
            step4Fragment = Step4Fragment.newInstance();
        }

        displayStep1Fragment();

        findViewById(R.id.ivBack).setOnClickListener(view -> {
            if (stepView.getCurrentStep() == 0) {
                onBackPressed();
            } else {
                stepChanged(false);
            }
        });

        ivNext.setOnClickListener(view -> stepChanged(true));

        btnSignup.setOnClickListener(view -> {
//            if (step1Fragment.validateForm()) {
//                userModel = step1Fragment.fillUserModelData(userModel);
//            } else {
//                stepView.go(0, true);
//                displayStep1Fragment();
//                return;
//            }
//            if (step2Fragment.validateForm()) {
//                userModel = step2Fragment.fillUserModelData(userModel);
//            } else {
//                stepView.go(1, true);
//                displayStep2Fragment();
//                return;
//            }
//            if (step3Fragment.validateForm()) {
//                userModel = step3Fragment.fillUserModelData(userModel);
//            } else {
//                stepView.go(2, true);
//                displayStep3Fragment();
//                return;
//            }
            if (step4Fragment.validateForm()) {
                userModel = step4Fragment.fillUserModelData(userModel);

                ValidateSignUpDataBody validateSignUpDataBody = new ValidateSignUpDataBody();
                validateSignUpDataBody.setUsername(userModel.getUsername());

                isValidatingData = true;
                signupPresenter.validateSignUpData(validateSignUpDataBody);
            }
        });

//        stepView.setOnStepClickListener(step -> {
//            Timber.i("Tapped tab :: " + step);
//            Timber.i("current tab :: " + stepView.getCurrentStep());
//
//            switch (step) {
//                case 0:
//                    stepView.go(0, true);
//                    displayStep1Fragment();
//                    btnSignup.setVisibility(View.GONE);
//                    ivNext.setVisibility(View.VISIBLE);
//                    break;
//                case 1:
//                    stepView.go(1, true);
//                    displayStep2Fragment();
//                    btnSignup.setVisibility(View.GONE);
//                    ivNext.setVisibility(View.VISIBLE);
//                    break;
//                case 2:
//                    stepView.go(2, true);
//                    displayStep3Fragment();
//                    btnSignup.setVisibility(View.GONE);
//                    ivNext.setVisibility(View.VISIBLE);
//                    break;
//                case 3:
//                    stepView.go(3, true);
//                    displayStep4Fragment();
//                    btnSignup.setVisibility(View.VISIBLE);
//                    ivNext.setVisibility(View.GONE);
//                    break;
//            }
//        });


    }

    private void stepChanged(boolean forward) {
        btnSignup.setVisibility(View.GONE);
        ivNext.setVisibility(View.VISIBLE);
        switch (stepView.getCurrentStep()) {
            case 0:
                if (forward) {
                    if (step1Fragment.validateForm()) {
                        userModel = step1Fragment.fillUserModelData(userModel);
                        ValidateSignUpDataBody validateSignUpDataBody = new ValidateSignUpDataBody();
                        validateSignUpDataBody.setEmail(userModel.getEmail());
                        validateSignUpDataBody.setMobileNumber(userModel.getMobileNumber());

                        isValidatingData = true;
                        signupPresenter.validateSignUpData(validateSignUpDataBody);
                    }
                }
                break;
            case 1:
                if (forward) {
                    if (step2Fragment.validateForm()) {
                        userModel = step2Fragment.fillUserModelData(userModel);
                        ValidateSignUpDataBody validateSignUpDataBody = new ValidateSignUpDataBody();
                        validateSignUpDataBody.setRegistrationNumber(userModel.getRegistrationNumber());

                        isValidatingData = true;
                        signupPresenter.validateSignUpData(validateSignUpDataBody);
                    }
                } else {
                    stepView.go(0, true);
                    displayStep1Fragment();
                }

                break;
            case 2:
                if (forward) {
                    if (step3Fragment.validateForm()) {
                        userModel = step3Fragment.fillUserModelData(userModel);
                        ValidateSignUpDataBody validateSignUpDataBody = new ValidateSignUpDataBody();
                        validateSignUpDataBody.setSaudiId(userModel.getSaudiId());
                        validateSignUpDataBody.setInsuranceNumber(userModel.getInsuranceNumber());
                        validateSignUpDataBody.setIban(userModel.getIban());

                        isValidatingData = true;
                        signupPresenter.validateSignUpData(validateSignUpDataBody);
                    }
                } else {
                    stepView.go(1, true);
                    displayStep2Fragment();
                }
                break;
            case 3:
                if (!forward) {
                    stepView.go(2, true);
                    displayStep3Fragment();
                }
                break;
        }
    }

    protected void displayStep1Fragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (step1Fragment.isAdded()) { // if the fragment is already in container
            ft.show(step1Fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.signup_framelayout, step1Fragment, "1");
        }
        // Hide fragment 2
        if (step2Fragment.isAdded()) {
            ft.hide(step2Fragment);
        }
        // Hide fragment 3
        if (step3Fragment.isAdded()) {
            ft.hide(step3Fragment);
        }
        // Hide fragment 4
        if (step4Fragment.isAdded()) {
            ft.hide(step4Fragment);
        }
        // Commit changes
        ft.commit();
    }

    protected void displayStep2Fragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (step2Fragment.isAdded()) { // if the fragment is already in container
            ft.show(step2Fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.signup_framelayout, step2Fragment, "2");
        }
        // Hide fragment 1
        if (step1Fragment.isAdded()) {
            ft.hide(step1Fragment);
        }
        // Hide fragment 3
        if (step3Fragment.isAdded()) {
            ft.hide(step3Fragment);
        }
        // Hide fragment 4
        if (step4Fragment.isAdded()) {
            ft.hide(step4Fragment);
        }
        // Commit changes
        ft.commit();
    }

    protected void displayStep3Fragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (step3Fragment.isAdded()) { // if the fragment is already in container
            ft.show(step3Fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.signup_framelayout, step3Fragment, "3");
        }
        // Hide fragment 2
        if (step2Fragment.isAdded()) {
            ft.hide(step2Fragment);
        }
        // Hide fragment 1
        if (step1Fragment.isAdded()) {
            ft.hide(step1Fragment);
        }
        // Hide fragment 4
        if (step4Fragment.isAdded()) {
            ft.hide(step4Fragment);
        }
        // Commit changes
        ft.commit();
    }

    protected void displayStep4Fragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (step4Fragment.isAdded()) { // if the fragment is already in container
            ft.show(step4Fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.signup_framelayout, step4Fragment, "4");
        }
        // Hide fragment 2
        if (step2Fragment.isAdded()) {
            ft.hide(step2Fragment);
        }
        // Hide fragment 3
        if (step3Fragment.isAdded()) {
            ft.hide(step3Fragment);
        }
        // Hide fragment 1
        if (step1Fragment.isAdded()) {
            ft.hide(step1Fragment);
        }
        // Commit changes
        ft.commit();
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        step2Fragment.setSpecialities(restLookupsResponse.getSpecializations());
        step2Fragment.setClinics(restLookupsResponse.getClinics());
        step2Fragment.setLevels(restLookupsResponse.getLevels());
        step4Fragment.setCountries(restLookupsResponse.getCountries());
        step4Fragment.setCities(restLookupsResponse.getCities());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 2) {
                if (data != null) {
                    boolean validationCodeDoneSuccessfully = data.getBooleanExtra("validationDone", false);
                    if (validationCodeDoneSuccessfully) {
                        signupPresenter.signup(userModel);
                    }
                }
                Timber.d("validationCodeDoneSuccessfully :: "
                        + data.getBooleanExtra("validationDone", false));
            }
        }
    }

    @Override
    public void signupCodeRetrievedSuccessfully(String code) {
        //no need here
    }

    @Override
    public void forgetPasswordCodeRetrievedSuccessfully(String code) {
        //no need here
    }

    @Override
    public void signupRequestSentSuccessfully() {
        Dialog dialog = CommonUtil.getInfoDialog(this, false, true,
                getString(R.string.after_sign_up_message_title), true,
                getString(R.string.after_sign_up_message_body),
                R.drawable.ic_check_done, getString(R.string.done), () -> finish());

        dialog.show();
    }

    @Override
    public void passwordResetSuccessfully() {
        // no need here
    }

    @Override
    public void dataValidatedSuccessfully() {
        isValidatingData = false;
        switch (stepView.getCurrentStep()) {
            case 0:
                userModel = step1Fragment.fillUserModelData(userModel);
                stepView.go(1, true);
                displayStep2Fragment();

                break;
            case 1:
                userModel = step2Fragment.fillUserModelData(userModel);
                stepView.go(2, true);
                displayStep3Fragment();

                break;
            case 2:
                userModel = step3Fragment.fillUserModelData(userModel);
                stepView.go(3, true);
                displayStep4Fragment();
                btnSignup.setVisibility(View.VISIBLE);
                ivNext.setVisibility(View.GONE);

                break;

            case 3:
                userModel = step4Fragment.fillUserModelData(userModel);
                Intent goToRequestSignupCode = new Intent(RegistrationActivity.this, OtbActivity.class);
                goToRequestSignupCode.putExtra("whatToVerify", userModel.getMobileNumber());
                goToRequestSignupCode.putExtra("type", OtbActivity.MOBILE_VERIFY_SIGN_UP);
                startActivityForResult(goToRequestSignupCode, 2);
                break;
        }
    }

    @Override
    public void showProgress(String message) {
        if(!isValidatingData) {
        CommonUtil.showProgressDialog(progressDialog, message);
        }else{
            if(stepView.getCurrentStep() != 3){
                ivNext.setVisibility(View.GONE);
            }else{
                btnSignup.setVisibility(View.GONE);
            }
            progressbarLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        if(!isValidatingData) {
            CommonUtil.dismissProgressDialog(progressDialog);
        }else{
            if(stepView.getCurrentStep() != 3){
                ivNext.setVisibility(View.VISIBLE);
            }else{
                btnSignup.setVisibility(View.VISIBLE);
            }
            progressbarLoading.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

        if(isValidatingData){
            isValidatingData = false;
        }
    }
}
