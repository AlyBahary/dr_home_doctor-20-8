package dr.doctorathome.presentation.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.Service.LocationService;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestProfileStatistcsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.network.services.LoginService;
import dr.doctorathome.presentation.presenters.ProfilePresenter;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.impl.ProfilePresenterImpl;
import dr.doctorathome.presentation.presenters.impl.UserDataPresenterImpl;
import dr.doctorathome.presentation.ui.activities.AboutUsActivity;
import dr.doctorathome.presentation.ui.activities.ContactUsActivity;
import dr.doctorathome.presentation.ui.activities.LoginActivity;
import dr.doctorathome.presentation.ui.activities.NotificationsActivity;
import dr.doctorathome.presentation.ui.activities.ProfileActivity;
import dr.doctorathome.presentation.ui.activities.ReportsActivity;
import dr.doctorathome.presentation.ui.activities.SettingsActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreFragment extends Fragment implements ProfilePresenter.View, UserDataPresenter.View {

    private UserDataPresenter userDataPresenter;
    private ProfilePresenter profilePresenter;

    @BindView(R.id.ivUserImage)
    ImageView ivUserImage;

    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @BindView(R.id.tvSpecialist)
    TextView tvSpecialist;

    @BindView(R.id.profileAction)
    ConstraintLayout profileAction;

    @BindView(R.id.settingsAction)
    ConstraintLayout settingsAction;

    @BindView(R.id.notificationAction)
    ConstraintLayout notificationAction;

    @BindView(R.id.aboutUsAction)
    ConstraintLayout aboutUsAction;

    @BindView(R.id.contactUsAction)
    ConstraintLayout contactUsAction;

    @BindView(R.id.logoutAction)
    ConstraintLayout logoutAction;

    @BindView(R.id.reportsAction)
    ConstraintLayout reportsAction;

    @BindView(R.id.mainScrollView)
    ScrollView mainScrollView;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.rbDoctorRating)
    RatingBar rbDoctorRating;

    @BindView(R.id.tvNumCompletedVisits)
    TextView tvNumCompletedVisits;

    @BindView(R.id.tvNumSatisfiedPatients)
    TextView tvNumSatisfiedPatients;

    @BindView(R.id.tvNumNewVisits)
    TextView tvNumNewVisits;

    @BindView(R.id.tvNotificationNumber)
    TextView tvNotificationNumber;

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        userDataPresenter = new UserDataPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        profilePresenter = new ProfilePresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this, new DoctorRepositoryImpl());

        profilePresenter.onRequestingProfileStatistics();

        logoutAction.setOnClickListener(view1 -> {
            LoginService loginService = RestClient.getClient().create(LoginService.class);
            loginService.logOut(
                    CommonUtil.commonSharedPref().getString("token", "")
                    , CommonUtil.commonSharedPref().getInt("doctorId", -1) + "").enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    userDataPresenter.changeDoctorStatuesData(new RestChangeDoctorStatuesBody("OFFLINE"
                            , CommonUtil.commonSharedPref().getInt("doctorId", -1)));
                    CommonUtil.logout();
                    getActivity().stopService(new Intent(getContext(), LocationService.class));
                    getActivity().finish();
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    intent.putExtra("backVisible", false);
                    getActivity().startActivity(intent);

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });


        });

        profileAction.setOnClickListener(view1 -> getActivity().startActivity(
                new Intent(getActivity(), ProfileActivity.class))
        );

        settingsAction.setOnClickListener(view1 -> getActivity().startActivity(
                new Intent(getActivity(), SettingsActivity.class))
        );

        contactUsAction.setOnClickListener(view1 -> getActivity().startActivity(
                new Intent(getActivity(), ContactUsActivity.class))
        );

        aboutUsAction.setOnClickListener(view1 -> getActivity().startActivity(
                new Intent(getActivity(), AboutUsActivity.class))
        );

        reportsAction.setOnClickListener(view1 -> getActivity().startActivity(
                new Intent(getActivity(), ReportsActivity.class))
        );

        notificationAction.setOnClickListener(view1 -> getActivity().startActivity(
                new Intent(getActivity(), NotificationsActivity.class))
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Glide.with(getActivity()).load(RestClient.REST_API_URL
                    + CommonUtil.commonSharedPref().getString("profilePicUrl", "").substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .error(R.drawable.test)
                    .placeholder(R.drawable.profile_placeholder_rounded).into(ivUserImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvUserName.setText(getString(R.string.dr) + " " + CommonUtil.commonSharedPref().getString("firstName", "") + " " +
                CommonUtil.commonSharedPref().getString("lastName", ""));

        tvSpecialist.setText(CommonUtil.commonSharedPref().getString("specialist", ""));
    }

    @Override
    public void profileStatisticsRetrieved(RestProfileStatistcsResponse restProfileStatistcsResponse) {
        if (restProfileStatistcsResponse.getDoctorRating() != null) {
            rbDoctorRating.setRating(restProfileStatistcsResponse.getDoctorRating());
        } else {
            rbDoctorRating.setRating(0);
        }

        if (restProfileStatistcsResponse.getUnreadNotificationsCount() != null) {
            tvNotificationNumber.setText("" + restProfileStatistcsResponse.getUnreadNotificationsCount());
        } else {
            tvNotificationNumber.setVisibility(View.GONE);
        }

        tvNumCompletedVisits.setText(restProfileStatistcsResponse.getCompletedConsultations() != null ?
                restProfileStatistcsResponse.getCompletedConsultations() + "" : "0");

        tvNumSatisfiedPatients.setText(restProfileStatistcsResponse.getStatisfiedPatients() != null ?
                restProfileStatistcsResponse.getStatisfiedPatients() + "" : "0");

        tvNumNewVisits.setText(restProfileStatistcsResponse.getNewConsultationsRequests() != null ?
                restProfileStatistcsResponse.getNewConsultationsRequests() + "" : "0");
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        mainScrollView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        mainScrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        if (getActivity().isFinishing())
            return;
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse) {

    }

    @Override
    public void doctorDataUpdatedSuccessfully(CommonResponse commonResponse) {

    }

    @Override
    public void doctorStatuesSuccessfully(CommonResponse commonResponse) {

    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {

    }

    @Override
    public void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse) {

    }

    @Override
    public void visitAcceptedSuccessfully() {

    }

    @Override
    public void visitRejectedSuccessfully() {

    }

    @Override
    public void visitCancelledSuccessfully() {

    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {

    }
}
