package dr.doctorathome.presentation.ui.activities;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RequestClearanceBody;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.presenters.SettingsPresenter;
import dr.doctorathome.presentation.presenters.impl.SettingsPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RequestClearanceActivity extends BasicActivity implements SettingsPresenter.View {

    SettingsPresenter settingsPresenter;

    @BindView(R.id.etDeactivateDate)
    EditText etDeactivateDate;

    @BindView(R.id.reasonEditText)
    EditText reasonEditText;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.viewLayout)
    RelativeLayout viewLayout;

    Calendar selectedDataCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_clearance);
        ButterKnife.bind(this);

        settingsPresenter = new SettingsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl(),
                new CommonDataRepositoryImpl());

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.request_clearence);


        selectedDataCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener datePickerListener = (view, year, monthOfYear, dayOfMonth) -> {
            Calendar selectedDateCalendar = Calendar.getInstance();
            selectedDateCalendar.set(Calendar.YEAR, year);
            selectedDateCalendar.set(Calendar.MONTH, monthOfYear);
            selectedDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDeactivateDate(selectedDateCalendar);
        };

        etDeactivateDate.setOnClickListener(view1 -> {
            Calendar nowCalendar = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(RequestClearanceActivity.this,
                    datePickerListener, selectedDataCalendar
                    .get(Calendar.YEAR), selectedDataCalendar.get(Calendar.MONTH),
                    selectedDataCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(nowCalendar.getTimeInMillis());
            datePickerDialog.show();
        });

        btnSubmit.setOnClickListener(view -> {
            if(!etDeactivateDate.getText().toString().isEmpty()
                    && !reasonEditText.getText().toString().isEmpty()){
                Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(RequestClearanceActivity.this,
                        true, true,
                        getString(R.string.do_you_want_to_deactivate_account), false,
                        "",
                        R.drawable.ic_warning, getString(R.string.yes), getString(R.string.no), () -> {
                            settingsPresenter.requestClearance(new RequestClearanceBody(CommonUtil.commonSharedPref()
                                    .getInt("doctorId", -1), reasonEditText.getText().toString(),
                                    CommonUtil.dateFullToServer(selectedDataCalendar.getTime())));
                        });

                dialog.show();
            }else{
                Toast.makeText(RequestClearanceActivity.this, R.string.fill_the_form_first, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateDeactivateDate(Calendar selectedDate) {
        selectedDataCalendar = selectedDate;

        String dateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        etDeactivateDate.setText(sdf.format(selectedDate.getTime()));
    }

    @Override
    public void clearanceRequestSubmittedSuccessfully(CommonResponse commonResponse) {
        Toast.makeText(this, R.string.request_submitted_success, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void passwordChangedSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void appSettingsRetrievedSuccessfully(RestAppSettingsResponse restAppSettingsResponse) {
        //no need here
    }

    @Override
    public void supportMessageSentSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void supportMessagesHistoryRetrievedSuccessfully(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse) {
        //no need here
    }

    @Override
    public void supportMessageDetailsRetrievedSuccessfully(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse) {
        // no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        viewLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        viewLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
