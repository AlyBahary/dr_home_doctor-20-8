package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.impl.UserDataPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.DataBackListener;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class BottomSheetEditBank extends BottomSheetDialogFragment implements UserDataPresenter.View {

    @BindView(R.id.progress_layout)
    RelativeLayout progressLayout;

    @BindView(R.id.etBankName)
    AppCompatEditText etBankName;

    @BindView(R.id.etIban)
    AppCompatEditText etIban;

    @BindView(R.id.btnSave)
    AppCompatButton btnSave;

    @BindView(R.id.btnCancel)
    AppCompatButton btnCancel;

    UserData userData;
    DataBackListener dataBackListener;

    private UserDataPresenter userDataPresenter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), R.style.MyDialog);
    }

    public BottomSheetEditBank() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomsheet_edit_bank, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userData = (UserData) getArguments().getSerializable("userData");

        userDataPresenter = new UserDataPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        if(userData != null){
            etBankName.setText(userData.getBank());
            etIban.setText(userData.getIban());
        }

        btnSave.setOnClickListener(view1 -> {
            if (etBankName.getText().toString().isEmpty()) {
                etBankName.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etIban.getText().toString().isEmpty()) {
                etIban.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            userData.setBank(etBankName.getText().toString());
            userData.setIban(etIban.getText().toString());

            UserDataForUpdateBody userDataForUpdateBody = new UserDataForUpdateBody();
            userDataForUpdateBody.setBank(etBankName.getText().toString());
            userDataForUpdateBody.setIban(etIban.getText().toString());
            userDataForUpdateBody.setId(userData.getId());

            userDataPresenter.updateDoctorData(userDataForUpdateBody);
        });
        btnCancel.setOnClickListener(view1 -> dismiss());

    }

    @Override
    public void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse) {
        // No need here
    }

    @Override
    public void doctorDataUpdatedSuccessfully(CommonResponse commonResponse) {
        if(dataBackListener != null){
            dataBackListener.dataSavedSuccessfully(userData);
        }
        dismiss();
        Toast.makeText(getActivity(), R.string.data_update_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void doctorStatuesSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        // no need here
    }

    @Override
    public void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse) {
        //no need here
    }

    @Override
    public void visitAcceptedSuccessfully() {
        //no need here
    }

    @Override
    public void visitRejectedSuccessfully() {
        //no need here
    }

    @Override
    public void visitCancelledSuccessfully() {
        //no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        setCancelable(false);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);
        setCancelable(true);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    public void setDataBackListener(DataBackListener dataBackListener) {
        this.dataBackListener = dataBackListener;
    }
}
