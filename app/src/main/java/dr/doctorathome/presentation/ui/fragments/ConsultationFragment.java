package dr.doctorathome.presentation.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.OnlineConsultationsBody;
import dr.doctorathome.network.model.RestOnlineConsultationRequestDetailsResponse;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.adapters.OnlineConsultationsAdapter;
import dr.doctorathome.presentation.presenters.ConsultationPresenter;
import dr.doctorathome.presentation.presenters.impl.ConsultationPresenterImpl;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import dr.doctorathome.utils.EndlessRecyclerViewScrollListener;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;


public class ConsultationFragment extends Fragment implements ConsultationPresenter.View {

    @BindView(R.id.allConsultationsTab)
    TextView allConsultationsTab;

    @BindView(R.id.newConsultationsTab)
    TextView newConsultationsTab;

    @BindView(R.id.runningConsultationsTab)
    TextView runningConsultationsTab;

    @BindView(R.id.completedConsultationsTab)
    TextView completedConsultationsTab;

    @BindView(R.id.ivFilter)
    AppCompatImageButton ivFilter;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.dataRecyclerView)
    RecyclerView dataRecyclerView;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    private String filterBy = null;

    private EndlessRecyclerViewScrollListener scrollListener;

    private int currentPage = 0, pagesCount = 0;

    private ConsultationPresenter consultationPresenter;

    private OnlineConsultationsAdapter onlineConsultationsAdapter;

    private int selectedTab;

    public ConsultationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consultation, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivBack.setVisibility(View.INVISIBLE);
        tvToolbarTitle.setText(R.string.menu_consultation);

        consultationPresenter = new ConsultationPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());
        ivFilter.setVisibility(View.GONE);

        ivFilter.setOnClickListener(view1 -> CommonUtil.showOnlineRequestFilterMenu(getActivity(), ivFilter, menuItem -> {
            Timber.i("selected MenuItem :: " + menuItem.toString());
            switch (menuItem.getItemId()) {
                case R.id.menuNew:
                    filterBy = RequestStatues.PENDING;
                    tvToolbarTitle.setText(R.string.menu_new_consultation);
                    break;
                case R.id.menuCompleted:
                    filterBy = RequestStatues.COMPLETED;
                    tvToolbarTitle.setText(R.string.menu_completed_consultation);
                    break;
                case R.id.menuCancelled:
                    filterBy = RequestStatues.CANCELLED;
                    tvToolbarTitle.setText(R.string.menu_cancelled_consultation);
                    break;
                case R.id.menuConfirmed:
                    filterBy = RequestStatues.CONFIRMED;
                    tvToolbarTitle.setText(R.string.menu_confirmed_consultation);
                    break;
                case R.id.menuRejected:
                    filterBy = RequestStatues.REJECTED;
                    tvToolbarTitle.setText(R.string.menu_rejected_consultation);
                    break;
            }
            currentPage = 0;
            loadAllConsultations();
            return true;
        }));

        allConsultationsTab.setOnClickListener(view1 -> {
            selectedTab = 0;
            tabChanged();
        });

        newConsultationsTab.setOnClickListener(view1 -> {
            selectedTab = 1;
            tabChanged();
        });

        runningConsultationsTab.setOnClickListener(view1 -> {
            selectedTab = 2;
            tabChanged();
        });

        completedConsultationsTab.setOnClickListener(view1 -> {
            selectedTab = 3;
            tabChanged();
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        dataRecyclerView.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Timber.i("SCROLL Happened " + "  ");
                if (page < (pagesCount - 1)) {
                    page++;
                    currentPage++;
                    loadAllConsultations();
                    Timber.i("currentPage : " + " " + Integer.toString(page));
                }
            }
        };

        dataRecyclerView.addOnScrollListener(scrollListener);

        //Set default tab
        selectedTab = 0;
        tabChanged();
    }

    private void tabChanged() {
        if (selectedTab == 0) {
            filterBy = null;
            allConsultationsTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            allConsultationsTab.setTextColor(getResources().getColor(R.color.colorWhite));
            newConsultationsTab.setBackground(null);
            newConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            runningConsultationsTab.setBackground(null);
            runningConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            completedConsultationsTab.setBackground(null);
            completedConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        } else if (selectedTab == 1) {
            filterBy = RequestStatues.PENDING;
            newConsultationsTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            newConsultationsTab.setTextColor(getResources().getColor(R.color.colorWhite));
            allConsultationsTab.setBackground(null);
            allConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            runningConsultationsTab.setBackground(null);
            runningConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            completedConsultationsTab.setBackground(null);
            completedConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        }else if (selectedTab == 2) {
            filterBy = RequestStatues.RUNNING;
            runningConsultationsTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            runningConsultationsTab.setTextColor(getResources().getColor(R.color.colorWhite));
            allConsultationsTab.setBackground(null);
            allConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            newConsultationsTab.setBackground(null);
            newConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            completedConsultationsTab.setBackground(null);
            completedConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        } else if (selectedTab == 3) {
            filterBy = RequestStatues.COMPLETED;
            completedConsultationsTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            completedConsultationsTab.setTextColor(getResources().getColor(R.color.colorWhite));
            allConsultationsTab.setBackground(null);
            allConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            newConsultationsTab.setBackground(null);
            newConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            runningConsultationsTab.setBackground(null);
            runningConsultationsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        }

        currentPage = 0;
        loadAllConsultations();
    }

    private void loadAllConsultations() {
        consultationPresenter.getDoctorConsultationRequests(new OnlineConsultationsBody(CommonUtil.commonSharedPref()
                .getInt("doctorId", -1),
                currentPage, filterBy
        ));
    }

    @Override
    public void doctorConsultationsRetrievedSuccessfully(
            RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse) {
        if(currentPage == 0) {
            pagesCount = restOnlineConsultationsRequestsResponse.getTotalPages() != null
                    ? restOnlineConsultationsRequestsResponse.getTotalPages() : 0;
            onlineConsultationsAdapter = new OnlineConsultationsAdapter(restOnlineConsultationsRequestsResponse.getConsultRequests(),
                    getActivity());
            dataRecyclerView.setAdapter(onlineConsultationsAdapter);
        }else{
            onlineConsultationsAdapter.getDoctorRequests().addAll(restOnlineConsultationsRequestsResponse.getConsultRequests());
            onlineConsultationsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void doctorConsultationDetailsRetrievedSuccessfully(RestOnlineConsultationRequestDetailsResponse restOnlineConsultationRequestDetailsResponse) {
        // No need here
    }

    @Override
    public void requestAcceptedSuccessfully() {
        // No need here
    }

    @Override
    public void requestRejectedSuccessfully() {
        // No need here
    }

    @Override
    public void requestCanceledSuccessfully() {
        // No need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        dataRecyclerView.setVisibility(View.GONE);
//        ivFilter.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        dataRecyclerView.setVisibility(View.VISIBLE);
//        ivFilter.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TTTTTTTTT", "onActivityResult: ");
        if(resultCode==RESULT_OK){
            Log.d("TTTTTTTTT", "onActivityResult: ");

            int id=data.getIntExtra("id",-1);
            loadAllConsultations();
        }
    }
}
