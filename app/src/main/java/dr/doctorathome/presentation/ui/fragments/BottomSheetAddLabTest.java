package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.LabTestInPrescription;
import dr.doctorathome.presentation.adapters.AllLabTestsAdapter;
import dr.doctorathome.presentation.ui.activities.PrescriptionActivity;
import timber.log.Timber;

public class BottomSheetAddLabTest extends BottomSheetDialogFragment {

    private ArrayList<LabTestInPrescription> labTestInPrescriptionList;
    private PrescriptionActivity prescriptionActivity;

    @BindView(R.id.rvLabTests)
    RecyclerView rvLabTests;

    @BindView(R.id.fabAddLabTests)
    FloatingActionButton fabAddLabTests;

    private AllLabTestsAdapter allLabTestsAdapter;

    public BottomSheetAddLabTest() {
    }

    public static BottomSheetAddLabTest newInstance(PrescriptionActivity prescriptionActivity,
                                                    ArrayList<LabTestInPrescription> allMedicinesList) {
        BottomSheetAddLabTest bottomSheetFragment = new BottomSheetAddLabTest();
        Bundle bundle = new Bundle();
        bundle.putSerializable("prescriptionActivity", prescriptionActivity);
        bundle.putParcelableArrayList("allLabTestList", allMedicinesList);
        bottomSheetFragment.setArguments(bundle);

        return bottomSheetFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), R.style.MyDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomsheet_add_lab_test, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prescriptionActivity = (PrescriptionActivity) getArguments().getSerializable("prescriptionActivity");
        labTestInPrescriptionList = getArguments().getParcelableArrayList("allLabTestList");

        Timber.i("labTestInPrescriptionList length :: " + labTestInPrescriptionList.size());
        Timber.i("labTestInPrescriptionList id :: " + labTestInPrescriptionList.get(0).getId());

        allLabTestsAdapter = new AllLabTestsAdapter(labTestInPrescriptionList, getContext());
        rvLabTests.setAdapter(allLabTestsAdapter);

        fabAddLabTests.setOnClickListener(view1 -> {
            List<LabTestInPrescription> labTestInPrescriptionList = new ArrayList<>();
            for (LabTestInPrescription lab : allLabTestsAdapter.getAllLabTests()) {
                if (lab.isChecked()) {
                    labTestInPrescriptionList.add(lab);
                }
            }

            if (!labTestInPrescriptionList.isEmpty()) {
                prescriptionActivity.addLabTests(labTestInPrescriptionList);
                dismiss();
            } else {
                Toast.makeText(getContext(), R.string.select_lab_test_first, Toast.LENGTH_LONG).show();
            }
        });
    }
}
