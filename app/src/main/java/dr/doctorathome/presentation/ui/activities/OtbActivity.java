package dr.doctorathome.presentation.ui.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.SignupPresenter;
import dr.doctorathome.presentation.presenters.impl.SignupPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class OtbActivity extends BasicActivity implements SignupPresenter.View {

    public static int MOBILE_VERIFY_SIGN_UP = 1;
    public static int RESET_PASSWORD = 2;

    @BindView(R.id.pinEntryEditText)
    PinEntryEditText pinEntryEditText;

    @BindView(R.id.btnActive)
    Button btnActive;
    //
    @BindView(R.id.btnResendCode)
    Button btnResendCode;

    private ProgressDialog progressDialog;

    private String returnedCode;

    private SignupPresenter signupPresenter;

    private String whatToVerify;
    //
    int counter;

    Integer typeOfCodeVerification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otb);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("type")) {
            typeOfCodeVerification = getIntent().getIntExtra("type", -1);
        }

        signupPresenter = new SignupPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new LoginRepositoryImpl(),
                new CommonDataRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        whatToVerify = getIntent().getStringExtra("whatToVerify");

        progressDialog = new ProgressDialog(this);

        pinEntryEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() != 4) {
                    btnActive.setEnabled(false);
                    btnActive.setAlpha(0.4f);
                } else {
                    btnActive.setEnabled(true);
                    btnActive.setAlpha(1f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (typeOfCodeVerification == MOBILE_VERIFY_SIGN_UP) {
            signupPresenter.requestSignupCode(whatToVerify);
        } else if (typeOfCodeVerification == RESET_PASSWORD) {
            signupPresenter.requestForgetPasswordCode(whatToVerify);
        } else {
            finish();
        }

        btnActive.setOnClickListener(view -> {
            Log.d("TTTTTTT", "onCreate: " + returnedCode);
            showProgress(getString(R.string.validating_code));
            new Thread(() -> {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    hideProgress();
                    if (returnedCode != null && returnedCode.equals(pinEntryEditText.getText().toString())) {
                        // the matches case
                        Timber.d("The code matches..");
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("validationDone", true);
                        returnIntent.putExtra("verificationCode", returnedCode);
                        setResult(Activity.RESULT_OK, returnIntent);
                        Toast.makeText(OtbActivity.this, R.string.validation_done, Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        // the not matches case
                        Timber.d("The code doesn't match..");
                        Toast.makeText(OtbActivity.this, R.string.incorrect_validation, Toast.LENGTH_LONG).show();
                    }
                });
            }).start();
        });

        btnResendCode.setOnClickListener(view ->
                signupPresenter.requestSignupCode(whatToVerify)
        );
    }

    private void createCountDownTimer() {
        btnResendCode.setEnabled(false);
        btnResendCode.setAlpha(0.4f);

        counter = 60;

        new CountDownTimer(60000, 1000) {

            @Override
            public void onTick(long l) {
                counter--;
                if (counter >= 0) {
                    btnResendCode.setText(getString(R.string.btn_resend) + "(" + counter + ")");
                }
            }

            @Override
            public void onFinish() {
                btnResendCode.setText(getString(R.string.btn_resend));
                btnResendCode.setEnabled(true);
                btnResendCode.setAlpha(1f);
            }
        }.start();
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        //no need here
    }

    @Override
    public void signupCodeRetrievedSuccessfully(String code) {
           Toast.makeText(this, ""+code, Toast.LENGTH_SHORT).show();

        returnedCode = code;
        createCountDownTimer();
    }

    @Override
    public void forgetPasswordCodeRetrievedSuccessfully(String code) {
              Toast.makeText(this, ""+code, Toast.LENGTH_SHORT).show();

        returnedCode = code;
        createCountDownTimer();
    }

    @Override
    public void signupRequestSentSuccessfully() {
        // no need here
    }

    @Override
    public void passwordResetSuccessfully() {
        // no need here
    }

    @Override
    public void dataValidatedSuccessfully() {
        // no need here
    }

    @Override
    public void showProgress(String message) {
        CommonUtil.showProgressDialog(progressDialog, message);
    }

    @Override
    public void hideProgress() {
        CommonUtil.dismissProgressDialog(progressDialog);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
