package dr.doctorathome.presentation.ui.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devlomi.record_view.OnRecordListener;
import com.devlomi.record_view.RecordButton;
import com.devlomi.record_view.RecordView;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.github.piasy.rxandroidaudio.AudioRecorder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ChatModel;
import dr.doctorathome.network.model.MessagesFromAPIModels.ChatAPIModel;
import dr.doctorathome.network.model.UploadFileChatModel;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.adapters.ChatAdapter;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.utils.CommonUtil;
import dr.doctorathome.utils.EndlessRecyclerViewScrollListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class FollowUpChatActivity extends BasicActivity {

    @BindView(R.id.layoutDetails)
    LinearLayout layoutDetails;
    long milisec;
    private Integer doctorId = -1;
    private String name, token, PlayerId = "";
    private String doctor_avatar;
    String dataUrl;
    FollowUpChatActivity instance = this;
    ChatModel chatModel;
    File mAudioFile;
    AudioRecorder mAudioRecorder;
    int min = 0, sec = 0;
    Date date = Calendar.getInstance().getTime();
    //firebase Database
    private DatabaseReference mDatabase;
    // ...
    ChatAdapter chatAdapter;
    ArrayList<ChatModel> chatModels;


    @BindView(R.id.rvChatMessages)
    RecyclerView rvChatMessages;


    @BindView(R.id.imagePick)
    ImageButton imagePick;

    @BindView(R.id.sendButton)
    ImageButton sendButton;

    @BindView(R.id.mes_Edit_Text)
    EditText mes_Edit_Text;

    @BindView(R.id.chat_buttons_sections)
    CardView chat_buttons_sections;

    @BindView(R.id.count_down)
    TextView count_down;

    @BindView(R.id.end_now)
    TextView end_now;

    @BindView(R.id.callSection)
    LinearLayout callSection;
    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;
    EndlessRecyclerViewScrollListener scrollListener;

    private ProgressDialog progressDialog;


    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};
    int currentPage = 0, totalPages = 0;
    private boolean itsTheFirstTime = true;

    //permission grante
    // Requesting permission to RECORD_AUDIO
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) finish();

    }

    //
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation_chat);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        layoutDetails.setVisibility(View.GONE);

        doctorId = CommonUtil.commonSharedPref().getInt("userId", -1);
        token = CommonUtil.commonSharedPref().getString("token", "");

        doctor_avatar = CommonUtil.commonSharedPref().getString("profilePicUrl", "") + "";

        //chat Adapter
        chatModels = new ArrayList<>();
        chatAdapter = new ChatAdapter(instance, chatModels, instance, new ChatAdapter.OnItemClick() {
            @Override
            public void setOnItemClick(int position) {

                if (chatModels.get(position).getMediaType().equals("IMAGE")) {

                    //startActivity Image
                    startActivity(new Intent(instance, ImageZoomingActivity.class)
                            .putExtra("Image", chatModels.get(position).getContent())
                    );

                }

            }
        },
                "" + doctorId
                , ""
                , "" + doctor_avatar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvChatMessages.setLayoutManager(linearLayoutManager);
        rvChatMessages.setAdapter(chatAdapter);

        //todo: some logic here
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (getIntent().getBooleanExtra("isFollowUp", false)) {
                    return;
                }
                if (currentPage < (totalPages)) {
                    page++;
                    currentPage++;
                    getMessages(currentPage, getIntent().getStringExtra("follow_up_id"));
                    Timber.i("currentPage : " + " " + Integer.toString(page));
                }
            }
        };
        //
        rvChatMessages.addOnScrollListener(scrollListener);

        imagePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.create(instance)
                        .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                        .theme(R.style.imagePickerTheme)
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(getResources().getColor(R.color.ef_white)) // Toolbar 'up' arrow color
                        .includeVideo(true) // Show video on image picker
                        .single() // single mode
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .enableLog(false) // disabling log
                        .start(); // start image picker activity with request code
            }
        });


        //recording Audio
        RecordView recordView = (RecordView) findViewById(R.id.record_view);
        RecordButton recordButton = (RecordButton) findViewById(R.id.record_button);
        recordButton.setRecordView(recordView);

        recordView.setSmallMicColor(Color.parseColor("#c2185b"));

        recordView.setSlideToCancelText("" + getString(R.string.swipe_to_cancel));

        //disable Sounds
        recordView.setSoundEnabled(false);

        //prevent recording under one Second (it's false by default)
        recordView.setLessThanSecondAllowed(false);

        //set Custom sounds onRecord
        //you can pass 0 if you don't want to play sound in certain state
        recordView.setCustomSounds(R.raw.record_start, R.raw.record_finished, 0);

        //change slide To Cancel Text Color
        recordView.setSlideToCancelTextColor((getResources().getColor(R.color.colorPrimary)));
        //change slide To Cancel Arrow Color
        recordView.setSlideToCancelArrowColor(((getResources().getColor(R.color.colorPrimary))));
        //change Counter Time (Chronometer) color
        recordView.setCounterTimeColor(Color.parseColor("#ff0000"));

        recordView.setCancelBounds(3);//dp


        // record operations


        recordView.setOnRecordListener(new OnRecordListener() {
            @Override
            public void onStart() {
                //Start Recording..
                if (ContextCompat.checkSelfPermission(instance,
                        Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(instance, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
                } else {


                    mes_Edit_Text.setVisibility(View.GONE);
                    recordView.setVisibility(View.VISIBLE);

                    mAudioFile = new File(
                            Environment.getExternalStorageDirectory().getAbsolutePath() +
                                    File.separator + System.nanoTime() + ".file.m4a");
                    mAudioRecorder = AudioRecorder.getInstance();
                    mAudioRecorder.prepareRecord(MediaRecorder.AudioSource.MIC,
                            MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.AudioEncoder.AAC,
                            mAudioFile);
                    mAudioRecorder.startRecord();


                }
            }

            @Override
            public void onCancel() {
                //On Swipe To Cancel
                mes_Edit_Text.setVisibility(View.VISIBLE);
                recordView.setVisibility(View.GONE);


            }

            @Override
            public void onFinish(long recordTime) {
                //Stop Recording..
                mes_Edit_Text.setVisibility(View.VISIBLE);
                recordView.setVisibility(View.GONE);
                mAudioRecorder.stopRecord();
                mAudioRecorder = null;


                String time = getHumanTimeText(recordTime);
                Log.d("RecordView", "onFinish");

                RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), mAudioFile);
                MultipartBody.Part body = MultipartBody.Part.createFormData("file", mAudioFile.getName(), reqFile);
                uploadPhotoService(body, getIntent().getStringExtra("follow_up_id")+"", doctorId);

            }

            @Override
            public void onLessThanSecond() {
                //When the record time is less than One Second
                mes_Edit_Text.setVisibility(View.VISIBLE);
                recordView.setVisibility(View.GONE);
            }
        });
        //end of recording
        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());


        Log.d("TTTT", "onCreate: "+getIntent().getBooleanExtra("isFollowUp", false));
        Log.d("TTTT", "onCreate: "+getIntent().getIntExtra("state", 0) );
        if (getIntent().getBooleanExtra("isFollowUp", false)) {
            //running
            if (getIntent().getIntExtra("state", 0) != 3) {
                chat_buttons_sections.setVisibility(View.VISIBLE);
                mDatabase = FirebaseDatabase.getInstance().getReference().child("followUpMessages").child(getIntent().getStringExtra("follow_up_id") + "");
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressLayout.setVisibility(View.GONE);
                        rvChatMessages.setVisibility(View.VISIBLE);
                        chatModels.clear();
                        if (dataSnapshot.exists()) {
                            for (final DataSnapshot dataSnap : dataSnapshot.getChildren()) {
                                chatModels.add(dataSnap.getValue(ChatModel.class));
                                if (dataSnap.getValue(ChatModel.class).getSenderId() == doctorId) {
                                    itsTheFirstTime = false;
                                }
                            }
                            chatAdapter.notifyDataSetChanged();
                            if (chatModels.size() > 3) {
                                rvChatMessages.scrollToPosition(chatModels.size() - 1);
                            }

                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                sendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mes_Edit_Text.getText().toString().trim().isEmpty()) {
                            date = Calendar.getInstance().getTime();
                            Log.d("TTTTTTTT", "onClick: " +
                                    DateFormat.format("yyyy-MM-dd'T'HH:mm:ss", new Date()));
                            // sendNotification(PlayerId, mes_Edit_Text.getText().toString());

                            chatModel = new ChatModel("" + mes_Edit_Text.getText().toString()
                                    , "txt"
                                    , "UNKNOWN"
                                    , "TEXT"
                                    , Integer.parseInt(getIntent().getStringExtra("patient_id") + "")
                                    , Integer.parseInt(doctorId + "")
                                    , "" + DateFormat.format("yyyy-MM-dd'T'HH:mm:ss", new Date())
                            );
                            chatModels.add(chatModel);
                            mDatabase.push().setValue(chatModel);
                            mes_Edit_Text.setText("");
                            chatAdapter.notifyDataSetChanged();
                        }
                    }
                });

            } else {
                chat_buttons_sections.setVisibility(View.GONE);
                getMessages(currentPage, getIntent().getStringExtra("follow_up_id"));
            }
            if(getIntent().getIntExtra("state", 0) == 2){
                chat_buttons_sections.setVisibility(View.GONE);

            }


        }


    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // or get a single image only
            Image image = ImagePicker.getFirstImageOrNull(data);
            File file = new File(image.getPath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
            uploadPhotoService(body, getIntent().getStringExtra("follow_up_id"), doctorId);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getHumanTimeText(long milliseconds) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }

    //function Upload Image
    public void uploadPhotoService(MultipartBody.Part body, String sessionId, int SenderId) {
        //CommonUtil.showProgressDialog(progressDialog, "" + getString(R.string.uploading));


        Toast toast = Toast.makeText(instance, "" + getString(R.string.uploading), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);


        doctorService
                .Upload_Img_follow_Up(body, getIntent().getStringExtra("follow_up_id")+"", SenderId, token).enqueue(new Callback<UploadFileChatModel>() {
            @Override
            public void onResponse(Call<UploadFileChatModel> call, Response<UploadFileChatModel> response) {
                if (response.isSuccessful()) {
                    // CommonUtil.dismissProgressDialog(progressDialog);
                    dataUrl = response.body().getRspBody();
                    Log.d("TTTT", "onResponse: " + call.request().toString());
                    Log.d("TTTTT", "onResponse: " + response.body().getRspStatus());
                    Log.d("TTTTT", "onResponse: " + response.body().getRspBody());
                } else {
                    Log.d("TTTTT", "onResponse: " + call.request().toString());
                    Log.d("TTTTT", "onResponse: " + response.code());


                }
            }

            @Override
            public void onFailure(Call<UploadFileChatModel> call, Throwable t) {

                //  CommonUtil.dismissProgressDialog(progressDialog);
                Toast toast = Toast.makeText(instance, "" + getString(R.string.Please_Check_your_internet_Connection), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                Log.d("TTTTTTT", "onFailure: " + t.toString());
                Log.d("TTTTTTT", "onFailure: " + t.getMessage());
                Log.d("TTTTTTT", "onFailure: " + t.getStackTrace());
                toast.show();


            }
        });
    }


    private void getMessages(int pageNum, String sessionId) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.getMessages_followUp(pageNum, sessionId, token).enqueue(new Callback<ChatAPIModel>() {
            @Override
            public void onResponse(Call<ChatAPIModel> call, Response<ChatAPIModel> response) {
                progressLayout.setVisibility(View.GONE);
                rvChatMessages.setVisibility(View.VISIBLE);

                if (response.isSuccessful()) {
                    Log.d("TTTTTTTTTTTT", "onResponse: " + response.body().getTotalPages());
                    Log.d("TTTTTTTTTTTT", "onResponse: " + response.raw().request().url().toString());
                    if (response.body().getMessages() != null) {
                        chatAdapter.isOld(true);
                        for (int j = 0; j < response.body().getMessages().size(); j++) {
                            chatModels.add(new ChatModel("" + response.body().getMessages().get(j).getContent()
                                            , ""
                                            , "" + response.body().getMessages().get(j).getMediaType()
                                            , "" + response.body().getMessages().get(j).getMsgType()
                                            , response.body().getMessages().get(j).getRecieverId()
                                            , response.body().getMessages().get(j).getSenderId()
                                            , "" + response.body().getMessages().get(j).getSentAt()
                                    )

                            );
                        }
                        if (response.body().getTotalPages() > 1) {
                            totalPages = response.body().getTotalPages();

                        }
                    }
                } else {

                }
                Log.d("TTTTTTTTTTTT", "onResponse not : " + response.code());
                Log.d("TTTTTTTTTTTT", "onResponsen ot: " + response.message() + sessionId + "--" + token);
                Log.d("TTTTTTTTTTTT", "onResponsen ot: " + response.raw().request().url());
                chatAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<ChatAPIModel> call, Throwable t) {
                Log.d("TTTTTTTTTTTT", "onFailure: " + t.getMessage());
            }
        });
    }


}
