package dr.doctorathome.presentation.ui.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.ReportModel;
import dr.doctorathome.network.model.RestReportDetailsResponse;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.adapters.ReportInfoAdapter;
import dr.doctorathome.presentation.presenters.ReportsPresenter;
import dr.doctorathome.presentation.presenters.impl.ReportsPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;

public class ReportsActivity extends BasicActivity implements ReportsPresenter.View {

    @BindView(R.id.btnGetVisitsReport)
    Button btnGetVisitsReport;
    @BindView(R.id.btnGetConsuktationsReport)
    Button btnGetConsuktationsReport;

    @BindView(R.id.rv_financial)
    RecyclerView rvFinancial;
    @BindView(R.id.rv_Statistics)
    RecyclerView rvStatistics;

    ArrayList<ReportModel> reportModelVistisFinacial = new ArrayList<>();
    ArrayList<ReportModel> reportModelVistisStatistics = new ArrayList<>();
    ArrayList<ReportModel> reportModelConsultationFinacial = new ArrayList<>();
    ArrayList<ReportModel> reportModelConsultationStatiscs = new ArrayList<>();

    ReportInfoAdapter reportInfoAdapterFinancial = new ReportInfoAdapter(this);
    ReportInfoAdapter reportInfoAdapterStatistics = new ReportInfoAdapter(this);
    private ReportsPresenter reportsPresenter;

    @BindView(R.id.mainScrollView)
    NestedScrollView mainScrollView;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.llReportResultDetails)
    LinearLayout llReportResultDetails;

    @BindView(R.id.etFromDate)
    AppCompatEditText etFromDate;

    @BindView(R.id.etToDate)
    AppCompatEditText etToDate;

    @BindView(R.id.btnGetReport)
    Button btnGetReport;

    @BindView(R.id.tvNumCompletedVisits)
    TextView tvNumCompletedVisits;

    @BindView(R.id.tvNumOnlineConsultations)
    TextView tvNumOnlineConsultations;


    //
    @BindView(R.id.tvNumCancelledVisits)
    TextView tvNumCancelledVisits;


    @BindView(R.id.tvNumPendingVisits)
    TextView tvNumPendingVisits;

    @BindView(R.id.tvNumDistributedMedicines)
    TextView tvNumDistributedMedicines;

    @BindView(R.id.tvNumDeliveredMedicines)
    TextView tvNumDeliveredMedicines;

    @BindView(R.id.tvDelayPercentageOfVisits)
    TextView tvDelayPercentageOfVisits;

    @BindView(R.id.tvRating)
    TextView tvRating;


    @BindView(R.id.currentDayReportTab)
    TextView currentDayReportTab;

    @BindView(R.id.currentWeekReportTab)
    TextView currentWeekReportTab;

    @BindView(R.id.currentMonthReportTab)
    TextView currentMonthReportTab;

    private int selectedTab;

    Calendar selectedFromDataCalendar, selectedToDataCalendar;

    Boolean isVisit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        ButterKnife.bind(this);

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.reports);

        reportsPresenter = new ReportsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

        etFromDate.setOnClickListener(view -> {
            Calendar nowCalendar = Calendar.getInstance();
//            nowCalendar.set(Calendar.YEAR, nowCalendar.get(Calendar.YEAR) - 1);

            DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this,
                    fromDatePickerListener, selectedFromDataCalendar
                    .get(Calendar.YEAR), selectedFromDataCalendar.get(Calendar.MONTH),
                    selectedFromDataCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMaxDate(nowCalendar.getTimeInMillis());
            datePickerDialog.setTitle("");
            datePickerDialog.show();
        });

        etToDate.setOnClickListener(view -> {
            Calendar nowCalendar = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this,
                    toDatePickerListener, selectedToDataCalendar
                    .get(Calendar.YEAR), selectedToDataCalendar.get(Calendar.MONTH),
                    selectedToDataCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMaxDate(nowCalendar.getTimeInMillis());
            datePickerDialog.setTitle("");
            datePickerDialog.show();
        });

        btnGetReport.setOnClickListener(view -> {
            if (selectedFromDataCalendar.before(selectedToDataCalendar) ||
                    selectedFromDataCalendar.equals(selectedToDataCalendar)) {
                reportsPresenter.onRequestingReport(etFromDate.getText().toString(),
                        etToDate.getText().toString());
            } else {
                Toast.makeText(ReportsActivity.this, R.string.date_validation
                        , Toast.LENGTH_LONG).show();
            }

        });
        btnGetConsuktationsReport.setOnClickListener(v -> {
            reportInfoAdapterFinancial.setReportModels(reportModelConsultationFinacial);
            rvFinancial.setAdapter(reportInfoAdapterFinancial);
            reportInfoAdapterStatistics.setReportModels(reportModelConsultationStatiscs);
            rvStatistics.setAdapter(reportInfoAdapterStatistics);

            isVisit = false;

            btnGetConsuktationsReport.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            btnGetConsuktationsReport.setTextColor(getResources().getColor(R.color.colorWhite));
            btnGetVisitsReport.setBackground(null);
            btnGetVisitsReport.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));

        });
        btnGetVisitsReport.setOnClickListener(v -> {
            reportInfoAdapterFinancial.setReportModels(reportModelVistisFinacial);
            rvFinancial.setAdapter(reportInfoAdapterFinancial);
            reportInfoAdapterStatistics.setReportModels(reportModelVistisStatistics);
            rvStatistics.setAdapter(reportInfoAdapterStatistics);

            isVisit = true;

            btnGetVisitsReport.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            btnGetVisitsReport.setTextColor(getResources().getColor(R.color.colorWhite));
            btnGetConsuktationsReport.setBackground(null);
            btnGetConsuktationsReport.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));

        });
        //set default values
        currentDayReportTab.setOnClickListener(view -> {
            selectedTab = 0;
            tabChanged();
        });

        currentWeekReportTab.setOnClickListener(view -> {
            selectedTab = 1;
            tabChanged();
        });

        currentMonthReportTab.setOnClickListener(view -> {
            selectedTab = 2;
            tabChanged();
        });

        selectedTab = 0;
        tabChanged();
    }

    private void tabChanged() {
        if (selectedTab != -1) {
            selectedFromDataCalendar = Calendar.getInstance();
            selectedToDataCalendar = Calendar.getInstance();
        }

        if (selectedTab == 0) {
            currentDayReportTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            currentDayReportTab.setTextColor(getResources().getColor(R.color.colorWhite));
            currentWeekReportTab.setBackground(null);
            currentWeekReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            currentMonthReportTab.setBackground(null);
            currentMonthReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        } else if (selectedTab == 1) {
            selectedFromDataCalendar.set(Calendar.WEEK_OF_MONTH, selectedFromDataCalendar.get(Calendar.WEEK_OF_MONTH) - 1);
            currentWeekReportTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            currentWeekReportTab.setTextColor(getResources().getColor(R.color.colorWhite));
            currentDayReportTab.setBackground(null);
            currentDayReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            currentMonthReportTab.setBackground(null);
            currentMonthReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        } else if (selectedTab == 2) {
            selectedFromDataCalendar.set(Calendar.MONTH, selectedFromDataCalendar.get(Calendar.MONTH) - 1);
            currentMonthReportTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            currentMonthReportTab.setTextColor(getResources().getColor(R.color.colorWhite));
            currentDayReportTab.setBackground(null);
            currentDayReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            currentWeekReportTab.setBackground(null);
            currentWeekReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        } else {
            currentDayReportTab.setBackground(null);
            currentDayReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            currentWeekReportTab.setBackground(null);
            currentWeekReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            currentMonthReportTab.setBackground(null);
            currentMonthReportTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        }

        if (selectedTab != -1) {
            datesChanged();
        }
    }

    private void datesChanged() {
        String dateFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        etToDate.setText(sdf.format(selectedToDataCalendar.getTime()));

        etFromDate.setText(sdf.format(selectedFromDataCalendar.getTime()));


        reportsPresenter.onRequestingReport(etFromDate.getText().toString(),
                etToDate.getText().toString());
    }

    DatePickerDialog.OnDateSetListener fromDatePickerListener = (view, year, monthOfYear, dayOfMonth) -> {
        Calendar selectedDateCalendar = Calendar.getInstance();
        selectedDateCalendar.set(Calendar.YEAR, year);
        selectedDateCalendar.set(Calendar.MONTH, monthOfYear);
        selectedDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateFromDate(selectedDateCalendar);
    };

    private void updateFromDate(Calendar selectedDate) {
        selectedFromDataCalendar = selectedDate;

        String dateFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        etFromDate.setText(sdf.format(selectedDate.getTime()));

        selectedTab = -1;
        tabChanged();
    }

    DatePickerDialog.OnDateSetListener toDatePickerListener = (view, year, monthOfYear, dayOfMonth) -> {
        Calendar selectedDateCalendar = Calendar.getInstance();
        selectedDateCalendar.set(Calendar.YEAR, year);
        selectedDateCalendar.set(Calendar.MONTH, monthOfYear);
        selectedDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateToDate(selectedDateCalendar);
    };

    private void updateToDate(Calendar selectedDate) {
        selectedToDataCalendar = selectedDate;

        String dateFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        etToDate.setText(sdf.format(selectedDate.getTime()));

        selectedTab = -1;
        tabChanged();
    }

    @Override
    public void reportDetailsRetrieved(RestReportDetailsResponse restReportDetailsResponse) {

        Log.d("TTTT", "reportDetailsRetrieved: " + restReportDetailsResponse.getTotalCompleteVisitRequests());
        tvNumCompletedVisits.setText(
                restReportDetailsResponse.getTotalCompleteVisitRequests() != null ?
                        (restReportDetailsResponse.getTotalCompleteVisitRequests() > 99 ?
                                "99+" : "" + restReportDetailsResponse.getTotalCompleteVisitRequests()) : "");

        tvNumOnlineConsultations.setText(restReportDetailsResponse.getTotalCompleteConsultationSession() != null ?
                (restReportDetailsResponse.getTotalCompleteConsultationSession() > 99 ?
                        "99+" : "" + restReportDetailsResponse.getTotalCompleteConsultationSession()) : "0");


        reportModelVistisFinacial.clear();
        reportModelVistisStatistics.clear();
        reportModelConsultationFinacial.clear();
        reportModelConsultationStatiscs.clear();


        /****************---- Visits ----**************/

        reportModelVistisStatistics.add(new ReportModel(getString(R.string.totalCancelledVisits)
                , restReportDetailsResponse.getTotalCancelledVisits() + ""));
        //
        reportModelVistisStatistics.add(new ReportModel(getString(R.string.totalCancelledVisitsByPatient)
                , Integer.parseInt(restReportDetailsResponse.getTotalCancelledSessionsByPatient() + "") + ""));
        //
        reportModelVistisStatistics.add(new ReportModel(getString(R.string.totalCancelledVisitsBySystem)
                , Integer.parseInt(restReportDetailsResponse.getTotalCancelledSessionsBySystem() + "") + ""));
        //
        reportModelVistisStatistics.add(new ReportModel(getString(R.string.totalPendingVisits)
                , restReportDetailsResponse.getTotalPendingVisits() + ""));
        //
        reportModelVistisFinacial.add(new ReportModel(getString(R.string.totalPremiumVisitRequests)
                , restReportDetailsResponse.getTotalPremiumVisitRequests() + " SAR"));
        //
        reportModelVistisFinacial.add(new ReportModel(getString(R.string.doctorCommissionAmount)
                , restReportDetailsResponse.getDoctorCommissionAmount() + " SAR"));
        //
        reportModelVistisFinacial.add(new ReportModel(getString(R.string.totalPaidPremium)
                , restReportDetailsResponse.getTotalPaidPremium() + " SAR"));
        //
        reportModelVistisFinacial.add(new ReportModel(getString(R.string.totalRemainingPremium)
                , restReportDetailsResponse.getTotalRemainingPremium() + " SAR"));
        //
        reportModelVistisFinacial.add(new ReportModel(getString(R.string.totalNotDeductions)
                , restReportDetailsResponse.getTotalNotDeductions() + " SAR"));
        //
        reportModelVistisFinacial.add(new ReportModel(getString(R.string.totalDeductions)
                , restReportDetailsResponse.getTotalNotDeductions() + " SAR"));
        //
        reportModelVistisFinacial.add(new ReportModel(getString(R.string.totalPremium)
                , restReportDetailsResponse.getTotalPremium() + " SAR"));
        //
        /****************---- consultation ----**************/
        reportModelConsultationStatiscs.add(new ReportModel(getString(R.string.totalCancelledSessionsByPatient),
                Integer.parseInt(restReportDetailsResponse.getTotalCancelledSessionsByPatient() + "") + ""));
        //
        reportModelConsultationStatiscs.add(new ReportModel(getString(R.string.totalCancelledSessionsBySystem),
                Integer.parseInt(restReportDetailsResponse.getTotalCancelledSessionsBySystem() + "") + ""));
        //
        reportModelConsultationStatiscs.add(new ReportModel(getString(R.string.totalPendingSessions),
                Integer.parseInt(restReportDetailsResponse.getTotalPendingSessions() + "") + ""));
        //

        //
        reportModelConsultationFinacial.add(new ReportModel(getString(R.string.totalPremiumConsultationSession),
                restReportDetailsResponse.getTotalPremiumConsultationSession() + " SAR"));
        //
        reportModelConsultationFinacial.add(new ReportModel(getString(R.string.doctorOnlineSessionCommissionAmount),
                restReportDetailsResponse.getDoctorOnlineSessionCommissionAmount() + " SAR"));
        //
        reportModelConsultationFinacial.add(new ReportModel(getString(R.string.totalRemainingPremiumOfSessions),
                restReportDetailsResponse.getTotalRemainingPremiumOfSessions() + " SAR"));
        //
        reportModelConsultationFinacial.add(new ReportModel(getString(R.string.totalRemainingPremiumOfSessions),
                restReportDetailsResponse.getTotalRemainingPremiumOfSessions() + " SAR"));
        //
        reportModelConsultationFinacial.add(new ReportModel(getString(R.string.totalPaidPremiumOfSessions),
                restReportDetailsResponse.getTotalPaidPremiumOfSessions() + " SAR"));
        //
        reportModelConsultationFinacial.add(new ReportModel(getString(R.string.totalNotDeductionsOfSessions),
                restReportDetailsResponse.getTotalNotDeductionsOfSessions() + " SAR"));
        //
        reportModelConsultationFinacial.add(new ReportModel(getString(R.string.totalDeductionsOfSessions),
                restReportDetailsResponse.getTotalDeductionsOfSessions() + " SAR"));
        //


        /****************----  ----**************/


        //
        tvRating.setText(restReportDetailsResponse.getRating() != null ?
                (restReportDetailsResponse.getRating() > 99 ?
                        "99+" : "" + restReportDetailsResponse.getRating()) : "0");

        tvNumCancelledVisits.setText(restReportDetailsResponse.getTotalCancelledVisits() != null ?
                (restReportDetailsResponse.getTotalCancelledVisits() > 99 ?
                        "99+" : "" + restReportDetailsResponse.getTotalCancelledVisits()) : "0");


        tvNumPendingVisits.setText(restReportDetailsResponse.getTotalPendingVisits() != null ?
                (restReportDetailsResponse.getTotalPendingVisits() > 99 ?
                        "99+" : "" + restReportDetailsResponse.getTotalPendingVisits()) : "0");

        tvNumDistributedMedicines.setText(restReportDetailsResponse.getTotalDistributedMedicine() != null ?
                (restReportDetailsResponse.getTotalDistributedMedicine() > 99 ?
                        "99+" : "" + restReportDetailsResponse.getTotalDistributedMedicine()) : "0");

        tvNumDeliveredMedicines.setText(restReportDetailsResponse.getTotalDeliveredMedicine() != null ?
                (restReportDetailsResponse.getTotalDeliveredMedicine() > 99 ?
                        "99+" : "" + restReportDetailsResponse.getTotalDeliveredMedicine()) : "0");

        tvDelayPercentageOfVisits.setText((restReportDetailsResponse.getDelayPercentage() != null ?
                ("" + restReportDetailsResponse.getDelayPercentage()) : "0") + "%");

        llReportResultDetails.setVisibility(View.VISIBLE);

        if (isVisit) {
            reportInfoAdapterFinancial.setReportModels(reportModelVistisFinacial);
            rvFinancial.setAdapter(reportInfoAdapterFinancial);

            reportInfoAdapterStatistics.setReportModels(reportModelVistisStatistics);
            rvStatistics.setAdapter(reportInfoAdapterStatistics);

        } else {
            reportInfoAdapterFinancial.setReportModels(reportModelConsultationFinacial);
            rvFinancial.setAdapter(reportInfoAdapterFinancial);
            reportInfoAdapterStatistics.setReportModels(reportModelConsultationStatiscs);
            rvStatistics.setAdapter(reportInfoAdapterStatistics);

        }
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        mainScrollView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        mainScrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

        llReportResultDetails.setVisibility(View.GONE);
    }
}
