package dr.doctorathome.presentation.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.CommonDataObject;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.utils.CommonUtil;

public class Step1Fragment extends Fragment {


    @BindView(R.id.etFirstName)
    AppCompatEditText etFirstName;

    @BindView(R.id.etLastName)
    AppCompatEditText etLastName;

    @BindView(R.id.etEmailAddress)
    AppCompatEditText etEmailAddress;

    @BindView(R.id.etPhoneNumber)
    AppCompatEditText etPhoneNumber;

    @BindView(R.id.chMale)
    CheckBox chMale;

    @BindView(R.id.chFemale)
    CheckBox chFemale;



    String gender;

    public Step1Fragment() {
        // Required empty public constructor
    }

    public static Step1Fragment newInstance() {
        return new Step1Fragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_step1, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gender = "MALE";

        chMale.setOnClickListener(view1 -> {
            chMale.setChecked(true);
            chMale.setClickable(false);
            chMale.setFocusable(false);
            gender = "MALE";

            chFemale.setChecked(false);
            chFemale.setClickable(true);
            chFemale.setFocusable(true);
        });

        chFemale.setOnClickListener(view1 -> {
            chFemale.setChecked(true);
            chFemale.setClickable(false);
            chFemale.setFocusable(false);
            gender = "FEMALE";

            chMale.setChecked(false);
            chMale.setClickable(true);
            chMale.setFocusable(true);
        });


    }



    public boolean validateForm(){
        if (etFirstName.getText().toString().trim().isEmpty()) {
            etFirstName.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etLastName.getText().toString().trim().isEmpty()) {
            etLastName.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etEmailAddress.getText().toString().trim().isEmpty()) {
            etEmailAddress.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (!CommonUtil.validateEmail(etEmailAddress.getText().toString())) {
            etEmailAddress.setError(getString(R.string.enter_valid_mail));
            return false;
        }

        if (etPhoneNumber.getText().toString().trim().isEmpty()) {
            etPhoneNumber.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (!CommonUtil.validatePhoneNumber(etPhoneNumber.getText().toString())) {
            etPhoneNumber.setError(getString(R.string.enter_valid_phone_number));
            return false;
        }
        return true;
    }

    public UserModel fillUserModelData(UserModel userModel){
        userModel.setFirstName(etFirstName.getText().toString());
        userModel.setLastName(etLastName.getText().toString());
        userModel.setEmail(etEmailAddress.getText().toString());
        userModel.setMobileNumber(etPhoneNumber.getText().toString());
        userModel.setGender(gender);
        return userModel;
    }
}
