package dr.doctorathome.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import dr.doctorathome.R;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;

import android.os.Bundle;
import android.view.View;

public class AuthorizationActivity extends BasicActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        findViewById(R.id.btnDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
