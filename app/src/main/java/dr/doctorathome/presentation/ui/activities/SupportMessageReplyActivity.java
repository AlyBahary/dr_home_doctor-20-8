package dr.doctorathome.presentation.ui.activities;


import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.presenters.SettingsPresenter;
import dr.doctorathome.presentation.presenters.impl.SettingsPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class SupportMessageReplyActivity extends BasicActivity implements SettingsPresenter.View {

    private SettingsPresenter settingsPresenter;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.dataLayout)
    RelativeLayout dataLayout;

    @BindView(R.id.adminHeaderCardView)
    CardView adminHeaderCardView;

    @BindView(R.id.adminReplyCardView)
    CardView adminReplyCardView;

    @BindView(R.id.tvItemHeader)
    TextView tvItemHeader;

    @BindView(R.id.tvTime)
    TextView tvTime;

    @BindView(R.id.ivItemTypeClosed)
    TextView ivItemTypeClosed;

    @BindView(R.id.ivItemTypeOpen)
    TextView ivItemTypeOpen;

    @BindView(R.id.tvItemBody)
    TextView tvItemBody;

    @BindView(R.id.tvReplyTime)
    TextView tvReplyTime;

    @BindView(R.id.tvReplyBody)
    TextView tvReplyBody;

    private Integer supportMessageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_message_reply);
        ButterKnife.bind(this);

        settingsPresenter = new SettingsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl(),
                new CommonDataRepositoryImpl());

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.support);

        supportMessageId = getIntent().getIntExtra("supportMessageId", -1);

        settingsPresenter.getSupportMessageDetails(supportMessageId);
    }

    @Override
    public void clearanceRequestSubmittedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void passwordChangedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void appSettingsRetrievedSuccessfully(RestAppSettingsResponse restAppSettingsResponse) {
        // no need here
    }

    @Override
    public void supportMessageSentSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void supportMessagesHistoryRetrievedSuccessfully(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse) {
        // no need here
    }

    @Override
    public void supportMessageDetailsRetrievedSuccessfully(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse) {
        if(restSupportMessageDetailsResponse.getReply() == null || restSupportMessageDetailsResponse.getReplyDate() == null){
            adminHeaderCardView.setVisibility(View.GONE);
            adminReplyCardView.setVisibility(View.GONE);
        }else{
            tvReplyBody.setText(restSupportMessageDetailsResponse.getReply());
            tvReplyTime.setText(CommonUtil.dateDaysAndTimeFromServerWithTime(restSupportMessageDetailsResponse.getReplyDate()));
        }

        tvItemHeader.setText(restSupportMessageDetailsResponse.getSubject());
        tvItemBody.setText(restSupportMessageDetailsResponse.getMessage());
        tvTime.setText(CommonUtil.dateDaysAndTimeFromServerWithTimeFull(restSupportMessageDetailsResponse.getCreationDate()));

        if(restSupportMessageDetailsResponse.getStatus().equals("OPEN")){
            ivItemTypeOpen.setVisibility(View.VISIBLE);
            ivItemTypeClosed.setVisibility(View.GONE);
        }else{
            ivItemTypeOpen.setVisibility(View.GONE);
            ivItemTypeClosed.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        dataLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        dataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
