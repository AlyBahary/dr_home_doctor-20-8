package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.impl.UserDataPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.DataBackListener;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class BottomSheetEditPersonalInfo extends BottomSheetDialogFragment implements UserDataPresenter.View {

    @BindView(R.id.progress_layout)
    RelativeLayout progressLayout;

    @BindView(R.id.etEmailAddress)
    AppCompatEditText etEmailAddress;

    @BindView(R.id.etPhoneNumber)
    AppCompatEditText etPhoneNumber;

    @BindView(R.id.chMale)
    AppCompatCheckBox chMale;

    @BindView(R.id.chFemale)
    AppCompatCheckBox chFemale;

    @BindView(R.id.btnSave)
    AppCompatButton btnSave;

    @BindView(R.id.btnCancel)
    AppCompatButton btnCancel;

    UserData userData;

    DataBackListener dataBackListener;

    private UserDataPresenter userDataPresenter;

    String gender;

    public BottomSheetEditPersonalInfo() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), R.style.MyDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomsheet_edit_person_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userData = (UserData) getArguments().getSerializable("userData");

        userDataPresenter = new UserDataPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        if(userData != null){
            gender = userData.getUser().getGender();
            if(gender.equals("MALE")){
                chMale.setChecked(true);
                chMale.setClickable(false);
                chMale.setFocusable(false);

                chFemale.setChecked(false);
                chFemale.setClickable(true);
                chFemale.setFocusable(true);
            }else{
                chFemale.setChecked(true);
                chFemale.setClickable(false);
                chFemale.setFocusable(false);

                chMale.setChecked(false);
                chMale.setClickable(true);
                chMale.setFocusable(true);
            }
            etEmailAddress.setText(userData.getUser().getEmail());
            etPhoneNumber.setText(userData.getUser().getMobileNumber());
        }

        chMale.setOnClickListener(view1 -> {
            chMale.setChecked(true);
            chMale.setClickable(false);
            chMale.setFocusable(false);
            gender = "MALE";

            chFemale.setChecked(false);
            chFemale.setClickable(true);
            chFemale.setFocusable(true);
        });

        chFemale.setOnClickListener(view1 -> {
            chFemale.setChecked(true);
            chFemale.setClickable(false);
            chFemale.setFocusable(false);
            gender = "FEMALE";

            chMale.setChecked(false);
            chMale.setClickable(true);
            chMale.setFocusable(true);
        });

        btnSave.setOnClickListener(view1 -> {
            if (etEmailAddress.getText().toString().isEmpty()) {
                etEmailAddress.setError(getString(R.string.error_this_field_is_required));
                return ;
            }
            if (!CommonUtil.validateEmail(etEmailAddress.getText().toString())) {
                etEmailAddress.setError(getString(R.string.enter_valid_mail));
                return ;
            }
            if (etPhoneNumber.getText().toString().isEmpty()) {
                etPhoneNumber.setError(getString(R.string.error_this_field_is_required));
                return ;
            }
            if (!CommonUtil.validatePhoneNumber(etPhoneNumber.getText().toString())) {
                etPhoneNumber.setError(getString(R.string.enter_valid_phone_number));
                return ;
            }
            userData.getUser().setEmail(etEmailAddress.getText().toString());
            userData.getUser().setMobileNumber(etPhoneNumber.getText().toString());
            userData.getUser().setGender(gender);

            UserDataForUpdateBody userDataForUpdateBody = new UserDataForUpdateBody();
            userDataForUpdateBody.setGender(gender);
            userDataForUpdateBody.setId(userData.getId());

            userDataPresenter.updateDoctorData(userDataForUpdateBody);
        });
        btnCancel.setOnClickListener(view1 -> dismiss());

    }

    @Override
    public void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse) {
        // No need here
    }

    @Override
    public void visitAcceptedSuccessfully() {
        //no need here
    }

    @Override
    public void visitRejectedSuccessfully() {
        //no need here
    }

    @Override
    public void visitCancelledSuccessfully() {
        //no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void doctorDataUpdatedSuccessfully(CommonResponse commonResponse) {
        if(dataBackListener != null){
            dataBackListener.dataSavedSuccessfully(userData);
        }
        dismiss();
        Toast.makeText(getActivity(), R.string.data_update_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void doctorStatuesSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        // no need here
    }

    @Override
    public void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse) {
        //no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        setCancelable(false);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);
        setCancelable(true);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    public void setDataBackListener(DataBackListener dataBackListener) {
        this.dataBackListener = dataBackListener;
    }
}
