package dr.doctorathome.presentation.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devlomi.record_view.OnRecordListener;
import com.devlomi.record_view.RecordButton;
import com.devlomi.record_view.RecordView;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.github.piasy.rxandroidaudio.AudioRecorder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ChatModel;
import dr.doctorathome.network.model.JoinSessionModel;
import dr.doctorathome.network.model.JoinSessionResponseModel;
import dr.doctorathome.network.model.MessagesFromAPIModels.ChatAPIModel;
import dr.doctorathome.network.model.SendDiagnosModel;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.network.model.SessionDetailsModels.OnlineConsultationDetailsModel;
import dr.doctorathome.network.model.UploadFileChatModel;
import dr.doctorathome.network.model.endSessionModel;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.adapters.newInstanceChatAdapter;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.utils.CommonUtil;
import dr.doctorathome.utils.EndlessRecyclerViewScrollListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ConsultationChatActivity extends BasicActivity {

    CountDownTimer countDownTimer = null;

    Boolean isRecording = false;
    private Integer id = -1;
    long milisec;
    //doctorID=doctor_user_ID
    private Integer patient_user_id, patientId, doctorId = -1;
    private String name, token, PlayerId = "";
    private String patient_avatar;
    private String doctor_avatar;
    String dataUrl;
    ConsultationChatActivity instance = this;
    ChatModel chatModel;
    File mAudioFile;
    AudioRecorder mAudioRecorder;
    int min = 0, sec = 0;
    Date date = Calendar.getInstance().getTime();
    //firebase Database
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseSessionStatus;
    // ...
    newInstanceChatAdapter chatAdapter;
    ArrayList<ChatModel> chatModels;


    @BindView(R.id.rvChatMessages)
    RecyclerView rvChatMessages;


    @BindView(R.id.imagePick)
    ImageButton imagePick;

    @BindView(R.id.sendButton)
    ImageButton sendButton;

    @BindView(R.id.mes_Edit_Text)
    EditText mes_Edit_Text;

    @BindView(R.id.chat_buttons_sections)
    CardView chat_buttons_sections;

    @BindView(R.id.count_down)
    TextView count_down;

    @BindView(R.id.end_now)
    TextView end_now;

    @BindView(R.id.callSection)
    LinearLayout callSection;
    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;
    EndlessRecyclerViewScrollListener scrollListener;

    private ProgressDialog progressDialog;


    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};
    int currentPage = 0, totalPages = 0;

    //permission grante
    // Requesting permission to RECORD_AUDIO
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) finish();

    }

    //
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation_chat);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        end_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (milisec == 0) {
                    Dialog dialog = new Dialog(ConsultationChatActivity.this);
                    dialog.setContentView(R.layout.visit_diagnosis_dialog_layout);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Button submitButton = dialog.findViewById(R.id.submitButton);
                    EditText diagnosisET = dialog.findViewById(R.id.diagnosisEditText);
                    submitButton.setOnClickListener(view1 -> {
                        if (diagnosisET.getText().toString().trim().equals("") ||
                                diagnosisET.getText().toString().trim().length() < 3) {
                            Toast toast = Toast.makeText(ConsultationChatActivity.this, R.string.write_diagnosis_first
                                    , Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);

                            toast.show();
                            return;
                        } else {
                            endSession(new endSessionModel(0, id, diagnosisET.getText().toString()));
                        }
                        dialog.dismiss();
                    });
                    dialog.show();
                } else {
                    endSession(new endSessionModel(0, id, ""));

                }
            }
        });

        //extra data
        if (getIntent().getIntExtra("session_id", -1) != -1) {
            id = getIntent().getIntExtra("session_id", -1);
            getSessionDetails(id);
            Log.d("sessionId", "" + id);
        }
        if (getIntent().getIntExtra("patient_id", -1) != -1) {
            patientId = getIntent().getIntExtra("patient_id", -1);
            patient_user_id = getIntent().getIntExtra("patient_user_id", -1);
        }
        if (getIntent().getStringExtra("patient_avatar") != null) {
            patient_avatar = getIntent().getStringExtra("patient_avatar");
        }
        if (getIntent().getStringExtra("name") != null) {
            name = getIntent().getStringExtra("name");
            ((TextView) findViewById(R.id.tvToolbarTitle)).setText(name);
        }
        doctorId = CommonUtil.commonSharedPref().getInt("userId", -1);
        token = CommonUtil.commonSharedPref().getString("token", "");


        doctor_avatar = CommonUtil.commonSharedPref().getString("profilePicUrl", "") + "";

        //chat Adapter
        chatModels = new ArrayList<>();
        chatAdapter = new newInstanceChatAdapter(instance, chatModels, instance, new newInstanceChatAdapter.OnItemClick() {
            @Override
            public void setOnItemClick(int position) {

                if (chatModels.get(position).getMediaType().equals("IMAGE")) {

                    //startActivity Image
                    startActivity(new Intent(instance, ImageZoomingActivity.class)
                            .putExtra("Image", chatModels.get(position).getContent())
                    );

                }

            }
        },
                "" + doctorId
                , patient_avatar + ""
                , "" + doctor_avatar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvChatMessages.setLayoutManager(linearLayoutManager);
        rvChatMessages.setAdapter(chatAdapter);


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if (currentPage < (totalPages)) {
                    page++;
                    currentPage++;
                    getMessages(currentPage, id);
                    Timber.i("currentPage : " + " " + Integer.toString(page));
                }
            }
        };
        //
        rvChatMessages.addOnScrollListener(scrollListener);

        imagePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.create(instance)
                        .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                        .theme(R.style.imagePickerTheme)
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(getResources().getColor(R.color.ef_white)) // Toolbar 'up' arrow color
                        .includeVideo(true) // Show video on image picker
                        .single() // single mode
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .enableLog(false) // disabling log
                        .start(); // start image picker activity with request code
            }
        });


        //recording Audio
        RecordView recordView = (RecordView) findViewById(R.id.record_view);
        RecordButton recordButton = (RecordButton) findViewById(R.id.record_button);
        recordButton.setRecordView(recordView);

        recordView.setSmallMicColor(Color.parseColor("#c2185b"));

        recordView.setSlideToCancelText("" + getString(R.string.swipe_to_cancel));

        //disable Sounds
        recordView.setSoundEnabled(false);

        //prevent recording under one Second (it's false by default)
        recordView.setLessThanSecondAllowed(false);


        //set Custom sounds onRecord
        //you can pass 0 if you don't want to play sound in certain state
        recordView.setCustomSounds(R.raw.record_start, R.raw.record_finished, 0);

        //change slide To Cancel Text Color
        recordView.setSlideToCancelTextColor((getResources().getColor(R.color.colorPrimary)));
        //change slide To Cancel Arrow Color
        recordView.setSlideToCancelArrowColor(((getResources().getColor(R.color.colorPrimary))));
        //change Counter Time (Chronometer) color
        recordView.setCounterTimeColor(Color.parseColor("#ff0000"));

        recordView.setCancelBounds(3);//dp


        // record operations


        recordView.setOnRecordListener(new OnRecordListener() {
            @Override
            public void onStart() {
                //Start Recording..
                isRecording = true;
                if (ContextCompat.checkSelfPermission(instance,
                        Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(instance,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(instance,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
                ) {
                    setOfPermissions();
                    //  ActivityCompat.requestPermissions(instance, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
                } else {


                    mes_Edit_Text.setVisibility(View.GONE);
                    recordView.setVisibility(View.VISIBLE);

                    mAudioFile = new File(
                            Environment.getExternalStorageDirectory().getAbsolutePath() +
                                    File.separator + System.nanoTime() + ".file.m4a");
                    mAudioRecorder = AudioRecorder.getInstance();
                    mAudioRecorder.prepareRecord(
                            MediaRecorder.AudioSource.MIC,
                            MediaRecorder.OutputFormat.MPEG_4
                            , MediaRecorder.AudioEncoder.AAC,
                            mAudioFile);

                    countDownTimer.start();

                    mAudioRecorder.startRecord();


                }
            }

            @Override
            public void onCancel() {
                //On Swipe To Cancel
                isRecording = false;
                countDownTimer.cancel();

                mes_Edit_Text.setVisibility(View.VISIBLE);
                recordView.setVisibility(View.GONE);


            }

            @Override
            public void onFinish(long recordTime) {
                //Stop Recording..
                isRecording = false;
                countDownTimer.cancel();

                mes_Edit_Text.setVisibility(View.VISIBLE);
                recordView.setVisibility(View.GONE);
                try {
                    mAudioRecorder.stopRecord();
                    mAudioRecorder = null;

                } catch (Exception x) {
                    return;
                }
                String time = getHumanTimeText(recordTime);
                Log.d("RecordView", "onFinish");

                RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), mAudioFile);
                MultipartBody.Part body = MultipartBody.Part.createFormData("file", mAudioFile.getName(), reqFile);


                try {
                    Log.d("TTTTTTT", "onFinish: " + body.body().contentLength());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("TTTTTTT", "onFinish: " + body.body().contentType().toString());
                uploadPhotoService(body, id, doctorId);

            }

            @Override
            public void onLessThanSecond() {
                isRecording = false;
                countDownTimer.cancel();

                //When the record time is less than One Second
                mes_Edit_Text.setVisibility(View.VISIBLE);
                recordView.setVisibility(View.GONE);
            }

        });
        //end of recording
        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());

        findViewById(R.id.layoutDetails).setOnClickListener(view -> {
            Intent intent = new Intent(ConsultationChatActivity.this, OnlineConsultationDetailsActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        });

        //send Message


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mes_Edit_Text.getText().toString().trim().isEmpty()) {
                    date = Calendar.getInstance().getTime();
                    Log.d("TTTTTTTT", "onClick: " +
                            android.text.format.DateFormat.format("yyyy-MM-dd'T'HH:mm:ss", new java.util.Date()));
                    sendNotification(PlayerId, mes_Edit_Text.getText().toString());

                    chatModel = new ChatModel("" + mes_Edit_Text.getText().toString()
                            , "txt"
                            , "UNKNOWN"
                            , "TEXT"
                            , Integer.parseInt(patient_user_id + "")
                            , Integer.parseInt(doctorId + "")
                            , "" + android.text.format.DateFormat.format("yyyy-MM-dd'T'HH:mm:ss", new java.util.Date())
                    );
                    chatModels.add(chatModel);
                    mDatabase.push().setValue(chatModel);
                    mes_Edit_Text.setText("");
                    chatAdapter.notifyDataSetChanged();
                }
            }
        });

        //firebase

        mDatabase = FirebaseDatabase.getInstance().getReference().child("messages").child(id + "");
        mDatabaseSessionStatus = FirebaseDatabase.getInstance().getReference().child("sessionsStatus").child(id + "");


        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (isRecording) {
                    Log.d("TTTTTTTTTTT", "onFinish:finished ");

                    //recordButton.setPressed(true);
                    MotionEvent event = MotionEvent.obtain(1, 1, MotionEvent.ACTION_UP, 1, 1, 1);
//                    MotionEvent event2 = MotionEvent.obtain(1, 1, MotionEvent.ACTION_DOWN,1,1,1);
//                    MotionEvent event3 = MotionEvent.obtain(1, 1, MotionEvent.ACTION_UP,1,1,1);
                    recordButton.dispatchTouchEvent(event);
                    MotionEvent event2 = MotionEvent.obtain(1, 1, MotionEvent.ACTION_DOWN, 1, 1, 1);
                    recordButton.dispatchTouchEvent(event2);
                    //recordButton.onTouchEvent(MotionEvent.ACTION_CANCEL);
                    //recordButton.onTouchEvent(MotionEvent.ACTION_CANCEL);
                }
            }

        };

    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // or get a single image only
            Image image = ImagePicker.getFirstImageOrNull(data);
            File file = new File(image.getPath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
            uploadPhotoService(body, id, doctorId);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getHumanTimeText(long milliseconds) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }

    //function Upload Image
    public void uploadPhotoService(MultipartBody.Part body, int sessionId, int SenderId) {
        //CommonUtil.showProgressDialog(progressDialog, "" + getString(R.string.uploading));

        progressLayout.setVisibility(View.VISIBLE);
        Toast toast = Toast.makeText(instance, "" + getString(R.string.uploading), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);


        doctorService
                .Upload_Img(body, sessionId, SenderId, token).enqueue(new Callback<UploadFileChatModel>() {
            @Override
            public void onResponse(Call<UploadFileChatModel> call, Response<UploadFileChatModel> response) {
                progressLayout.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    // CommonUtil.dismissProgressDialog(progressDialog);
                    dataUrl = response.body().getRspBody();
                    Log.d("TTTT", "onResponse: " + call.request().toString());
                    Log.d("TTTTT", "onResponse: " + response.body().getRspStatus());
                    Log.d("TTTTT", "onResponse: " + response.body().getRspBody());
                } else {
                    Log.d("TTTTT", "onResponse: " + call.request().toString());
                    Log.d("TTTTT", "onResponse: " + response.code());


                }
            }

            @Override
            public void onFailure(Call<UploadFileChatModel> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                //  CommonUtil.dismissProgressDialog(progressDialog);
                Toast toast = Toast.makeText(instance, "" + getString(R.string.Please_Check_your_internet_Connection), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                Log.d("TTTTTTT", "onFailure: " + t.toString());
                Log.d("TTTTTTT", "onFailure: " + t.getMessage());
                Log.d("TTTTTTT", "onFailure: " + t.getStackTrace());
                toast.show();


            }
        });
    }

    private void addSessionDiagnose(SendDiagnosModel sendDiagnosModel) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.addSessionDiagnose(sendDiagnosModel, CommonUtil.commonSharedPref().getString("token", ""))
                .enqueue(new Callback<ServerCommonResponse>() {
                    @Override
                    public void onResponse(Call<ServerCommonResponse> call, Response<ServerCommonResponse> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(ConsultationChatActivity.this, "" + response.body().getRspBody(), Toast.LENGTH_SHORT).show();
                            // getSessionDetails(id);

                        }
                    }

                    @Override
                    public void onFailure(Call<ServerCommonResponse> call, Throwable t) {

                    }
                });
    }

    private void showDiagnoseDialoge(Boolean diagnose) {
        if (diagnose == false) {
            Dialog dialog = new Dialog(ConsultationChatActivity.this);
            dialog.setContentView(R.layout.visit_diagnosis_dialog_layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            Button submitButton = dialog.findViewById(R.id.submitButton);
            EditText diagnosisET = dialog.findViewById(R.id.diagnosisEditText);
            submitButton.setOnClickListener(view1 -> {
                if (diagnosisET.getText().toString().trim().equals("") ||
                        diagnosisET.getText().toString().trim().length() < 3) {
                    Toast.makeText(ConsultationChatActivity.this, R.string.write_diagnosis_first
                            , Toast.LENGTH_LONG).show();
                    return;
                }
                dialog.dismiss();
                addSessionDiagnose(new SendDiagnosModel(diagnosisET.getText().toString(), id));
            });
            if (!((Activity) ConsultationChatActivity.this).isFinishing()) {
                dialog.show();
            }
        }
    }

    private void getSessionDetails(int session) {
        progressLayout.setVisibility(View.VISIBLE);
        rvChatMessages.setVisibility(View.GONE);
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.getSessionDetails(session, "" + CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<OnlineConsultationDetailsModel>() {
            @Override
            public void onResponse(Call<OnlineConsultationDetailsModel> call, Response<OnlineConsultationDetailsModel> response) {
                if (response.isSuccessful()) {

                    if (response.body().getRequestStatus().getName().equals("COMPLETED")) {
                        chat_buttons_sections.setVisibility(View.GONE);
                        //will be delete after complete process
                        getMessages(currentPage, session);
                        if (response.body().getDetails().getDiagnosise() == null)
                            showPrescriptionAlert();
                        //showDiagnoseDialoge(false);
                    } else {
                        callSection.setVisibility(View.VISIBLE);
                        PlayerId = response.body().getPatientPlayerId();
                        Log.d("TTTTT", "onResponse: " + PlayerId);

                        //joinSession(session, "");
                        Log.d("JOINSESSION", "isJoinBefore: " + response.body().getJoinBefore());
                        if (!response.body().getJoinBefore()) {
                            joinSession(session, "");
                            Log.d("JOINSESSION", "JOINSESSION: ");
                        }

                        //Long T=response.body().getConsultaionDate();
                        new CountDownTimer(response.body().getDetails().getSessionSettings().getRemainingTime() * 1000, 1000) {

                            public void onTick(long millisUntilFinished) {

                                milisec = millisUntilFinished;
                                count_down.setText("" + getString(R.string.Session_end_in)
                                        + " " + new SimpleDateFormat("mm:ss").format(millisUntilFinished));
                                //here you can have your logic to set text to edittext
                            }

                            public void onFinish() {

                            }

                        }.start();
                        mDatabase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                progressLayout.setVisibility(View.GONE);
                                rvChatMessages.setVisibility(View.VISIBLE);
                                chatModels.clear();
                                if (dataSnapshot.exists()) {
                                    for (final DataSnapshot dataSnap : dataSnapshot.getChildren()) {
                                        chatModels.add(dataSnap.getValue(ChatModel.class));

                                    }
                                    chatAdapter.notifyDataSetChanged();
                                    if (chatModels.size() > 3) {
                                        rvChatMessages.scrollToPosition(chatModels.size() - 1);
                                    }

                                }


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        mDatabaseSessionStatus.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                int intVal = Integer.valueOf(String.valueOf(dataSnapshot.child("statusCode").getValue()));

                                if (intVal == 4) {

                                    Toast toast = Toast.makeText(getApplicationContext(), "" + getString(R.string.session_ended), Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                    showPrescriptionAlert();
                                    chat_buttons_sections.setVisibility(View.GONE);
                                    //will be delete after complete process
                                    callSection.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }


                }

            }

            @Override
            public void onFailure(Call<OnlineConsultationDetailsModel> call, Throwable t) {

            }
        });
    }

    private void getMessages(int pageNum, int sessionId) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.getMessages(pageNum, sessionId, token).enqueue(new Callback<ChatAPIModel>() {
            @Override
            public void onResponse(Call<ChatAPIModel> call, Response<ChatAPIModel> response) {
                progressLayout.setVisibility(View.GONE);
                rvChatMessages.setVisibility(View.VISIBLE);

                if (response.isSuccessful()) {
                    Log.d("TTTTTTTTTTTT", "onResponse: " + response.body().getTotalPages());
                    Log.d("TTTTTTTTTTTT", "onResponse: " + response.raw().request().url().toString());
                    if (response.body().getMessages() != null) {
                        chatAdapter.isOld(true);
                        for (int j = 0; j < response.body().getMessages().size(); j++) {
                            chatModels.add(new ChatModel("" + response.body().getMessages().get(j).getContent()
                                            , ""
                                            , "" + response.body().getMessages().get(j).getMediaType()
                                            , "" + response.body().getMessages().get(j).getMsgType()
                                            , response.body().getMessages().get(j).getRecieverId()
                                            , response.body().getMessages().get(j).getSenderId()
                                            , "" + response.body().getMessages().get(j).getSentAt()
                                    )

                            );
                        }
                        if (response.body().getTotalPages() > 1) {
                            totalPages = response.body().getTotalPages();

                        }
                    }
                } else {

                }
                Log.d("TTTTTTTTTTTT", "onResponse not : " + response.code());
                Log.d("TTTTTTTTTTTT", "onResponsen ot: " + response.message() + sessionId + "--" + token);
                Log.d("TTTTTTTTTTTT", "onResponsen ot: " + response.raw().request().url());
                chatAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<ChatAPIModel> call, Throwable t) {
                Log.d("TTTTTTTTTTTT", "onFailure: " + t.getMessage());
            }
        });
    }

    private void endSession(endSessionModel startEndSessionBody) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.endSession(startEndSessionBody, token).enqueue(new Callback<ServerCommonResponse>() {
            @Override
            public void onResponse(Call<ServerCommonResponse> call, Response<ServerCommonResponse> response) {
                Log.d("TTTTTTTTTT", "onResponse:success " + response.code());

                if (response.isSuccessful()) {
                    Toast toast = Toast.makeText(instance, "" + response.body().getRspBody(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    toast.show();
                    Log.d("TTTTTTTTTT", "onResponse:success ");
                    Log.d("TTTTTTTTTT", "onResponse:success " + response.code());
                    Log.d("TTTTTTTTTT", "onResponse:success " + response.body().getRspStatues());
                    if (response.body().getRspStatues().equals("Success"))
                        finish();


                }
            }

            @Override
            public void onFailure(Call<ServerCommonResponse> call, Throwable t) {
                Log.d("TTTTTTTTTT", "onfail:success " + t.getMessage());

            }
        });
    }

    private void joinSession(int sessionId, String connectionId) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.joinSession(new JoinSessionModel(connectionId, sessionId), "" + token).enqueue(new Callback<JoinSessionResponseModel>() {
            @Override
            public void onResponse(Call<JoinSessionResponseModel> call, Response<JoinSessionResponseModel> response) {
                if (response.isSuccessful()) {
                    PlayerId = response.body().getPatientplayerid();
                    Log.d("TTTTT", "onResponse: " + PlayerId);
                }
            }

            @Override
            public void onFailure(Call<JoinSessionResponseModel> call, Throwable t) {

            }
        });
    }

    private void sendNotification(String PlayerId, String message) {

        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        //String userId = "afbc1489-5053-4815-8486-5ca62b99edb2";
        String userId = PlayerId;
        boolean isSubscribed = status.getSubscriptionStatus().getSubscribed();

        //textView.setText("Subscription Status, is subscribed:" + isSubscribed);


        if (!isSubscribed)
            return;

        try {


            JSONObject notificationContent = new JSONObject(
                    "{'contents': {'en': " + message + "'}," +
                            "'app_id': '78cbd234-360a-43da-b8ee-750a349df9aa'," +
                            "'include_player_ids': ['" + userId + "'], " +
                            "'headings': {'en': 'You have a new message'}, " +
                            "'big_picture': ''}");
            OneSignal.postNotification(notificationContent, new OneSignal.PostNotificationResponseHandler() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d("sendNotification", "onSuccess: sendNotification" + response.toString());
                }

                @Override
                public void onFailure(JSONObject response) {
                    Log.d("sendNotification", "onFail: sendNotification" + userId);
                    Log.d("sendNotification", "onFail: sendNotification" + response.toString());


                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("sendNotification", "Exception: sendNotification" + e.toString());
            Log.d("sendNotification", "Exception: sendNotification" + e.getMessage());
            Log.d("sendNotification", "Exception: sendNotification" + e.getLocalizedMessage());

        }
    }

    private void setOfPermissions() {


        String[] permissions = {
                Manifest.permission.RECORD_AUDIO
                , Manifest.permission.READ_EXTERNAL_STORAGE
                , Manifest.permission.WRITE_EXTERNAL_STORAGE};
        String rationale = "" + getString(R.string.Please_provide_some_permissions_to_can_provide_best_performance);
        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("")
                .setSettingsDialogTitle("" + getString(R.string.important));

        Permissions.check(this/*context*/, permissions, rationale, options, new PermissionHandler() {
            @Override
            public void onGranted() {
                // Build the camera


            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                // permission denied, block the feature.
            }
        });

    }

    @Override
    protected void onResume() {
        updateSpeaker();
        super.onResume();

    }

    private void updateSpeaker() {
        AudioManager audioManager;
        audioManager = (AudioManager) getSystemService(this.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(true);
    }


    private void showPrescriptionAlert() {
        if (this.isFinishing())
            return;
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.dont_forget_to_add_prescription))

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ConsultationChatActivity.this, ConsultationPrescriptionActivity.class)
                                .putExtra("session_id", id));
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
            //    .setNegativeButton(null, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
