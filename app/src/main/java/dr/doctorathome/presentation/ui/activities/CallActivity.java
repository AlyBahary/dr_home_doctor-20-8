package dr.doctorathome.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.JoinSessionModel;
import dr.doctorathome.network.model.JoinSessionResponseModel;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.utils.CommonUtil;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Subscriber;
import com.opentok.android.OpentokError;

import androidx.annotation.NonNull;

import android.Manifest;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.opentok.android.OpentokError;

import android.media.AudioManager;
import android.opengl.GLSurfaceView;
import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


public class CallActivity extends AppCompatActivity implements Session.SessionListener,
        PublisherKit.PublisherListener {

    private static String API_KEY = "46548062";
    private static String SESSION_ID = "";
    private static String TOKEN = "";
    private static int id;
    private static final String LOG_TAG = CallActivity.class.getSimpleName();
    private static final int RC_SETTINGS_SCREEN_PERM = 123;
    private static final int RC_VIDEO_APP_PERM = 124;
    private int speakerFlag = 0, muteFlag = 0, test = 0;
    long seconds = 0, mins = -1;
    private String doctorToken = "";
    Timer T;

    //
    private Session mSession;
    private Publisher mPublisher;
    private Subscriber mSubscriber;

    //

    AudioManager audioManager;

    @BindView(R.id.ivUserImage)
    androidx.appcompat.widget.AppCompatImageView ivUserImage;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.counter)
    TextView counter;

    @BindView(R.id.speaker)
    ImageView speaker;

    @BindView(R.id.mute)
    ImageView mute;

    @BindView(R.id.endCall)
    ImageView endCall;

    Boolean isMuted = false;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        ButterKnife.bind(this);
        doctorToken = CommonUtil.commonSharedPref().getString("token", "");
        audioManager = (AudioManager) getApplicationContext().getSystemService(this.AUDIO_SERVICE);


        if ((audioManager.getMode() == AudioManager.MODE_IN_CALL)
                || (audioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION)) {
            audioManager.setMode(AudioManager.STREAM_VOICE_CALL);
            audioManager.setSpeakerphoneOn(true);
            audioManager.setSpeakerphoneOn(false);
        }
        SESSION_ID = getIntent().getStringExtra("sessionID");
        TOKEN = getIntent().getStringExtra("doctorID");
        id = getIntent().getIntExtra("id", -1);
        Glide.with(CallActivity.this).load(RestClient.REST_API_URL + getIntent().getStringExtra("avatar_url"))
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.profile_placeholder_rounded).into(ivUserImage);

        name.setText(getIntent().getStringExtra("patient_Name"));


        requestPermissions();


        speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (speakerFlag == 0) {
                    speaker.setColorFilter(ContextCompat.getColor(CallActivity.this
                            , R.color.colorPrimaryTransparent), android.graphics.PorterDuff.Mode.MULTIPLY);
                    audioManager.setSpeakerphoneOn(true);

                    speakerFlag = 1;
                } else {
                    speaker.setColorFilter(null);
                    speakerFlag = 0;
                    audioManager.setSpeakerphoneOn(false);

                }

            }
        });
        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Mute mic
                if (!isMuted) {

                    if ((audioManager.getMode() == AudioManager.MODE_IN_CALL) || (audioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION)) {
                        audioManager.setMicrophoneMute(true);
                    }
                    mute.setColorFilter(ContextCompat.getColor(CallActivity.this
                            , R.color.colorPrimaryTransparent), android.graphics.PorterDuff.Mode.MULTIPLY);

                    isMuted = true;

                } else {

                    if ((audioManager.getMode() == AudioManager.MODE_IN_CALL) || (audioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION)) {
                        audioManager.setMicrophoneMute(false);
                    }
                    mute.setColorFilter(null);
                    isMuted = false;
                }
            }
        });
        endCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (T != null) {
                        T.cancel();
                    }

                    TOKEN = "empty";

                    if (mSession == null) {
                        return;
                    }

                    if (mSubscriber != null) {
                        mSession.unsubscribe(mSubscriber);
                        mSubscriber.destroy();
                        mSubscriber = null;
                    }

                    if (mPublisher != null) {
                        mSession.unpublish(mPublisher);
                        mPublisher.destroy();
                        mPublisher = null;
                    }

                    Toast.makeText(CallActivity.this, "" + getString(R.string.Call_had_been_ended), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {

                }

            }
        });


    }

    @AfterPermissionGranted(RC_VIDEO_APP_PERM)
    private void requestPermissions() {
        String[] perms = {Manifest.permission.INTERNET, Manifest.permission.RECORD_AUDIO};
        if (EasyPermissions.hasPermissions(this, perms)) {
            acceptCall();

        } else {
            EasyPermissions.requestPermissions(this, "This app needs access to your  mic to make Voice calls", RC_VIDEO_APP_PERM, perms);
        }
    }


    @Override
    public void onConnected(Session session) {
        Log.i(LOG_TAG, "Session Connected");
        mPublisher = new Publisher.Builder(this).build();
        mPublisher.setPublisherListener(this);
        //session.getConnection().getConnectionId();
        if (mPublisher.getView() instanceof GLSurfaceView) {
            ((GLSurfaceView) mPublisher.getView()).setZOrderOnTop(true);
        }
        mSession.publish(mPublisher);
        speaker.performClick();
        speaker.performClick();
        mute.performClick();
        mute.performClick();
        joinSession(id, session.getConnection().getConnectionId());

    }

    @Override
    public void onDisconnected(Session session) {

        mSession = null;
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Received");
        Log.i(LOG_TAG, "Stream Received" + stream.getName());

//        if (stream.getName().equals("")) {
//            try {
//                try {
//                    if (mSubscriber != null) {
//                        mSession.unsubscribe(mSubscriber);
//                        mSubscriber.destroy();
//                        mSubscriber = null;
//                    }
//
//                    if (mPublisher != null) {
//                        mSession.unpublish(mPublisher);
//                        mPublisher.destroy();
//                        mPublisher = null;
//                    }
//                    mSession.disconnect();
//                } catch (Exception e) {
//
//                }
//                mSession.disconnect();
//                Toast.makeText(this, "" + getString(R.string.Call_had_been_ended), Toast.LENGTH_SHORT).show();
//                finish();
//
//            } catch (Exception e) {
//
//            }
//        }

        if (mSubscriber == null) {
            mSubscriber = new Subscriber.Builder(this, stream).build();
            mSession.subscribe(mSubscriber);
        }
        speaker.performClick();
        speaker.performClick();
        mute.performClick();
        mute.performClick();

    }

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        speaker.performClick();
        speaker.performClick();
        mute.performClick();
        mute.performClick();

        Log.i(LOG_TAG, "onStreamCreated " + publisherKit.getName());
        Log.d(LOG_TAG, "onStreamCreated: " + stream.getSession().getConnection().getConnectionId());
        Log.d(LOG_TAG, "onStreamCreated: " + publisherKit.getSession().getCapabilities().canSubscribe);

        startClock();

    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
        Log.d(LOG_TAG, "onStreamDestroyed: ");


    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
        Log.d(LOG_TAG, "onError:publisherKit ");

    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        Log.d("CallActivity", "onStreamDropped:1 ");

        TOKEN = "empty";

        if (mSession == null) {
            Log.d("CallActivity", "onStreamDropped:2 ");
            return;
        }

        if (mSubscriber != null) {
            mSession.unsubscribe(mSubscriber);
            mSubscriber.destroy();
            mSubscriber = null;
            Log.d("CallActivity", "onStreamDropped:3 ");
        }
        if (mPublisher != null) {
            mSession.unpublish(mPublisher);
            mPublisher.destroy();
            mPublisher = null;
            Log.d("CallActivity", "onStreamDropped:3 ");
        }

            Toast.makeText(CallActivity.this, "" + getString(R.string.Call_had_been_ended), Toast.LENGTH_SHORT).show();
            finish();


        }

        @Override
        public void onError (Session session, OpentokError opentokError){
            Log.d(LOG_TAG, "onError: " + opentokError.getMessage());
            Log.d(LOG_TAG, "onError: " + opentokError.getErrorCode());

        }

        private void acceptCall () {
            // initialize and connect to the session
            mSession = new Session.Builder(this, API_KEY, SESSION_ID).build();
            mSession.setSessionListener(this);
            mSession.connect(TOKEN);
            mSession.getConnection().getConnectionId();
            Log.d("TTTTTTTTTTTTTTTTT", "acceptCall:2 " + mSession.getConnection().getConnectionId());
        }

        private void startClock () {
            T = new Timer();
            T.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (seconds % 60 != 0) {

                            } else {
                                mins++;
                                seconds = 0;
                            }
                            String curTime = String.format("%02d:%02d", mins, seconds);
                            counter.setText("" + curTime);
                            seconds++;
                        }
                    });
                }
            }, 1000, 1000);

        }

        @Override
        protected void onDestroy () {
            super.onDestroy();


        }

        private void joinSession ( int sessionId, String connectionId){
            DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

            doctorService.joinSession(new JoinSessionModel(connectionId, sessionId), "" + doctorToken).enqueue(new Callback<JoinSessionResponseModel>() {
                @Override
                public void onResponse(Call<JoinSessionResponseModel> call, Response<JoinSessionResponseModel> response) {

                }

                @Override
                public void onFailure(Call<JoinSessionResponseModel> call, Throwable t) {

                }
            });
        }

        @Override
        protected void onPause () {


            super.onPause();

        }

        @Override
        public void onBackPressed () {

        }
    }
