package dr.doctorathome.presentation.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Calendar;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.ChatModel;
import dr.doctorathome.network.model.DoctorNewLocationBody;
import dr.doctorathome.network.model.RatePatientBody;
import dr.doctorathome.network.model.RestVisitDetailsResponse;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.network.model.StartEndSessionBody;
import dr.doctorathome.network.model.VitalSignsModel;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.presenters.VisitPresenter;
import dr.doctorathome.presentation.presenters.impl.VisitPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class VisitDetailsActivity extends BasicActivity implements VisitPresenter.View, OnMapReadyCallback {

    private VisitPresenter visitPresenter;

    @BindView(R.id.mapLayout)
    RelativeLayout mapLayout;

    @BindView(R.id.acceptRejectButtonsLayout)
    LinearLayout acceptRejectButtonsLayout;

    @BindView(R.id.rejectButton)
    Button rejectButton;

    @BindView(R.id.acceptButton)
    Button acceptButton;

    @BindView(R.id.startSessionButton)
    Button startSessionButton;

    @BindView(R.id.startNavigationButton)
    Button startNavigationButton;

    @BindView(R.id.cancelButton)
    Button cancelButton;

    @BindView(R.id.writePrescriptionButton)
    Button writePrescriptionButton;

    @BindView(R.id.addVitalSignsButton)
    Button addVitalSignsButton;

    @BindView(R.id.endSessionLayout)
    RelativeLayout endSessionLayout;

    @BindView(R.id.sessionCountDownTime)
    TextView sessionCountDownTime;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.visitDetailsMainScrollView)
    ScrollView visitDetailsMainScrollView;

    @BindView(R.id.tvPatientName)
    AppCompatTextView tvPatientName;

    @BindView(R.id.tvPatientLocation)
    AppCompatTextView tvPatientLocation;

    @BindView(R.id.tvTime)
    AppCompatTextView tvTime;
    @BindView(R.id.tvNumber)
    AppCompatTextView tvNumber;

    @BindView(R.id.ivPatientImage)
    AppCompatImageView ivPatientImage;
    @BindView(R.id.imageCall)
    AppCompatImageView imageCall;

    @BindView(R.id.tvStatuesText)
    TextView tvStatuesText;

    @BindView(R.id.tvSignificantPastDisease)
    TextView tvSignificantPastDisease;

    @BindView(R.id.tvSurgeryAndComplications)
    TextView tvSurgeryAndComplications;

    @BindView(R.id.tvDrugHistory)
    TextView tvDrugHistory;

    @BindView(R.id.tvFamilyHistory)
    TextView tvFamilyHistory;

    @BindView(R.id.tvComplain)
    TextView tvComplain;

    @BindView(R.id.diagnosisTV)
    TextView diagnosisTV;

    @BindView(R.id.openLocationLayout)
    RelativeLayout openLocationLayout;

    @BindView(R.id.cbDiabetes)
    CheckBox cbDiabetes;

    @BindView(R.id.cbBloodPressure)
    CheckBox cbBloodPressure;

    @BindView(R.id.cbHeartDisease)
    CheckBox cbHeartDisease;

    @BindView(R.id.cbRheumatism)
    CheckBox cbRheumatism;

    @BindView(R.id.cbCholesterol)
    CheckBox cbCholesterol;

    @BindView(R.id.goToFollowUP)
    Button goToFollowUP;

    private SupportMapFragment mapFragment;
    private GoogleMap gmap;
    private CountDownTimer countDownTimer;
    private GoogleApiClient mGoogleApiClient;

    private ProgressDialog progressDialog;

    private boolean isChangingVisitStatues = false;

    private Integer visitId = -1;
    long timeToStartWith = -1;
    private boolean anySuccessfullChangeHappened;

    //make response global
    RestVisitDetailsResponse restVisitDetailsResponse;
    //

    Dialog sessionEndedDialoge;

    Marker marker;

    private LatLng mine;

    private boolean arrived, diagnoseIsAdded = false;

    private FusedLocationProviderClient fusedLocationClient;
    private String visitStatus, Navigate = "NO";

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_details);
        ButterKnife.bind(this);

        mGoogleApiClient = new GoogleApiClient.Builder(VisitDetailsActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .build();

        progressDialog = new ProgressDialog(this);

        visitPresenter = new VisitPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

        anySuccessfullChangeHappened = false;

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (getIntent().getIntExtra("visitId", -1) != -1) {
            visitId = getIntent().getIntExtra("visitId", -1);
            //visitPresenter.getVisitDetails(visitId);
        }

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.visit_details);

        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view));
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        } else {
            mapLayout.setVisibility(View.GONE);
        }

        acceptButton.setOnClickListener(view -> {
                    isChangingVisitStatues = true;
                    visitPresenter.acceptVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.CONFIRMED));
                }
        );

        rejectButton.setOnClickListener(view -> {
            isChangingVisitStatues = true;
            visitPresenter.rejectVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.REJECTED));
        });

        cancelButton.setOnClickListener(view -> {
            isChangingVisitStatues = true;
            visitPresenter.cancelVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.CANCELLED));
        });

        startSessionButton.setOnClickListener(view -> {
            isChangingVisitStatues = true;
            visitPresenter.startSession(new StartEndSessionBody(visitId, CommonUtil.dateFullToServer(
                    Calendar.getInstance().getTime())));
        });

        arrived = false;
        mDatabase = FirebaseDatabase.getInstance().getReference().child("visitsStatus").child(visitId + "").child("statusCode");
        showProgress("");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (getIntent().getIntExtra("visitId", -1) != -1) {
                    hideProgress();

                    visitId = getIntent().getIntExtra("visitId", -1);
                    visitPresenter.getVisitDetails(visitId);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        endSessionLayout.setOnClickListener(view -> {
                    Dialog dialog = new Dialog(VisitDetailsActivity.this);
                    dialog.setContentView(R.layout.visit_diagnosis_dialog_layout);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Button submitButton = dialog.findViewById(R.id.submitButton);
                    EditText diagnosisET = dialog.findViewById(R.id.diagnosisEditText);
                    submitButton.setOnClickListener(view1 -> {
                        if (diagnosisET.getText().toString().trim().equals("") ||
                                diagnosisET.getText().toString().trim().length() < 3) {
                            Toast toast = Toast.makeText(VisitDetailsActivity.this, R.string.write_diagnosis_first
                                    , Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }

                        isChangingVisitStatues = true;
                        diagnoseIsAdded = true;

                        visitPresenter.endSession(new StartEndSessionBody(visitId, diagnosisET.getText().toString()
                                , CommonUtil.dateFullToServer(
                                Calendar.getInstance().getTime())));
                        dialog.dismiss();
                    });
                    dialog.show();
                }

        );
        getFollowUp();
    }

    private void startTheCountDown(long seconds) {
        sessionCountDownTime.setText("00:00");
        timeToStartWith = seconds;
        final long[] secondsLeft = {timeToStartWith};
        countDownTimer = new CountDownTimer((timeToStartWith + 1) * 1000, 1000) {
            @Override
            public void onTick(long l) {
                secondsLeft[0]--;
                long min = secondsLeft[0] / 60;
                long sec = secondsLeft[0] % 60;
                sessionCountDownTime.setText((min < 10 ? ("0" + min) : min)
                        + ":" + (sec < 10 ? ("0" + sec) : sec));
            }

            @Override
            public void onFinish() {
                sessionCountDownTime.setText("00:00");


                sessionEndedDialoge = CommonUtil.getConfirmation2BtnsDialog(VisitDetailsActivity.this, false, true,
                        getString(R.string.session_ended), false,
                        "",
                        R.drawable.ic_warning, getString(R.string.end_session_now), getString(R.string.continue_session),
                        () ->
                                endSessionLayout.performClick());

                if (!((Activity) VisitDetailsActivity.this).isFinishing()) {
                    if (visitStatus.equals(RequestStatues.RUNNING)) {
                        if (!diagnoseIsAdded)
                            sessionEndedDialoge.show();
                    }
                }

                //                endSessionLayout.setVisibility(View.GONE);
                if (diagnoseIsAdded && sessionCountDownTime.isShown()) {
                    sessionEndedDialoge.dismiss();
                }
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        if (anySuccessfullChangeHappened) {
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setMinZoomPreference(15f);
        if (mine == null) {
            mine = new LatLng(29.3363806, 48.0020064);
        }

        gmap.moveCamera(CameraUpdateFactory.newLatLng(mine));
        marker = gmap.addMarker(new MarkerOptions()
                .position(mine)
                .title("Patient Location")
                .snippet("This is patient location")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
    }

    void updateLocation() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d("TTTTTTTTTTTTT", "updateLocation:1 ");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION
                                , Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
                Log.d("TTTTTTTTTTTTT", "updateLocation:2 ");

            }
            Log.d("TTTTTTTTTTTTT", "updateLocation:3 ");

            return;
        } else {
            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();
            Log.d("TTTTTTTTTTTTT", "updateLocation:4 ");

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            visitPresenter.updateDoctorLocation(new DoctorNewLocationBody(CommonUtil.commonSharedPref()
                                    .getInt("doctorId", -1), visitId, location.getLatitude(), location.getLongitude()));
                            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + restVisitDetailsResponse.getDoctorRequest()
                                    .getPatient().getLocation().getLatitude() + "," + restVisitDetailsResponse.getDoctorRequest()
                                    .getPatient().getLocation().getLongitude());
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            startActivity(mapIntent);

                        } else {
                            Toast toast = Toast.makeText(this, "" + getString(R.string.open_gps_message), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    });

        }
    }

    void requestLocation() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION
                                , Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        } else {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            Timber.i(location.getLongitude() + " " + location.getLatitude());
                            //    visitPresenter.isArrived(new DoctorNewLocationBody(CommonUtil.commonSharedPref()
                            //          .getInt("doctorId", -1), visitId, location.getLatitude(), location.getLongitude()));
                            isArrived(new DoctorNewLocationBody(CommonUtil.commonSharedPref()
                                    .getInt("doctorId", -1), visitId, location.getLatitude(), location.getLongitude()));

                            Log.d("TTTTT", "requestLocation: " + "\n" +
                                    "doc id " + CommonUtil.commonSharedPref().getInt("doctorId", -1)
                                    + "Visit ID" + visitId
                                    + "lat" + location.getLatitude()
                                    + "long" + location.getLongitude()
                            );
                        }
                    });

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                requestLocation();
                break;
            default:
                break;
        }
    }

    @Override
    public void visitDetailsRetrievedSuccessfully(RestVisitDetailsResponse restVisitDetailsResponse) {
        this.restVisitDetailsResponse = restVisitDetailsResponse;
        if (restVisitDetailsResponse.getDoctorRequest().getDiagnosise() != null
                && !restVisitDetailsResponse.getDoctorRequest().getDiagnosise().equals("")) {
            diagnoseIsAdded = true;
        }
        if (restVisitDetailsResponse.getDoctorRequest().getNavigationStarted().equals("YES")) {
            Navigate = "YES";
            requestLocation();
        }
        if (restVisitDetailsResponse.getDoctorRequest().getWarningMessges() != null) {
            Toast toast = Toast.makeText(this, restVisitDetailsResponse.getDoctorRequest().getWarningMessges() + "", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        tvPatientName.setText(restVisitDetailsResponse.getDoctorRequest().getPatient().getUser().getFirstName()
                + " "
                + restVisitDetailsResponse.getDoctorRequest().getPatient().getUser().getLastName());

        tvPatientLocation.setText(restVisitDetailsResponse.getDoctorRequest().getPatient().getAddress());

        if (restVisitDetailsResponse.getDoctorRequest().getPatient().getLocation() != null) {
            mine = new LatLng(restVisitDetailsResponse.getDoctorRequest().getPatient().getLocation().getLatitude()
                    , restVisitDetailsResponse.getDoctorRequest().getPatient().getLocation().getLongitude());
            try {
                gmap.moveCamera(CameraUpdateFactory.newLatLng(mine));
                if (marker != null) {
                    marker.setPosition(mine);
                } else {
                    marker = gmap.addMarker(new MarkerOptions()
                            .position(mine)
                            .title("Patient Location")
                            .snippet("This is patient location")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            startNavigationButton.setOnClickListener(view -> {
                updateLocation();

//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("saddr:" + restVisitDetailsResponse.getDoctorRequest()
//                        .getPatient().getLocation().getLatitude() + "," + restVisitDetailsResponse.getDoctorRequest()
//                        .getPatient().getLocation().getLongitude() + "?q=" + restVisitDetailsResponse.getDoctorRequest()
//                        .getPatient().getLocation().getLatitude() + "," + restVisitDetailsResponse.getDoctorRequest()
//                        .getPatient().getLocation().getLongitude() + "(Patient+Location)"));
//                startActivity(intent);
//                Intent goToNavigationIntent = new Intent(VisitDetailsActivity.this, NavigationToPatientActivity.class);
//                goToNavigationIntent.putExtra("lat", restVisitDetailsResponse.getDoctorRequest()
//                        .getPatient().getLocation().getLatitude());
//                goToNavigationIntent.putExtra("lng", restVisitDetailsResponse.getDoctorRequest()
//                        .getPatient().getLocation().getLongitude());
//                goToNavigationIntent.putExtra("visitId", visitId);

                //                goToNavigationIntent.putExtra("lat", 30.044420);
                //                goToNavigationIntent.putExtra("lng", 31.235712);
                //startActivityForResult(goToNavigationIntent, 20);
            });

            openLocationLayout.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + restVisitDetailsResponse.getDoctorRequest()
                        .getPatient().getLocation().getLatitude() + "," + restVisitDetailsResponse.getDoctorRequest()
                        .getPatient().getLocation().getLongitude() + "?q=" + restVisitDetailsResponse.getDoctorRequest()
                        .getPatient().getLocation().getLatitude() + "," + restVisitDetailsResponse.getDoctorRequest()
                        .getPatient().getLocation().getLongitude() + "(Patient+Location)"));
                startActivity(intent);
            });

        }
        //        else {
        //            openLocationLayout.setOnClickListener(view -> {
        //                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:29.3363806,48.0020064" + "?q=29.3363806,48.0020064(Patient+Location)"));
        //                startActivity(intent);
        //            });
        //        }
        //        else {
        //            startNavigationButton.setOnClickListener(view -> {
        //                Intent goToNavigationIntent = new Intent(VisitDetailsActivity.this, NavigationToPatientActivity.class);
        //                goToNavigationIntent.putExtra("lat", 29.968467);
        //                goToNavigationIntent.putExtra("lng", 31.248529);
        //                startActivity(goToNavigationIntent);
        //            });
        //        }

        try {
            Glide.with(this).load(RestClient.REST_API_URL
                    + restVisitDetailsResponse.getDoctorRequest().getPatient().getProfilePicUrl().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.profile_placeholder_rounded).into(ivPatientImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvTime.setText(CommonUtil.dateDaysAndTimeFromServerWithTime(
                restVisitDetailsResponse.getDoctorRequest().getRequestdVisitDate()));
        tvNumber.setText(restVisitDetailsResponse.getDoctorRequest().getPatient().getUser().getMobileNumber());
        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + restVisitDetailsResponse.getDoctorRequest().getPatient().getUser().getMobileNumber()));
                if (ContextCompat.checkSelfPermission(VisitDetailsActivity.this,
                        Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(VisitDetailsActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            1);

                    // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                } else {
                    //You already have permission
                    try {
                        VisitDetailsActivity.this.startActivity(intent);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        if (restVisitDetailsResponse.getDoctorRequest().getVisitStatus() != null) {
            visitStatus = restVisitDetailsResponse.getDoctorRequest().getVisitStatus().getName();
            visitStatues(visitStatus);

            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            if (restVisitDetailsResponse.getDoctorRequest().getVisitStatus().getName().equals(RequestStatues.RUNNING)) {
                startTheCountDown((30 * 60) -
                        (restVisitDetailsResponse.getDoctorRequest().getSessionStartTime() != null ? CommonUtil.secondsBetweenNowAndServerDate(
                                restVisitDetailsResponse.getDoctorRequest().getSessionStartTime()) : 0));
            }
        }

        tvSignificantPastDisease.setText(restVisitDetailsResponse.getDoctorRequest()
                .getPatient().getSignificantPastDisease());

        tvSurgeryAndComplications.setText(restVisitDetailsResponse.getDoctorRequest()
                .getPatient().getSurgeryAndComplications());

        tvDrugHistory.setText(restVisitDetailsResponse.getDoctorRequest()
                .getPatient().getDrugHistory());

        tvFamilyHistory.setText(restVisitDetailsResponse.getDoctorRequest()
                .getPatient().getFamilyHistory());

        tvComplain.setText(restVisitDetailsResponse.getDoctorRequest().getComplainDescription());

        diagnosisTV.setText(restVisitDetailsResponse.getDoctorRequest().getDiagnosise());

        if (restVisitDetailsResponse.getDoctorRequest().getPatient().getDiabetes() != null &&
                restVisitDetailsResponse.getDoctorRequest().getPatient().getDiabetes().equals("YES")) {
            cbDiabetes.setChecked(true);
        } else {
            cbDiabetes.setChecked(false);
        }

        if (restVisitDetailsResponse.getDoctorRequest().getPatient().getBloodPressure() != null &&
                restVisitDetailsResponse.getDoctorRequest().getPatient().getBloodPressure().equals("YES")) {
            cbBloodPressure.setChecked(true);
        } else {
            cbBloodPressure.setChecked(false);
        }

        if (restVisitDetailsResponse.getDoctorRequest().getPatient().getHeartDisease() != null &&
                restVisitDetailsResponse.getDoctorRequest().getPatient().getHeartDisease().equals("YES")) {
            cbHeartDisease.setChecked(true);
        } else {
            cbHeartDisease.setChecked(false);
        }

        if (restVisitDetailsResponse.getDoctorRequest().getPatient().getRheumatism() != null &&
                restVisitDetailsResponse.getDoctorRequest().getPatient().getRheumatism().equals("YES")) {
            cbRheumatism.setChecked(true);
        } else {
            cbRheumatism.setChecked(false);
        }

        if (restVisitDetailsResponse.getDoctorRequest().getPatient().getCholestrol() != null &&
                restVisitDetailsResponse.getDoctorRequest().getPatient().getCholestrol().equals("YES")) {
            cbCholesterol.setChecked(true);
        } else {
            cbCholesterol.setChecked(false);
        }

        if (restVisitDetailsResponse.getDoctorRequest().getPrescriptionId() != null
                && restVisitDetailsResponse.getDoctorRequest().getPrescriptionId() != 0) {
            if (restVisitDetailsResponse.getDoctorRequest().getVisitStatus().getName().equals(
                    RequestStatues
                            .COMPLETED) || (
                    restVisitDetailsResponse.getDoctorRequest().getPrescriptionPaymenetStatus() != null
                            && restVisitDetailsResponse.getDoctorRequest().getPrescriptionPaymenetStatus().equals("PAID"))
            ) {
                writePrescriptionButton.setText(R.string.view_prescription);
            } else {
                writePrescriptionButton.setText(R.string.update_prescription);
            }
        } else {
            writePrescriptionButton.setText(R.string.write_prescription);
        }

        addVitalSignsButton.setOnClickListener(view -> {
            openAddVitalSignsDialoge(visitId);
        });
        writePrescriptionButton.setOnClickListener(view -> {
            Intent goToPrescriptionIntent = new Intent(
                    VisitDetailsActivity.this, PrescriptionActivity.class);
            goToPrescriptionIntent.putExtra("visitId", visitId);
            if (restVisitDetailsResponse.getDoctorRequest().getPrescriptionId() != null
                    && restVisitDetailsResponse.getDoctorRequest().getPrescriptionId() != 0) {
                goToPrescriptionIntent.putExtra("updateMode", true);
                if (restVisitDetailsResponse.getDoctorRequest().getVisitStatus().getName().equals(
                        RequestStatues
                                .COMPLETED)
                        &&
                        (restVisitDetailsResponse.getDoctorRequest().getPrescriptionPaymenetStatus() != null
                                && restVisitDetailsResponse.getDoctorRequest().getPrescriptionPaymenetStatus().equals("PAID"))
                ) {
                    goToPrescriptionIntent.putExtra("viewOnly", true);
                }
            } else {
                goToPrescriptionIntent.putExtra("updateMode", false);
            }
            startActivityForResult(goToPrescriptionIntent, 2);
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 2) {
            visitPresenter.getVisitDetails(visitId);
            anySuccessfullChangeHappened = true;
        } else if (resultCode == RESULT_OK && requestCode == 20) {
            arrived = true;
            visitStatues(visitStatus);
        }
    }

    private void visitStatues(String visitStatues) {
        switch (visitStatues) {
            case RequestStatues
                    .PENDING:
                tvStatuesText.setText(R.string.text_new);
                acceptRejectButtonsLayout.setVisibility(View.VISIBLE);
                startSessionButton.setVisibility(View.GONE);
                endSessionLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                startNavigationButton.setVisibility(View.GONE);
                writePrescriptionButton.setVisibility(View.GONE);
                addVitalSignsButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .CANCELLED:
                tvStatuesText.setText(R.string.text_canceled);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                startSessionButton.setVisibility(View.GONE);
                endSessionLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                startNavigationButton.setVisibility(View.GONE);
                writePrescriptionButton.setVisibility(View.GONE);
                addVitalSignsButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .PAID:
                tvStatuesText.setText(R.string.text_paid);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                if (!arrived) {
                    startSessionButton.setVisibility(View.GONE);
                } else {
                    startSessionButton.setVisibility(View.VISIBLE);
                }
                endSessionLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                if (!arrived) {
                    startNavigationButton.setVisibility(View.VISIBLE);
                } else {
                    startNavigationButton.setVisibility(View.GONE);
                }
                writePrescriptionButton.setVisibility(View.GONE);
                addVitalSignsButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .REJECTED:
                tvStatuesText.setText(R.string.text_rejected);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                startSessionButton.setVisibility(View.GONE);
                endSessionLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                startNavigationButton.setVisibility(View.GONE);
                writePrescriptionButton.setVisibility(View.GONE);
                addVitalSignsButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .RUNNING:
                tvStatuesText.setText(R.string.text_running);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                startSessionButton.setVisibility(View.GONE);
                endSessionLayout.setVisibility(View.VISIBLE);
                writePrescriptionButton.setVisibility(View.VISIBLE);
                addVitalSignsButton.setVisibility(View.VISIBLE);
                startNavigationButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .CONFIRMED:
                tvStatuesText.setText(R.string.text_confirmed);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                startSessionButton.setVisibility(View.GONE);
                endSessionLayout.setVisibility(View.GONE);
                startNavigationButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                writePrescriptionButton.setVisibility(View.GONE);
                addVitalSignsButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .COMPLETED:
                tvStatuesText.setText(R.string.text_completed);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                startSessionButton.setVisibility(View.GONE);
                endSessionLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                startNavigationButton.setVisibility(View.GONE);
                writePrescriptionButton.setVisibility(View.VISIBLE);
                addVitalSignsButton.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void visitAcceptedSuccessfully() {
        Toast toast = Toast.makeText(this, R.string.visit_accepted_success, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        visitStatues(RequestStatues
                .CONFIRMED);
        isChangingVisitStatues = false;
        anySuccessfullChangeHappened = true;
    }

    @Override
    public void visitRejectedSuccessfully() {
        Toast toast = Toast.makeText(this, R.string.visit_rejected_success, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        visitStatues(RequestStatues
                .REJECTED);
        isChangingVisitStatues = false;
        anySuccessfullChangeHappened = true;
    }

    @Override
    public void visitCanceledSuccessfully() {
        Toast toast = Toast.makeText(this, R.string.visit_cancelled_success, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        visitStatues(RequestStatues
                .CANCELLED);
        isChangingVisitStatues = false;
        anySuccessfullChangeHappened = true;
    }

    @Override
    public void sessionStartedSuccessfully() {
        Toast toast = Toast.makeText(this, R.string.session_started_success, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        visitStatues(RequestStatues
                .RUNNING);
        isChangingVisitStatues = false;
        startTheCountDown(30 * 60);
        anySuccessfullChangeHappened = true;
    }

    @Override
    public void sessionEndedSuccessfully() {
        Toast toast = Toast.makeText(this, R.string.session_ended_success, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        visitStatues(RequestStatues
                .COMPLETED);
        isChangingVisitStatues = false;
        anySuccessfullChangeHappened = true;

        Dialog dialog = new Dialog(VisitDetailsActivity.this);
        dialog.setContentView(R.layout.rate_patient_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button rateButton = dialog.findViewById(R.id.rateButton);
        EditText feedbackEditText = dialog.findViewById(R.id.feedbackEditText);
        RatingBar patientRateBar = dialog.findViewById(R.id.patientRateBar);
        rateButton.setOnClickListener(view1 -> {
            if (feedbackEditText.getText().toString().equals("")) {
                Toast toast1 = Toast.makeText(VisitDetailsActivity.this, R.string.write_feedback_first
                        , Toast.LENGTH_LONG);
                toast1.setGravity(Gravity.CENTER, 0, 0);
                toast1.show();
                return;
            }

            isChangingVisitStatues = true;
            visitPresenter.ratePatient(visitId, new RatePatientBody(patientRateBar.getRating(),
                    feedbackEditText.getText().toString()));
            dialog.dismiss();
        });
        dialog.show();
        //            countDownTimer.onFinish()
    }

    @Override
    public void ratePatientDoneSuccessfully() {
        Toast toast = Toast.makeText(this, R.string.patient_rate_success, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        isChangingVisitStatues = false;
        anySuccessfullChangeHappened = true;
    }

    @Override
    public void doctorNewLocationSavedSuccessfully(ServerCommonResponse serverCommonResponse) {
        if (serverCommonResponse.getRspBody().equals("Arrived")) {
            arrived = true;
            visitStatues(visitStatus);
        }
    }

    @Override
    public void isDoctorArrivedSuccessfully(ServerCommonResponse serverCommonResponse) {
        if (serverCommonResponse.getRspBody().equals("Arrived")) {
            arrived = true;
            visitStatues(visitStatus);
        }
    }

    @Override
    public void showProgress(String message) {
        if (!isChangingVisitStatues) {
            progressLayout.setVisibility(View.VISIBLE);
            visitDetailsMainScrollView.setVisibility(View.GONE);
        } else {
            CommonUtil.showProgressDialog(progressDialog, message);
        }
    }

    @Override
    public void hideProgress() {
        if (!isChangingVisitStatues) {
            progressLayout.setVisibility(View.GONE);
            visitDetailsMainScrollView.setVisibility(View.VISIBLE);
        } else {
            CommonUtil.dismissProgressDialog(progressDialog);
        }
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        if (isChangingVisitStatues) {
            isChangingVisitStatues = false;
        }
    }

    private void isArrived(DoctorNewLocationBody doctorNewLocationBody) {

        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.isArrived(doctorNewLocationBody
                , CommonUtil.commonSharedPref().getString("token", ""))
                .enqueue(new Callback<ServerCommonResponse>() {
                    @Override
                    public void onResponse(Call<ServerCommonResponse> call, Response<ServerCommonResponse> response) {

                        if (response.isSuccessful()) {
                            if (response.body().getRspBody().equals("Arrived")) {
                                arrived = true;
                                visitStatues(visitStatus);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ServerCommonResponse> call, Throwable t) {

                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        //visitPresenter.getVisitDetails(visitId);


    }

    private void openAddVitalSignsDialoge(int visitId) {

        Dialog dialog = new Dialog(VisitDetailsActivity.this);
        dialog.setContentView(R.layout.add_vital_signs_dialoge);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        EditText pulseEditText = dialog.findViewById(R.id.pulseEditText);
        EditText tempEditText = dialog.findViewById(R.id.tempEditText);
        EditText BPEditText = dialog.findViewById(R.id.BPEditText);
        EditText glueMatterEditText = dialog.findViewById(R.id.glueMatterEditText);
        EditText medicalHistoryEditText = dialog.findViewById(R.id.medicalHistoryEditText);

        if (restVisitDetailsResponse.getDoctorRequest().getTemperature() != null) {
            pulseEditText.setText(restVisitDetailsResponse.getDoctorRequest().getPulse() + "");
            tempEditText.setText(restVisitDetailsResponse.getDoctorRequest().getTemperature() + "");
            BPEditText.setText(restVisitDetailsResponse.getDoctorRequest().getBloodPressure() + "");
            glueMatterEditText.setText(restVisitDetailsResponse.getDoctorRequest().getGlucoseMeter() + "");
            medicalHistoryEditText.setText(restVisitDetailsResponse.getDoctorRequest().getMedicalHistory() + "");
        }

        String pressurePattern = "^\\d{1,3}\\/\\d{1,3}$";


        Button submitButton = dialog.findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TTTTTTTT", "onClick: " + String.valueOf("120/60").matches(pressurePattern));
                if (pulseEditText.getText().toString().trim().equals("") ||
                        tempEditText.getText().toString().trim().equals("") ||
                        BPEditText.getText().toString().trim().equals("") ||
                        glueMatterEditText.getText().toString().trim().equals("") ||
                        medicalHistoryEditText.getText().toString().trim().equals("")) {
                    Toast.makeText(VisitDetailsActivity.this, "" + getString(R.string.All_fields_are_required), Toast.LENGTH_SHORT).show();
                } else if (!BPEditText.getText().toString().trim().matches(pressurePattern)) {
                    Toast.makeText(VisitDetailsActivity.this, "" + getString(R.string.please_add_valid_blood_pressure), Toast.LENGTH_SHORT).show();
                } else {

                    addVitalSign(BPEditText.getText().toString()
                            , glueMatterEditText.getText().toString()
                            , medicalHistoryEditText.getText().toString()
                            , pulseEditText.getText().toString()
                            , tempEditText.getText().toString()
                            , dialog
                    );
                }
            }
        });

        dialog.show();
    }

    private void addVitalSign(String bloodPressure, String glucoseMeter, String medicalHistory, String pulse, String temp, Dialog dialog) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.addVitalSigns(new VitalSignsModel(bloodPressure, glucoseMeter, medicalHistory, pulse, temp, visitId)
                , CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<ServerCommonResponse>() {
            @Override
            public void onResponse(Call<ServerCommonResponse> call, Response<ServerCommonResponse> response) {
                Log.d("TTTTTTTTTTTTTTT", "onResponse: " + visitId);
                Log.d("TTTTTTTTTTTTTTT", "onResponse: " + response.code());
                Log.d("TTTTTTTTTTTTTTT", "onResponse: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    Toast.makeText(VisitDetailsActivity.this, "" + response.body().getRspBody(), Toast.LENGTH_SHORT).show();
                    visitPresenter.getVisitDetails(visitId);

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ServerCommonResponse> call, Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void getFollowUp() {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.getFollowUp(visitId + ""
                , CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    goToFollowUP.setVisibility(View.VISIBLE);
                    goToFollowUP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(VisitDetailsActivity.this, FollowUpActivity.class).putExtra("id",response.body().get("id").getAsInt()+""));

                        }
                    });

                } else {
                    goToFollowUP.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}
