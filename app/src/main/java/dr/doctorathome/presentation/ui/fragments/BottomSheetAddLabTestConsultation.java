package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.LabTestModel;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.adapters.EPrescriptionLabListAdapter;
import dr.doctorathome.presentation.adapters.EPrescriptionListAdapter;
import dr.doctorathome.presentation.ui.activities.MedicineOutOfSystemActivity;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottomSheetAddLabTestConsultation extends BottomSheetDialogFragment {
    private static final String LABTEST_FILTER ="LABTEST_FILTER" ;
    @BindView(R.id.addMedicine)
    Button addMedicine;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.labEt)
    EditText labEt;
    ArrayList<LabTestModel> labTestModels = new ArrayList<>();
    EPrescriptionLabListAdapter adapter;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), R.style.MyDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_add_labtest_consultation, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        if (dialog != null) {
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        View view = getView();
        view.post(() -> {
            View parent = (View) view.getParent();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();
            BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
            bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            ((View)bottomSheet.getParent()).setBackgroundColor(Color.TRANSPARENT);
        });

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addMedicine.setOnClickListener(v -> {
            startActivityForResult(new Intent(getContext(), MedicineOutOfSystemActivity.class), 1);
        });

         adapter=new EPrescriptionLabListAdapter(getContext(), new EPrescriptionLabListAdapter.OnItemClick() {
             @Override
             public void setOnItemClick(int position) {
                 Intent someIntent = new Intent(LABTEST_FILTER);
                 someIntent.putExtra("LabMode",labTestModels.get(position));
                 LocalBroadcastManager.getInstance(getContext()).sendBroadcast(someIntent);
                 dismiss();
             }
         });
        rv.setAdapter(adapter);

        //Creating the ArrayAdapter instance having the country list
        labEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (labEt.length() > 1)
                    getMedicine(labEt.getText().toString());
                else {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

        }

    }

    private void getMedicine(String name) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.getLabTest(name, CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<ArrayList<LabTestModel>>() {
            @Override
            public void onResponse(Call<ArrayList<LabTestModel>> call, Response<ArrayList<LabTestModel>> response) {
                if (response.code() == 200) {
                    if (response.body().isEmpty()) {

                        Toast.makeText(getContext(), "" + getString(R.string.Cannont_find_lab_name) + " " + name, Toast.LENGTH_SHORT).show();
                    }

                    labTestModels.clear();
                    labTestModels.addAll(response.body());
                    adapter.setLabTestModels(labTestModels);
                    adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<LabTestModel>> call, Throwable t) {

            }
        });
    }



}
