package dr.doctorathome.presentation.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.LabTestModel;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.presentation.adapters.EPrescriptionLabAdapter;
import dr.doctorathome.presentation.ui.activities.ConsultationPrescriptionActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultationLabFragment extends Fragment {
    private static final String LABTEST_FILTER ="LABTEST_FILTER" ;

    @BindView(R.id.addMedView)
    CardView addMedView;
    @BindView(R.id.noDataImage)
    ImageView noDataImage;
    @BindView(R.id.noDataTxt)
    TextView noDataTxt;
    BottomSheetAddLabTestConsultation bottomSheetAddLabTestConsultation = new BottomSheetAddLabTestConsultation();
    @BindView(R.id.rv)
    RecyclerView rv;
    EPrescriptionLabAdapter ePrescriptionLabAdapter;

    public static ConsultationLabFragment newInstace() {
        return new ConsultationLabFragment();
    }

    public ConsultationLabFragment() {
        // Required empty public constructor
    }

    public ArrayList<Integer> getLabIds(){
        ArrayList<Integer> ids=new ArrayList<>();
        for (int i = 0; i <ePrescriptionLabAdapter.getMedicineModels().size() ; i++) {
            ids.add(ePrescriptionLabAdapter.getMedicineModels().get(i).getId());
        }
        return ids;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_consultation_lab, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ePrescriptionLabAdapter = new EPrescriptionLabAdapter(getContext());
        rv.setAdapter(ePrescriptionLabAdapter);
        addMedView.setOnClickListener(v -> {
            bottomSheetAddLabTestConsultation.show(getFragmentManager(), ConsultationMedicineFragment.class.getSimpleName());
        });
        SetLabs(((ConsultationPrescriptionActivity)getActivity()).getLab());
    }


    public void SetLabs(ArrayList<LabTestModel> labTests) {
        if (labTests == null)
            return;
        noDataImage.setVisibility(View.GONE);
        noDataTxt.setVisibility(View.GONE);
        ePrescriptionLabAdapter.setMedicineModels(labTests);
        ePrescriptionLabAdapter.notifyDataSetChanged();
    }
    private BroadcastReceiver someBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LabTestModel labTestModel= (LabTestModel) intent.getSerializableExtra("LabMode");

            ePrescriptionLabAdapter.addTestModels(labTestModel);
            ePrescriptionLabAdapter.notifyDataSetChanged();
            noDataImage.setVisibility(View.GONE);
            noDataTxt.setVisibility(View.GONE);

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(someBroadcastReceiver,
                new IntentFilter(LABTEST_FILTER));
    }
    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(someBroadcastReceiver);
        super.onPause();
    }
}
