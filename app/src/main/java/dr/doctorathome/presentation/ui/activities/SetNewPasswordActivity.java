package dr.doctorathome.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SetNewPasswordActivity extends AppCompatActivity {

    @BindView(R.id.etNewPassword)
    EditText etNewPassword;

    @BindView(R.id.etRePassword)
    EditText etRePassword;

    @BindView(R.id.btnReset)
    Button btnReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_new_password);
        ButterKnife.bind(this);

        btnReset.setOnClickListener(view -> {
            if (etNewPassword.getText().toString().isEmpty()) {
                Animation shake = AnimationUtils.loadAnimation(SetNewPasswordActivity.this, R.anim.shake);
                etNewPassword.startAnimation(shake);
            } else if (etRePassword.getText().toString().isEmpty()) {
                Animation shake = AnimationUtils.loadAnimation(SetNewPasswordActivity.this, R.anim.shake);
                etRePassword.startAnimation(shake);
            } else if (!etNewPassword.getText().toString().equals(etRePassword.getText().toString())) {
                Toast.makeText(SetNewPasswordActivity.this, getString(R.string.password_not_matched),
                        Toast.LENGTH_SHORT).show();
            } else {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("newPassword", etNewPassword.getText().toString());
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}
