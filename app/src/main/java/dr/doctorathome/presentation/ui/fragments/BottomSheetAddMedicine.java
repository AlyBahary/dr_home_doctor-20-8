package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.MedicineTypes;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.MedicineBox;
import dr.doctorathome.network.model.MedicineInPrescription;
import dr.doctorathome.network.model.RestAllLabTestsResponse;
import dr.doctorathome.network.model.RestAllMedicinesResponse;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.adapters.DoctorMedicineBoxesAdapter;
import dr.doctorathome.presentation.presenters.PrescriptionPresenter;
import dr.doctorathome.presentation.presenters.impl.PrescriptionPresenterImpl;
import dr.doctorathome.presentation.ui.activities.PrescriptionActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class BottomSheetAddMedicine extends BottomSheetDialogFragment implements PrescriptionPresenter.View {

    private ArrayList<MedicineInPrescription> allMedicinesList;
    private PrescriptionActivity prescriptionActivity;

    @BindView(R.id.etMedicineName)
    EditText etMedicineName;

    @BindView(R.id.etDose)
    EditText etDose;

    @BindView(R.id.ivIncreaseAmount)
    ImageView ivIncreaseAmount;

    @BindView(R.id.ivDecreaseAmount)
    ImageView ivDecreaseAmount;

    @BindView(R.id.amountTV)
    TextView amountTV;

    @BindView(R.id.rvDoctorCustody)
    RecyclerView rvDoctorCustody;

    @BindView(R.id.fabAddMedicine)
    FloatingActionButton fabAddMedicine;

    @BindView(R.id.cbOutSystemMedicine)
    CheckBox cbOutSystemMedicine;

    @BindView(R.id.inSystemMedicineLayout)
    LinearLayout inSystemMedicineLayout;

    @BindView(R.id.outSystemMedicineEditText)
    EditText outSystemMedicineEditText;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    private MedicineInPrescription selectedMedicine;

    ArrayList<String> medicinesStrings = new ArrayList<>();

    private PrescriptionPresenter prescriptionPresenter;

    private DoctorMedicineBoxesAdapter doctorMedicineBoxesAdapter;

    public BottomSheetAddMedicine() {
    }

    public static BottomSheetAddMedicine newInstance(PrescriptionActivity prescriptionActivity,
                                                     ArrayList<MedicineInPrescription> allMedicinesList) {
        BottomSheetAddMedicine bottomSheetFragment = new BottomSheetAddMedicine();
        Bundle bundle = new Bundle();
        bundle.putSerializable("prescriptionActivity", prescriptionActivity);
        bundle.putParcelableArrayList("allMedicinesList", allMedicinesList);
        bottomSheetFragment .setArguments(bundle);

        return bottomSheetFragment ;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), R.style.MyDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomsheet_add_medicine, container, false);
        ButterKnife.bind(this, view);
//        getDialog().setOnShowListener(dialog -> {
//            BottomSheetDialog d = (BottomSheetDialog) dialog;
//            View bottomSheetInternal = d.findViewById(android.support.design.R.id.design_bottom_sheet);
//            BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
//        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prescriptionPresenter = new PrescriptionPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

//        selectedMedicine = new MedicineInPrescription();
//        etMedicineName.setText("");
//        etDose.setText("");
//        amountTV.setText("1");
//        if(doctorMedicineBoxesAdapter != null) {
//            doctorMedicineBoxesAdapter.setMedicineBoxes(new ArrayList<>());
//            doctorMedicineBoxesAdapter.notifyDataSetChanged();
//        }
//        cbOutSystemMedicine.setChecked(false);

        prescriptionActivity = (PrescriptionActivity) getArguments().getSerializable("prescriptionActivity");
        allMedicinesList = getArguments().getParcelableArrayList("allMedicinesList");

        Timber.i("allMedicinesList length :: " + allMedicinesList.size());

        if(medicinesStrings.isEmpty()) {
            for (MedicineInPrescription med : allMedicinesList) {
                medicinesStrings.add(med.getName());
            }
        }

        etMedicineName.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_medicine)
                , (selectedMedicine == null ? -1 : allMedicinesList.indexOf(selectedMedicine)), medicinesStrings,
                (dialogInterface, i) -> {
                    etMedicineName.setText(medicinesStrings.get(i));
                    etDose.setText(allMedicinesList.get(i).getDose());
                    selectedMedicine = allMedicinesList.get(i);
                    selectedMedicine.setCount(1);
                    amountTV.setText("" + selectedMedicine.getCount());
                    prescriptionPresenter.getDoctorMedicineBoxes(selectedMedicine.getId());
                    dialogInterface.dismiss();
                }));

        ivIncreaseAmount.setOnClickListener(view1 -> {
            if(selectedMedicine != null) {
                selectedMedicine.setCount(selectedMedicine.getCount()
                        + 1);
                amountTV.setText("" + selectedMedicine.getCount());
            }else{
                Toast.makeText(getContext(), R.string.select_med_first, Toast.LENGTH_LONG).show();
            }
        });

        ivDecreaseAmount.setOnClickListener(view1 -> {
            if(selectedMedicine != null) {
                if (selectedMedicine.getCount() != 1) {
                    selectedMedicine.setCount(selectedMedicine.getCount() - 1);
                    amountTV.setText("" + selectedMedicine.getCount());
                }
            }else{
                Toast.makeText(getContext(), R.string.select_med_first, Toast.LENGTH_LONG).show();
            }
        });

        fabAddMedicine.setOnClickListener(view1 -> {
            if(cbOutSystemMedicine.isChecked() && outSystemMedicineEditText.getText().toString().isEmpty()){
                Toast.makeText(getContext(), R.string.enter_med_name_first, Toast.LENGTH_LONG).show();
            }else if(!cbOutSystemMedicine.isChecked() && selectedMedicine == null){
                Toast.makeText(getContext(), R.string.select_med_first, Toast.LENGTH_LONG).show();
            }else {
                MedicineInPrescription newMedicineToSend;
                List<MedicineInPrescription> medicinesToSent = new ArrayList<>();
                if(cbOutSystemMedicine.isChecked()) {
                    newMedicineToSend = new MedicineInPrescription();
                    newMedicineToSend.setUniqueViewId((int) Math.round(Math.random()));
                    newMedicineToSend.setName(outSystemMedicineEditText.getText().toString());
                    newMedicineToSend.setMedicineType(MedicineTypes.OUT_OF_THE_SYSTEM);
                    medicinesToSent.add(newMedicineToSend);
                }else{
                    List<Integer> selectedBoxes = new ArrayList<>();

                    for(MedicineBox mb: doctorMedicineBoxesAdapter.getDoctorMedicineBoxes()){
                        if(mb.isChecked()){
                            selectedBoxes.add(mb.getId());
                            //                        medicinesToSent.add(new MedicineInPrescription(mb.getId(), 0, mb.getCode(),
                            //                                "", "", MedicineTypes.MEDICINE_BOX));
                        }
                    }

                    if(selectedBoxes.isEmpty()){
                        newMedicineToSend = new MedicineInPrescription((int) Math.round(Math.random()), selectedMedicine.getId(), selectedMedicine.getCount(),
                                selectedMedicine.getName(), selectedMedicine.getDose(), selectedMedicine.getPrice());
                        newMedicineToSend.setMedicineType(MedicineTypes.RECOMENDED_MEDICINE);
                        medicinesToSent.add(newMedicineToSend);
                    }else{
                        newMedicineToSend = new MedicineInPrescription((int) Math.round(Math.random()), selectedMedicine.getId(), selectedBoxes.size(),
                                selectedMedicine.getName(), selectedMedicine.getDose(), selectedMedicine.getPrice());
                        newMedicineToSend.setBoxesList(selectedBoxes);
                        newMedicineToSend.setMedicineType(MedicineTypes.MEDICINE_BOX);
                        medicinesToSent.add(newMedicineToSend);
                    }

                }
                prescriptionActivity.addMedicines(medicinesToSent);
                dismiss();
            }
        });

        cbOutSystemMedicine.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b){
                outSystemMedicineEditText.setVisibility(View.VISIBLE);
                inSystemMedicineLayout.setVisibility(View.GONE);
            }else{
                outSystemMedicineEditText.setVisibility(View.GONE);
                inSystemMedicineLayout.setVisibility(View.VISIBLE);
            }
        });

        KeyboardVisibilityEvent.setEventListener(
                getActivity(),
                isOpen -> {
                    if(isOpen){
                        fabAddMedicine.hide();
                    }else{
                        fabAddMedicine.show();
                    }
                });
    }

    @Override
    public void allSystemMedicinesRetrievedSuccessfully(RestAllMedicinesResponse restAllMedicinesResponse) {
        // no need here
    }

    @Override
    public void allLabTestsRetrievedSuccessfully(RestAllLabTestsResponse restAllLabTestsResponse) {
        // no need here
    }

    @Override
    public void allDoctorMedicineBoxesRetrievedSuccessfully(RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse) {
        Timber.i("length of boxes list :: " + restDoctorMedicineBoxesResponse.getMedicineBoxes().size());
        if(restDoctorMedicineBoxesResponse.getMedicineBoxes().isEmpty()){
            ivDecreaseAmount.setVisibility(View.VISIBLE);
            ivIncreaseAmount.setVisibility(View.VISIBLE);
            amountTV.setText("1");
        }
        doctorMedicineBoxesAdapter = new DoctorMedicineBoxesAdapter(restDoctorMedicineBoxesResponse.getMedicineBoxes(),
                getContext(), this);
        rvDoctorCustody.setAdapter(doctorMedicineBoxesAdapter);
    }

    @Override
    public void prescriptionSavedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void prescriptionDetailsRetrievedSuccessfully(RestPrescriptionDetailsResponse restPrescriptionDetailsResponse) {
        // no need here
    }

    public void checkedItemsChanged(){
        int numberOfCheckedItems = 0;

        for(MedicineBox mb: doctorMedicineBoxesAdapter.getDoctorMedicineBoxes()){
            if(mb.isChecked()){
                numberOfCheckedItems ++;
            }
        }

        if(numberOfCheckedItems == 0){
            ivDecreaseAmount.setVisibility(View.VISIBLE);
            ivIncreaseAmount.setVisibility(View.VISIBLE);
            amountTV.setText("1");
        }else{
            ivDecreaseAmount.setVisibility(View.GONE);
            ivIncreaseAmount.setVisibility(View.GONE);
            amountTV.setText("" + numberOfCheckedItems);
        }
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        fabAddMedicine.hide();
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        fabAddMedicine.show();
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}
