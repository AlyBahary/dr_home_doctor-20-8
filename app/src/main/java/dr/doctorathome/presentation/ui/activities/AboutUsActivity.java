package dr.doctorathome.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.utils.CommonUtil;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class AboutUsActivity extends AppCompatActivity {

    @BindView(R.id.tvAboutUsText)
    TextView tvAboutUsText;

    @BindView(R.id.btnContactUsNow)
    Button btnContactUsNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.about_us);

        tvAboutUsText.setText(CommonUtil.commonSharedPref().getString("aboutUS", ""));

        btnContactUsNow.setOnClickListener(view -> startActivity(new Intent(AboutUsActivity.this,
                ContactUsActivity.class)));
    }
}
