package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.FollowUpResponseModel;
import dr.doctorathome.network.model.RestDoctorServicesResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.VisitRequestsBody;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.adapters.HomeDoctorVisitFollowUpAdapter;
import dr.doctorathome.presentation.adapters.HomeDoctorVisitRequestsAdapter;
import dr.doctorathome.presentation.presenters.ServicesPresenter;
import dr.doctorathome.presentation.presenters.impl.ServicesPresenterImpl;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import dr.doctorathome.utils.EndlessRecyclerViewScrollListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;


public class HistoryFragment extends Fragment implements ServicesPresenter.View {

    //    private int selectedTab;

    @BindView(R.id.runningVisitsTab)
    TextView runningVisitsTab;

    @BindView(R.id.followUpTab)
    TextView followUpTab;

    @BindView(R.id.allVisitsTab)
    TextView allVisitsTab;

    @BindView(R.id.currentFilterName)
    TextView currentFilterName;

    @BindView(R.id.ivFilter)
    AppCompatImageButton ivFilter;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.dataRecyclerView)
    RecyclerView dataRecyclerView;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;

    @BindView(R.id.filterLayout)
    LinearLayout filterLayout;

    @BindView(R.id.loadMoreProgressbar)
    ProgressBar loadMoreProgressbar;

    private String filterBy = null;

    private EndlessRecyclerViewScrollListener scrollListener;

    private int currentPage = 0, pagesCount = 0;

    private ServicesPresenter servicesPresenter;

    private HomeDoctorVisitRequestsAdapter historyDoctorVisitRequestsAdapter;
    private HomeDoctorVisitFollowUpAdapter homeDoctorVisitFollowUpAdapter;

    private ProgressDialog progressDialog;

    private boolean isChangingVisitStatues = false;

    private int selectedTab;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getContext());

        ivBack.setVisibility(View.INVISIBLE);
        tvToolbarTitle.setText(R.string.menu_running);
        currentFilterName.setText(R.string.menu_running);
        filterLayout.setVisibility(View.GONE);
        currentPage = 0;


        servicesPresenter = new ServicesPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

        runningVisitsTab.setOnClickListener(view1 -> {
            selectedTab = 0;
            filterLayout.setVisibility(View.GONE);
            tabChanged();
        });
        followUpTab.setOnClickListener(view1 -> {
            selectedTab = 3;
            filterLayout.setVisibility(View.GONE);
            tabChanged();
        });

        allVisitsTab.setOnClickListener(view1 -> {
            selectedTab = 1;
            filterLayout.setVisibility(View.VISIBLE);
            tabChanged();
        });

        //Set default tab
        selectedTab = 0;
        tabChanged();

        filterLayout.setOnClickListener(view1 -> CommonUtil.showRequestFilterMenu(getActivity(), filterLayout, menuItem -> {
            Timber.i("selected MenuItem :: " + menuItem.toString());
            switch (menuItem.getItemId()) {
                case R.id.menuCompleted:
                    filterBy = RequestStatues.COMPLETED;
                    tvToolbarTitle.setText(R.string.menu_completed);
                    currentFilterName.setText(R.string.menu_completed);
                    break;
                case R.id.menuRunning:
                    filterBy = RequestStatues.RUNNING;
                    tvToolbarTitle.setText(R.string.menu_running);
                    currentFilterName.setText(R.string.menu_running);
                    break;
                case R.id.menuPending:
                    filterBy = RequestStatues.PENDING;
                    tvToolbarTitle.setText(R.string.menu_pending);
                    currentFilterName.setText(R.string.menu_pending);
                    break;
                case R.id.menuCancelled:
                    filterBy = RequestStatues.CANCELLED;
                    tvToolbarTitle.setText(R.string.menu_cancelled);
                    currentFilterName.setText(R.string.menu_cancelled);
                    break;
                case R.id.menuConfirmed:
                    filterBy = RequestStatues.CONFIRMED;
                    tvToolbarTitle.setText(R.string.menu_confirmed);
                    currentFilterName.setText(R.string.menu_confirmed);
                    break;
                case R.id.menuPaid:
                    filterBy = RequestStatues.PAID;
                    tvToolbarTitle.setText(R.string.menu_paid);
                    currentFilterName.setText(R.string.menu_paid);
                    break;
                case R.id.menuRejected:
                    filterBy = RequestStatues.REJECTED;
                    tvToolbarTitle.setText(R.string.menu_rejected);
                    currentFilterName.setText(R.string.menu_rejected);
                    break;
            }
            currentPage = 0;
            loadVisits();
            return true;
        }));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        dataRecyclerView.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Timber.i("SCROLL Happened " + "  ");
                if (page < (pagesCount - 1)) {
                    page++;
                    currentPage++;
                    loadVisits();
                    Timber.i("currentPage : " + " " + Integer.toString(page));
                }
            }
        };

        dataRecyclerView.addOnScrollListener(scrollListener);

        //        loadVisits();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 2) {
            currentPage = 0;
            loadVisits();
        }
    }

    private void tabChanged() {
        if (selectedTab == 0) {
            filterBy = RequestStatues.RUNNING;
            tvToolbarTitle.setText(R.string.menu_running);
            currentFilterName.setText(R.string.menu_running);
            runningVisitsTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            runningVisitsTab.setTextColor(getResources().getColor(R.color.colorWhite));
            allVisitsTab.setBackground(null);
            followUpTab.setBackground(null);
            allVisitsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            followUpTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        } else if (selectedTab == 1) {
            filterBy = null;
            tvToolbarTitle.setText(R.string.all_visits);
            currentFilterName.setText(R.string.all_visits);
            allVisitsTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            allVisitsTab.setTextColor(getResources().getColor(R.color.colorWhite));
            runningVisitsTab.setBackground(null);
            followUpTab.setBackground(null);
            runningVisitsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            followUpTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
        } else if (selectedTab == 3) {
            tvToolbarTitle.setText(R.string.Follow_Up);
            currentFilterName.setText(R.string.Follow_Up);
            currentFilterName.append(" ");
            currentFilterName.append(getString(R.string.visits));
            followUpTab.setBackground(getResources().getDrawable(R.drawable.shape_btn_rounded));
            followUpTab.setTextColor(getResources().getColor(R.color.colorWhite));
            runningVisitsTab.setBackground(null);
            allVisitsTab.setBackground(null);
            allVisitsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            runningVisitsTab.setTextColor(getResources().getColor(R.color.colorUnSelectedTabText));
            homeDoctorVisitFollowUpAdapter=new HomeDoctorVisitFollowUpAdapter(getContext());
            getFollowUp();
            dataRecyclerView.setAdapter(homeDoctorVisitFollowUpAdapter);
            return;
        }

        loadVisits();
    }

    private void getFollowUp() {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        HashMap<String,String> body=new HashMap<>();
        Log.d("doctorId", "getFollowUp: "+CommonUtil.commonSharedPref().getInt("doctorId", -1));
        body.put("doctorId",CommonUtil.commonSharedPref().getInt("doctorId", -1)+"");
        doctorService.getFollowUp(body,CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<FollowUpResponseModel>() {
            @Override
            public void onResponse(Call<FollowUpResponseModel> call, Response<FollowUpResponseModel> response) {
                homeDoctorVisitFollowUpAdapter.setFollowUpModels(response.body().getFollowups());
                homeDoctorVisitFollowUpAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<FollowUpResponseModel> call, Throwable t) {

            }
        });
    }

    private void loadVisits() {
        servicesPresenter.getDoctorVisitRequests(new VisitRequestsBody(CommonUtil.commonSharedPref()
                .getInt("doctorId", -1),
                currentPage, filterBy
        ));
    }

    public void blockPatient(int patientId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_block_patient), false,
                "",
                R.drawable.ic_warning, getString(R.string.block), getString(R.string.cancel), () ->
                        servicesPresenter.blockPatient(patientId));

        dialog.show();

    }

    public void unblockPatient(int patientId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_unblock_patient), false,
                "",
                R.drawable.ic_check_done, getString(R.string.unblock), getString(R.string.cancel), () ->
                        servicesPresenter.unblockPatient(patientId));

        dialog.show();
    }

    @Override
    public void doctorServicesRetrievedSuccessfully(RestDoctorServicesResponse restDoctorServicesResponse) {
        // no need here
    }

    @Override
    public void doctorServiceEnabledDisabledSuccessfully(CommonResponse commonResponse, int position) {
        // no need here
    }

    @Override
    public void doctorServiceEnabledDisableFailed(String message, int position) {
        // no need here
    }

    @Override
    public void doctorVisitRequestRetrievedSuccessfully(RestVisitRequestsResponse restVisitRequestsResponse) {
        if (currentPage == 0) {
            pagesCount = restVisitRequestsResponse.getTotalPages() != null
                    ? restVisitRequestsResponse.getTotalPages() : 0;

            historyDoctorVisitRequestsAdapter = new HomeDoctorVisitRequestsAdapter(restVisitRequestsResponse.getVisitRequests(), getActivity());
            historyDoctorVisitRequestsAdapter.setCallerFragment(this);
            dataRecyclerView.setAdapter(historyDoctorVisitRequestsAdapter);
        } else {
            historyDoctorVisitRequestsAdapter.getDoctorRequests().addAll(restVisitRequestsResponse.getVisitRequests());
            historyDoctorVisitRequestsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void patientBlockedSuccessfully(CommonResponse commonResponse) {
        Toast toast =Toast.makeText(getContext(), "" + getString(R.string.patient_blocked_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
        toast.show();
       // Toast.makeText(getContext(), R.string.patient_blocked_success, Toast.LENGTH_LONG).show();
        currentPage = 0;
        loadVisits();
    }

    @Override
    public void patientUnblockedSuccessfully(CommonResponse commonResponse) {

        Toast toast =Toast.makeText(getContext(), "" + getString(R.string.patient_unblocked_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
        toast.show();
   //     Toast.makeText(getContext(), R.string.patient_unblocked_success, Toast.LENGTH_LONG).show();
        currentPage = 0;
        loadVisits();
    }

    public void acceptRequest(int visitId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_accept_visit), false,
                "",
                R.drawable.ic_check_done, getString(R.string.accept), getString(R.string.cancel), () -> {
                    isChangingVisitStatues = true;
                    servicesPresenter.acceptVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.CONFIRMED));
                });

        dialog.show();
    }

    public void rejectRequest(int visitId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_reject_visit), false,
                "",
                R.drawable.ic_warning, getString(R.string.reject), getString(R.string.cancel), () -> {
                    isChangingVisitStatues = true;
                    servicesPresenter.rejectVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.REJECTED));
                });

        dialog.show();
    }

    public void cancelRequest(int visitId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_cancel_visit), false,
                "",
                R.drawable.ic_warning, getString(R.string.yes), getString(R.string.no), () -> {
                    isChangingVisitStatues = true;
                    servicesPresenter.cancelVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.CANCELLED));
                });

        dialog.show();
    }

    @Override
    public void visitAcceptedSuccessfully() {
        isChangingVisitStatues = false;
       // Toast.makeText(getContext(), R.string.visit_accepted_success, Toast.LENGTH_LONG).show();

        Toast toast =Toast.makeText(getContext(), "" + getString(R.string.visit_accepted_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);

        toast.show();
        currentPage = 0;
        loadVisits();
    }

    @Override
    public void visitRejectedSuccessfully() {
        isChangingVisitStatues = false;
        //Toast.makeText(getContext(), R.string.visit_rejected_success, Toast.LENGTH_LONG).show();
        Toast toast =Toast.makeText(getContext(), "" + getString(R.string.visit_rejected_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);

        toast.show();

        currentPage = 0;
        loadVisits();
    }

    @Override
    public void visitCancelledSuccessfully() {
        isChangingVisitStatues = false;
        Toast toast =Toast.makeText(getContext(), "" + getString(R.string.visit_cancelled_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
        toast.show();

      //  Toast.makeText(getContext(), R.string.visit_cancelled_success, Toast.LENGTH_LONG).show();
        currentPage = 0;
        loadVisits();
    }

    @Override
    public void showProgress(String message) {
        if (!isChangingVisitStatues && currentPage == 0) {
            progressLayout.setVisibility(View.VISIBLE);
            dataRecyclerView.setVisibility(View.GONE);
            //            ivFilter.setVisibility(View.GONE);
        } else if (!isChangingVisitStatues) {
            loadMoreProgressbar.setVisibility(View.VISIBLE);
        } else {
            CommonUtil.showProgressDialog(progressDialog, message);
        }
    }

    @Override
    public void hideProgress() {
        if (!isChangingVisitStatues && currentPage == 0) {
            progressLayout.setVisibility(View.GONE);
            dataRecyclerView.setVisibility(View.VISIBLE);
            //            ivFilter.setVisibility(View.VISIBLE);
        } else if (!isChangingVisitStatues) {
            loadMoreProgressbar.setVisibility(View.GONE);
        } else {
            CommonUtil.dismissProgressDialog(progressDialog);
        }
    }

    @Override
    public void showError(String message, boolean hideView) {
      //  Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        Toast toast =Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
        toast.show();
        if (isChangingVisitStatues) {
            isChangingVisitStatues = false;
        }
    }

    //    private void tabChanged() {
    //        if (selectedTab == 0) {
    //            runningVisitsTab.setBackground(getActivity().getResources().getDrawable(R.drawable.shape_btn_rounded));
    //            runningVisitsTab.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
    //
    //            allVisitsTab.setBackgroundColor(getActivity().getResources().getColor(R.color.colorWhite));
    //            allVisitsTab.setTextColor(getActivity().getResources().getColor(R.color.grayDark));
    //        } else if (selectedTab == 1) {
    //            allVisitsTab.setBackground(getActivity().getResources().getDrawable(R.drawable.shape_btn_rounded));
    //            allVisitsTab.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
    //
    //            runningVisitsTab.setBackgroundColor(getActivity().getResources().getColor(R.color.colorWhite));
    //            runningVisitsTab.setTextColor(getActivity().getResources().getColor(R.color.grayDark));
    //        }
    //    }
}
