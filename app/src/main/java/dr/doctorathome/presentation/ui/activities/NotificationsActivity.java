package dr.doctorathome.presentation.ui.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestNotificationsResponse;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.adapters.AllNotificationsAdapter;
import dr.doctorathome.presentation.presenters.NotificationPresenter;
import dr.doctorathome.presentation.presenters.impl.NotificationPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.EndlessRecyclerViewScrollListener;
import timber.log.Timber;

public class NotificationsActivity extends BasicActivity implements NotificationPresenter.View {

    private NotificationPresenter notificationPresenter;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.notificationsRecyclerView)
    RecyclerView notificationsRecyclerView;

    @BindView(R.id.loadMoreProgressbar)
    ProgressBar loadMoreProgressbar;

    @BindView(R.id.pullToRefreshMore)
    androidx.swiperefreshlayout.widget.SwipeRefreshLayout pullToRefresh;

    private int currentPage = 0, pagesCount = 0;

    private EndlessRecyclerViewScrollListener scrollListener;

    private AllNotificationsAdapter allNotificationsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.notifications);

        notificationPresenter = new NotificationPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

        allNotificationsAdapter = new AllNotificationsAdapter(new ArrayList<>(), this);
        notificationsRecyclerView.setAdapter(allNotificationsAdapter);

        currentPage = 0;
        getNotifications();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        notificationsRecyclerView.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Timber.i("SCROLL Happened " + " page: " + page + "page count : " + pagesCount);
                if (page < (pagesCount - 1)) {
                    page++;
                    currentPage++;
                    getNotifications();
                    Timber.i("currentPage : " + " " + Integer.toString(page));
                }
            }
        };

        notificationsRecyclerView.addOnScrollListener(scrollListener);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage=0;
                pagesCount=0;
                getNotifications();
                scrollListener.resetState();
            }
        });
    }

    private void getNotifications() {
        Timber.i("GETNOTIFICATION : " + " " + currentPage);
        pullToRefresh.setRefreshing(false);
        notificationPresenter.requestUserNotifications(currentPage);
    }

    private void getNewNotifications() {
        Timber.i("GETNOTIFICATION : " + " " + currentPage);
        pullToRefresh.setRefreshing(false);

        notificationPresenter.requestUserNotifications(0);
    }

    public void markNotificationAsRead(int notificationId) {
        notificationPresenter.markNotificationAsRead(notificationId);
    }

    @Override
    public void userNotificationsRetrievedSuccessfully(RestNotificationsResponse restNotificationsResponse) {

        Log.d("TTTTT", "userNotificationsRetrievedSuccessfully: " + restNotificationsResponse.getTotalPages());
        if (restNotificationsResponse.getNotificationModels() != null
                && !restNotificationsResponse.getNotificationModels().isEmpty()) {
            if (currentPage == 0) {
                pagesCount = restNotificationsResponse.getTotalPages();
                allNotificationsAdapter.setAllNotifications(restNotificationsResponse.getNotificationModels());
            } else {
                allNotificationsAdapter.addToAllNotifications(restNotificationsResponse.getNotificationModels());

            }

            allNotificationsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void notificationMarkedAsReadSuccessfully(CommonResponse commonResponse) {
        currentPage = 0;
        getNotifications();
    }

    @Override
    public void showProgress(String message) {
        if (currentPage == 0) {
            progressLayout.setVisibility(View.VISIBLE);
            notificationsRecyclerView.setVisibility(View.GONE);
        } else {
            loadMoreProgressbar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void hideProgress() {
        if (currentPage == 0) {
            progressLayout.setVisibility(View.GONE);
            notificationsRecyclerView.setVisibility(View.VISIBLE);
        } else {
            loadMoreProgressbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String message, boolean hideView) {
        try {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
