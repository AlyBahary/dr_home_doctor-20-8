package dr.doctorathome.presentation.ui.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SettingsActivity extends BasicActivity {

    @BindView(R.id.deactivateAccount)
    CardView deactivateAccount;

    @BindView(R.id.changePassword)
    CardView changePassword;

    @BindView(R.id.changeLanguage)
    CardView changeLanguage;

    @BindView(R.id.notifications)
    CardView notifications;

    private boolean languageChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        languageChanged = getIntent().getBooleanExtra("languageChanged", false);

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.settings);

        deactivateAccount.setOnClickListener(view -> startActivity(new Intent(SettingsActivity.this,
                RequestClearanceActivity.class)));

        changePassword.setOnClickListener(view -> startActivity(new Intent(SettingsActivity.this,
                ChangePasswordActivity.class)));

        notifications.setOnClickListener(view -> startActivity(new Intent(SettingsActivity.this,
                NotificationsActivity.class)));

        changeLanguage.setOnClickListener(view -> {
            Intent goToChangeLanguageIntent = new Intent(SettingsActivity.this, LanguageSelectActivity.class);
            goToChangeLanguageIntent.putExtra("cameFromSettings", true);
            startActivityForResult(goToChangeLanguageIntent, 1);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 1 && resultCode == RESULT_OK){
            finish();
            Intent refresh = new Intent(this, SettingsActivity.class);
            refresh.putExtra("languageChanged", true);
            startActivity(refresh);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (languageChanged) {
            Intent applyDrawerNewLanguage = new Intent(this, MainActivity.class);
            applyDrawerNewLanguage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(applyDrawerNewLanguage);
        } else {
            super.onBackPressed();
        }
    }
}
