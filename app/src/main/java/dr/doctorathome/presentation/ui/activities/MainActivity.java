package dr.doctorathome.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.presentation.ui.fragments.ConsultationFragment;
import dr.doctorathome.presentation.ui.fragments.HistoryFragment;
import dr.doctorathome.presentation.ui.fragments.HomeFragment;
import dr.doctorathome.presentation.ui.fragments.MoreFragment;
import dr.doctorathome.presentation.ui.fragments.ServicesFragment;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.presentation.ui.helpers.CurvedBottomNavigationView;
import dr.doctorathome.utils.CommonUtil;

public class MainActivity extends BasicActivity {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    final FragmentManager fragmentManager = getSupportFragmentManager();

    // define your fragments here
    final Fragment historyFragment = new HistoryFragment();
    final Fragment servicesFragment = new ServicesFragment();
    final Fragment homeFragment = new HomeFragment();
    final Fragment consultationFragment = new ConsultationFragment();
    final Fragment moreFragment = new MoreFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplication.setAppLocale(CommonUtil.commonSharedPref().getString("appLanguage", "en"));
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Log.d("TTTTTTTTTTTT", "onCreate: ----1111");
        //        ((TextView) findViewById(R.id.hello_user)).setText("Hello, "
        //                + CommonUtil.commonSharedPref().getString("firstName", "NoName") + " "
        //                + CommonUtil.commonSharedPref().getString("lastName", "NoName"));

        // handle navigation selection

        bottomNavigationView.setOnNavigationItemSelectedListener(
                item -> {
                    Fragment fragment;
                    switch (item.getItemId()) {
                        case R.id.menuHistory:
                            fragment = historyFragment;
                            break;
                        case R.id.menuServices:
                            fragment = servicesFragment;
                            break;
                        case R.id.menuConsultation:
                            fragment = consultationFragment;
                            break;
                        case R.id.menuMore:
                            fragment = moreFragment;
                            break;
                        case R.id.menuHome:
                        default:
                            fragment = homeFragment;
                            break;
                    }
                    fragmentManager.beginTransaction().replace(R.id.container_view, fragment).commit();
                    return true;
                });
        // Set default selection
        bottomNavigationView.setSelectedItemId(R.id.menuHome);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Fragment fragment = fragmentManager.findFragmentById(R.id.container_view);
        if (fragment instanceof HomeFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        } else if (fragment instanceof HistoryFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        } else if (fragment instanceof ConsultationFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = fragmentManager.findFragmentById(R.id.container_view);
        if (fragment instanceof HomeFragment) {
            super.onBackPressed();
        } else {
            bottomNavigationView.setSelectedItemId(R.id.menuHome);
        }
    }

    public BottomNavigationView getBottomNavigationView() {
        return bottomNavigationView;
    }


}
