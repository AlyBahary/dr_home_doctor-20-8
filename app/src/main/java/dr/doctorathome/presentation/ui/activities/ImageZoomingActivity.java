package dr.doctorathome.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import uk.co.senab.photoview.PhotoViewAttacher;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class ImageZoomingActivity extends AppCompatActivity  {

    @BindView(R.id.image)
    ImageView image;
    String url;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_zooming);
        ButterKnife.bind(this);
        url = getIntent().getStringExtra("Image");
        Glide.with(this).load(RestClient.REST_API_URL + url)
                .into(image);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) image.getLayoutParams();
        params.gravity = Gravity.CENTER;
        image.setLayoutParams(params);
        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(image);
        pAttacher.update();


    }
//
}
