package dr.doctorathome.presentation.ui.helpers;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import com.google.android.material.snackbar.Snackbar;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.config.Config;
import dr.doctorathome.utils.CommonUtil;


public class BasicActivity extends AppCompatActivity {
    private Locale mCurrentLocale;
    Snackbar snackbar;
    View parentLayout;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateBaseContextLocale(getApplicationContext());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        languageCheck();
    }

    protected void languageCheck() {
        SharedPreferences sharedPref = getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
        Locale locale = new Locale(sharedPref.getString("appLanguage", "ar"));
        Locale.setDefault(locale);

        if (!locale.equals(mCurrentLocale)) {
            mCurrentLocale = locale;
            recreate();
            CommonUtil.makeDefaultLocaleToArabic(getBaseContext());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        CommonUtil.makeDefaultLocaleToArabic(getBaseContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
        String language = sharedPref.getString("appLanguage", "ar"); // Helper method to get saved language from SharedPreferences
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

    }

    //get last state
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        AndroidApplication.setAppLocale(CommonUtil.commonSharedPref().getString("appLanguage", "en"));
    }
    public void adderrorSnackbar(String text) {
        parentLayout = findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout, "", Snackbar.LENGTH_LONG);
        snackbar.setText(text);
        View view = snackbar.getView();
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(ContextCompat.getColor(this,android.R.color.holo_red_dark));

        ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);
        snackbar.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark))
                .show();
    }

    public void addsuccessSnackbar(String text) {
        parentLayout = findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout, "", Snackbar.LENGTH_LONG);
        snackbar.setText(text);
        View view = snackbar.getView();
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(ContextCompat.getColor(this,android.R.color.holo_green_light));

        ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);
        snackbar.setActionTextColor(getResources().getColor(android.R.color.holo_green_light))
                .show();
    }

}
