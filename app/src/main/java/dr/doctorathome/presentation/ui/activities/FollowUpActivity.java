package dr.doctorathome.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.HashMap;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.FollowUpModel;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.adapters.labItemInFollowUpAdapter;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowUpActivity extends BasicActivity {

    String id;

    @BindView(R.id.RV)
    RecyclerView RV;
    dr.doctorathome.presentation.adapters.labItemInFollowUpAdapter labItemInFollowUpAdapter;
    @BindView(R.id.tvToolbarTitle)
    AppCompatTextView tvToolbarTitle;
    @BindView(R.id.ivBack)
    AppCompatImageButton ivBack;

    @BindView(R.id.ivPatientImage)
    AppCompatImageView ivPatientImage;
    @BindView(R.id.tvPatientName)
    AppCompatTextView tvPatientName;
    @BindView(R.id.tvPatientLocation)
    AppCompatTextView tvPatientLocation;
    @BindView(R.id.msg_btn)
    LinearLayout msgBtn;
    @BindView(R.id.remaining_days)
    TextView remainingDays;
    @BindView(R.id.progressbarLoading)
    ProgressBar progressbarLoading;
    @BindView(R.id.tivToolbarEndAction)
    AppCompatImageButton tivToolbarEndAction;

    @BindView(R.id.ivNewNotification)
    AppCompatImageView ivNewNotification;
    @BindView(R.id.tvTime)
    AppCompatTextView tvTime;
    @BindView(R.id.tvNumber)
    AppCompatTextView tvNumber;
    @BindView(R.id.imageCall)
    ImageView imageCall;
    @BindView(R.id.patientInfoLayout)
    RelativeLayout patientInfoLayout;
    @BindView(R.id.tvComplain)
    TextView tvComplain;
    @BindView(R.id.tvSignificantPastDisease)
    EditText tvSignificantPastDisease;
    @BindView(R.id.medicalHistoryLayout)
    RelativeLayout medicalHistoryLayout;
    @BindView(R.id.endFollowUpBtn)
    Button endFollowUpBtn;
    @BindView(R.id.visitDetailsMainScrollView)
    ScrollView visitDetailsMainScrollView;
    @BindView(R.id.adddiagoseFollowUpBtn)
    Button adddiagoseFollowUpBtn;

    String patinetID = "";
    int state = 3;
    @BindView(R.id.viewVisit)
    Button viewVisit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_up);
        ButterKnife.bind(this);
        ivBack.setOnClickListener(v -> {
            finish();
        });
        id = getIntent().getStringExtra("id");
        labItemInFollowUpAdapter = new labItemInFollowUpAdapter(this);
        RV.setAdapter(labItemInFollowUpAdapter);
        getFollowUp();
        adddiagoseFollowUpBtn.setOnClickListener(v -> {
            if (!tvSignificantPastDisease.getText().toString().trim().isEmpty())
                addDiagnose();
        });
        if (Config.notificationCount > 0)
            ivNewNotification.setVisibility(View.VISIBLE);

        endFollowUpBtn.setOnClickListener(v -> {
            endfollowUp();
        });
        msgBtn.setOnClickListener(v -> {
            startActivity(new Intent(FollowUpActivity.this, FollowUpChatActivity.class)
                    .putExtra("isFollowUp", true)
                    .putExtra("state", state)
                    .putExtra("follow_up_id", id + "")
                    .putExtra("patient_id", patinetID)
            );
        });

    }

    private void addDiagnose() {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        HashMap<String, String> body = new HashMap<>();
        body.put("followUpId", id);
        body.put("diagnosise", tvSignificantPastDisease.getText().toString());
        doctorService.addDiagnose(body, CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                tvSignificantPastDisease.setText("");
                Toast.makeText(FollowUpActivity.this, "" + response.body().get("rspBody").getAsString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(FollowUpActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void endfollowUp() {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        HashMap<String, String> body = new HashMap<>();
        body.put("followUpId", id);
        body.put("diagnosise", tvSignificantPastDisease.getText().toString());
        doctorService.endFollowUp(body, CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                getFollowUp();
                tvSignificantPastDisease.setText("");
                Toast.makeText(FollowUpActivity.this, "" + response.body().get("rspBody").getAsString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(FollowUpActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getFollowUp() {
        progressbarLoading.setVisibility(View.VISIBLE);
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.getFollowUpbyId(id, CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<FollowUpModel>() {
            @Override
            public void onResponse(Call<FollowUpModel> call, Response<FollowUpModel> response) {

                viewVisit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent goToVisitDetails = new Intent(FollowUpActivity.this, VisitDetailsActivity.class);
                        goToVisitDetails.putExtra("visitId", response.body().getVisitId());
                        startActivity(goToVisitDetails);
                    }
                });
                progressbarLoading.setVisibility(View.GONE);
                tvPatientName.setText(response.body().getPatient().getUser().getFirstName()
                        + " " +
                        response.body().getPatient().getUser().getFirstName());
                patinetID = response.body().getPatient().getUser().getId() + "";
                tvPatientLocation.setText(response.body().getPatient().getAddress());
                tvTime.setText(response.body().getPatient().getDateOfBirth());
                tvComplain.setText(response.body().getVisitDiagnoise());
                tvNumber.setText("");
                tvSignificantPastDisease.setText(response.body().getVisitDiagnoise());
                for (int i = 0; i < response.body().getTests().size(); i++) {
                    if (response.body().getTests().get(i).getNotes() == null)
                        continue;
                    tvSignificantPastDisease.append("\n" + response.body().getTests().get(i).getNotes());
                }
                labItemInFollowUpAdapter.setTestModels(response.body().getTests());
                labItemInFollowUpAdapter.notifyDataSetChanged();
                state = response.body().getStatus().getId();
                if (response.body().getStatus().getId() == 3 || response.body().getStatus().getId() == 2) {
                    endFollowUpBtn.setVisibility(View.GONE);
                    adddiagoseFollowUpBtn.setVisibility(View.GONE);
                }


//                Glide.with(FollowUpActivity.this).load(RestClient.REST_API_URL + avatar_url)
//                        .apply(RequestOptions.circleCropTransform())
//                        .placeholder(R.drawable.profile_placeholder_rounded).into(ivUserImage);


            }

            @Override
            public void onFailure(Call<FollowUpModel> call, Throwable t) {

            }
        });
    }

}
