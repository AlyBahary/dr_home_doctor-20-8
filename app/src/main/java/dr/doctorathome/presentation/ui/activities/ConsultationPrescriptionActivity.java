package dr.doctorathome.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.shuhart.stepview.StepView;

import java.util.ArrayList;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.EPrescriptionModel;
import dr.doctorathome.network.model.EPrescriptionSendModel;
import dr.doctorathome.network.model.LabTestModel;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.ui.fragments.ConsultaionDiagnoseFragment;
import dr.doctorathome.presentation.ui.fragments.ConsultationLabFragment;
import dr.doctorathome.presentation.ui.fragments.ConsultationMedicineFragment;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConsultationPrescriptionActivity extends BasicActivity {

    @BindView(R.id.tvToolbarTitle)
    AppCompatTextView tvToolbarTitle;
    @BindView(R.id.ivBack)
    AppCompatImageButton ivBack;
    @BindView(R.id.tivToolbarEndAction)
    AppCompatImageButton tivToolbarEndAction;
    @BindView(R.id.ivNewNotification)
    AppCompatImageView ivNewNotification;
    @BindView(R.id.stepView)
    StepView stepView;
    @BindView(R.id.framelayout)
    FrameLayout framelayout;
    @BindView(R.id.BackBtn)
    Button BackBtn;
    @BindView(R.id.NextBtn)
    Button NextBtn;
    int currentStep = 0;

    ConsultaionDiagnoseFragment consultaionDiagnoseFragment;
    ConsultationMedicineFragment consultationMedicineFragment;
    ConsultationLabFragment consultationLabFragment;

    EPrescriptionSendModel ePrescriptionSendModel;

    EPrescriptionModel ePrescriptionModel = new EPrescriptionModel();
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.firstImage)
    ImageView firstImage;
    @BindView(R.id.lastImage)
    ImageView lastImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation_prescription);
        ButterKnife.bind(this);

        tivToolbarEndAction.setOnClickListener(v -> {
            startActivity(new Intent(this, NotificationsActivity.class));
        });
        if (Config.notificationCount > 0)
            ivNewNotification.setVisibility(View.VISIBLE);

        ivBack.setOnClickListener(v -> {
            finish();
        });
//        if (CommonUtil.commonSharedPref().getString("appLanguage", "en").equals("en")) {
//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
//                    60,
//                    60
//            );
//            params.setMargins(15, 10, 0, 0);
//            firstImage.setLayoutParams(params);
//        } else {
//
//            setMargins(firstImage,0,0,20,0);
//            setMargins(lastImage,0,0,-10,0);
//        }
        tvToolbarTitle.setText(getString(R.string.prescription));
        stepView.getState()
                // You should specify only stepsNumber or steps array of strings.
                // In case you specify both steps array is chosen.
                .steps(new ArrayList<String>() {{
                    add("" + getString(R.string.Diagnoses));
                    add("" + getString(R.string.LabTest));
                    add("" + getString(R.string.Medicine));

                }})
                .animationDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                .textSize(getResources().getDimensionPixelSize(R.dimen.sp14))
                .stepNumberTextSize(getResources().getDimensionPixelSize(R.dimen.sp0))
                .typeface(ResourcesCompat.getFont(this, R.font.regular))
                .doneTextColor(ContextCompat.getColor(this, R.color.colorWhite))
                .nextTextColor(ContextCompat.getColor(this, R.color.colorWhite))
                // other state methods are equal to the corresponding xml attributes
                .commit();
        NextBtn.setOnClickListener(v -> {
            if (currentStep == 2) {
                savePrescription();
                return;
            } else if ((currentStep == 1))
                displayMedicineFragment();
            else if (currentStep == 0)
                displayLabTest();
            if (currentStep == 1) {
                NextBtn.setText(getString(R.string.send_to_patient));
            } else {
                NextBtn.setText(getString(R.string.next_btn_text));
            }
            currentStep++;
            stepView.go(currentStep, true);
        });
        BackBtn.setOnClickListener(v -> {
            if (currentStep == 0) {
                return;
            }
            if (currentStep == 1)
                displayDiagnoseFragment();
            if (currentStep == 2)
                displayLabTest();
            if (currentStep == 2)
                NextBtn.setText(getString(R.string.next_btn_text));
            currentStep--;
            stepView.go(currentStep, true);
        });

        findViewById(R.id.ivBack).setOnClickListener(view -> {
            if (stepView.getCurrentStep() == 0) {
                onBackPressed();
            } else {
                //stepChanged(false);
            }
        });

        consultaionDiagnoseFragment = ConsultaionDiagnoseFragment.newInstance();
        consultationMedicineFragment = ConsultationMedicineFragment.newInstace();
        consultationLabFragment = ConsultationLabFragment.newInstace();
        if (savedInstanceState == null)
            getEPrescription();
    }

    protected void displayDiagnoseFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (consultaionDiagnoseFragment.isAdded()) { // if the fragment is already in container
            ft.show(consultaionDiagnoseFragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.framelayout, consultaionDiagnoseFragment, "1");
        }

        // Hide fragment 2
        if (consultationMedicineFragment.isAdded()) {
            ft.hide(consultationMedicineFragment);
        }
        if (consultationLabFragment.isAdded()) {
            ft.hide(consultationLabFragment);
        }
        ft.commit();
    }

    protected void displayMedicineFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (consultationMedicineFragment.isAdded()) { // if the fragment is already in container
            ft.show(consultationMedicineFragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.framelayout, consultationMedicineFragment, "2");
        }

        // Hide fragment 2
        if (consultaionDiagnoseFragment.isAdded()) {
            ft.hide(consultaionDiagnoseFragment);
        }
        if (consultationLabFragment.isAdded()) {
            ft.hide(consultationLabFragment);
        }
        ft.commit();
    }

    protected void displayLabTest() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (consultationLabFragment.isAdded()) { // if the fragment is already in container
            ft.show(consultationLabFragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.framelayout, consultationLabFragment, "3");
        }

        // Hide fragment 2
        if (consultaionDiagnoseFragment.isAdded()) {
            ft.hide(consultaionDiagnoseFragment);
        }
        if (consultationMedicineFragment.isAdded()) {
            ft.hide(consultationMedicineFragment);
        }
        ft.commit();
    }

    public String getDiagnose() {

        return ePrescriptionModel.getDiagnose() == null || ePrescriptionModel.getDiagnose().equals("null") ? "" : ePrescriptionModel.getDiagnose();
    }

    public ArrayList<MedicineModel> getMedicine() {
        return ePrescriptionModel.getMedicines();
    }

    public ArrayList<LabTestModel> getLab() {
        return ePrescriptionModel.getLabTests();
    }

    public void getEPrescription() {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.getEPrescription(getIntent().getIntExtra("session_id", 0) + ""
                , CommonUtil.commonSharedPref().getString("token", ""))
                .enqueue(new Callback<EPrescriptionModel>() {
                    @Override
                    public void onResponse(Call<EPrescriptionModel> call, Response<EPrescriptionModel> response) {
                        displayDiagnoseFragment();
                        progress.setVisibility(View.GONE);

                        if (response.isSuccessful()) {
                            ePrescriptionModel = response.body();
//                            consultaionDiagnoseFragment.setDiagnose(response.body().getDiagnose());
//                            consultationMedicineFragment.setMedicins(response.body().getMedicines());
//                            consultationLabFragment.SetLabs(response.body().getLabTests());

                        }

                    }

                    @Override
                    public void onFailure(Call<EPrescriptionModel> call, Throwable t) {
                        displayDiagnoseFragment();
                        progress.setVisibility(View.GONE);

                    }
                });
    }

    public void savePrescription() {
        progress.setVisibility(View.VISIBLE);

        ePrescriptionSendModel = new EPrescriptionSendModel();
        ePrescriptionSendModel.setLabTestIds(consultationLabFragment.getLabIds());
        ePrescriptionSendModel.setMedicines(consultationMedicineFragment.getMedicinsIds());
        ePrescriptionSendModel.setNewMedicins(consultationMedicineFragment.getNewMedicinModels());
        ePrescriptionSendModel.setDiagnose(consultaionDiagnoseFragment.getDiagonse());
        ePrescriptionSendModel.setCommonDiseaseId(consultaionDiagnoseFragment.getCommonDiagonseId());
        ePrescriptionSendModel.setRequestId(getIntent().getIntExtra("session_id", 0));
        ePrescriptionSendModel.setId(getIntent().getIntExtra("patient_id", 0));
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.savePrescription(ePrescriptionSendModel, CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    progress.setVisibility(View.GONE);
                    finish();
                    Toast.makeText(ConsultationPrescriptionActivity.this, getString(R.string.prescription_sent_success), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ConsultationPrescriptionActivity.this, getString(R.string.unknown_errror_message), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progress.setVisibility(View.GONE);

            }
        });

    }
    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}
