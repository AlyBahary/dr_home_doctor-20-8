//package dr.doctorathome.presentation.ui.activities;
//
//import android.annotation.SuppressLint;
//import android.app.Dialog;
//import android.location.Location;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//
//import com.mapbox.android.core.location.LocationEngine;
//import com.mapbox.android.core.location.LocationEngineCallback;
//import com.mapbox.android.core.location.LocationEngineProvider;
//import com.mapbox.android.core.location.LocationEngineRequest;
//import com.mapbox.android.core.location.LocationEngineResult;
//import com.mapbox.android.core.permissions.PermissionsListener;
//import com.mapbox.android.core.permissions.PermissionsManager;
//import com.mapbox.api.directions.v5.models.DirectionsResponse;
//import com.mapbox.api.directions.v5.models.DirectionsRoute;
//import com.mapbox.geojson.Point;
//import com.mapbox.mapboxsdk.Mapbox;
//import com.mapbox.mapboxsdk.location.LocationComponent;
//import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
//import com.mapbox.mapboxsdk.location.modes.CameraMode;
//import com.mapbox.mapboxsdk.location.modes.RenderMode;
//import com.mapbox.mapboxsdk.maps.MapView;
//import com.mapbox.mapboxsdk.maps.MapboxMap;
//import com.mapbox.mapboxsdk.maps.Style;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
//import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
//import com.mapbox.services.android.navigation.v5.navigation.MapboxNavigation;
//import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
//import com.mapbox.services.android.navigation.v5.routeprogress.ProgressChangeListener;
//import com.mapbox.services.android.navigation.v5.routeprogress.RouteProgress;
//
//import java.lang.ref.WeakReference;
//import java.util.List;
//
//import dr.doctorathome.R;
//import dr.doctorathome.domain.executor.impl.ThreadExecutor;
//import dr.doctorathome.network.model.DoctorNewLocationBody;
//import dr.doctorathome.network.model.RestVisitDetailsResponse;
//import dr.doctorathome.network.model.ServerCommonResponse;
//import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
//import dr.doctorathome.presentation.presenters.VisitPresenter;
//import dr.doctorathome.presentation.presenters.impl.VisitPresenterImpl;
//import dr.doctorathome.presentation.ui.helpers.BasicActivity;
//import dr.doctorathome.presentation.ui.helpers.CustomNavigationLauncher;
//import dr.doctorathome.threading.MainThreadImpl;
//import dr.doctorathome.utils.CommonUtil;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import timber.log.Timber;
//
//public class NavigationToPatientActivity extends BasicActivity implements
//        PermissionsListener, VisitPresenter.View {
//
//    private VisitPresenter visitPresenter;
//
//    private MapView mapView;
//    private PermissionsManager permissionsManager;
//    private LocationComponent locationComponent;
//    private MapboxMap mapboxMap;
//
//    private DirectionsRoute currentRoute;
//    private static final String TAG = "DirectionsActivity";
//    private NavigationMapRoute navigationMapRoute;
//
//    private Double destinationLat, destinationLng;
//    private LocationManager mLocationManager;
//
//    // Variables needed to add the location engine
//    private LocationEngine locationEngine;
//    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
//    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
//
//    // Variables needed to listen to location updates
//    private NavigationToPatientActivityLocationCallback callback = new NavigationToPatientActivityLocationCallback(this);
//
//    Location lastSentLocation;
//
//    private int visitId;
//
//    private boolean wentToNavigationView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, "pk.eyJ1IjoiYWJkYWxsYWhnYWJlciIsImEiOiJjandyeXY5eGgyeG54NGJuMGphbW5kNDV6In0.VuPliinnIyB8_AfagyyzUw");
//        setContentView(R.layout.activity_navigation_to_patient);
//
//        visitPresenter = new VisitPresenterImpl(ThreadExecutor.getInstance(),
//                MainThreadImpl.getInstance(),
//                this,
//                new DoctorRepositoryImpl());
//
//        destinationLat = getIntent().getDoubleExtra("lat", 0.0);
//        destinationLng = getIntent().getDoubleExtra("lng", 0.0);
//        visitId = getIntent().getIntExtra("visitId", -1);
//
//
//        mapView = findViewById(R.id.mapView);
//        mapView.onCreate(savedInstanceState);
//        mapView.getMapAsync(mapboxMap -> {
//            NavigationToPatientActivity.this.mapboxMap = mapboxMap;
//
//            mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
//
//                // Map is set up and the style has loaded. Now you can add data or make other map adjustments
//                enableLocationComponent(style);
//
//            });
//        });
//
//    }
//
//    /**
//     * Set up the LocationEngine and the parameters for querying the device's location
//     */
//    @SuppressLint("MissingPermission")
//    private void initLocationEngine() {
//        locationEngine = LocationEngineProvider.getBestLocationEngine(this);
//
//        LocationEngineRequest request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
//                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
//                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();
//
//        locationEngine.requestLocationUpdates(request, callback, getMainLooper());
//        locationEngine.getLastLocation(callback);
//    }
//
//    private void startDrawingRoute() {
//        Point destinationPoint = Point.fromLngLat(destinationLng, destinationLat);
//        Location location = getLastKnownLocation();
//        if (location != null) {
//            Point originPoint = Point.fromLngLat(location.getLongitude(),
//                    location.getLatitude());
//
//            Timber.d("Origin :: %f, %f", location.getLongitude(), location.getLatitude());
//            Timber.d("destinationPoint :: %f, %f", destinationLng, destinationLat);
//
//            getRoute(originPoint, destinationPoint);
//        } else {
//            Toast.makeText(this, R.string.open_gps_message, Toast.LENGTH_LONG).show();
//        }
//    }
//
//    @SuppressLint("MissingPermission")
//    private Location getLastKnownLocation() {
//        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
//        List<String> providers = mLocationManager.getProviders(true);
//        Location bestLocation = null;
//        for (String provider : providers) {
//            Location l = mLocationManager.getLastKnownLocation(provider);
//            if (l == null) {
//                continue;
//            }
//            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
//                // Found best last known location: %s", l);
//                bestLocation = l;
//            }
//        }
//        return bestLocation;
//    }
//
//    private void getRoute(Point origin, Point destination) {
//        NavigationRoute.builder(this)
//                .accessToken(Mapbox.getAccessToken())
//                .origin(origin)
//                .destination(destination)
//                .build()
//                .getRoute(new Callback<DirectionsResponse>() {
//                    @Override
//                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
//                        // You can get the generic HTTP info about the response
//                        Log.d(TAG, "Response code: " + response.code());
//                        if (response.body() == null) {
//                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
//                            Toast.makeText(NavigationToPatientActivity.this, R.string.no_routes_found, Toast.LENGTH_LONG).show();
//                            findViewById(R.id.navigateFAB).setVisibility(View.GONE);
//                            return;
//                        } else if (response.body().routes().size() < 1) {
//                            Log.e(TAG, "No routes found");
//                            return;
//                        }
//
//                        currentRoute = response.body().routes().get(0);
//
//                        // Draw the route on the map
//                        if (navigationMapRoute != null) {
//                            navigationMapRoute.removeRoute();
//                        } else {
//                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
//                        }
//                        MapboxNavigation mapboxNavigation = new MapboxNavigation(NavigationToPatientActivity.this
//                                , Mapbox.getAccessToken());
//                        mapboxNavigation.addProgressChangeListener((location, routeProgress) -> {
//                                    Timber.d("LocationChange New Point In Navigation : " + location.getLatitude() + ", " +
//                                            location.getLongitude());
//                                    Toast.makeText(NavigationToPatientActivity.this, "Nav : " + location.getLatitude() + ", " +
//                                            location.getLongitude(), Toast.LENGTH_SHORT).show();
//                                }
//
//                        );
//                        navigationMapRoute.addRoute(currentRoute);
//                        navigationMapRoute.addProgressChangeListener(mapboxNavigation);
//                    }
//
//                    @Override
//                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
//                        Log.e(TAG, "Error: " + throwable.getMessage());
//                    }
//                });
//    }
//
//    @SuppressWarnings({"MissingPermission"})
//    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
//        // Check if permissions are enabled and if not request
//        if (PermissionsManager.areLocationPermissionsGranted(this)) {
//
//            // Get an instance of the component
//            locationComponent = mapboxMap.getLocationComponent();
//
//            // Activate with options
//            locationComponent.activateLocationComponent(
//                    LocationComponentActivationOptions.builder(this, loadedMapStyle).build());
//
//            // Enable to make component visible
//            locationComponent.setLocationComponentEnabled(true);
//
//            // Set the component's camera mode
//            locationComponent.setCameraMode(CameraMode.TRACKING);
//
//            // Set the component's render mode
//            locationComponent.setRenderMode(RenderMode.COMPASS);
//
//            initLocationEngine();
//
//            startDrawingRoute();
//
//            findViewById(R.id.navigateFAB).setOnClickListener(view -> {
//                NavigationLauncherOptions options = NavigationLauncherOptions.builder()
//                        .directionsRoute(currentRoute)
//                        //                        .shouldSimulateRoute(true)
//                        .build();
//                // Call this method with Context from within an Activity
//                CustomNavigationLauncher.startNavigation(NavigationToPatientActivity.this, options, 100);
//
//            });
//        } else {
//            permissionsManager = new PermissionsManager(this);
//            permissionsManager.requestLocationPermissions(this);
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    @Override
//    public void onExplanationNeeded(List<String> permissionsToExplain) {
//        Toast.makeText(this, "user_location_permission_explanation", Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onPermissionResult(boolean granted) {
//        if (granted) {
//            mapboxMap.getStyle(style -> enableLocationComponent(style));
//        } else {
//            Toast.makeText(this, "user_location_permission_not_granted", Toast.LENGTH_LONG).show();
//            finish();
//        }
//    }
//
//    @Override
//    @SuppressWarnings({"MissingPermission"})
//    public void onStart() {
//        super.onStart();
//        mapView.onStart();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mapView.onResume();
//        wentToNavigationView = false;
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        mapView.onPause();
//        wentToNavigationView = true;
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        mapView.onStop();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mapView.onLowMemory();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mapView.onDestroy();
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
//    }
//
//    @Override
//    public void visitDetailsRetrievedSuccessfully(RestVisitDetailsResponse restVisitDetailsResponse) {
//        // No need here
//    }
//
//    @Override
//    public void visitAcceptedSuccessfully() {
//        // No need here
//    }
//
//    @Override
//    public void visitRejectedSuccessfully() {
//        // No need here
//    }
//
//    @Override
//    public void visitCanceledSuccessfully() {
//        // No need here
//    }
//
//    @Override
//    public void sessionStartedSuccessfully() {
//        // No need here
//    }
//
//    @Override
//    public void sessionEndedSuccessfully() {
//        // No need here
//    }
//
//    @Override
//    public void ratePatientDoneSuccessfully() {
//        // No need here
//    }
//
//    @Override
//    public void doctorNewLocationSavedSuccessfully(ServerCommonResponse serverCommonResponse) {
////        Toast.makeText(this, serverCommonResponse.getRspBody(), Toast.LENGTH_SHORT).show();
//        if (serverCommonResponse.getRspBody().equals("Arrived")) {
//            if(wentToNavigationView){
//                finishActivity(100);
//            }
//            Dialog dialog = CommonUtil.getInfoDialog(this, false, true,
//                    getString(R.string.doctor_arrived_message), false,
//                    "",
//                    R.drawable.ic_check_done, getString(R.string.done), () -> {
//                        setResult(RESULT_OK);
//                       // finish();
//                    });
//
//            dialog.show();
//            finish();
//        }
//    }
//
//    @Override
//    public void isDoctorArrivedSuccessfully(ServerCommonResponse serverCommonResponse) {
//        // no need here
//    }
//
//    @Override
//    public void showProgress(String message) {
//        // No need here
//    }
//
//    @Override
//    public void hideProgress() {
//        // No need here
//    }
//
//    @Override
//    public void showError(String message, boolean hideView) {
//        // No need here
//    }
//
//    private static class NavigationToPatientActivityLocationCallback
//            implements LocationEngineCallback<LocationEngineResult> {
//
//        private final WeakReference<NavigationToPatientActivity> activityWeakReference;
//
//        NavigationToPatientActivityLocationCallback(NavigationToPatientActivity activity) {
//            this.activityWeakReference = new WeakReference<>(activity);
//        }
//
//        /**
//         * The LocationEngineCallback interface's method which fires when the device's location has changed.
//         *
//         * @param result the LocationEngineResult object which has the last known location within it.
//         */
//        @Override
//        public void onSuccess(LocationEngineResult result) {
//            NavigationToPatientActivity activity = activityWeakReference.get();
//
//            if (activity != null) {
//                Location location = result.getLastLocation();
//
//                if (location == null) {
//                    return;
//                }
//
//                if (activity.lastSentLocation == null) {
//                    activity.lastSentLocation = location;
//                    activity.visitPresenter.updateDoctorLocation(new DoctorNewLocationBody(CommonUtil.commonSharedPref()
//                            .getInt("doctorId", -1), activity.visitId, location.getLatitude(), location.getLongitude()));
////                    Toast.makeText(activity, "Map : " + result.getLastLocation().getLatitude() + ", " +
////                            result.getLastLocation().getLongitude(), Toast.LENGTH_SHORT).show();
//                }
//
//                // Create a Toast which displays the new location's coordinates
//                //                Toast.makeText(activity, String.format(activity.getString(R.string.new_location),
//                //                        String.valueOf(result.getLastLocation().getLatitude()), String.valueOf(result.getLastLocation().getLongitude())),
//                //                        Toast.LENGTH_SHORT).show();
//                if ((activity.lastSentLocation.getLatitude() != location.getLatitude()
//                        || activity.lastSentLocation.getLongitude()
//                        != location.getLongitude())
//                        && activity.lastSentLocation.distanceTo(location) > 10) {
//                    Timber.d("LocationChange New Point : " + result.getLastLocation().getLatitude() + ", " +
//                            result.getLastLocation().getLongitude());
////                    Toast.makeText(activity, "Map : " + result.getLastLocation().getLatitude() + ", " +
////                            result.getLastLocation().getLongitude(), Toast.LENGTH_SHORT).show();
//                    Log.d(TAG, "onSuccess:distance "+activity.lastSentLocation.distanceTo(location));
//                    activity.lastSentLocation = location;
//                    activity.visitPresenter.updateDoctorLocation(new DoctorNewLocationBody(CommonUtil.commonSharedPref()
//                            .getInt("doctorId", -1), activity.visitId, location.getLatitude(), location.getLongitude()));
//                }
//                // Pass the new location to the Maps SDK's LocationComponent
//                if (activity.mapboxMap != null && result.getLastLocation() != null) {
//                    activity.mapboxMap.getLocationComponent().forceLocationUpdate(result.getLastLocation());
//                }
//            }
//        }
//
//        /**
//         * The LocationEngineCallback interface's method which fires when the device's location can not be captured
//         *
//         * @param exception the exception message
//         */
//        @Override
//        public void onFailure(@NonNull Exception exception) {
//            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
//            NavigationToPatientActivity activity = activityWeakReference.get();
//            if (activity != null) {
//                Toast.makeText(activity, exception.getLocalizedMessage(),
//                        Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//}
