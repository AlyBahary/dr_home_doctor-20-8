package dr.doctorathome.presentation.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.NewMedicinModel;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;

public class MedicineOutOfSystemActivity extends BasicActivity {

    @BindView(R.id.tvToolbarTitle)
    AppCompatTextView tvToolbarTitle;
    @BindView(R.id.ivBack)
    AppCompatImageButton ivBack;
    @BindView(R.id.ivNewNotification)
    AppCompatImageView ivNewNotification;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.etMedicineName)
    EditText etMedicineName;
    @BindView(R.id.etMedicineDose)
    EditText etMedicineDose;
    @BindView(R.id.spinnerMedicineUnit)
    Spinner spinnerMedicineUnit;
    @BindView(R.id.etMedicineRoute)
    EditText etMedicineRoute;
    @BindView(R.id.etMedicineFrequency)
    EditText etMedicineFrequency;
    @BindView(R.id.etMedicineDuration)
    EditText etMedicineDuration;
    @BindView(R.id.spinnerMedicineDurationUnit)
    Spinner spinnerMedicineDurationUnit;
    @BindView(R.id.etMedicinePNRIndecations)
    EditText etMedicinePNRIndecations;
    @BindView(R.id.etMedicinePNROrderInstructions)
    EditText etMedicinePNROrderInstructions;
    @BindView(R.id.BackBtn)
    Button BackBtn;
    @BindView(R.id.NextBtn)
    Button NextBtn;
    @BindView(R.id.btnView)
    LinearLayout btnView;
    ArrayList<String> units;
    String unit;
    ArrayList<String> durantionUnits;
    String durationUnit;

    NewMedicinModel newMedicinModel = new NewMedicinModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_out_of_system);
        ButterKnife.bind(this);
        ivBack.setOnClickListener(v -> {
            finish();
        });
        tvToolbarTitle.setText(getString(R.string.add_drug));
        ivNewNotification.setVisibility(View.GONE);
        setUnitSpinner();
        setDurationUnitSpinner();
        NextBtn.setOnClickListener(v -> {
            newMedicinModel.setName(etMedicineName.getText().toString());
            newMedicinModel.setDose(etMedicineDose.getText().toString());
            newMedicinModel.setUnit(unit);
            newMedicinModel.setRoot(etMedicineRoute.getText().toString());
            newMedicinModel.setFrequency(etMedicineFrequency.getText().toString());
//            newMedicinModel.(etMedicineDuration.getText().toString());
            newMedicinModel.setDurationUnit(durationUnit);
            newMedicinModel.setCategory(new NewMedicinModel.CategoryModel(1));
            newMedicinModel.setActive(true);


            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", newMedicinModel);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });
    }

    private void setUnitSpinner() {
        // Spinner click listener
        spinnerMedicineUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                unit = units.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        units = new ArrayList<String>();
        units.add("PACKET");
        units.add("MM");
        units.add("GRAM");
        units.add("MG");


        unit = "PACKET";
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, units) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextColor(
                        getResources().getColorStateList(R.color.ef_grey)
                );
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerMedicineUnit.setAdapter(dataAdapter);


    }
    private void setDurationUnitSpinner() {
        // Spinner click listener
        spinnerMedicineDurationUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                durationUnit = durantionUnits.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        durantionUnits = new ArrayList<String>();
        durantionUnits.add("DAY");
        durantionUnits.add("WEEK");
        durantionUnits.add("MONTH");
        durantionUnits.add("YEAR");
        durantionUnits.add("HOUR");


        durationUnit = "DAY";
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, durantionUnits) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextColor(
                        getResources().getColorStateList(R.color.ef_grey)
                );
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerMedicineDurationUnit.setAdapter(dataAdapter);


    }
}
