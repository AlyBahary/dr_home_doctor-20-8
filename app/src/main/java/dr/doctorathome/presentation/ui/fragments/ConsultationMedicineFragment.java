package dr.doctorathome.presentation.ui.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.network.model.NewMedicinModel;
import dr.doctorathome.presentation.adapters.EPrescriptionMedicineAdapter;
import dr.doctorathome.presentation.adapters.EPrescriptionNewMedicineAdapter;
import dr.doctorathome.presentation.adapters.MedicinesAdapter;
import dr.doctorathome.presentation.ui.activities.ConsultationPrescriptionActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultationMedicineFragment extends Fragment {

    public static final String MEDICIN_FILTER = "MEDICIN_FILTER";
    public static final String NEW_MEDICIN_FILTER = "NEW_MEDICIN_FILTER";
    @BindView(R.id.addMedView)
    CardView addMedView;
    @BindView(R.id.noDataImage)
    ImageView noDataImage;
    @BindView(R.id.noDataTxt)
    TextView noDataTxt;
    BottomSheetAddMdeicineConsultation bottomSheetAddMdeicineConsultation = new BottomSheetAddMdeicineConsultation();
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.rvNewMed)
    RecyclerView rvNewMed;
    EPrescriptionMedicineAdapter medicinesAdapter;
    EPrescriptionNewMedicineAdapter newMedicineAdapter;

    public static ConsultationMedicineFragment newInstace() {
        return new ConsultationMedicineFragment();
    }

    public ConsultationMedicineFragment() {
        // Required empty public constructor
    }

    public void setMedicins(ArrayList<MedicineModel> medicineModels) {
        if (medicineModels == null)
            return;
        noDataImage.setVisibility(View.GONE);
        noDataTxt.setVisibility(View.GONE);
        medicinesAdapter.setMedicineModels(medicineModels);
        medicinesAdapter.notifyDataSetChanged();

    }

    public ArrayList<HashMap<String, Integer>> getMedicinsIds() {
        ArrayList<HashMap<String, Integer>> ids = new ArrayList<>();
        HashMap<String,Integer> item;
        for (int i = 0; i < medicinesAdapter.getMedicineModels().size(); i++) {
            item=new HashMap<>();
            Log.d("TAG", "getMedicinsIds: "+medicinesAdapter.getMedicineModels().size());
            Log.d("TAG", "getMedicinsIds: "+medicinesAdapter.getMedicineModels().get(i).getId());
            item.put("id",medicinesAdapter.getMedicineModels().get(i).getId());
            item.put("quantity",medicinesAdapter.getMedicineModels().get(i).getQuantity());

            ids.add(item);
        }

        return ids;
    }

    public ArrayList<NewMedicinModel> getNewMedicinModels() {
        return newMedicineAdapter.getMedicineModels();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_consultation_medicine, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(someBroadcastReceiver,
                new IntentFilter(MEDICIN_FILTER));

        addMedView.setOnClickListener(v -> {
            bottomSheetAddMdeicineConsultation.show(getFragmentManager(), ConsultationMedicineFragment.class.getSimpleName());
        });

        medicinesAdapter = new EPrescriptionMedicineAdapter(getContext());
        rv.setAdapter(medicinesAdapter);
        newMedicineAdapter = new EPrescriptionNewMedicineAdapter(getContext());
        rvNewMed.setAdapter(newMedicineAdapter);
        setMedicins(((ConsultationPrescriptionActivity) getActivity()).getMedicine());
    }

    private BroadcastReceiver someBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //TODO extract extras from intent
            Log.d("TAG", "broadCas: ");

            if (intent.getBooleanExtra("IS_NEW", false)) {
                NewMedicinModel medicineModel = (NewMedicinModel) intent.getSerializableExtra("NEW_MED");
                newMedicineAdapter.addMedicineModels(medicineModel);
                newMedicineAdapter.notifyDataSetChanged();
                noDataImage.setVisibility(View.GONE);
                noDataTxt.setVisibility(View.GONE);
                Log.d("TAG", "broadCas: ");

            } else {
                MedicineModel medicineModel = (MedicineModel) intent.getSerializableExtra("MedModel");
                medicinesAdapter.addMedicineModels(medicineModel);
                medicinesAdapter.notifyDataSetChanged();
                noDataImage.setVisibility(View.GONE);
                noDataTxt.setVisibility(View.GONE);
            }
        }
    };


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(someBroadcastReceiver);
        super.onDestroy();
    }
}
