package dr.doctorathome.presentation.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.UserModel;

public class Step3Fragment extends Fragment {

    @BindView(R.id.etSaudiId)
    AppCompatEditText etSaudiId;

    @BindView(R.id.etInsurance)
    AppCompatEditText etInsurance;

    @BindView(R.id.etBankName)
    AppCompatEditText etBankName;

    @BindView(R.id.etIban)
    AppCompatEditText etIban;

    public Step3Fragment() {
        // Required empty public constructor
    }

    public static Step3Fragment newInstance() {
        return new Step3Fragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_step3, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public boolean validateForm(){
        if (etSaudiId.getText().toString().trim().isEmpty()) {
            etSaudiId.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etInsurance.getText().toString().trim().isEmpty()) {
            etInsurance.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etBankName.getText().toString().trim().isEmpty()) {
            etBankName.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etIban.getText().toString().trim().isEmpty()) {
            etIban.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etIban.getText().toString().length()!=24) {
            etIban.setError(getString(R.string.Iban_should_be_24_charachter));
            return false;
        }

        return true;
    }

    public UserModel fillUserModelData(UserModel userModel){
        userModel.setSaudiId(etSaudiId.getText().toString());
        userModel.setInsuranceNumber(etInsurance.getText().toString());
        userModel.setBank(etBankName.getText().toString());
        userModel.setIban(etIban.getText().toString());
        return userModel;
    }
}
