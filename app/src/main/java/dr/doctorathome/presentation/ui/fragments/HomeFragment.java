package dr.doctorathome.presentation.ui.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.Service.LocationService;
import dr.doctorathome.config.Config;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.DashboardModel;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.adapters.DashBoardAdapter;
import dr.doctorathome.presentation.adapters.HomeDoctorVisitRequestsAdapter;
import dr.doctorathome.presentation.adapters.HomeOnlineConsultationRequestsAdapter;
import dr.doctorathome.presentation.adapters.ImmediateConsAdapter;
import dr.doctorathome.presentation.presenters.UpdateLocationPresenter;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.impl.UserDataPresenterImpl;
import dr.doctorathome.presentation.ui.activities.MainActivity;
import dr.doctorathome.presentation.ui.activities.NotificationsActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;


public class HomeFragment extends Fragment implements UserDataPresenter.View {

    @BindView(R.id.rvDashBoard)
    RecyclerView rvDashBoard;
    ArrayList<DashboardModel> dashboardModels=new ArrayList<>();
    DashBoardAdapter dashBoardAdapter=new DashBoardAdapter(getContext());

    private UserDataPresenter userDataPresenter;
    private UpdateLocationPresenter updateLocationPresenter;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.homeMainScrollView)
    NestedScrollView homeMainScrollView;

    @BindView(R.id.swVisit)
    SwitchCompat swVisit;

    @BindView(R.id.doc_statues_change_progress_layout)
    RelativeLayout docStatuesChangeProgressLayout;

    @BindView(R.id.listDoctorVisit)
    RecyclerView listDoctorVisit;

    @BindView(R.id.listOnlineConsultation)
    RecyclerView listOnlineConsultation;

    @BindView(R.id.listDoctorImmediateCons)
    RecyclerView listDoctorImmediateCons;

    @BindView(R.id.btnViewAllRequests)
    Button btnViewAllRequests;

    @BindView(R.id.btnViewAll)
    Button btnViewAll;


    @BindView(R.id.ivNotification)
    AppCompatImageButton ivNotification;
    @BindView(R.id.tvSearchBox)
    EditText tvSearchBox;

    private boolean isChangingDocStatues = false;

    private boolean found = false;

    private HomeDoctorVisitRequestsAdapter homeDoctorVisitRequestsAdapter;

    private HomeOnlineConsultationRequestsAdapter homeOnlineConsultationRequestsAdapter;

    private ProgressDialog progressDialog;

    private boolean isChangingVisitStatues = false, isActive = false;

    private GoogleApiClient mGoogleApiClient;
    private FusedLocationProviderClient fusedLocationClient;

    Intent service;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("TAG", "approveImmediateCons: "+CommonUtil.commonSharedPref().getInt("doctorId",0));

        //2
        service = new Intent(getContext(), LocationService.class);
        //

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .build();

        progressDialog = new ProgressDialog(getContext());

        userDataPresenter = new UserDataPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        updateLocationPresenter = new UserDataPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        userDataPresenter.getDoctorHomePageData();

        if (!CommonUtil.commonSharedPref().getString("token", "").equals("")
                && !CommonUtil.commonSharedPref().getBoolean("userIdSaved", false)) {
            userDataPresenter.saveOneSignalUserId(CommonUtil.commonSharedPref().getString("oneSignalUserId", ""));
        }

        tvSearchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (true) {

                    found = false;
                    if (!tvSearchBox.getText().toString().trim().equals("")) {
                        for (int i = 0; i < homeDoctorVisitRequestsAdapter.getItemCount(); i++)
                            if (tvSearchBox.getText().toString().toLowerCase()
                                    .equals(homeDoctorVisitRequestsAdapter.getDoctorRequests().get(i).getPatient().getUser().getFirstName().toLowerCase())
                                    || tvSearchBox.getText().toString().toLowerCase()
                                    .equals(homeDoctorVisitRequestsAdapter.getDoctorRequests().get(i).getPatient().getUser().getLastName().toLowerCase())) {
                                listDoctorVisit.scrollToPosition(i);
                                found = true;
                                return false;

                            }

                        for (int i = 0; i < homeOnlineConsultationRequestsAdapter.getItemCount(); i++) {
                            Log.d("TTTTTTTT", "onEditorAction: " + homeOnlineConsultationRequestsAdapter.getConsultationRequests().get(i).getPatient().getUser().getFirstName().toLowerCase());
                            if (tvSearchBox.getText().toString().toLowerCase()
                                    .equals(homeOnlineConsultationRequestsAdapter.getConsultationRequests().get(i).getPatient().getUser().getFirstName().toLowerCase())
                                    || tvSearchBox.getText().toString().toLowerCase()
                                    .equals(homeOnlineConsultationRequestsAdapter.getConsultationRequests().get(i).getPatient().getUser().getLastName().toLowerCase())) {
                                listOnlineConsultation.scrollToPosition(i);
                                found = true;
                                Log.d("TTTTT", "onEditorAction: " + found);
                                return false;

                            }
                        }
                        Log.d("TTTTT", "onEditorAction:2 " + found);

                        if (!found) {
                            Toast toast = Toast.makeText(getContext(), tvSearchBox.getText().toString() + " " + getString(R.string.not_Found), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            TextView vv = (TextView) toast.getView().findViewById(android.R.id.message);
                            vv.setTextColor(getResources().getColor(R.color.ef_white));
                            View view = toast.getView();
                            view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);

                            toast.show();

                        }

                    }
                }
                return false;
            }
        });

        swVisit.setOnClickListener(view1 -> {
            if (swVisit.isChecked()) {
                requestLocation();
            } else {
                isChangingDocStatues = true;
                userDataPresenter.changeDoctorStatuesData(new RestChangeDoctorStatuesBody("OFFLINE"
                        , CommonUtil.commonSharedPref().getInt("doctorId", -1)));
                isActive = false;
                getActivity().stopService(service);

            }
        });

        btnViewAllRequests.setOnClickListener(view1 -> ((MainActivity) getActivity())
                .getBottomNavigationView().setSelectedItemId(R.id.menuHistory)
        );

        btnViewAll.setOnClickListener(view1 -> ((MainActivity) getActivity())
                .getBottomNavigationView().setSelectedItemId(R.id.menuConsultation)
        );
        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationsActivity.class));
            }
        });

        //        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        //        listener = new LocationListener() {
        //            @Override
        //            public void onLocationChanged(Location location) {
        //                //                t.append("\n " + location.getLongitude() + " " + location.getLatitude());
        //                Timber.i(location.getLongitude() + " " + location.getLatitude());
        //                locationManager.removeUpdates(listener);
        //                isChangingDocStatues = true;
        //                userDataPresenter.changeDoctorStatuesData(new RestChangeDoctorStatuesBody("ONLINE"
        //                        , CommonUtil.commonSharedPref().getInt("doctorId", -1),
        //                        location.getLatitude(), location.getLongitude()));
        //            }
        //
        //            @Override
        //            public void onStatusChanged(String s, int i, Bundle bundle) {
        //
        //            }
        //
        //            @Override
        //            public void onProviderEnabled(String s) {
        //
        //            }
        //
        //            @Override
        //            public void onProviderDisabled(String s) {
        //                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        //                startActivity(i);
        //            }
        //        };
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 2) {
            userDataPresenter.getDoctorHomePageData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.i("Home On Resume..");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                requestLocation();
                break;
            default:
                break;
        }
    }

    void requestLocation() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION
                                , Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        } else {
            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();

            swVisit.setVisibility(View.INVISIBLE);
            docStatuesChangeProgressLayout.setVisibility(View.VISIBLE);
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            Timber.i(location.getLongitude() + " " + location.getLatitude());
                            isChangingDocStatues = true;
                            userDataPresenter.changeDoctorStatuesData(new RestChangeDoctorStatuesBody("ONLINE"
                                    , CommonUtil.commonSharedPref().getInt("doctorId", -1),
                                    location.getLatitude(), location.getLongitude()));
                            isActive = true;

                            getActivity().startService(service);


                        } else {
                            swVisit.setVisibility(View.VISIBLE);
                            swVisit.setChecked(false);
                            docStatuesChangeProgressLayout.setVisibility(View.INVISIBLE);
                            //open location here

                            Toast toast = Toast.makeText(getContext(), "" + getString(R.string.please_open_gps), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                            v.setTextColor(getResources().getColor(R.color.ef_white));
                            View view = toast.getView();
                            view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
                            toast.show();

                        }
                    });

            //            locationManager.requestLocationUpdates("gps", 5000, 0, listener);
        }
    }

    @Override
    public void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse) {
        // no need here
    }

    @Override
    public void doctorDataUpdatedSuccessfully(CommonResponse commonResponse) {

        // no need here
    }

    @Override
    public void doctorStatuesSuccessfully(CommonResponse commonResponse) {
        isChangingDocStatues = false;

        Toast toast = Toast.makeText(getContext(), "" + getString(R.string.status_changed_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
        toast.show();

        //Toast.makeText(getActivity(), R.string.status_changed_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        // no need here
    }

    public void acceptRequest(int visitId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_accept_visit), false,
                "",
                R.drawable.ic_check_done, getString(R.string.accept), getString(R.string.cancel), () -> {
                    isChangingVisitStatues = true;
                    userDataPresenter.acceptVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.CONFIRMED));
                });

        dialog.show();
    }

    public void rejectRequest(int visitId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_reject_visit), false,
                "",
                R.drawable.ic_warning, getString(R.string.reject), getString(R.string.cancel), () -> {
                    isChangingVisitStatues = true;
                    userDataPresenter.rejectVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.REJECTED));
                });

        dialog.show();
    }

    public void cancelRequest(int visitId) {
        Dialog dialog = CommonUtil.getConfirmation2BtnsDialog(getActivity(), true, true,
                getString(R.string.do_you_want_to_cancel_visit), false,
                "",
                R.drawable.ic_warning, getString(R.string.yes), getString(R.string.no), () -> {
                    isChangingVisitStatues = true;
                    userDataPresenter.cancelVisit(new ChangeVisitStatuesBody(visitId, RequestStatues.CANCELLED));
                });

        dialog.show();
    }

    @Override
    public void visitAcceptedSuccessfully() {
        isChangingVisitStatues = false;
        Toast toast = Toast.makeText(getContext(), "" + getString(R.string.visit_accepted_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);

        toast.show();

        // Toast.makeText(getContext(), R.string.visit_accepted_success, Toast.LENGTH_LONG).show();
        userDataPresenter.getDoctorHomePageData();
    }

    @Override
    public void visitRejectedSuccessfully() {
        isChangingVisitStatues = false;

        Toast toast = Toast.makeText(getContext(), "" + getString(R.string.visit_rejected_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);

        toast.show();

        //  Toast.makeText(getContext(), R.string.visit_rejected_success, Toast.LENGTH_LONG).show();
        userDataPresenter.getDoctorHomePageData();
    }

    @Override
    public void visitCancelledSuccessfully() {
        isChangingVisitStatues = false;
        Toast toast = Toast.makeText(getContext(), "" + getString(R.string.visit_cancelled_success), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.ef_white));
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
        toast.show();

        //   Toast.makeText(getContext(), R.string.visit_cancelled_success, Toast.LENGTH_LONG).show();
        userDataPresenter.getDoctorHomePageData();
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        SharedPreferences.Editor editor = CommonUtil.commonSharedPref().edit();
        editor.putBoolean("userIdSaved", true);
        editor.apply();
        editor.commit();
    }

    @Override
    public void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse) {
        if (restHomePageResponse.getDashboardData() != null) {

            rvDashBoard.setAdapter(dashBoardAdapter);
            dashboardModels.clear();
            dashboardModels.trimToSize();
            //sessions Data
            dashboardModels.add(new DashboardModel(getString(R.string.Sessions),restHomePageResponse.getDashboardData().getActiveSessions()+"",getString(R.string.text_confirmed)));
            dashboardModels.add(new DashboardModel(getString(R.string.Sessions),restHomePageResponse.getDashboardData().getPendingSessions()+"",getString(R.string.text_pending)));
            dashboardModels.add(new DashboardModel(getString(R.string.Sessions),restHomePageResponse.getDashboardData().getCancelledSessions()+"",getString(R.string.text_canceled)));
            dashboardModels.add(new DashboardModel(getString(R.string.Sessions),restHomePageResponse.getDashboardData().getCompletedSessions()+"",getString(R.string.text_completed)));
            // visits Data
            dashboardModels.add(new DashboardModel(getString(R.string.visits),restHomePageResponse.getDashboardData().getActiveVisits()+"",getString(R.string.text_confirmed)));
            dashboardModels.add(new DashboardModel(getString(R.string.visits),restHomePageResponse.getDashboardData().getPendingVisits()+"",getString(R.string.text_pending)));
            dashboardModels.add(new DashboardModel(getString(R.string.visits),restHomePageResponse.getDashboardData().getCancelledVisits()+"",getString(R.string.text_canceled)));
            dashboardModels.add(new DashboardModel(getString(R.string.visits),restHomePageResponse.getDashboardData().getCompletedVisits()+"",getString(R.string.text_completed)));

            LinearLayoutManager linearLayoutManager= new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false);
            rvDashBoard.setLayoutManager(linearLayoutManager);
            Log.d("TTTTT", "doctorHomePageDataRetrievedSuccessfully: "+dashboardModels.size());
            dashBoardAdapter.setDashboardModels(dashboardModels);
            dashBoardAdapter.notifyDataSetChanged();
        }

        if (restHomePageResponse.getWarningMessage() != null && !restHomePageResponse.getWarningMessage().equals("")) {

            Toast toast = Toast.makeText(getContext(), "  " + restHomePageResponse.getWarningMessage() + " ", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(getResources().getColor(R.color.ef_white));
            View view = toast.getView();
            view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
            toast.show();

        }
        if (restHomePageResponse.getActivityStatus() == null ||
                (!restHomePageResponse.getActivityStatus().equals("ONLINE")
                        && !restHomePageResponse.getActivityStatus().equals("OFFLINE"))) {
            swVisit.setChecked(false);
        } else {
            swVisit.setChecked(restHomePageResponse.getActivityStatus().equals("ONLINE") ? true : false);
        }
        if (swVisit.isChecked()) {
            isActive = true;


//            getActivity().startService(service);

        }
        //obtainLocation();
        homeOnlineConsultationRequestsAdapter =
                new HomeOnlineConsultationRequestsAdapter(
                        restHomePageResponse.getConsultationRequests()
                        , getActivity());
        listOnlineConsultation.setAdapter(homeOnlineConsultationRequestsAdapter);

        homeDoctorVisitRequestsAdapter = new HomeDoctorVisitRequestsAdapter(restHomePageResponse.getRequests(), getActivity());
        homeDoctorVisitRequestsAdapter.setCallerFragment(this);
        listDoctorVisit.setAdapter(homeDoctorVisitRequestsAdapter);
        listDoctorVisit.setNestedScrollingEnabled(false);

        ImmediateConsAdapter immediateConsAdapter=new ImmediateConsAdapter(restHomePageResponse.getImmediateRequests(),getContext(),position -> {
            approveImmediateCons(restHomePageResponse.getImmediateRequests().get(position).getId()+"");
        });
        listDoctorImmediateCons.setAdapter(immediateConsAdapter);

        if (restHomePageResponse.getTotalUnreadMessages() != null)
            Config.notificationCount = restHomePageResponse.getTotalUnreadMessages();
        if (restHomePageResponse.getTotalUnreadMessages() > 0)
            ivNotification.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress(String message) {
        if (!isChangingDocStatues && !isChangingVisitStatues) {
            progressLayout.setVisibility(View.VISIBLE);
            homeMainScrollView.setVisibility(View.GONE);
        } else if (isChangingDocStatues) {
            swVisit.setVisibility(View.INVISIBLE);
            docStatuesChangeProgressLayout.setVisibility(View.VISIBLE);
        } else if (isChangingVisitStatues) {
            CommonUtil.showProgressDialog(progressDialog, message);
        }
    }

    @Override
    public void hideProgress() {
        if (!isChangingDocStatues && !isChangingVisitStatues) {
            progressLayout.setVisibility(View.GONE);
            homeMainScrollView.setVisibility(View.VISIBLE);
        } else if (isChangingDocStatues) {
            swVisit.setVisibility(View.VISIBLE);
            docStatuesChangeProgressLayout.setVisibility(View.GONE);
        } else if (isChangingVisitStatues) {
            CommonUtil.dismissProgressDialog(progressDialog);
        }
    }

    @Override
    public void showError(String message, boolean hideView) {
        try {

            Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(getResources().getColor(R.color.ef_white));
            View view = toast.getView();
            view.setBackgroundResource(R.drawable.shape_btn_rounded_grey);
            toast.show();

//            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            if (isChangingDocStatues) {
                swVisit.setChecked(!swVisit.isChecked());
                isChangingDocStatues = false;
            }

            if (isChangingVisitStatues) {
                isChangingVisitStatues = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void obtainLocation() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);


        // getting GPS status
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        String provider;
        if (!isGPSEnabled) {
            provider = LocationManager.NETWORK_PROVIDER;
            Log.d("TTTTTTTTTTTTTT", "onMapReady:network prodvider " + provider);
        } else {
            provider = LocationManager.GPS_PROVIDER;
            Log.d("TTTTTTTTTTTTTT", "onMapReady:GPS prodvider " + provider);

        }
        if (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(provider, 0, 100, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                if (isActive) {
                    updateLocation((new RestChangeDoctorStatuesBody("ONLINE"
                            , CommonUtil.commonSharedPref().getInt("doctorId", -1),
                            location.getLatitude(), location.getLongitude())));
                    Log.d("TTTTTTTTTTTTTT", "onLocationChanged: " + location);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });
    }

    private void updateLocation(RestChangeDoctorStatuesBody restChangeDoctorStatuesBody) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.changeDoctorStatues(restChangeDoctorStatuesBody, CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                if (response.isSuccessful()) {
                    Log.d("TTTTTTTTTTTTTT", "onResponse: " + response);
                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {

            }
        });

    }

    private void approveImmediateCons(String id){
        progressLayout.setVisibility(View.VISIBLE);
        homeMainScrollView.setVisibility(View.GONE);
        HashMap<String,String> map=new HashMap<>();
        map.put("doctorId",CommonUtil.commonSharedPref().getInt("doctorId",0)+"");
        map.put("quickSellId",id);
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.approveImmediateCons(map,CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()&&response.body().get("rspBody").equals("true"))
                    Toast.makeText(getContext(), getString(R.string.request_accepted_success), Toast.LENGTH_SHORT).show();
                progressLayout.setVisibility(View.GONE);
                homeMainScrollView.setVisibility(View.VISIBLE);
                userDataPresenter.getDoctorHomePageData();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                homeMainScrollView.setVisibility(View.VISIBLE);

            }
        });
    }
}
