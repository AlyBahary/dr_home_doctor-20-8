package dr.doctorathome.presentation.ui.helpers;

import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.presentation.ui.activities.CallRequestRecivingActivity;
import dr.doctorathome.presentation.ui.activities.NotificationsActivity;
import dr.doctorathome.presentation.ui.activities.OnlineConsultationDetailsActivity;
import dr.doctorathome.presentation.ui.activities.SupportMessageReplyActivity;
import dr.doctorathome.presentation.ui.activities.VisitDetailsActivity;

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler, OneSignal.NotificationReceivedHandler {
    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        //        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        String actionType;
        Integer id;

        if (data != null) {
            actionType = data.optString("actionType", null);
            id = data.optInt("targetId");
            if (actionType != null && id != null) {
                if (actionType.equals("VIEW_VISIT_ACTION")) {
                    Intent goToVisitDetails = new Intent(AndroidApplication.getAppContext(), VisitDetailsActivity.class);
                    goToVisitDetails.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    goToVisitDetails.putExtra("visitId", id);
                    AndroidApplication.getAppContext().startActivity(goToVisitDetails);
                } else if (actionType.equals("VIEW_CONTACTUS_ACTION")) {
                    Intent goToSupportMessageDetails = new Intent(AndroidApplication.getAppContext(), SupportMessageReplyActivity.class);
                    goToSupportMessageDetails.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    goToSupportMessageDetails.putExtra("supportMessageId", id);
                    AndroidApplication.getAppContext().startActivity(goToSupportMessageDetails);
                } else if (actionType.equals("VIEW_ONLINE_CONSULTATION_REQUEST_ACTION")) {
                    Intent goToOnlineConsultationDetails = new Intent(AndroidApplication.getAppContext(), OnlineConsultationDetailsActivity.class);
                    goToOnlineConsultationDetails.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    goToOnlineConsultationDetails.putExtra("id", id);
                    AndroidApplication.getAppContext().startActivity(goToOnlineConsultationDetails);
                } else if (actionType.equals("HAVE_CALL")) {
                    Intent callActivity = new Intent(AndroidApplication.getAppContext(), CallRequestRecivingActivity.class);
                    callActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    callActivity.putExtra("id", id);
                    AndroidApplication.getAppContext().startActivity(callActivity);
                } else if (data.optString("actionType").equals("VIEW_ONLINE_CONSULTATION_CALL_ACTION")) {
                    Intent callActivity = new Intent(AndroidApplication.getAppContext(), CallRequestRecivingActivity.class);
                    callActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    callActivity.putExtra("id", Integer.parseInt(data.optString("targetId")));
                    Log.d("TTTTTTTTTTTT", "notificationReceived: " + data.optString("targetId"));
                    AndroidApplication.getAppContext().startActivity(callActivity);
                } else {
                    Intent goToNotifications = new Intent(AndroidApplication.getAppContext(), NotificationsActivity.class);
                    goToNotifications.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    AndroidApplication.getAppContext().startActivity(goToNotifications);
                }
            } else {
                Intent goToNotifications = new Intent(AndroidApplication.getAppContext(), NotificationsActivity.class);
                goToNotifications.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                AndroidApplication.getAppContext().startActivity(goToNotifications);
            }
            //                Log.i("OneSignalExample", "customkey set with value: " + actionType);
        } else {
            Intent goToNotifications = new Intent(AndroidApplication.getAppContext(), NotificationsActivity.class);
            goToNotifications.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            AndroidApplication.getAppContext().startActivity(goToNotifications);
        }


    }

    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;

        Log.d("TTTTTTT", "notificationReceived: " + notification.payload.additionalData.optString("actionType"));
        Log.d("TTTTTTT", "notificationReceived:itargetIdd " + notification.payload.additionalData.optString("targetId"));
        if (data.optString("actionType").equals("VIEW_ONLINE_CONSULTATION_CALL_ACTION")) {

            Intent callActivity = new Intent(AndroidApplication.getAppContext(), CallRequestRecivingActivity.class);
            callActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            callActivity.putExtra("id", Integer.parseInt(data.optString("targetId")));
            callActivity.putExtra("NotificationID", notification.androidNotificationId);
            Log.d("TTTTTTTTTTTT", "notificationReceived: " + data.optString("targetId"));
            AndroidApplication.getAppContext().startActivity(callActivity);

            //  OneSignal.cancelNotification(notification.androidNotificationId);

        }
        Log.d("Notification->", "notificationReceived: " + notification.androidNotificationId);

    }
}
