package dr.doctorathome.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.SaveOneSignalBody;
import dr.doctorathome.network.services.CommonDataService;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanguageSelectActivity extends BasicActivity {

    @BindView(R.id.english_lng_choose)
    CardView englishLngChoose;

    @BindView(R.id.arabic_lng_choose)
    CardView arabicLngChoose;

    boolean cameFromSettings = false;
    CommonDataService commonDataService = RestClient.getClient().create(CommonDataService.class);
    @BindView(R.id.progressbarLoading)
    ProgressBar progressbarLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_select);
        ButterKnife.bind(this);


        if (getIntent().hasExtra("cameFromSettings")) {
            cameFromSettings = getIntent().getBooleanExtra("cameFromSettings", false);
        }

        arabicLngChoose.setOnClickListener(view -> {

            changeLang("ar");
            AndroidApplication.setAppLocale("ar");

        });
        englishLngChoose.setOnClickListener(view -> {
            changeLang("en");
            AndroidApplication.setAppLocale("en");
        });
    }

    private void changeLang(String language) {
        progressbarLoading.setVisibility(View.VISIBLE);
        if (!CommonUtil.commonSharedPref().getString("token", "").equals("")) {
            commonDataService
                    .saveOneSignalUserId(CommonUtil.commonSharedPref().getString("token", "")
                            , new SaveOneSignalBody(CommonUtil.commonSharedPref().getInt("userId", -1), "", language)
                    ).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                    progressbarLoading.setVisibility(View.GONE);
                    if (!cameFromSettings) {
                        startActivity(new Intent(LanguageSelectActivity.this, IntroActivity.class));
                    } else {
                        setResult(RESULT_OK);
                    }
                    finish();
                    progressbarLoading.setVisibility(View.GONE);


                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    progressbarLoading.setVisibility(View.GONE);

                }
            });

        }
        progressbarLoading.setVisibility(View.GONE);

        if (!cameFromSettings) {
            startActivity(new Intent(LanguageSelectActivity.this, IntroActivity.class));
        } else {
            setResult(RESULT_OK);
        }
        finish();

    }
}
