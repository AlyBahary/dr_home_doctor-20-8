package dr.doctorathome.presentation.ui.helpers;

import dr.doctorathome.network.model.UserData;

public interface DataBackListener {
    void dataSavedSuccessfully(UserData userData);
}
