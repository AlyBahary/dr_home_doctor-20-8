package dr.doctorathome.presentation.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.EnableDisableServiceBody;
import dr.doctorathome.network.model.RestDoctorServicesResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.Service;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.adapters.AllServicesAdapter;
import dr.doctorathome.presentation.presenters.ServicesPresenter;
import dr.doctorathome.presentation.presenters.impl.ServicesPresenterImpl;
import dr.doctorathome.presentation.ui.activities.NotificationsActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class ServicesFragment extends Fragment implements ServicesPresenter.View {
    @BindView(R.id.tivToolbarEndAction)
    AppCompatImageButton notitificationTV;
    @BindView(R.id.ivNewNotification)
    AppCompatImageView ivNewNotification;
    private ServicesPresenter servicesPresenter;

    @BindView(R.id.servicesRecyclerView)
    RecyclerView servicesRecyclerView;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;

    private AllServicesAdapter allServicesAdapter;

    public ServicesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_services, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivBack.setVisibility(View.INVISIBLE);

        tvToolbarTitle.setText(R.string.menu_services);
        if (Config.notificationCount > 0)
            ivNewNotification.setVisibility(View.VISIBLE);

        servicesPresenter = new ServicesPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

        servicesPresenter.getDoctorServices();
        notitificationTV.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), NotificationsActivity.class));

        });

    }

    public void changeServiceStatues(int position) {
        allServicesAdapter.getAllServices().get(position).setSubmittingToServer(true);
        allServicesAdapter.notifyItemChanged(position);
        servicesPresenter.enableDisableDoctorService(new EnableDisableServiceBody(
                CommonUtil.commonSharedPref().getInt("doctorId", -1),
                allServicesAdapter.getAllServices().get(position).getId(),
                allServicesAdapter.getAllServices().get(position).isSelected()), position);
    }

    @Override
    public void doctorServicesRetrievedSuccessfully(RestDoctorServicesResponse restDoctorServicesResponse) {
        List<Service> allServices = new ArrayList<>();
        Service iteratorService;
        boolean found;
        for (Service service : restDoctorServicesResponse.getDoctorServices().getAllServices()) {
            iteratorService = service;
            found = false;
            for (Service docService : restDoctorServicesResponse.getDoctorServices().getDoctorCurrentServices()) {
                if (docService.getId() == service.getId()) {
                    found = true;
                    break;
                }
            }
            iteratorService.setSelected(found);
            iteratorService.setSubmittingToServer(false);
            allServices.add(iteratorService);
        }
        allServicesAdapter = new AllServicesAdapter(allServices, getActivity(), this);
        servicesRecyclerView.setAdapter(allServicesAdapter);
    }

    @Override
    public void doctorServiceEnabledDisabledSuccessfully(CommonResponse commonResponse, int position) {
        allServicesAdapter.getAllServices().get(position).setSubmittingToServer(false);
        allServicesAdapter.notifyItemChanged(position);

        if (allServicesAdapter.getAllServices().get(position).isSelected()) {
            Toast.makeText(getActivity(), R.string.service_enable_success_message, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), R.string.service_disable_success_message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void doctorServiceEnabledDisableFailed(String message, int position) {
        allServicesAdapter.getAllServices().get(position).setSubmittingToServer(false);
        allServicesAdapter.getAllServices().get(position).setSelected(
                !allServicesAdapter.getAllServices().get(position).isSelected());
        allServicesAdapter.notifyItemChanged(position);

        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void doctorVisitRequestRetrievedSuccessfully(RestVisitRequestsResponse restVisitRequestsResponse) {
        // no need here
    }

    @Override
    public void patientBlockedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void patientUnblockedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void visitAcceptedSuccessfully() {
        // no need here
    }

    @Override
    public void visitRejectedSuccessfully() {
        // no need here
    }

    @Override
    public void visitCancelledSuccessfully() {
        //no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        servicesRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        servicesRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        try {
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
