package dr.doctorathome.presentation.ui.activities;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.SessionDetailsModels.OnlineConsultationDetailsModel;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OneSignal;

public class CallRequestRecivingActivity extends BasicActivity {

    @BindView(R.id.ivUserImage)
    androidx.appcompat.widget.AppCompatImageView ivUserImage;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.Accept)
    ImageView accept;

    @BindView(R.id.reject)
    ImageView reject;

    String sessionID, doctorID = "=1", avatar_url, patient_Name;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_request_reciving);
        ButterKnife.bind(this);
        getSessionDetails(getIntent().getIntExtra("id", -1));
        Log.d("TTTTTTTTTTTT", "onCreate: " + (getIntent().getIntExtra("id", -1)));
        //getSessionDetails(156);
        //set firebase
        mDatabase = FirebaseDatabase.getInstance().getReference().child("sessionsStatus").child(getIntent().getIntExtra("id", -1) + "");
        try {

            int NotificationID = getIntent().getIntExtra("NotificationID", 0);
            OneSignal.cancelNotification(NotificationID);

        } catch (Exception e) {

        }

        //
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!doctorID.equals("-1")) {
                    r.stop();

                    startActivity(new Intent(CallRequestRecivingActivity.this, CallActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra("sessionID", "" + sessionID)
                            .putExtra("doctorID", "" + doctorID)
                            .putExtra("avatar_url", avatar_url)
                            .putExtra("id", getIntent().getIntExtra("id", -1))
                            .putExtra("patient_name", "" + patient_Name));
                    Log.d("TTTTTTT", "onClick: " + sessionID + "---\n" + doctorID);
                    finish();
                }
            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                r.stop();
                mDatabase.child("callStatus").setValue("REJECTED");
                mDatabase.child("callStatus").setValue("0");
                finish();
            }
        });
    }

    private void getSessionDetails(int session) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.getSessionDetails(session, "" + CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<OnlineConsultationDetailsModel>() {
            @Override
            public void onResponse(Call<OnlineConsultationDetailsModel> call, Response<OnlineConsultationDetailsModel> response) {
                if (response.isSuccessful()) {
                    doctorID = response.body().getDetails().getDoctorTokenId();
                    sessionID = response.body().getDetails().getSessionId();
                    avatar_url = response.body().getPatient().getProfilePicUrl();
                    patient_Name = response.body().getPatient().getUser().getFirstName()
                            + " " + response.body().getPatient().getUser().getLastName();
                    name.setText(patient_Name);
                    Glide.with(CallRequestRecivingActivity.this).load(RestClient.REST_API_URL + avatar_url)
                            .apply(RequestOptions.circleCropTransform())
                            .placeholder(R.drawable.profile_placeholder_rounded).into(ivUserImage);

                }

            }

            @Override
            public void onFailure(Call<OnlineConsultationDetailsModel> call, Throwable t) {

            }
        });
    }
}
