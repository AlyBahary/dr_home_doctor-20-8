package dr.doctorathome.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.config.RequestStatues;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.ChangeOnlineConsultationStatuesBody;
import dr.doctorathome.network.model.RestOnlineConsultationRequestDetailsResponse;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.model.SendDiagnosModel;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.network.model.SessionDetailsModels.OnlineConsultationDetailsModel;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.presenters.ConsultationPresenter;
import dr.doctorathome.presentation.presenters.impl.ConsultationPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlineConsultationDetailsActivity extends BasicActivity implements ConsultationPresenter.View {

    @BindView(R.id.diagnosis_title_tv)
    TextView diagnosisTitleTv;
    @BindView(R.id.diagnosis_tv)
    TextView diagnosisTv;
    @BindView(R.id.tivToolbarEndAction)
    AppCompatImageButton notificationTv;
    @BindView(R.id.ivNewNotification)
    AppCompatImageView ivNewNotification;

    private ConsultationPresenter consultationPresenter;

    @BindView(R.id.acceptRejectButtonsLayout)
    LinearLayout acceptRejectButtonsLayout;

    @BindView(R.id.rejectButton)
    Button rejectButton;

    @BindView(R.id.acceptButton)
    Button acceptButton;

    @BindView(R.id.cancelButton)
    Button cancelButton;

    @BindView(R.id.goToChatButton)
    Button goToChatButton;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.onlineConsultationDetailsMainScrollView)
    ScrollView onlineConsultationDetailsMainScrollView;

    @BindView(R.id.tvPatientName)
    AppCompatTextView tvPatientName;

    @BindView(R.id.tvPatientLocation)
    AppCompatTextView tvPatientLocation;

    @BindView(R.id.tvTime)
    AppCompatTextView tvTime;

    @BindView(R.id.ivPatientImage)
    AppCompatImageView ivPatientImage;

    @BindView(R.id.tvStatuesText)
    TextView tvStatuesText;

    @BindView(R.id.ageTextView)
    TextView ageTextView;

    @BindView(R.id.weightTextView)
    TextView weightTextView;

    @BindView(R.id.tvComplain)
    TextView tvComplain;

    @BindView(R.id.tvSignificantPastDisease)
    TextView tvSignificantPastDisease;

    @BindView(R.id.tvAllergy)
    TextView tvAllergy;

    @BindView(R.id.cbDiabetes)
    CheckBox cbDiabetes;

    @BindView(R.id.cbBloodPressure)
    CheckBox cbBloodPressure;

    @BindView(R.id.cbHeartDisease)
    CheckBox cbHeartDisease;

    @BindView(R.id.cbRheumatism)
    CheckBox cbRheumatism;

    @BindView(R.id.cbCholesterol)
    CheckBox cbCholesterol;

    @BindView(R.id.addPrescriptionBtn)
    Button addPrescriptionBtn;

    @BindView(R.id.simpleInfoCard)
    CardView simpleInfoCard;

    @BindView(R.id.commonDiseaseCard)
    CardView commonDiseaseCard;

    private ProgressDialog progressDialog;

    private boolean isChangingVisitStatues = false;
    private boolean getDetailIsCalled = false;

    private Integer patient_User_ID, patientID, id = -1;


    private String name, patientAvatar;

    private String consultationStatus;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_consultation_details);
        ButterKnife.bind(this);

        notificationTv.setOnClickListener(v -> {
            startActivity(new Intent(this, NotificationsActivity.class));

        });
        if (Config.notificationCount > 0)
            ivNewNotification.setVisibility(View.VISIBLE);

        Log.d("test", "onCreate: ");
        progressDialog = new ProgressDialog(this);

        consultationPresenter = new ConsultationPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());


        if (getIntent().getIntExtra("id", -1) != -1) {
            id = getIntent().getIntExtra("id", -1);
        }
        consultationPresenter.getDoctorConsultationRequestDetails(id);
        getDetailIsCalled = true;

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.consultation_details);

        acceptButton.setOnClickListener(view -> {
                    isChangingVisitStatues = true;
                    consultationPresenter.acceptRequest(new ChangeOnlineConsultationStatuesBody(id, RequestStatues.CONFIRMED));
                }
        );

        rejectButton.setOnClickListener(view -> {
            isChangingVisitStatues = true;
            consultationPresenter.rejectRequest(new ChangeOnlineConsultationStatuesBody(id, RequestStatues.REJECTED));
        });

        cancelButton.setOnClickListener(view -> {
            isChangingVisitStatues = true;
            consultationPresenter.cancelRequest(new ChangeOnlineConsultationStatuesBody(id, RequestStatues.CANCELLED));
        });

        goToChatButton.setOnClickListener(view -> {
            Intent intent = new Intent(OnlineConsultationDetailsActivity.this, ConsultationChatActivity.class);
            intent.putExtra("session_id", id);
            intent.putExtra("name", name);
            intent.putExtra("patient_id", patientID);
            intent.putExtra("patient_user_id", patient_User_ID);
            intent.putExtra("patient_avatar", patientAvatar);
            startActivity(intent);
        });
        addPrescriptionBtn.setOnClickListener(v -> {
            Intent intent = new Intent(OnlineConsultationDetailsActivity.this, ConsultationPrescriptionActivity.class);
            intent.putExtra("session_id", id);
            intent.putExtra("name", name);
            intent.putExtra("patient_id", patientID);
            intent.putExtra("patient_user_id", patient_User_ID);
            intent.putExtra("patient_avatar", patientAvatar);
            startActivity(intent);
        });
        mDatabase = FirebaseDatabase.getInstance().getReference().child("sessionsStatus").child(id + "");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (getIntent().getIntExtra("id", -1) != -1) {
                    if (getDetailIsCalled != true)
                        consultationPresenter.getDoctorConsultationRequestDetails(id);
                    getDetailIsCalled = false;
                }
                int intVal = Integer.valueOf(String.valueOf(dataSnapshot.child("statusCode").getValue()));

                Log.d("TTTTTT", "onDataChange: " + (intVal == 4));
                if (intVal == 4) {
                    Log.d("TTTTTT", "onDataChange: " + (intVal == 4));
                    Intent i = new Intent();
                    i.putExtra("id", id);
                    setResult(RESULT_OK, i);
                    //finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Log.d("TTTTTTTTTTTTTTTTTTTTTTT", "onCancelled: " + databaseError.getDetails());
                Log.d("TTTTTTTTTTTTTTTTTTTTTTT", "onCancelled: " + databaseError.getMessage());
                Log.d("TTTTTTTTTTTTTTTTTTTTTTT", "onCancelled: " + databaseError.getCode());
            }
        });
    }

    private void visitStatues(String visitStatues) {
        switch (visitStatues) {
            case RequestStatues
                    .PENDING:
                tvStatuesText.setText(R.string.text_new);
                acceptRejectButtonsLayout.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.GONE);
                goToChatButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .CANCELLED:
                tvStatuesText.setText(R.string.text_canceled);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                goToChatButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .PAID:
                tvStatuesText.setText(R.string.text_paid);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                goToChatButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .REJECTED:
                tvStatuesText.setText(R.string.text_rejected);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                goToChatButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .RUNNING:
                tvStatuesText.setText(R.string.text_running);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                goToChatButton.setVisibility(View.VISIBLE);
                addPrescriptionBtn.setVisibility(View.VISIBLE);

                break;
            case RequestStatues
                    .CONFIRMED:
                tvStatuesText.setText(R.string.text_confirmed);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                goToChatButton.setVisibility(View.GONE);
                break;
            case RequestStatues
                    .COMPLETED:
                tvStatuesText.setText(R.string.text_completed);
                acceptRejectButtonsLayout.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                goToChatButton.setVisibility(View.VISIBLE);
                addPrescriptionBtn.setVisibility(View.VISIBLE);

                break;
        }
    }


    private void addSessionDiagnose(SendDiagnosModel sendDiagnosModel) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.addSessionDiagnose(sendDiagnosModel, CommonUtil.commonSharedPref().getString("token", ""))
                .enqueue(new Callback<ServerCommonResponse>() {
                    @Override
                    public void onResponse(Call<ServerCommonResponse> call, Response<ServerCommonResponse> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(OnlineConsultationDetailsActivity.this, "" + response.body().getRspBody(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServerCommonResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void showProgress(String message) {
        if (!isChangingVisitStatues) {
            progressLayout.setVisibility(View.VISIBLE);
            onlineConsultationDetailsMainScrollView.setVisibility(View.GONE);
        } else {
            CommonUtil.showProgressDialog(progressDialog, message);
        }
    }

    @Override
    public void hideProgress() {
        if (!isChangingVisitStatues) {
            progressLayout.setVisibility(View.GONE);
            onlineConsultationDetailsMainScrollView.setVisibility(View.VISIBLE);
        } else {
            CommonUtil.dismissProgressDialog(progressDialog);
        }
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        if (isChangingVisitStatues) {
            isChangingVisitStatues = false;
        }
    }

    @Override
    public void doctorConsultationsRetrievedSuccessfully(RestOnlineConsultationsRequestsResponse
                                                                 restOnlineConsultationsRequestsResponse) {
        // no need here
    }

    @Override
    public void doctorConsultationDetailsRetrievedSuccessfully(RestOnlineConsultationRequestDetailsResponse
                                                                       restOnlineConsultationRequestDetailsResponse) {
        name = restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getUser().getFirstName()
                + " "
                + restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getUser().getLastName();
        patientID = restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getId();
        patient_User_ID = restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getUser().getId();
        patientAvatar = restOnlineConsultationRequestDetailsResponse.getConsultRequest()
                .getPatient().getProfilePicUrl();

        tvPatientName.setText(name);

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getAddress() != null &&
                !restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getAddress().equals("")) {
            tvPatientLocation.setText(restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getAddress());
        } else {
            tvPatientLocation.setVisibility(View.GONE);
        }

        try {
            Glide.with(this).load(RestClient.REST_API_URL
                    + restOnlineConsultationRequestDetailsResponse.getConsultRequest().getPatient().getProfilePicUrl().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.profile_placeholder_rounded).into(ivPatientImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvTime.setText(CommonUtil.dateFullFromServerWithTime(
                restOnlineConsultationRequestDetailsResponse.getConsultRequest().getRequestdDate()));

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getRequestStatus() != null) {
            consultationStatus = restOnlineConsultationRequestDetailsResponse.getConsultRequest().getRequestStatus().getName();

            visitStatues(consultationStatus);
            getSessionDetails();


        }

        try {
            ageTextView.setText((restOnlineConsultationRequestDetailsResponse.getConsultRequest().getAge()) + "");
            Log.d("TAG", "doctorConsultationDetailsRetrievedSuccessfully: " + restOnlineConsultationRequestDetailsResponse.getConsultRequest().getAge());


        } catch (Exception e) {
            Log.d("TAG", "doctorConsultationDetailsRetrievedSuccessfully: " + e.getStackTrace());
        }

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getWeight() != null)
            weightTextView.setText("" + restOnlineConsultationRequestDetailsResponse.getConsultRequest().getWeight());
        else
            weightTextView.setText("");

        tvComplain.setText(restOnlineConsultationRequestDetailsResponse.getConsultRequest().getComplain());
        tvSignificantPastDisease.setText(restOnlineConsultationRequestDetailsResponse.getConsultRequest().getSignificantPastDisease());
        tvAllergy.setText(restOnlineConsultationRequestDetailsResponse.getConsultRequest().getAllergy());

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getDiabetes() != null &&
                restOnlineConsultationRequestDetailsResponse.getConsultRequest().getDiabetes().equals("YES")) {
            cbDiabetes.setChecked(true);
        } else {
            cbDiabetes.setChecked(false);
        }

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getBloodPressure() != null &&
                restOnlineConsultationRequestDetailsResponse.getConsultRequest().getBloodPressure().equals("YES")) {
            cbBloodPressure.setChecked(true);
        } else {
            cbBloodPressure.setChecked(false);
        }

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getHeartDisease() != null &&
                restOnlineConsultationRequestDetailsResponse.getConsultRequest().getHeartDisease().equals("YES")) {
            cbHeartDisease.setChecked(true);
        } else {
            cbHeartDisease.setChecked(false);
        }

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getRheumatism() != null &&
                restOnlineConsultationRequestDetailsResponse.getConsultRequest().getRheumatism().equals("YES")) {
            cbRheumatism.setChecked(true);
        } else {
            cbRheumatism.setChecked(false);
        }

        if (restOnlineConsultationRequestDetailsResponse.getConsultRequest().getCholestrol() != null &&
                restOnlineConsultationRequestDetailsResponse.getConsultRequest().getCholestrol().equals("YES")) {
            cbCholesterol.setChecked(true);
        } else {
            cbCholesterol.setChecked(false);
        }
    }

    @Override
    public void requestAcceptedSuccessfully() {
        Toast.makeText(this, R.string.request_accepted_success, Toast.LENGTH_LONG).show();
        visitStatues(RequestStatues
                .CONFIRMED);
        isChangingVisitStatues = false;
    }

    @Override
    public void requestRejectedSuccessfully() {
        Toast.makeText(this, R.string.request_rejected_success, Toast.LENGTH_LONG).show();
        visitStatues(RequestStatues
                .REJECTED);
        isChangingVisitStatues = false;
    }

    @Override
    public void requestCanceledSuccessfully() {
        Toast.makeText(this, R.string.request_cancelled_success, Toast.LENGTH_LONG).show();
        visitStatues(RequestStatues
                .CANCELLED);
        isChangingVisitStatues = false;
    }

    private void getSessionDetails() {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);

        doctorService.getSessionDetails(id
                , "" + CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<OnlineConsultationDetailsModel>() {
            @Override
            public void onResponse(Call<OnlineConsultationDetailsModel> call, Response<OnlineConsultationDetailsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getType().equals("immediateConsultation")) {
                        simpleInfoCard.setVisibility(View.GONE);
                        commonDiseaseCard.setVisibility(View.GONE);
                    }
                    if (response.body().getRequestStatus().getName().equals("COMPLETED")) {
                        if (response.body().getDetails().getDiagnosise() == null) {
                            diagnosisTitleTv.setVisibility(View.GONE);
                            diagnosisTv.setVisibility(View.GONE);
                            //  showDiagnoseDialoge(consultationStatus, false);
                        } else {
                            diagnosisTitleTv.setVisibility(View.VISIBLE);
                            diagnosisTv.setVisibility(View.VISIBLE);
                            diagnosisTv.setText(response.body().getDetails().getDiagnosise() + "");

                        }
                        if (response.body().getHasEPrescriptions()) {
                            addPrescriptionBtn.setText(getString(R.string.View_prescription));
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<OnlineConsultationDetailsModel> call, Throwable t) {

            }

        });
    }

//    private void showDiagnoseDialoge(String visitStatues, Boolean diagnose) {
//        if (diagnose == false
//                && visitStatues.equals(RequestStatues.COMPLETED)) {
//            Dialog dialog = new Dialog(OnlineConsultationDetailsActivity.this);
//            dialog.setContentView(R.layout.visit_diagnosis_dialog_layout);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//
//            Button submitButton = dialog.findViewById(R.id.submitButton);
//            EditText diagnosisET = dialog.findViewById(R.id.diagnosisEditText);
//            submitButton.setOnClickListener(view1 -> {
//                if (diagnosisET.getText().toString().trim().equals("") ||
//                        diagnosisET.getText().toString().trim().length() < 3) {
//                    Toast.makeText(OnlineConsultationDetailsActivity.this, R.string.write_diagnosis_first
//                            , Toast.LENGTH_LONG).show();
//                    return;
//                }
//                dialog.dismiss();
//                addSessionDiagnose(new SendDiagnosModel(diagnosisET.getText().toString(), id));
//                diagnosisTitleTv.setVisibility(View.VISIBLE);
//                diagnosisTv.setVisibility(View.VISIBLE);
//                diagnosisTv.setText(diagnosisET.getText().toString() + "");
//
//            });
//            if (!((Activity) OnlineConsultationDetailsActivity.this).isFinishing()) {
//                dialog.show();
//            }
//        }
//    }

}