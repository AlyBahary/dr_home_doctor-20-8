package dr.doctorathome.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.presenters.SettingsPresenter;
import dr.doctorathome.presentation.presenters.impl.SettingsPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;

public class ContactUsActivity extends BasicActivity implements SettingsPresenter.View {

    private SettingsPresenter settingsPresenter;

    @BindView(R.id.ibHistory)
    ImageButton ibHistory;

    @BindView(R.id.mainScrollView)
    ScrollView mainScrollView;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.btnSend)
    Button btnSend;

    @BindView(R.id.etTitle)
    EditText etTitle;

    @BindView(R.id.etMessage)
    EditText etMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);

        settingsPresenter = new SettingsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl(),
                new CommonDataRepositoryImpl());

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.contact_us);

        ibHistory.setOnClickListener(view -> startActivity(new Intent(ContactUsActivity.this,
                ContactUsHistoryListActivity.class)));

        btnSend.setOnClickListener(view -> {
            if(etTitle.getText().toString().isEmpty()){
                Toast.makeText(ContactUsActivity.this, R.string.support_message_title_validation, Toast.LENGTH_LONG).show();
                return;
            }

            if(etMessage.getText().toString().isEmpty()){
                Toast.makeText(ContactUsActivity.this, R.string.support_message_message_validation, Toast.LENGTH_LONG).show();
                return;
            }

            settingsPresenter.sendSupportMessage(etTitle.getText().toString(), etMessage.getText().toString());
        });
    }

    @Override
    public void clearanceRequestSubmittedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void passwordChangedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void appSettingsRetrievedSuccessfully(RestAppSettingsResponse restAppSettingsResponse) {
        // no need here
    }

    @Override
    public void supportMessageSentSuccessfully(CommonResponse commonResponse) {
        Toast.makeText(this, R.string.support_message_sent_success, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void supportMessagesHistoryRetrievedSuccessfully(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse) {
        // no need here
    }

    @Override
    public void supportMessageDetailsRetrievedSuccessfully(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse) {
        // no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        mainScrollView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        mainScrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
