package dr.doctorathome.presentation.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.ChangePasswordBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.presenters.SettingsPresenter;
import dr.doctorathome.presentation.presenters.impl.SettingsPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class ChangePasswordActivity extends BasicActivity implements SettingsPresenter.View {

    SettingsPresenter settingsPresenter;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.mainScrollView)
    ScrollView mainScrollView;

    @BindView(R.id.etOldPassword)
    EditText etOldPassword;

    @BindView(R.id.etNewPassword)
    EditText etNewPassword;

    @BindView(R.id.etConfirmNewPassword)
    EditText etConfirmNewPassword;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        settingsPresenter = new SettingsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl(),
                new CommonDataRepositoryImpl());

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.change_password);

        btnSubmit.setOnClickListener(view -> {
            if(etOldPassword.getText().toString().isEmpty()){
                Toast.makeText(ChangePasswordActivity.this, R.string.old_password_validation
                        , Toast.LENGTH_LONG).show();
                return;
            }

            if(etNewPassword.getText().toString().isEmpty()){
                Toast.makeText(ChangePasswordActivity.this, R.string.new_password_validation
                        , Toast.LENGTH_LONG).show();
                return;
            }

            if(etConfirmNewPassword.getText().toString().isEmpty()){
                Toast.makeText(ChangePasswordActivity.this, R.string.confirm_new_password_validation
                        , Toast.LENGTH_LONG).show();
                return;
            }

            if(!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString())){
                Toast.makeText(ChangePasswordActivity.this, R.string.new_password_and_confirmation_validation
                        , Toast.LENGTH_LONG).show();
                return;
            }

            settingsPresenter.changePassword(new ChangePasswordBody(CommonUtil.commonSharedPref().getInt("userId", -1),
                    etNewPassword.getText().toString(), etOldPassword.getText().toString()));
        });
    }

    @Override
    public void clearanceRequestSubmittedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void passwordChangedSuccessfully(CommonResponse commonResponse) {
        Toast.makeText(this, R.string.password_changed_success, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void appSettingsRetrievedSuccessfully(RestAppSettingsResponse restAppSettingsResponse) {
        //no need here
    }

    @Override
    public void supportMessageSentSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void supportMessagesHistoryRetrievedSuccessfully(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse) {
        //no need here
    }

    @Override
    public void supportMessageDetailsRetrievedSuccessfully(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse) {
        //no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        mainScrollView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        mainScrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
