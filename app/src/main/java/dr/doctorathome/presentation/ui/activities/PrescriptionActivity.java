package dr.doctorathome.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.config.MedicineTypes;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ExternalMedicine;
import dr.doctorathome.network.model.LabTestInPrescription;
import dr.doctorathome.network.model.MedicineBox;
import dr.doctorathome.network.model.MedicineInPrescription;
import dr.doctorathome.network.model.RecommendedMedicineToSent;
import dr.doctorathome.network.model.RestAllLabTestsResponse;
import dr.doctorathome.network.model.RestAllMedicinesResponse;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;
import dr.doctorathome.network.model.SavingPrescreptionBody;
import dr.doctorathome.network.model.TestToSendModel;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.adapters.LabTestsAdapter;
import dr.doctorathome.presentation.adapters.MedicinesAdapter;
import dr.doctorathome.presentation.presenters.PrescriptionPresenter;
import dr.doctorathome.presentation.presenters.impl.PrescriptionPresenterImpl;
import dr.doctorathome.presentation.ui.fragments.BottomSheetAddLabTest;
import dr.doctorathome.presentation.ui.fragments.BottomSheetAddMedicine;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PrescriptionActivity extends BasicActivity implements PrescriptionPresenter.View, Serializable {
    private PrescriptionPresenter prescriptionPresenter;

    @BindView(R.id.dataRecyclerView)
    RecyclerView dataRecyclerView;

    @BindView(R.id.labTestsRecyclerView)
    RecyclerView labTestsRecyclerView;

    @BindView(R.id.addMedicineLayout)
    LinearLayout addMedicineLayout;

    @BindView(R.id.addLabTestLayout)
    LinearLayout addLabTestLayout;

    @BindView(R.id.prescriptionScrollView)
    ScrollView prescriptionScrollView;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.sendToPatientBtn)
    Button sendToPatientBtn;

    private MedicinesAdapter medicinesAdapter;

    private LabTestsAdapter labTestsAdapter;

    private List<MedicineInPrescription> medicinesInPrescription;

    private List<LabTestInPrescription> labTestInPrescriptionList;

    private BottomSheetAddMedicine bottomSheetAddMedicine;

    private BottomSheetAddLabTest bottomSheetAddLabTest;

    private ArrayList<MedicineInPrescription> allSystemMedicines = new ArrayList<>();

    private ArrayList<LabTestInPrescription> allLabTests = new ArrayList<>();

    private Integer visitId;

    private boolean updateMode, viewOnly;

    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription);
        ButterKnife.bind(this);

        if (getIntent().getIntExtra("visitId", -1) != -1) {
            visitId = getIntent().getIntExtra("visitId", -1);
        }

        viewOnly = getIntent().getBooleanExtra("viewOnly", false);

        if (getIntent().getBooleanExtra("updateMode", false)) {
            updateMode = getIntent().getBooleanExtra("updateMode", false);
        } else {
            updateMode = false;
        }

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        if (updateMode && !viewOnly) {
            ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.update_prescription);
        } else {
            ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.prescription);
        }

        prescriptionPresenter = new PrescriptionPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

        prescriptionPresenter.getAllSystemMedicines();

        medicinesInPrescription = new ArrayList<>();

        medicinesAdapter = new MedicinesAdapter(medicinesInPrescription, this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        dataRecyclerView.setLayoutManager(linearLayoutManager);
        dataRecyclerView.setAdapter(medicinesAdapter);
        dataRecyclerView.setNestedScrollingEnabled(false);
        dataRecyclerView.setHasFixedSize(true);

        labTestInPrescriptionList = new ArrayList<>();


        labTestsAdapter = new LabTestsAdapter(labTestInPrescriptionList, this);

        LinearLayoutManager labTestsLinearLayoutManager = new LinearLayoutManager(this);
        labTestsRecyclerView.setLayoutManager(labTestsLinearLayoutManager);
        labTestsRecyclerView.setAdapter(labTestsAdapter);
        labTestsRecyclerView.setNestedScrollingEnabled(false);
        labTestsRecyclerView.setHasFixedSize(true);

        sendToPatientBtn.setOnClickListener(view -> {
            if (medicinesAdapter.getAllMedicines().isEmpty() && labTestsAdapter.getLabTestsInPrescription().isEmpty()) {
                Toast.makeText(PrescriptionActivity.this, R.string.at_least_1_entry_prescription
                        , Toast.LENGTH_LONG).show();
                return;
            }

            List<String> externalMedicines = new ArrayList<>();
            List<Integer> medicineBoxes = new ArrayList<>();
            List<TestToSendModel> tests = new ArrayList<>();
            List<RecommendedMedicineToSent> recommendedMedicines = new ArrayList<>();

            for (MedicineInPrescription med : medicinesAdapter.getAllMedicines()) {
                if (med.getMedicineType() == MedicineTypes.RECOMENDED_MEDICINE) {
                    recommendedMedicines.add(new RecommendedMedicineToSent(med.getId(), med.getCount()));
                } else if (med.getMedicineType() == MedicineTypes.MEDICINE_BOX) {
                    recommendedMedicines.add(new RecommendedMedicineToSent(med.getId(), med.getCount()));
                    medicineBoxes.addAll(med.getBoxesList());
                } else if (med.getMedicineType() == MedicineTypes.OUT_OF_THE_SYSTEM) {
                    externalMedicines.add(med.getName());
                }
            }

            for (LabTestInPrescription testInPrescription : labTestsAdapter.getLabTestsInPrescription()) {
                Log.d("TTTTTTTTTT", "onCreate: "+testInPrescription.getId()+"-------"+testInPrescription.isPayable());
                tests.add(new TestToSendModel(testInPrescription.getId(),testInPrescription.isPayable()));
            }

            SavingPrescreptionBody savingPrescreptionBody = new SavingPrescreptionBody(visitId, externalMedicines,
                    medicineBoxes, tests, recommendedMedicines);

            prescriptionPresenter.savePrescription(savingPrescreptionBody);
        });

        if (viewOnly) {
            sendToPatientBtn.setVisibility(View.GONE);
            addMedicineLayout.setVisibility(View.GONE);
            addLabTestLayout.setVisibility(View.GONE);
        }
        getPrescriptionStatus();
    }

    private void getPrescriptionStatus() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("visitsStatus").child(visitId + "").child("prescriptionPaymenetStatus");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    if (dataSnapshot.getValue().equals("PAID")) {
                        sendToPatientBtn.setVisibility(View.GONE);
                        addMedicineLayout.setVisibility(View.GONE);
                        addLabTestLayout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addLabTests(List<LabTestInPrescription> labTestInPrescriptionList1) {
        labTestsAdapter.setLabTestsInPrescription(labTestInPrescriptionList1);
        labTestsAdapter.notifyDataSetChanged();
    }

    public void addMedicines(List<MedicineInPrescription> medicineInPrescriptionList1) {
        if (medicinesAdapter.getAllMedicines().size() != 0) {

            for (int i = 0; i < medicinesAdapter.getAllMedicines().size(); i++) {


                if (medicinesAdapter.getAllMedicines().get(i).getId() == medicineInPrescriptionList1.get(0).getId()) {
                    medicinesAdapter.getAllMedicines().get(i).setCount(medicinesAdapter.getAllMedicines().get(i).getCount() + medicineInPrescriptionList1.get(0).getCount());
                    break;
                }
                else if (i == medicinesAdapter.getAllMedicines().size()-1) {
                    medicinesAdapter.getAllMedicines().addAll(medicineInPrescriptionList1);
                    break;
                }
            }
        } else {

            ///
            medicinesAdapter.getAllMedicines().addAll(medicineInPrescriptionList1);

        }
        medicinesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (medicinesAdapter != null) {
            medicinesAdapter.saveStates(outState);
        }
        if (labTestsAdapter != null) {
            labTestsAdapter.saveStates(outState);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (medicinesAdapter != null) {
            medicinesAdapter.restoreStates(savedInstanceState);
        }

        if (labTestsAdapter != null) {
            labTestsAdapter.restoreStates(savedInstanceState);
        }
    }

    @Override
    public void allSystemMedicinesRetrievedSuccessfully(RestAllMedicinesResponse restAllMedicinesResponse) {
        allSystemMedicines = new ArrayList<>(restAllMedicinesResponse.getMedicineInPrescriptionList());

        addMedicineLayout.setOnClickListener(view -> {
                    bottomSheetAddMedicine = BottomSheetAddMedicine.newInstance(this, allSystemMedicines);
                    bottomSheetAddMedicine.show(getSupportFragmentManager()
                            , BottomSheetAddMedicine.class.getSimpleName());
                }

        );
    }

    @Override
    public void allLabTestsRetrievedSuccessfully(RestAllLabTestsResponse restAllLabTestsResponse) {
        allLabTests = new ArrayList<>(restAllLabTestsResponse.getLabTestInPrescriptionList());

        bottomSheetAddLabTest = BottomSheetAddLabTest.newInstance(this, allLabTests);

        addLabTestLayout.setOnClickListener(view ->
                bottomSheetAddLabTest.show(getSupportFragmentManager()
                        , BottomSheetAddLabTest.class.getSimpleName())
        );

        if (updateMode) {
            prescriptionPresenter.getPrescriptionDetails(visitId);
        }
    }

    @Override
    public void allDoctorMedicineBoxesRetrievedSuccessfully(RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse) {
        // no need here
    }

    @Override
    public void prescriptionSavedSuccessfully(CommonResponse commonResponse) {
        Toast.makeText(this, R.string.prescription_sent_success, Toast.LENGTH_LONG).show();
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void prescriptionDetailsRetrievedSuccessfully(RestPrescriptionDetailsResponse restPrescriptionDetailsResponse) {
        List<Integer> medicineBoxes;
        if (restPrescriptionDetailsResponse.getMedicines() != null) {
            for (MedicineInPrescription medicineInPrescription : restPrescriptionDetailsResponse.getMedicines()) {
                if (medicineInPrescription.getMedicineBoxs() != null && !medicineInPrescription.getMedicineBoxs().isEmpty()) {
                    medicineBoxes = new ArrayList<>();
                    for (MedicineBox medicineBox : medicineInPrescription.getMedicineBoxs()) {
                        medicineBoxes.add(medicineBox.getId());
                    }
                    MedicineInPrescription medicine = new MedicineInPrescription((int) Math.round(Math.random()),
                            medicineInPrescription.getId(),
                            medicineInPrescription.getCount(), medicineInPrescription.getName(), medicineInPrescription.getDose(),
                            medicineInPrescription.getPrice(), MedicineTypes.MEDICINE_BOX);
                    medicine.setBoxesList(medicineBoxes);
                    medicinesAdapter.getAllMedicines().add(medicine);
                } else {
                    medicinesAdapter.getAllMedicines().add(new MedicineInPrescription((int) Math.round(Math.random()),
                            medicineInPrescription.getId(),
                            medicineInPrescription.getCount(), medicineInPrescription.getName(), medicineInPrescription.getDose(),
                            medicineInPrescription.getPrice(), MedicineTypes.RECOMENDED_MEDICINE));
                }
            }
        }

        if (restPrescriptionDetailsResponse.getExternalMedicines() != null) {
            for (ExternalMedicine external : restPrescriptionDetailsResponse.getExternalMedicines()) {
                medicinesAdapter.getAllMedicines().add(new MedicineInPrescription((int) Math.round(Math.random()), external.getId(),
                        0, external.getPrescription(), "",
                        "", MedicineTypes.OUT_OF_THE_SYSTEM));
            }
        }

        medicinesAdapter.notifyDataSetChanged();

        if (restPrescriptionDetailsResponse.getTests() != null) {
            for (LabTestInPrescription labTestInPrescription : restPrescriptionDetailsResponse.getTests()) {
                labTestsAdapter.getLabTestsInPrescription().add(new LabTestInPrescription(labTestInPrescription.getTest().getId(),
                        labTestInPrescription.getTest().getName(), labTestInPrescription.getTest().getPrice()));
            }

            labTestsAdapter.notifyDataSetChanged();
        }

        if (viewOnly) {
            sendToPatientBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        prescriptionScrollView.setVisibility(View.GONE);
        sendToPatientBtn.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        prescriptionScrollView.setVisibility(View.VISIBLE);
        sendToPatientBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public boolean isViewOnly() {
        return viewOnly;
    }
}
//
//
//                Log.d("TTTTT", "addMedicines: "+medicinesAdapter.getAllMedicines().get(i).getCount());
//                Log.d("TTTTT", "addMedicines: "+medicinesAdapter.getAllMedicines().get(i).getName());
//                Log.d("TTTTT", "addMedicines: "+medicinesAdapter.getAllMedicines().get(i).getName().trim().toLowerCase());
//                Log.d("TTTTT", "addMedicines: "+medicinesAdapter.getAllMedicines().get(i).getId());
//                if(medicinesAdapter.getAllMedicines().get(i).getCount()==null){
//                    continue;
//                }
//                if (medicineInPrescriptionList1.get(0).getCount() == null) {
//                    medicinesAdapter.getAllMedicines().addAll(medicineInPrescriptionList1);
//                    break;
//                }