package dr.doctorathome.presentation.ui.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.CommonDataObject;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;

public class Step2Fragment extends Fragment {

    @BindView(R.id.etSpecialist)
    AppCompatEditText etSpecialist;

    @BindView(R.id.etLevel)
    AppCompatEditText etLevel;

    CommonDataObject selectedLevel;

    List<CommonDataObject> levels = new ArrayList<>();
    ArrayList<String> levelsStrings = new ArrayList<>();


    @BindView(R.id.etClinicName)
    AppCompatEditText etClinicName;

    @BindView(R.id.etRegistrationCard)
    AppCompatEditText etRegistrationCard;

    @BindView(R.id.etRegistrationNumber)
    AppCompatEditText etRegistrationNumber;

    @BindView(R.id.etExpireDate)
    AppCompatEditText etExpireDate;

    @BindView(R.id.etQualification)
    EditText etQualification;

    List<CommonDataObject> clinics = new ArrayList<>();
    ArrayList<String> clinicsStrings = new ArrayList<>();

    CommonDataObject selectedClinic;

    String registrationCardBase64, registrationCardName;

    Calendar selectedDataCalendar;

    CommonDataObject selectedSpeciality;


    List<CommonDataObject> specialities = new ArrayList<>();
    ArrayList<String> specialitiesStrings = new ArrayList<>();


    public Step2Fragment() {
        // Required empty public constructor
    }

    public static Step2Fragment newInstance() {
        return new Step2Fragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_step2, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSpecialist.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_speciality)
                , (selectedSpeciality == null ? -1 : specialities.indexOf(selectedSpeciality)), specialitiesStrings
                , (dialogInterface, i) -> {
                    etSpecialist.setText(specialitiesStrings.get(i));
                    selectedSpeciality = specialities.get(i);
                    dialogInterface.dismiss();
                }));

        etLevel.setOnClickListener(view1 ->
                CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_level)
                        , (selectedLevel == null ? -1 : levels.indexOf(selectedLevel)), levelsStrings
                        , (dialogInterface, i) -> {
                            etLevel.setText(levelsStrings.get(i));
                            selectedLevel = levels.get(i);
                            dialogInterface.dismiss();
                        }));

        etClinicName.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_clinic)
                , (selectedClinic == null ? -1 : clinics.indexOf(selectedClinic)), clinicsStrings,
                (dialogInterface, i) -> {
                    etClinicName.setText(clinicsStrings.get(i));
                    selectedClinic = clinics.get(i);
                    dialogInterface.dismiss();
                }));
        selectedDataCalendar = Calendar.getInstance();

        etRegistrationCard.setOnClickListener(view1 -> {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    Toast.makeText(getContext(), R.string.weNeedAccess, Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                } else {

                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });

        etExpireDate.setOnClickListener(view1 -> {
            Calendar nowCalendar = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), datePickerListner, selectedDataCalendar
                    .get(Calendar.YEAR), selectedDataCalendar.get(Calendar.MONTH),
                    selectedDataCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(nowCalendar.getTimeInMillis());
            datePickerDialog.show();
        });
    }

    DatePickerDialog.OnDateSetListener datePickerListner = (view, year, monthOfYear, dayOfMonth) -> {
        Calendar selectedDateCalendar = Calendar.getInstance();
        selectedDateCalendar.set(Calendar.YEAR, year);
        selectedDateCalendar.set(Calendar.MONTH, monthOfYear);
        selectedDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateExpireDate(selectedDateCalendar);
    };

    public void setSpecialities(List<CommonDataObject> specialities) {
        this.specialities = specialities;
        specialitiesStrings = new ArrayList<>();
        for (CommonDataObject commonDataObject : specialities) {
            specialitiesStrings.add(commonDataObject.getName());
        }
    }

    public void setLevels(List<CommonDataObject> levels) {
        this.levels = levels;
        levelsStrings = new ArrayList<>();
        for (CommonDataObject commonDataObject : levels) {
            levelsStrings.add(commonDataObject.getName());
        }
    }

    private void updateExpireDate(Calendar selectedDate) {
        selectedDataCalendar = selectedDate;

        String dateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        etExpireDate.setText(sdf.format(selectedDate.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 1) {
                final Uri selectedImageUri = data.getData();
                File imageFile = new File(selectedImageUri.getPath());
                Timber.d(imageFile.getName());


                registrationCardName = imageFile.getName();
                etRegistrationCard.setText(getFileName(selectedImageUri));
                registrationCardBase64 = CommonUtil.convertFileToBase64(getContext(), selectedImageUri);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            } else {
                // User refused to grant permission.
                Toast.makeText(getContext(), R.string.access_denied, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void setClinics(List<CommonDataObject> clinics) {
        this.clinics = clinics;
        clinicsStrings = new ArrayList<>();
        for (CommonDataObject commonDataObject : clinics) {
            clinicsStrings.add(commonDataObject.getName());
        }
    }

    public boolean validateForm() {

        if (etClinicName.getText().toString().trim().isEmpty()) {
            etClinicName.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else {
            etClinicName.setError(null);
        }
        if (etSpecialist.getText().toString().trim().isEmpty()) {
            etSpecialist.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else {
            etSpecialist.setError(null);
        }
        if (etLevel.getText().toString().trim().isEmpty()) {
            etLevel.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else {
            etLevel.setError(null);
        }
        if (etRegistrationCard.getText().toString().trim().isEmpty()) {
            etRegistrationCard.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else {
            etRegistrationCard.setError(null);
        }
        if (etRegistrationNumber.getText().toString().trim().isEmpty()) {
            etRegistrationNumber.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etExpireDate.getText().toString().trim().isEmpty()) {
            etExpireDate.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else {
            etExpireDate.setError(null);
        }
        if (etQualification.getText().toString().trim().isEmpty()) {
            etQualification.setError(getString(R.string.error_this_field_is_required));
            return false;
        }

        return true;
    }

    public UserModel fillUserModelData(UserModel userModel) {
        userModel.setLevel(selectedLevel.getId());

        userModel.setClinicId(selectedClinic.getId());
        userModel.setRegistrationNumber(etRegistrationNumber.getText().toString());
        userModel.setQualificationDesc(etQualification.getText().toString());
        userModel.setRegExpiryDate(etExpireDate.getText().toString());
        userModel.setRegistartionCard(registrationCardBase64);
        userModel.setSpecialtyId(selectedSpeciality.getId());

        return userModel;
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
}
