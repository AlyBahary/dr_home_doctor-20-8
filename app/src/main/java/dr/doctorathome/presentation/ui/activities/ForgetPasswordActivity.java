package dr.doctorathome.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.SignupPresenter;
import dr.doctorathome.presentation.presenters.impl.SignupPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class ForgetPasswordActivity extends BasicActivity implements SignupPresenter.View{

    @BindView(R.id.etEmailAddress)
    EditText etEmailAddress;

    @BindView(R.id.btnSend)
    Button btnSend;

    private SignupPresenter signupPresenter;

    private ProgressDialog progressDialog;

    private String verificationCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        signupPresenter = new SignupPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new LoginRepositoryImpl(),
                new CommonDataRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());

        btnSend.setOnClickListener(view -> {
            if (etEmailAddress.getText().toString().isEmpty()) {
                Animation shake = AnimationUtils.loadAnimation(ForgetPasswordActivity.this, R.anim.shake);
                etEmailAddress.startAnimation(shake);
            } else if (!CommonUtil.validateEmail(etEmailAddress.getText().toString())) {
                etEmailAddress.setError(getString(R.string.enter_valid_mail));
            } else {
                Intent goToRequestForgetPasswordCode = new Intent(ForgetPasswordActivity.this, OtbActivity.class);
                goToRequestForgetPasswordCode.putExtra("whatToVerify", etEmailAddress.getText().toString());
                goToRequestForgetPasswordCode.putExtra("type", OtbActivity.RESET_PASSWORD);
                startActivityForResult(goToRequestForgetPasswordCode, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 1) {
                if (data != null) {
                    boolean validationCodeDoneSuccessfully = data.getBooleanExtra("validationDone", false);
                    verificationCode = data.getStringExtra("verificationCode");
                    if (validationCodeDoneSuccessfully) {
                        Intent goToSetNewPassword = new Intent(ForgetPasswordActivity.this, SetNewPasswordActivity.class);
                        startActivityForResult(goToSetNewPassword, 2);
                    }
                }
                Timber.d("validationCodeDoneSuccessfully :: "
                        + data.getBooleanExtra("validationDone", false));
            } else if (requestCode == 2) {
                if (data != null) {
                    String newPassword = data.getStringExtra("newPassword");
                    Timber.d("newPassword :: "
                            + newPassword);
                    signupPresenter.resetPassword(etEmailAddress.getText().toString(), newPassword, verificationCode);
                }

            }
        }
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        // no need here
    }

    @Override
    public void signupCodeRetrievedSuccessfully(String code) {
        // no need here
    }

    @Override
    public void forgetPasswordCodeRetrievedSuccessfully(String code) {
        // no need here
    }

    @Override
    public void signupRequestSentSuccessfully() {
        // no need here
    }

    @Override
    public void passwordResetSuccessfully() {
        Toast.makeText(this, "Password reset successfully, now you can login", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void dataValidatedSuccessfully() {
        // no need here
    }

    @Override
    public void showProgress(String message) {
        CommonUtil.showProgressDialog(progressDialog, message);
    }

    @Override
    public void hideProgress() {
        CommonUtil.dismissProgressDialog(progressDialog);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
