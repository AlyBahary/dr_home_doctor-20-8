package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.City;
import dr.doctorathome.network.model.Clinic;
import dr.doctorathome.network.model.CommonDataObject;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.Country;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.SpecialtyDoctor;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.impl.UserDataPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.DataBackListener;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class BottomSheetEditHeaderInfo extends BottomSheetDialogFragment implements UserDataPresenter.View {

    @BindView(R.id.progress_layout)
    RelativeLayout progressLayout;

    @BindView(R.id.etFirstName)
    AppCompatEditText etFirstName;

    @BindView(R.id.etLastName)
    AppCompatEditText etLastName;

    @BindView(R.id.etSpecialist)
    AppCompatEditText etSpecialist;

    @BindView(R.id.etCountry)
    AppCompatEditText etCountry;

    @BindView(R.id.etCity)
    AppCompatEditText etCity;

    @BindView(R.id.btnSave)
    AppCompatButton btnSave;

    @BindView(R.id.btnCancel)
    AppCompatButton btnCancel;

    UserData userData;

    DataBackListener dataBackListener;

    private UserDataPresenter userDataPresenter;

    List<SpecialtyDoctor> specialities = new ArrayList<>();
    ArrayList<String> specialitiesStrings = new ArrayList<>();

    SpecialtyDoctor selectedSpeciality;

    List<Country> countries = new ArrayList<>();
    ArrayList<String> countriesStrings = new ArrayList<>();

    Country selectedCountry;

    List<City> cities = new ArrayList<>();
    ArrayList<String> citiesStrings = new ArrayList<>();

    City selectedCity;

    private RestLookupsResponse restLookupsResponse;

    public BottomSheetEditHeaderInfo() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), R.style.MyDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomsheet_edit_header_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userData = (UserData) getArguments().getSerializable("userData");

        userDataPresenter = new UserDataPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        userDataPresenter.getLookups();

        if (userData != null) {
            selectedSpeciality = userData.getSpecialtyDoctor();
            selectedCountry = userData.getUser().getCountry();
            selectedCity = userData.getUser().getCity();
            etSpecialist.setText(selectedSpeciality.getName());
            etCountry.setText(selectedCountry.getNameEn());
            etCity.setText(selectedCity.getNameEn());
            etFirstName.setText(userData.getUser().getFirstName());
            etLastName.setText(userData.getUser().getLastName());
        }

        etSpecialist.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_speciality)
                , (selectedSpeciality == null ? -1 : specialities.indexOf(selectedSpeciality)), specialitiesStrings
                , (dialogInterface, i) -> {
                    etSpecialist.setText(specialitiesStrings.get(i));
                    selectedSpeciality = specialities.get(i);
                    dialogInterface.dismiss();
                }));
        etCountry.setOnClickListener(view1 -> {
                    if (citiesStrings.isEmpty()) {
                        Toast.makeText(getContext(), R.string.select_country_first, Toast.LENGTH_LONG).show();
                    } else {
                        CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_country)
                                , (selectedCountry == null ? -1 : countries.indexOf(selectedCountry)), countriesStrings
                                , (dialogInterface, i) -> {
                                    etCountry.setText(countriesStrings.get(i));
                                    selectedCountry = countries.get(i);
                                    selectedCity = null;
                                    etCity.setText("");
                                    cities = new ArrayList<>();
                                    citiesStrings = new ArrayList<>();
                                    for (CommonDataObject cdo : restLookupsResponse.getCities()) {
                                        if (cdo.getCountryId() != null && cdo.getCountryId() == selectedCountry.getId()) {
                                            cities.add(new City(cdo.getId(), cdo.getNameEn()));
                                            citiesStrings.add(cdo.getNameEn());
                                        }

                                    }
                                    dialogInterface.dismiss();
                                });
                    }
                }
        );
        etCity.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_city)
                , (selectedCity == null ? -1 : cities.indexOf(selectedCity)), citiesStrings
                , (dialogInterface, i) -> {
                    etCity.setText(citiesStrings.get(i));
                    selectedCity = cities.get(i);
                    dialogInterface.dismiss();
                }));

        btnSave.setOnClickListener(view1 -> {
            if (etFirstName.getText().toString().isEmpty()) {
                etFirstName.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etLastName.getText().toString().isEmpty()) {
                etLastName.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etSpecialist.getText().toString().isEmpty()) {
                etSpecialist.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etCountry.getText().toString().isEmpty()) {
                etCountry.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etCity.getText().toString().isEmpty()) {
                etCity.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            userData.getUser().setFirstName(etFirstName.getText().toString());
            userData.getUser().setLastName(etLastName.getText().toString());
            userData.setSpecialtyDoctor(selectedSpeciality);
            userData.getUser().setCountry(selectedCountry);
            userData.getUser().setCity(selectedCity);

            UserDataForUpdateBody userDataForUpdateBody = new UserDataForUpdateBody();
            userDataForUpdateBody.setSpecialtyDoctorId(selectedSpeciality.getId());
            userDataForUpdateBody.setCountryId(selectedCountry.getId());
            userDataForUpdateBody.setCityId(selectedCity.getId());
            userDataForUpdateBody.setId(userData.getId());

            userDataPresenter.updateDoctorData(userDataForUpdateBody);
        });
        btnCancel.setOnClickListener(view1 -> dismiss());

    }

    @Override
    public void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse) {
        // No need here
    }

    @Override
    public void visitAcceptedSuccessfully() {
        //no need here
    }

    @Override
    public void visitRejectedSuccessfully() {
        //no need here
    }

    @Override
    public void visitCancelledSuccessfully() {
        //no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void doctorDataUpdatedSuccessfully(CommonResponse commonResponse) {
        if (dataBackListener != null) {
            dataBackListener.dataSavedSuccessfully(userData);
        }
        dismiss();
        Toast.makeText(getActivity(), R.string.data_update_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void doctorStatuesSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        this.restLookupsResponse = restLookupsResponse;

        specialities = new ArrayList<>();
        for (CommonDataObject cdo : restLookupsResponse.getSpecializations()) {
            specialities.add(new SpecialtyDoctor(cdo.getId(), cdo.getName()));
        }
        specialitiesStrings = new ArrayList<>();
        for (SpecialtyDoctor specialtyDoctor : specialities) {
            specialitiesStrings.add(specialtyDoctor.getName());
        }

        countries = new ArrayList<>();
        for (CommonDataObject cdo : restLookupsResponse.getCountries()) {
            countries.add(new Country(cdo.getId(), cdo.getNameEn()));
        }
        countriesStrings = new ArrayList<>();
        for (Country country : countries) {
            countriesStrings.add(country.getNameEn());
        }

        cities = new ArrayList<>();
        citiesStrings = new ArrayList<>();
        for (CommonDataObject cdo : restLookupsResponse.getCities()) {
            if (cdo.getCountryId() != null && selectedCountry != null && cdo.getCountryId() == selectedCountry.getId()) {
                cities.add(new City(cdo.getId(), cdo.getNameEn()));
                citiesStrings.add(cdo.getNameEn());
            }

        }

        //        cities = new ArrayList<>();
        //        for(CommonDataObject cdo: restLookupsResponse.getCities()){
        //            cities.add(new City(cdo.getId(), cdo.getNameEn()));
        //        }
        //        citiesStrings = new ArrayList<>();
        //        for (City city : cities) {
        //            citiesStrings.add(city.getNameEn());
        //        }
    }

    @Override
    public void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse) {
        //no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        setCancelable(false);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);
        setCancelable(true);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    public void setDataBackListener(DataBackListener dataBackListener) {
        this.dataBackListener = dataBackListener;
    }
}
