package dr.doctorathome.presentation.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;

import com.onesignal.OneSignal;

import java.util.Locale;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.presenters.SettingsPresenter;
import dr.doctorathome.presentation.presenters.impl.SettingsPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class SplashActivity extends BasicActivity implements SettingsPresenter.View {
    private SettingsPresenter settingsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String currentLocaleLanguage;

        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("TTTTTTTT", "onCreate: "+android_id);

        settingsPresenter = new SettingsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl(),
                new CommonDataRepositoryImpl());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            currentLocaleLanguage = Resources.getSystem().getConfiguration().getLocales().get(0).getLanguage();
        } else {
            currentLocaleLanguage = Resources.getSystem().getConfiguration().locale.getLanguage();
        }
        Timber.d("Current Language :: " + currentLocaleLanguage);
        Timber.d("Saved Language :: " + CommonUtil.commonSharedPref().getString("appLanguage", "NONE"));

        if (currentLocaleLanguage.equals("en") || currentLocaleLanguage.equals("ar")) {
            AndroidApplication.setAppLocale(CommonUtil.commonSharedPref().getString("appLanguage", currentLocaleLanguage));
        } else {
            AndroidApplication.setAppLocale(CommonUtil.commonSharedPref().getString("appLanguage", "ar"));
        }
        setContentView(R.layout.activity_splash);
        Log.d("TAG", "lcaol: " + CommonUtil.commonSharedPref().getString("appLanguage", "ar"));
        Log.d("TAG", "app lang: " + Locale.getDefault().getLanguage());
        Log.d("TAG", "app lang: " + Locale.getDefault().getDisplayLanguage());
        Log.d("TAG", "app lang: " + getResources().getConfiguration().locale);
        Log.d("TAG", "system lang: " + Resources.getSystem().getConfiguration().locale.getLanguage());


        if (CommonUtil.commonSharedPref().getString("oneSignalUserId", "").equals("")) {
            OneSignal.idsAvailable((userId, registrationId) -> {
                SharedPreferences.Editor editor = CommonUtil.commonSharedPref().edit();
                editor.putString("oneSignalUserId", userId);
                editor.putBoolean("userIdSaved", false);
                editor.apply();
                editor.commit();

            });
        }

        setSplashAnimation();
    }

    void setSplashAnimation() {
        Animation fadeInAnim = AnimationUtils.loadAnimation(this, R.anim.fadein);
        findViewById(R.id.ivSplashBackground).startAnimation(fadeInAnim);
        fadeInAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Timber.e("Done..");
                if (!CommonUtil.commonSharedPref().getString("token", "").equals("")
                        && !CommonUtil.commonSharedPref().getBoolean("userIdSaved", false)) {
                    settingsPresenter.saveOneSignalUserId(CommonUtil.commonSharedPref().getString("oneSignalUserId", ""));
                } else {
                    settingsPresenter.getAppSettings();
                }
                //                }else {
                //                    finish();
                //                    if(CommonUtil.commonSharedPref().getString("token", "").equals("")){
                //                        startActivity(new Intent(SplashActivity.this, LanguageSelectActivity.class));
                //                    }else{
                //                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                //                    }
                //                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    @Override
    public void clearanceRequestSubmittedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void passwordChangedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void appSettingsRetrievedSuccessfully(RestAppSettingsResponse restAppSettingsResponse) {
        //        startTheCountDown();
        decideWhereToGo();
    }

    private void decideWhereToGo() {
        finish();
        if (CommonUtil.commonSharedPref().getString("token", "").equals("")) {
            startActivity(new Intent(SplashActivity.this, LanguageSelectActivity.class));
        } else {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        }
    }

    @Override
    public void supportMessageSentSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void supportMessagesHistoryRetrievedSuccessfully(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse) {
        // no need here
    }

    @Override
    public void supportMessageDetailsRetrievedSuccessfully(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse) {
        // no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        settingsPresenter.getAppSettings();
        SharedPreferences.Editor editor = CommonUtil.commonSharedPref().edit();
        editor.putBoolean("userIdSaved", true);
        editor.apply();
        editor.commit();
    }

    @Override
    public void showProgress(String message) {
        // no need here
    }

    @Override
    public void hideProgress() {
        // no need here
    }

    @Override
    public void showError(String message, boolean hideView) {
        decideWhereToGo();
    }
}
