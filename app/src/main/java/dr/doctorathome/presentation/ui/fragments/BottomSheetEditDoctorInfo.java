package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.Clinic;
import dr.doctorathome.network.model.CommonDataObject;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.Level;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.impl.UserDataPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.DataBackListener;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class BottomSheetEditDoctorInfo extends BottomSheetDialogFragment implements UserDataPresenter.View {

    @BindView(R.id.progress_layout)
    RelativeLayout progressLayout;

    @BindView(R.id.etClinicName)
    AppCompatEditText etClinicName;

    @BindView(R.id.etLevelName)
    AppCompatEditText etLevelName;

    @BindView(R.id.etRegistrationNumber)
    AppCompatEditText etRegistrationNumber;

    @BindView(R.id.etQualification)
    EditText etQualification;

    @BindView(R.id.btnSave)
    AppCompatButton btnSave;

    @BindView(R.id.btnCancel)
    AppCompatButton btnCancel;

    UserData userData;

    DataBackListener dataBackListener;

    private UserDataPresenter userDataPresenter;

    List<Clinic> clinics = new ArrayList<>();
    ArrayList<String> clinicsStrings = new ArrayList<>();

    Clinic selectedClinic;

    List<Level> levels = new ArrayList<>();
    ArrayList<String> levelStrings = new ArrayList<>();

    Level selectedLevel;

    public BottomSheetEditDoctorInfo() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), R.style.MyDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomsheet_edit_doctor_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userData = (UserData) getArguments().getSerializable("userData");

        userDataPresenter = new UserDataPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        userDataPresenter.getLookups();

        if(userData != null){
            selectedClinic = userData.getClinic();
            etClinicName.setText(userData.getClinic().getName());
            etRegistrationNumber.setText(userData.getRegistrationNumber());
            etLevelName.setText(userData.getLevel().getName());
            selectedLevel = userData.getLevel();
            etQualification.setText(userData.getQualificationDesc());
        }

        etClinicName.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext()
                , getString(R.string.select_clinic)
                , (selectedClinic == null ? -1 : clinics.indexOf(selectedClinic)), clinicsStrings,
                (dialogInterface, i) -> {
                    etClinicName.setText(clinicsStrings.get(i));
                    selectedClinic = clinics.get(i);
                    dialogInterface.dismiss();
                }));

        etLevelName.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext()
                , getString(R.string.select_level)
                , (selectedLevel == null ? -1 : levels.indexOf(selectedLevel)), levelStrings,
                (dialogInterface, i) -> {
                    etLevelName.setText(levelStrings.get(i));
                    selectedLevel = levels.get(i);
                    dialogInterface.dismiss();
                }));

        btnSave.setOnClickListener(view1 -> {
            if (etClinicName.getText().toString().isEmpty()) {
                etClinicName.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etRegistrationNumber.getText().toString().isEmpty()) {
                etRegistrationNumber.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etLevelName.getText().toString().isEmpty()) {
                etLevelName.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            if (etQualification.getText().toString().isEmpty()) {
                etQualification.setError(getString(R.string.error_this_field_is_required));
                return;
            }
            userData.setClinic(selectedClinic);
            userData.setLevel(selectedLevel);
            userData.setRegistrationNumber(etRegistrationNumber.getText().toString());
            userData.setQualificationDesc(etQualification.getText().toString());

            UserDataForUpdateBody userDataForUpdateBody = new UserDataForUpdateBody();
            userDataForUpdateBody.setRegistrationNumber(etRegistrationNumber.getText().toString());
            userDataForUpdateBody.setQualificationDesc(etQualification.getText().toString());
            userDataForUpdateBody.setLevelId(selectedLevel.getId());
            userDataForUpdateBody.setId(userData.getId());

            userDataPresenter.updateDoctorData(userDataForUpdateBody);
        });
        btnCancel.setOnClickListener(view1 -> dismiss());

    }

    @Override
    public void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse) {
        // No need here
    }


    @Override
    public void visitAcceptedSuccessfully() {
        //no need here
    }

    @Override
    public void visitRejectedSuccessfully() {
        //no need here
    }

    @Override
    public void visitCancelledSuccessfully() {
        //no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void doctorDataUpdatedSuccessfully(CommonResponse commonResponse) {
        if(dataBackListener != null){
            dataBackListener.dataSavedSuccessfully(userData);
        }
        dismiss();
        Toast.makeText(getActivity(), R.string.data_update_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void doctorStatuesSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        clinics = new ArrayList<>();
        for(CommonDataObject cdo: restLookupsResponse.getClinics()){
            clinics.add(new Clinic(cdo.getId(), cdo.getName()));
        }
        clinicsStrings = new ArrayList<>();
        for (Clinic clinic : clinics) {
            clinicsStrings.add(clinic.getName());
        }

        levels = new ArrayList<>();
        for(CommonDataObject cdo: restLookupsResponse.getLevels()){
            levels.add(new Level(cdo.getId(), cdo.getName()));
        }

        levelStrings = new ArrayList<>();
        for (Level level : levels) {
            levelStrings.add(level.getName());
        }
    }

    @Override
    public void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse) {
        //no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        setCancelable(false);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);
        setCancelable(true);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    public void setDataBackListener(DataBackListener dataBackListener) {
        this.dataBackListener = dataBackListener;
    }
}
