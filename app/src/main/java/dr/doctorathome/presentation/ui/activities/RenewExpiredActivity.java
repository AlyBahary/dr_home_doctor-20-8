package dr.doctorathome.presentation.ui.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.presenters.RenewCardPresenter;
import dr.doctorathome.presentation.presenters.impl.RenewCardPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class RenewExpiredActivity extends BasicActivity implements RenewCardPresenter.View {

    private RenewCardPresenter renewCardPresenter;

    @BindView(R.id.etDate)
    AppCompatEditText etDate;

    @BindView(R.id.cardLayout)
    RelativeLayout cardLayout;

    @BindView(R.id.ivUploadedCard)
    ImageView ivUploadedCard;

    @BindView(R.id.tvUploadCard)
    TextView tvUploadCard;

    @BindView(R.id.btnRenewCard)
    Button btnRenewCard;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.viewLayout)
    RelativeLayout viewLayout;

    @BindView(R.id.layoutAlreadySubmitted)
    RelativeLayout layoutAlreadySubmitted;

    @BindView(R.id.btnDone)
    Button btnDone;

    @BindView(R.id.alreadySubmittedTV)
    TextView alreadySubmittedTV;

    private String registrationCardBase64;

    private Calendar selectedDataCalendar;

    private File imageFile;

    private String token;

    private int doctorId;

    private boolean hasAlreadySubmitted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_expired);
        ButterKnife.bind(this);

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        hasAlreadySubmitted = getIntent().getBooleanExtra("hasAlreadySubmitted", false);

        if(hasAlreadySubmitted){
            ((TextView) findViewById(R.id.tvToolbarTitle)).setText("");
        }else{
            hasAlreadySubmitted = false;
            ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.renew_expired);
        }

        if(getIntent().hasExtra("hasAlreadySubmitted")){
            token = getIntent().getStringExtra("token");
        }else{
            token = "";
        }

        if(getIntent().hasExtra("doctorId")){
            doctorId = getIntent().getIntExtra("doctorId", -1);
        }else{
            doctorId = -1;
        }

        selectedDataCalendar = Calendar.getInstance();

        renewCardPresenter = new RenewCardPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl());

        cardLayout.setOnClickListener(view1 -> {
            if (ContextCompat.checkSelfPermission(RenewExpiredActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(RenewExpiredActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    Toast.makeText(RenewExpiredActivity.this, R.string.weNeedAccess, Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(RenewExpiredActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                } else {

                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(RenewExpiredActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });

        etDate.setOnClickListener(view1 -> {
            Calendar nowCalendar = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(RenewExpiredActivity.this, datePickerListener, selectedDataCalendar
                    .get(Calendar.YEAR), selectedDataCalendar.get(Calendar.MONTH),
                    selectedDataCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(nowCalendar.getTimeInMillis()+86400000);
            datePickerDialog.show();
        });

        btnRenewCard.setOnClickListener(view -> {
            if(!etDate.getText().toString().isEmpty()
                    && registrationCardBase64 != null && !registrationCardBase64.isEmpty()){
                renewCardPresenter.onRenewingCard(doctorId, etDate.getText().toString(), registrationCardBase64, token);
            }else{
                Toast.makeText(RenewExpiredActivity.this, R.string.fill_the_form_first, Toast.LENGTH_LONG).show();
            }
        });

        if(hasAlreadySubmitted){
            viewLayout.setVisibility(View.GONE);
            layoutAlreadySubmitted.setVisibility(View.VISIBLE);
        }else{
            viewLayout.setVisibility(View.VISIBLE);
            layoutAlreadySubmitted.setVisibility(View.GONE);
        }

        btnDone.setOnClickListener(view -> finish());
    }

    DatePickerDialog.OnDateSetListener datePickerListener = (view, year, monthOfYear, dayOfMonth) -> {
        Calendar selectedDateCalendar = Calendar.getInstance();
        selectedDateCalendar.set(Calendar.YEAR, year);
        selectedDateCalendar.set(Calendar.MONTH, monthOfYear);
        selectedDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateExpireDate(selectedDateCalendar);
    };

    private void updateExpireDate(Calendar selectedDate) {
        selectedDataCalendar = selectedDate;

        String dateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        etDate.setText(sdf.format(selectedDate.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 1) {
                final Uri selectedImageUri = data.getData();
                imageFile = new File(selectedImageUri.getPath());
                Timber.d(imageFile.getName());

                ivUploadedCard.setVisibility(View.VISIBLE);
                ivUploadedCard.setImageURI(selectedImageUri);
                tvUploadCard.setVisibility(View.GONE);

                registrationCardBase64 = CommonUtil.convertFileToBase64(RenewExpiredActivity.this, selectedImageUri);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            } else {
                // User refused to grant permission.
                Toast.makeText(RenewExpiredActivity.this, R.string.access_denied, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void renewCardSuccess(CommonResponse commonResponse) {
        Dialog dialog = CommonUtil.getInfoDialog(this, false, true,
                getString(R.string.after_renew_card_message), false,
                "",
                R.drawable.ic_check_done, getString(R.string.done), () -> finish());

        dialog.show();
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        viewLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        viewLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
