package dr.doctorathome.presentation.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.UserDataPresenter;
import dr.doctorathome.presentation.presenters.impl.UserDataPresenterImpl;
import dr.doctorathome.presentation.ui.fragments.BottomSheetEditBank;
import dr.doctorathome.presentation.ui.fragments.BottomSheetEditDoctorInfo;
import dr.doctorathome.presentation.ui.fragments.BottomSheetEditHeaderInfo;
import dr.doctorathome.presentation.ui.fragments.BottomSheetEditPersonalInfo;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;
import timber.log.Timber;

public class ProfileActivity extends BasicActivity implements UserDataPresenter.View {

    private UserDataPresenter userDataPresenter;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    @BindView(R.id.dataScrollView)
    ScrollView dataScrollView;

    @BindView(R.id.ivUserImage)
    ImageView ivUserImage;

    @BindView(R.id.tvDrName)
    TextView tvDrName;

    @BindView(R.id.tvSpecialistHeader)
    TextView tvSpecialistHeader;

    @BindView(R.id.tvAddress)
    TextView tvAddress;

    @BindView(R.id.tvEmail)
    TextView tvEmail;

    @BindView(R.id.tvPhone)
    TextView tvPhone;

    @BindView(R.id.tvAddressPersonal)
    TextView tvAddressPersonal;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvGender)
    TextView tvGender;

    @BindView(R.id.tvClinic)
    TextView tvClinic;

    @BindView(R.id.tvDescription)
    TextView tvDescription;

    @BindView(R.id.tvRegistrationNumber)
    TextView tvRegistrationNumber;

    @BindView(R.id.tvLevel)
    TextView tvLevel;

    @BindView(R.id.tvBankName)
    TextView tvBankName;

    @BindView(R.id.tvIban)
    TextView tvIban;

    @BindView(R.id.ivEditBankInfo)
    ImageView ivEditBankInfo;

    @BindView(R.id.ivEditDoctorInfo)
    ImageView ivEditDoctorInfo;

    @BindView(R.id.ivEditPersonalInfo)
    ImageView ivEditPersonalInfo;

    @BindView(R.id.ivEditHeader)
    ImageView ivEditHeader;

    @BindView(R.id.swOnLine)
    SwitchCompat swOnLine;

    @BindView(R.id.doc_statues_change_progress_layout)
    RelativeLayout docStatuesChangeProgressLayout;

    @BindView(R.id.image_uploading_progress_layout)
    RelativeLayout imageUploadingProgressLayout;

    private BottomSheetEditBank bottomSheetEditBank;

    private BottomSheetEditDoctorInfo bottomSheetEditDoctorInfo;

    private BottomSheetEditPersonalInfo bottomSheetEditPersonalInfo;

    private BottomSheetEditHeaderInfo bottomSheetEditHeaderInfo;

    private boolean isUploadingImage = false;

    private boolean isChangingDocStatues = false;

    private UserData userData;


    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplication.setAppLocale(CommonUtil.commonSharedPref().getString("appLanguage", "en"));
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        userDataPresenter = new UserDataPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new CommonDataRepositoryImpl(),
                new LoginRepositoryImpl(),
                new DoctorRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        userDataPresenter.getUserData();

        bottomSheetEditBank = new BottomSheetEditBank();

        bottomSheetEditDoctorInfo = new BottomSheetEditDoctorInfo();

        bottomSheetEditPersonalInfo = new BottomSheetEditPersonalInfo();

        bottomSheetEditHeaderInfo = new BottomSheetEditHeaderInfo();

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.profile);

        swOnLine.setOnClickListener(view -> {
            if (swOnLine.isChecked()) {
                requestLocation();
            } else {
                isChangingDocStatues = true;
                userDataPresenter.changeDoctorStatuesData(new RestChangeDoctorStatuesBody("OFFLINE", userData.getId()));
            }
        });

//        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//
//        listener = new LocationListener() {
//            @Override
//            public void onLocationChanged(Location location) {
//                //                t.append("\n " + location.getLongitude() + " " + location.getLatitude());
//                Timber.i(location.getLongitude() + " " + location.getLatitude());
//                locationManager.removeUpdates(listener);
//                isChangingDocStatues = true;
//                userDataPresenter.changeDoctorStatuesData(new RestChangeDoctorStatuesBody("ONLINE", userData.getId(),
//                        location.getLatitude(), location.getLongitude()));
//            }
//
//            @Override
//            public void onStatusChanged(String s, int i, Bundle bundle) {
//                Timber.i("onStatusChanged");
//            }
//
//            @Override
//            public void onProviderEnabled(String s) {
//                Timber.i("onProviderEnabled");
//            }
//
//            @Override
//            public void onProviderDisabled(String s) {
//                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(i);
//                swOnLine.setChecked(false);
//                swOnLine.setVisibility(View.VISIBLE);
//                docStatuesChangeProgressLayout.setVisibility(View.GONE);
//                locationManager.removeUpdates(listener);
//            }
//        };

        ivUserImage.setOnClickListener(view1 -> {
            if (ContextCompat.checkSelfPermission(ProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    Toast.makeText(ProfileActivity.this, R.string.weNeedAccess, Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(ProfileActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                } else {

                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(ProfileActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                requestLocation();
                break;
            default:
                break;
        }
    }

    void requestLocation() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION
                                , Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }else {
            swOnLine.setVisibility(View.INVISIBLE);
            docStatuesChangeProgressLayout.setVisibility(View.VISIBLE);
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            Timber.i(location.getLongitude() + " " + location.getLatitude());
                            isChangingDocStatues = true;
                            userDataPresenter.changeDoctorStatuesData(new RestChangeDoctorStatuesBody("ONLINE", userData.getId(),
                                    location.getLatitude(), location.getLongitude()));
                        }
                    });

//            locationManager.requestLocationUpdates("gps", 5000, 0, listener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 1) {
                final Uri selectedImageUri = data.getData();
                File imageFile = new File(selectedImageUri.getPath());
                Timber.d(imageFile.getName());

                long size = imageFile.length();

                Timber.d("size :: " + size);


                String profileImageBase64 = CommonUtil.convertFileToBase64(this, selectedImageUri);
                Glide.with(this).load(selectedImageUri)
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile_placeholder_rounded).into(ivUserImage);

                isUploadingImage = true;
                UserDataForUpdateBody userDataForUpdateBody = new UserDataForUpdateBody();
                userDataForUpdateBody.setId(userData.getId());
                userDataForUpdateBody.setProfilePicBase64(profileImageBase64);
                userDataPresenter.updateDoctorData(userDataForUpdateBody);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void userDataRetrievedSuccessfully(RestUserDataResponse restUserDataResponse) {
        userData = restUserDataResponse.getUserData();

//        userDataForUpdateBody = new UserDataForUpdateBody(restUserDataResponse.getUserData().getBank(),
//                restUserDataResponse.getUserData().getUser().getCity().getId(),
//                restUserDataResponse.getUserData().getUser().getCountry().getId(),
//                restUserDataResponse.getUserData().getUser().getFirstName(),
//                restUserDataResponse.getUserData().getUser().getGender(),
//                restUserDataResponse.getUserData().getIban(),
//                restUserDataResponse.getUserData().getId(),
//                restUserDataResponse.getUserData().getInsuranceNumber(),
//                restUserDataResponse.getUserData().getUser().getLastName(),
//                restUserDataResponse.getUserData().getLevel().getId(),"",
//                restUserDataResponse.getUserData().getQualificationDesc(),
//                restUserDataResponse.getUserData().getRegExpiryDate(),
//                restUserDataResponse.getUserData().getRegistrationNumber(),
//                restUserDataResponse.getUserData().getSpecialtyDoctor().getId());
        fillDataInSharedPref();
        fillData();
    }

    private void fillDataInSharedPref() {
        try {
            SharedPreferences.Editor editor = CommonUtil.commonSharedPref().edit();
            editor.putString("firstName", userData.getUser().getFirstName());
            editor.putString("lastName", userData.getUser().getLastName());
            editor.putString("email", userData.getUser().getEmail());
            editor.putString("specialist", userData.getSpecialtyDoctor().getName());
            editor.putString("profilePicUrl", userData.getProfilePic());
            editor.apply();
            editor.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void doctorDataUpdatedSuccessfully(CommonResponse commonResponse) {
        isUploadingImage = false;
    }

    @Override
    public void doctorStatuesSuccessfully(CommonResponse commonResponse) {
        isChangingDocStatues = false;
        Toast.makeText(this, R.string.status_changed_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void lookupsRetrievedSuccessfully(RestLookupsResponse restLookupsResponse) {
        // no need here
    }

    @Override
    public void doctorHomePageDataRetrievedSuccessfully(RestHomePageResponse restHomePageResponse) {
        //no need here
    }

    @Override
    public void visitAcceptedSuccessfully() {
        //no need here
    }

    @Override
    public void visitRejectedSuccessfully() {
        //no need here
    }

    @Override
    public void visitCancelledSuccessfully() {
        //no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        //no need here
    }

    private void fillData() {
        try {
            Glide.with(this).load(RestClient.REST_API_URL + userData.getProfilePic().substring(1))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.profile_placeholder_rounded).into(ivUserImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvDrName.setText(userData.getUser().getFirstName() + " " + userData.getUser().getLastName());
        tvSpecialistHeader.setText(userData.getSpecialtyDoctor().getName());
        tvAddress.setText(userData.getUser().getCountry().getNameEn() + ", "
                + userData.getUser().getCity().getNameEn());
        if (userData.getActivityStatus() == null ||
                (!userData.getActivityStatus().equals("ONLINE")
                        && !userData.getActivityStatus().equals("OFFLINE"))) {
            swOnLine.setChecked(false);
        } else {
            swOnLine.setChecked(userData.getActivityStatus().equals("ONLINE") ? true : false);
        }

        tvEmail.setText(userData.getUser().getEmail());
        tvPhone.setText(userData.getUser().getMobileNumber());
        tvAddressPersonal.setText(userData.getUser().getCountry().getNameEn() + ", "
                + userData.getUser().getCity().getNameEn());
        tvDate.setText(CommonUtil.dateFromServer(userData.getUser().getRegistrationDate()));
        tvGender.setText(userData.getUser().getGender().toLowerCase());
        tvClinic.setText(userData.getClinic().getName());
        tvDescription.setText(userData.getQualificationDesc());
        tvRegistrationNumber.setText(userData.getRegistrationNumber());
        tvLevel.setText(userData.getLevel().getName());
        tvBankName.setText(getString(R.string.bank) + " " + userData.getBank());
        tvIban.setText(getString(R.string.iban) + " " + userData.getIban());

        ivEditBankInfo.setOnClickListener(view -> {
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            bottomSheetEditBank.setArguments(data);
            bottomSheetEditBank.setDataBackListener(userDataArg -> {
                tvBankName.setText(getString(R.string.bank) + " " + userDataArg.getBank());
                tvIban.setText(getString(R.string.iban) + " " + userDataArg.getIban());
            });
            bottomSheetEditBank.show(getSupportFragmentManager(), BottomSheetEditBank.class.getSimpleName());
        });

        ivEditDoctorInfo.setOnClickListener(view -> {
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            bottomSheetEditDoctorInfo.setArguments(data);
            bottomSheetEditDoctorInfo.setDataBackListener(userDataArg -> {
                tvClinic.setText(userDataArg.getClinic().getName());
                tvDescription.setText(userDataArg.getQualificationDesc());
                tvRegistrationNumber.setText(userDataArg.getRegistrationNumber());
                tvLevel.setText(userDataArg.getLevel().getName());
            });
            bottomSheetEditDoctorInfo.show(getSupportFragmentManager(), BottomSheetEditDoctorInfo.class.getSimpleName());
        });

        ivEditPersonalInfo.setOnClickListener(view -> {
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            bottomSheetEditPersonalInfo.setArguments(data);
            bottomSheetEditPersonalInfo.setDataBackListener(userDataArg -> {
                tvEmail.setText(userDataArg.getUser().getEmail());
                tvPhone.setText(userDataArg.getUser().getMobileNumber());
                tvGender.setText(userDataArg.getUser().getGender().toLowerCase());
            });
            bottomSheetEditPersonalInfo.show(getSupportFragmentManager(), BottomSheetEditPersonalInfo.class.getSimpleName());
        });

        ivEditHeader.setOnClickListener(view -> {
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            bottomSheetEditHeaderInfo.setArguments(data);
            bottomSheetEditHeaderInfo.setDataBackListener(userDataArg -> {
                tvDrName.setText(userDataArg.getUser().getFirstName() + " " + userDataArg.getUser().getLastName());
                tvSpecialistHeader.setText(userDataArg.getSpecialtyDoctor().getName());
                tvAddress.setText(userDataArg.getUser().getCountry().getNameEn() + ", "
                        + userDataArg.getUser().getCity().getNameEn());
                tvAddressPersonal.setText(userDataArg.getUser().getCountry().getNameEn() + ", "
                        + userDataArg.getUser().getCity().getNameEn());
            });
            bottomSheetEditHeaderInfo.show(getSupportFragmentManager(), BottomSheetEditHeaderInfo.class.getSimpleName());
        });
    }

    @Override
    public void showProgress(String message) {
        if (!isUploadingImage && !isChangingDocStatues) {
            progressLayout.setVisibility(View.VISIBLE);
            dataScrollView.setVisibility(View.GONE);
        } else if (isUploadingImage) {
            ivUserImage.setClickable(false);
            imageUploadingProgressLayout.setVisibility(View.VISIBLE);
        } else if (isChangingDocStatues) {
            swOnLine.setVisibility(View.INVISIBLE);
            docStatuesChangeProgressLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        if (!isUploadingImage && !isChangingDocStatues) {
            progressLayout.setVisibility(View.GONE);
            dataScrollView.setVisibility(View.VISIBLE);
        } else if (isUploadingImage) {
            ivUserImage.setClickable(true);
            imageUploadingProgressLayout.setVisibility(View.GONE);
        } else if (isChangingDocStatues) {
            swOnLine.setVisibility(View.VISIBLE);
            docStatuesChangeProgressLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        if (isUploadingImage) {
            try {
                Glide.with(this).load(RestClient.REST_API_URL + userData.getProfilePic())
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile_placeholder_rounded).into(ivUserImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            isUploadingImage = false;
        } else if (isChangingDocStatues) {
            swOnLine.setChecked(!swOnLine.isChecked());
            isChangingDocStatues = false;
        }
    }
}
