package dr.doctorathome.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.presentation.presenters.LoginPresenter;
import dr.doctorathome.presentation.presenters.impl.LoginPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;
import dr.doctorathome.utils.CommonUtil;

public class LoginActivity extends BasicActivity implements LoginPresenter.View {

    @BindView(R.id.etEmailAddress)
    EditText usernameET;

    @BindView(R.id.etPassword)
    EditText passwordET;

    @BindView(R.id.btnLogin)
    Button signInButton;

    @BindView(R.id.btnForgetPassword)
    Button btnForgetPassword;

    private LoginPresenter loginPresenter;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplication.setAppLocale(CommonUtil.commonSharedPref().getString("appLanguage", "en"));
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        loginPresenter = new LoginPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new LoginRepositoryImpl(),
                CommonUtil.commonSharedPref()
        );

        if (getIntent().hasExtra("backVisible")) {
            if (!getIntent().getBooleanExtra("backVisible", true)) {
                findViewById(R.id.ivBack).setVisibility(View.GONE);
            }
        }

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.btnNewAccount).setOnClickListener(view ->
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class)));

        btnForgetPassword.setOnClickListener(view ->
                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class)));
        signInButton.setOnClickListener(view -> {
            if (validateBeforeGoToServer()) {
                String username = usernameET.getText().toString().trim();
                String password = passwordET.getText().toString();

                loginPresenter.onLoginWithMail("DOCTOR/"+username, password);
            }
        });
    }

    private boolean validateBeforeGoToServer() {
        if (usernameET.getText().toString().equals("")) {
            usernameET.setError(getString(R.string.warning_enter_email));
            usernameET.requestFocus();
            return false;
        } else if (passwordET.getText().toString().equals("")) {
            passwordET.setError(getString(R.string.warning_enter_password));
            passwordET.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void loginSuccessAndSaved() {
        if (CommonUtil.commonSharedPref().getBoolean("isExpired", false)) {
            Intent intent = new Intent(LoginActivity.this, RenewExpiredActivity.class);
            intent.putExtra("token", CommonUtil.commonSharedPref().getString("token", ""));
            intent.putExtra("doctorId", CommonUtil.commonSharedPref().getInt("doctorId", -1));
            intent.putExtra("hasAlreadySubmitted", CommonUtil.commonSharedPref()
                    .getString("uploadedExpiredCardStatus", "").equals("PENDING RENEWAL"));

            CommonUtil.logout();
            startActivity(intent);
        } else if (CommonUtil.commonSharedPref().getString("accountStatus", "").equals("PENDING REGISTRATION")) {
            Intent intent = new Intent(LoginActivity.this, RenewExpiredActivity.class);
            intent.putExtra("hasAlreadySubmitted", true);
            CommonUtil.logout();
            startActivity(intent);
        } else {
            finish();
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void showProgress(String message) {
        CommonUtil.showProgressDialog(progressDialog, message);
    }

    @Override
    public void hideProgress() {
        CommonUtil.dismissProgressDialog(progressDialog);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


}
