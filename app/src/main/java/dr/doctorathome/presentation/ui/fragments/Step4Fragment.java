package dr.doctorathome.presentation.ui.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.model.CommonDataObject;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.utils.CommonUtil;

public class Step4Fragment extends Fragment {


    @BindView(R.id.etCountry)
    AppCompatEditText etCountry;

    @BindView(R.id.etCity)
    AppCompatEditText etCity;

    @BindView(R.id.etUserName)
    AppCompatEditText etUserName;

    @BindView(R.id.etPassword)
    AppCompatEditText etPassword;

    @BindView(R.id.chPrivacy)
    CheckBox chPrivacy;

    @BindView(R.id.tvPrivacy)
    TextView tvPrivacy;


    List<CommonDataObject> countries = new ArrayList<>();
    ArrayList<String> countriesStrings = new ArrayList<>();

    List<CommonDataObject> cities = new ArrayList<>();
    ArrayList<String> citiesStrings = new ArrayList<>();

    CommonDataObject selectedCountry;
    CommonDataObject selectedCity;

    private List<CommonDataObject> allCities;

    public Step4Fragment() {
        // Required empty public constructor
    }

    public static Step4Fragment newInstance() {
        return new Step4Fragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_step4, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etCountry.setOnClickListener(view1 -> CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_country)
                , (selectedCountry == null ? -1 : countries.indexOf(selectedCountry)), countriesStrings
                , (dialogInterface, i) -> {
                    etCountry.setText(countriesStrings.get(i));
                    selectedCountry = countries.get(i);
                    selectedCity = null;
                    etCity.setText("");
                    cities = new ArrayList<>();
                    citiesStrings = new ArrayList<>();
                    for (CommonDataObject cdo : allCities) {
                        if (cdo.getCountryId() != null && cdo.getCountryId() == selectedCountry.getId()) {
                            cities.add(cdo);
                            citiesStrings.add(cdo.getNameEn());
                        }

                    }
                    dialogInterface.dismiss();
                }));
        etCity.setOnClickListener(view1 -> {
            if (citiesStrings.isEmpty()) {
                Toast.makeText(getContext(), R.string.select_country_first, Toast.LENGTH_LONG).show();
            } else {
                CommonUtil.showSelectionDialog(getContext(), getString(R.string.select_city)
                        , (selectedCity == null ? -1 : cities.indexOf(selectedCity)), citiesStrings
                        , (dialogInterface, i) -> {
                            etCity.setText(citiesStrings.get(i));
                            selectedCity = cities.get(i);
                            dialogInterface.dismiss();
                        });
            }
        });

        tvPrivacy.setOnClickListener(view1 -> {
            Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.privacy_policy_dialog_layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button acceptButton = dialog.findViewById(R.id.acceptButton);
            TextView tvPrivacyTerms = dialog.findViewById(R.id.tvPrivacyTerms);
            tvPrivacyTerms.setText(Html.fromHtml(CommonUtil.commonSharedPref()
                    .getString("termsAndConditions", "")));
            acceptButton.setOnClickListener(view2 -> {
                chPrivacy.setChecked(true);
                dialog.dismiss();
            });
            dialog.show();
        });
    }



    public void setCountries(List<CommonDataObject> countries) {
        this.countries = countries;
        countriesStrings = new ArrayList<>();
        for (CommonDataObject commonDataObject : countries) {
            countriesStrings.add(commonDataObject.getNameEn());
        }
    }

    public void setCities(List<CommonDataObject> cities) {
        this.allCities = cities;
        this.cities = new ArrayList<>();
        citiesStrings = new ArrayList<>();
        //        for (CommonDataObject commonDataObject : cities) {
        //            citiesStrings.add(commonDataObject.getNameEn());
        //        }
    }

    public boolean validateForm() {

        if (etCountry.getText().toString().trim().isEmpty()) {
            etCountry.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else {
            etCountry.setError(null);
        }
        if (etCity.getText().toString().trim().isEmpty()) {
            etCity.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else {
            etCity.setError(null);
        }
        if (etUserName.getText().toString().trim().isEmpty()) {
            etUserName.setError(getString(R.string.error_this_field_is_required));
            return false;
        }
        if (etPassword.getText().toString().trim().isEmpty()) {
            etPassword.setError(getString(R.string.error_this_field_is_required));
            return false;
        } else if (!isValidPassword(etPassword.getText().toString())) {
            etPassword.setError(getString(R.string.password_must_contain_lower_and_upper_catacters));
            etPassword.requestFocus();
            return false;
        }
        if (!chPrivacy.isChecked()) {
            Toast.makeText(getContext(), R.string.accept_privacy_validation, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public UserModel fillUserModelData(UserModel userModel) {
        userModel.setCountry(selectedCountry.getId());
        userModel.setCity(selectedCity.getId());
        userModel.setUsername(etUserName.getText().toString());
        userModel.setPassword(etPassword.getText().toString());
        return userModel;
    }
    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN =  "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        Log.d("TAG", "isValidPassword: "+matcher.matches());
        Log.d("TAG", "isValidPassword: "+matcher.pattern());
        return matcher.matches();

    }

}
