package dr.doctorathome.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.duolingo.open.rtlviewpager.RtlViewPager;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import dr.doctorathome.R;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;

public class IntroActivity extends BasicActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private RtlViewPager mViewPager;

    private LinearLayout dotsLayout;
    private TextView[] dots;

    int currentPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        List<PlaceholderFragment> placeholderFragments = new ArrayList<>();
        placeholderFragments.add(
                PlaceholderFragment.newInstance(getString(R.string.intro_desc1)
                        , R.drawable.ic_onboard_one));
        placeholderFragments.add(
                PlaceholderFragment.newInstance(getString(R.string.intro_desc2), R.drawable.ic_onboarding_two));
        placeholderFragments.add(
                PlaceholderFragment.newInstance(getString(R.string.intro_desc3), R.drawable.ic_onboarding_three));


        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), placeholderFragments);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(currentPosition + 1);
            }
        });

        findViewById(R.id.back_button).setOnClickListener(view -> mViewPager.setCurrentItem(currentPosition - 1));

        findViewById(R.id.go_to_login_btn).setOnClickListener(view -> startActivity(new Intent(IntroActivity.this, LoginActivity.class)));

        findViewById(R.id.go_to_register_btn).setOnClickListener(view -> {
            startActivity(new Intent(IntroActivity.this, RegistrationActivity.class));
            startActivity(new Intent(IntroActivity.this, AuthorizationActivity.class));

        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                updateBottomDots(currentPosition, i);
                currentPosition = i;
                if (i == 0) {
                    findViewById(R.id.back_button).setVisibility(View.INVISIBLE);
                    findViewById(R.id.login_register_layout).setVisibility(View.GONE);
                } else if (i == 2) {
                    findViewById(R.id.next_button).setVisibility(View.GONE);
                    findViewById(R.id.back_button).setVisibility(View.GONE);
                    findViewById(R.id.login_register_layout).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.next_button).setVisibility(View.VISIBLE);
                    findViewById(R.id.back_button).setVisibility(View.VISIBLE);
                    findViewById(R.id.login_register_layout).setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        dotsLayout = findViewById(R.id.layoutDots);

        // adding bottom dots
        addBottomDots();

        updateBottomDots(0, 0);

    }

    private void addBottomDots() {
        if (dotsLayout == null)
            return;

        int dotSize = 3;
        dotsLayout.removeAllViews();

        dots = new TextView[dotSize];
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(45);
            dots[i].setTextColor(getResources().getColor(R.color.dot_inactive));
            dotsLayout.addView(dots[i]);
        }
    }

    private void updateBottomDots(int prevPosition, int curPosition) {
        if (dots == null)
            return;

        int dotLength = dots.length;
        if ((dotLength > prevPosition) && (dotLength > curPosition)) {
            dots[prevPosition].setTextColor(getResources().getColor(R.color.dot_inactive));
            dots[curPosition].setTextColor(getResources().getColor(R.color.dot_active));
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SLIDER_TEXT = "slider_text";
        private static final String ARG_SLIDER_IMAGE = "slider_image";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(String text, int image) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_SLIDER_TEXT, text);
            args.putInt(ARG_SLIDER_IMAGE, image);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_intro, container, false);
            TextView textView = rootView.findViewById(R.id.slider_text);
            ImageView imageView = rootView.findViewById(R.id.slider_image);
            textView.setText(getArguments().getString(ARG_SLIDER_TEXT));
            imageView.setImageDrawable(getResources().getDrawable(getArguments().getInt(ARG_SLIDER_IMAGE)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm, List<PlaceholderFragment> fragmentList) {
            super(fm);
            this.fragmentList = fragmentList;
        }

        List<PlaceholderFragment> fragmentList;

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
