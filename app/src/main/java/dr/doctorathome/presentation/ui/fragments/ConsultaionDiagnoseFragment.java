package dr.doctorathome.presentation.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.presentation.ui.activities.ConsultationPrescriptionActivity;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultaionDiagnoseFragment extends Fragment {

    ArrayAdapter<String> dataAdapter;

    @BindView(R.id.diagnosisEditText)
    EditText diagnosisEditText;

    @BindView(R.id.common_diseases_spinner)
    Spinner commonDiseasesSpinner;
    ArrayList<String> DiseaseStrings;
    ArrayList<Integer> DiseaseIds;

    String diagnoseName = "", diagnoseId = "";
    @BindView(R.id.diagnos_card)
    CardView diagnoseCard;

    @BindView(R.id.diagnos_tv)
    TextView diagnos_tv;

    @BindView(R.id.delete_diagnose_img)
    ImageView delete_diagnose_img;

    public ConsultaionDiagnoseFragment() {
        // Required empty public constructor
    }

    public static ConsultaionDiagnoseFragment newInstance() {
        return new ConsultaionDiagnoseFragment();
    }

    public void setDiagnose(String diagnose) {
        if (diagnose == null)
            return;
        diagnosisEditText.setText(diagnose);
    }

    public String getDiagonse() {
        return diagnosisEditText.getText().toString();
    }

    public String getCommonDiagonseName() {
        return diagnoseName;
    }

    public Integer getCommonDiagonseId() {
        return Integer.valueOf(diagnoseId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_consultaion_diagnose, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setDiseaseSpinner();
        diagnosisEditText.setText(((ConsultationPrescriptionActivity) getActivity()).getDiagnose() + "");
        delete_diagnose_img.setOnClickListener(v -> {
            diagnoseCard.setVisibility(View.GONE);
            diagnoseName = "";
            diagnoseId = "";
            ((ConsultationPrescriptionActivity) getActivity()).addsuccessSnackbar(getString(R.string.diagnose_deleted_successfully));
        });
        diagnoseCard.setVisibility(View.GONE);


    }


    private void setDiseaseSpinner() {
        // Spinner click listener
        commonDiseasesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    return;
                diagnoseCard.setVisibility(View.VISIBLE);
                diagnoseName = DiseaseStrings.get(position);
                diagnos_tv.setText(diagnoseName);
                diagnoseId = DiseaseIds.get(position) + "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        DiseaseStrings = new ArrayList<String>();
        DiseaseIds = new ArrayList<Integer>();
        DiseaseStrings.add(getString(R.string._choose_Common_Diagnose));
        DiseaseIds.add(-1);
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, DiseaseStrings) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextColor(
                        getResources().getColorStateList(R.color.ef_grey)
                );
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        commonDiseasesSpinner.setAdapter(dataAdapter);
        getCommonDisease();
    }

    private void getCommonDisease() {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        doctorService.getCommonDisease("0", new HashMap<>(), CommonUtil.commonSharedPref().getString("token", "")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().get("content").getAsJsonArray().size(); i++) {
                        DiseaseIds.add(response.body().get("content").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsInt());
                        DiseaseStrings.add(response.body().get("content").getAsJsonArray().get(i).getAsJsonObject().get("name").getAsString());

                    }
                }
                dataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

    }
}
