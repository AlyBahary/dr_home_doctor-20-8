package dr.doctorathome.presentation.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dr.doctorathome.R;
import dr.doctorathome.domain.executor.impl.ThreadExecutor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.repositoryImpl.CommonDataRepositoryImpl;
import dr.doctorathome.network.repositoryImpl.DoctorRepositoryImpl;
import dr.doctorathome.presentation.adapters.SupportMessagesHistoryAdapter;
import dr.doctorathome.presentation.presenters.SettingsPresenter;
import dr.doctorathome.presentation.presenters.impl.SettingsPresenterImpl;
import dr.doctorathome.presentation.ui.helpers.BasicActivity;
import dr.doctorathome.threading.MainThreadImpl;

public class ContactUsHistoryListActivity extends BasicActivity implements SettingsPresenter.View {

    private SettingsPresenter settingsPresenter;

    @BindView(R.id.rvData)
    RecyclerView rvData;

    @BindView(R.id.progressLayout)
    RelativeLayout progressLayout;

    private SupportMessagesHistoryAdapter supportMessagesHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us_history_list);
        ButterKnife.bind(this);

        settingsPresenter = new SettingsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new DoctorRepositoryImpl(),
                new CommonDataRepositoryImpl());

        supportMessagesHistoryAdapter = new SupportMessagesHistoryAdapter(new ArrayList<>(), this);
        rvData.setAdapter(supportMessagesHistoryAdapter);

        settingsPresenter.getSupportMessagesHistory();

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.tvToolbarTitle)).setText(R.string.history_list_contact_us);
    }

    @Override
    public void clearanceRequestSubmittedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void passwordChangedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void appSettingsRetrievedSuccessfully(RestAppSettingsResponse restAppSettingsResponse) {
        // no need here
    }

    @Override
    public void supportMessageSentSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void supportMessagesHistoryRetrievedSuccessfully(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse) {
        if (restSupportMessagesHistoryResponse.getSupportMessages() != null) {
            supportMessagesHistoryAdapter.setSupportMessages(restSupportMessagesHistoryResponse.getSupportMessages());
            supportMessagesHistoryAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void supportMessageDetailsRetrievedSuccessfully(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse) {
        // no need here
    }

    @Override
    public void oneSignalUserIdSavedSuccessfully(CommonResponse commonResponse) {
        // no need here
    }

    @Override
    public void showProgress(String message) {
        progressLayout.setVisibility(View.VISIBLE);
        rvData.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressLayout.setVisibility(View.GONE);
        rvData.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message, boolean hideView) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
