package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.EndSessionInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.StartEndSessionBody;
import timber.log.Timber;

public class EndSessionInteractorImpl extends AbstractInteractor
        implements EndSessionInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private StartEndSessionBody startEndSessionBody;
    private String token;

    public EndSessionInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , StartEndSessionBody startEndSessionBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.startEndSessionBody = startEndSessionBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: EndSessionInteractorImpl");
        //make attempt to end session
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.endSession(startEndSessionBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onEndingSessionSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onEndingSessionFailed(commonResponse.getMessage()));
        }
    }
}