package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetReportInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.GetReportBody;
import dr.doctorathome.network.model.RestReportDetailsResponse;
import timber.log.Timber;

public class GetReportInteractorImpl extends AbstractInteractor
        implements GetReportInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private GetReportBody getReportBody;
    private String token;

    public GetReportInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , GetReportBody getReportBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.getReportBody = getReportBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetReportInteractorImpl");
        //make attempt to get report
        final RestReportDetailsResponse restReportDetailsResponse;
        restReportDetailsResponse = mDoctorRepository.getReport(getReportBody, token);

        if (restReportDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingReportSuccess(restReportDetailsResponse));
            return;
        }

        if (!restReportDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingReportFailed(restReportDetailsResponse.getMessage()));
        }
    }
}