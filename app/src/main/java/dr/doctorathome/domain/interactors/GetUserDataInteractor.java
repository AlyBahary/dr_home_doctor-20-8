package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestUserDataResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetUserDataInteractor extends Interactor {
    interface Callback{
        void onGettingUserDataSuccess(RestUserDataResponse restUserDataResponse);
        void onGettingUserDataFailed(String failedMessage);
    }
}
