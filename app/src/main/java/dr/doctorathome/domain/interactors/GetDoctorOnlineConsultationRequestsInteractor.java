package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetDoctorOnlineConsultationRequestsInteractor extends Interactor {
    interface Callback{
        void onGettingDoctorOnlineConsultationRequestsSuccess(
                RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse);
        void onGettingDoctorOnlineConsultationRequestsFailed(String failedMessage);
    }
}
