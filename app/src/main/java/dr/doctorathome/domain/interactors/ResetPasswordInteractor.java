package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface ResetPasswordInteractor extends Interactor {
    interface Callback{
        void onResetPasswordSuccess(CommonResponse commonResponse);
        void onResetPasswordFailed(String failedMessage);
    }
}
