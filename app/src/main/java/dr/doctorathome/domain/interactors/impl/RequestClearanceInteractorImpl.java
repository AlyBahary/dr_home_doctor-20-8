package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.RequestClearanceInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RequestClearanceBody;
import timber.log.Timber;

public class RequestClearanceInteractorImpl extends AbstractInteractor
        implements RequestClearanceInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private RequestClearanceBody requestClearanceBody;
    private String token;

    public RequestClearanceInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , RequestClearanceBody requestClearanceBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.requestClearanceBody = requestClearanceBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: RequestClearanceInteractorImpl");
        //make attempt to request clearance
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.requestClearance(requestClearanceBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRequestClearanceSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRequestClearanceFailed(commonResponse.getMessage()));
        }
    }
}