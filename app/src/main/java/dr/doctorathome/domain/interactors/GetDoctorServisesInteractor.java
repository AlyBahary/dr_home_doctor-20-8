package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestDoctorServicesResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetDoctorServisesInteractor extends Interactor {
    interface Callback {
        void onGettingDoctorServicesSuccess(RestDoctorServicesResponse restDoctorServicesResponse);

        void onGettingDoctorServicesFailed(String failedMessage);
    }
}
