package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by AbdallahGaber on 22/06/19.
 */

public interface SaveOneSignalUserIdInteractor extends Interactor {
    interface Callback{
        void onSavingOneSignalUserIdSuccess(CommonResponse commonResponse);
        void onSavingOneSignalUserIdFailed(String failedMessage);
    }
}
