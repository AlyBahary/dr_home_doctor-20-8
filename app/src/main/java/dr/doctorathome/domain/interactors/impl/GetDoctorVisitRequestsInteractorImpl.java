package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetDoctorVisitRequestsInteractor;
import dr.doctorathome.domain.interactors.UpdateDoctorInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.VisitRequestsBody;
import timber.log.Timber;

public class GetDoctorVisitRequestsInteractorImpl extends AbstractInteractor
        implements GetDoctorVisitRequestsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private VisitRequestsBody visitRequestsBody;
    private String token;

    public GetDoctorVisitRequestsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, VisitRequestsBody visitRequestsBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.visitRequestsBody = visitRequestsBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetDoctorVisitRequestsInteractorImpl");
        //make attempt to get doctor visit requests
        final RestVisitRequestsResponse restVisitRequestsResponse;
        restVisitRequestsResponse = mDoctorRepository.getDoctorVisits(visitRequestsBody, token);

        if (restVisitRequestsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorVisitRequestsSuccess(restVisitRequestsResponse));
            return;
        }

        if (!restVisitRequestsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorVisitRequestsFailed(restVisitRequestsResponse.getMessage()));
        }
    }
}