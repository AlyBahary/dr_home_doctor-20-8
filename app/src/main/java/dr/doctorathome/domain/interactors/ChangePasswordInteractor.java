package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by abdallahgaber on 26/05/19.
 */

public interface ChangePasswordInteractor extends Interactor {
    interface Callback {
        void onChangingPasswordSuccess(CommonResponse commonResponse);

        void onChangingPasswordFailed(String failedMessage);
    }
}
