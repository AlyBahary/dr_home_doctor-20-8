package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.BlockUnblockPatientInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.BlockPatientBody;
import dr.doctorathome.network.model.CommonResponse;
import timber.log.Timber;

public class BlockUnblockPatientInteractorImpl extends AbstractInteractor
        implements BlockUnblockPatientInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private BlockPatientBody blockPatientBody;
    private String token;

    public BlockUnblockPatientInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , BlockPatientBody blockPatientBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.blockPatientBody = blockPatientBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: BlockUnblockPatientInteractorImpl");
        //make attempt to block/unblock patient
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.blockUnblockPatient(blockPatientBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onBlockUnblockPatientSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onBlockUnblockPatientFailed(commonResponse.getMessage()));
        }
    }
}