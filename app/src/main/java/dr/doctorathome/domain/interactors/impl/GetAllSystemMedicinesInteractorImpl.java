package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetAllSystemMedicinesInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestAllMedicinesResponse;
import timber.log.Timber;

public class GetAllSystemMedicinesInteractorImpl extends AbstractInteractor
        implements GetAllSystemMedicinesInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private String token;

    public GetAllSystemMedicinesInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetAllSystemMedicinesInteractorImpl");
        //make attempt to get all system medicines
        final RestAllMedicinesResponse restAllMedicinesResponse;
        restAllMedicinesResponse = mDoctorRepository.getAllMedicines(token);

        if (restAllMedicinesResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingAllSystemMedicinesSuccess(
                    restAllMedicinesResponse));
            return;
        }

        if (!restAllMedicinesResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingAllSystemMedicinesFailed(
                    restAllMedicinesResponse.getMessage()));
        }
    }
}