package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestAllMedicinesResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetAllSystemMedicinesInteractor extends Interactor {
    interface Callback{
        void onGettingAllSystemMedicinesSuccess(
                RestAllMedicinesResponse restAllMedicinesResponse);
        void onGettingAllSystemMedicinesFailed(String failedMessage);
    }
}
