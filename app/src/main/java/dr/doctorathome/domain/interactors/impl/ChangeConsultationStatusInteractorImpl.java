package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangeConsultationStatusInteractor;
import dr.doctorathome.domain.interactors.ChangeVisitStatuesInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.ChangeOnlineConsultationStatuesBody;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import timber.log.Timber;

public class ChangeConsultationStatusInteractorImpl extends AbstractInteractor
        implements ChangeConsultationStatusInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody;
    private String token;

    public ChangeConsultationStatusInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.changeOnlineConsultationStatuesBody = changeOnlineConsultationStatuesBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: ChangeConsultationStatusInteractorImpl");
        //make attempt to change consultation status
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.changeOnlineConsultationStatus(changeOnlineConsultationStatuesBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangingConsultationStatusSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangingConsultationStatusFailed(commonResponse.getMessage()));
        }
    }
}