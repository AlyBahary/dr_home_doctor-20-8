package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.SaveOneSignalUserIdInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.SaveOneSignalBody;
import timber.log.Timber;

public class SaveOneSignalUserIdInteractorImpl extends AbstractInteractor
        implements SaveOneSignalUserIdInteractor {

    private Callback mCallback;
    private CommonDataRepository mCommonDataRepository;
    private SaveOneSignalBody saveOneSignalBody;
    private String token;

    public SaveOneSignalUserIdInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, CommonDataRepository mCommonDataRepository, SaveOneSignalBody saveOneSignalBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mCommonDataRepository = mCommonDataRepository;
        this.saveOneSignalBody = saveOneSignalBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: SaveOneSignalUserIdInteractorImpl");
        //make attempt to save one signal user id
        final CommonResponse commonResponse;
        commonResponse = mCommonDataRepository.saveOneSignalUserId(token, saveOneSignalBody);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSavingOneSignalUserIdSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSavingOneSignalUserIdFailed(commonResponse.getMessage()));
        }
    }
}