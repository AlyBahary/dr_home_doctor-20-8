package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.MarkNotificationAsReadInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import timber.log.Timber;

public class MarkNotificationAsReadInteractorImpl extends AbstractInteractor
        implements MarkNotificationAsReadInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int notificationId;
    private String token;

    public MarkNotificationAsReadInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, int notificationId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.notificationId = notificationId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: MarkNotificationAsReadInteractorImpl");
        //make attempt to mark notification as read
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.markNotificationAsRead(notificationId, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onMarkingNotificationAsReadSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onMarkingNotificationAsReadFailed(commonResponse.getMessage()));
        }
    }
}