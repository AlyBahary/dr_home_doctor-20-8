package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.EnableDisableDoctorServiceInteractor;
import dr.doctorathome.domain.interactors.GetDoctorServisesInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.EnableDisableServiceBody;
import dr.doctorathome.network.model.RestDoctorServicesResponse;
import timber.log.Timber;

public class EnableDisableDoctorServiceInteractorImpl extends AbstractInteractor
        implements EnableDisableDoctorServiceInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private EnableDisableServiceBody enableDisableServiceBody;
    private String token;
    private int position;

    public EnableDisableDoctorServiceInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , EnableDisableServiceBody enableDisableServiceBody, String token, int position) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.enableDisableServiceBody = enableDisableServiceBody;
        this.token = token;
        this.position = position;
    }

    @Override
    public void run() {
        Timber.w("run :: EnableDisableDoctorServiceInteractorImpl");
        //make attempt to enable/disable doctor service
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.enableDisableDoctorService(enableDisableServiceBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onEnablingDisablingDoctorServiceSuccess(commonResponse, position));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onEnablingDisablingDoctorServiceFailed(commonResponse.getMessage(), position));
        }
    }
}