package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestVisitRequestsResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetDoctorVisitRequestsInteractor extends Interactor {
    interface Callback{
        void onGettingDoctorVisitRequestsSuccess(RestVisitRequestsResponse restVisitRequestsResponse);
        void onGettingDoctorVisitRequestsFailed(String failedMessage);
    }
}
