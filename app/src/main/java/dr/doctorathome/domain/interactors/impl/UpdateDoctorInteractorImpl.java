package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.UpdateDoctorInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import timber.log.Timber;

public class UpdateDoctorInteractorImpl extends AbstractInteractor
        implements UpdateDoctorInteractor {

    private Callback mCallback;
    private LoginRepository mLoginRepository;
    private UserDataForUpdateBody userData;
    private String token;

    public UpdateDoctorInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, UserDataForUpdateBody userData, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.userData = userData;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: UpdateDoctorInteractorImpl");
        //make attempt to update doctor
        final CommonResponse commonResponse;
        commonResponse = mLoginRepository.updateDoctor(userData, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onUpdateDoctorSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onUpdateDoctorFailed(commonResponse.getMessage()));
        }
    }
}