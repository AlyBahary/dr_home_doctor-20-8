package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.SendSupportMessageInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.SendSupportMessageBody;
import timber.log.Timber;

public class SendSupportMessageInteractorImpl extends AbstractInteractor
        implements SendSupportMessageInteractor {

    private Callback mCallback;
    private CommonDataRepository mCommonDataRepository;
    private SendSupportMessageBody sendSupportMessageBody;
    private String token;

    public SendSupportMessageInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, CommonDataRepository mCommonDataRepository,
                                            SendSupportMessageBody sendSupportMessageBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mCommonDataRepository = mCommonDataRepository;
        this.sendSupportMessageBody = sendSupportMessageBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: SendSupportMessageInteractorImpl");
        //make attempt to send support message
        final CommonResponse commonResponse;
        commonResponse = mCommonDataRepository.sendSupportMessage(token, sendSupportMessageBody);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSendingSupportMessageSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSendingSupportMessageFailed(commonResponse.getMessage()));
        }
    }
}