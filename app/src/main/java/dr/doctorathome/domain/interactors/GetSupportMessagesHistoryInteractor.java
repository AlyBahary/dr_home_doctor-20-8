package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetSupportMessagesHistoryInteractor extends Interactor {
    interface Callback{
        void onGettingSupportMessagesHistorySuccess(RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse);
        void onGettingSupportMessagesHistoryFailed(String failedMessage);
    }
}
