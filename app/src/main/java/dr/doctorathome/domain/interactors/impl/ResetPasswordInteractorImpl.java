package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ResetPasswordInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ResetPasswordModel;
import timber.log.Timber;

public class ResetPasswordInteractorImpl extends AbstractInteractor
        implements ResetPasswordInteractor {

    private Callback mCallback;
    private LoginRepository mLoginRepository;
    private ResetPasswordModel resetPasswordModel;

    public ResetPasswordInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, ResetPasswordModel resetPasswordModel) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.resetPasswordModel = resetPasswordModel;
    }

    @Override
    public void run() {
        Timber.w("run :: ResetPasswordInteractorImpl");
        //make attempt to reset password
        final CommonResponse commonResponse;
        commonResponse = mLoginRepository.resetPassword(resetPasswordModel);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onResetPasswordSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onResetPasswordFailed(commonResponse.getMessage()));
        }
    }
}