package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface RatePatientInteractor extends Interactor {
    interface Callback {
        void onRatingPatientSuccess(CommonResponse commonResponse);

        void onRatingPatientFailed(String failedMessage);
    }
}
