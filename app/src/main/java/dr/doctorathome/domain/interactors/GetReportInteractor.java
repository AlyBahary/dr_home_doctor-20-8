package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestReportDetailsResponse;

/**
 * Created by AbdallahGaber on 10/06/19.
 */

public interface GetReportInteractor extends Interactor {
    interface Callback {
        void onGettingReportSuccess(RestReportDetailsResponse restReportDetailsResponse);

        void onGettingReportFailed(String failedMessage);
    }
}
