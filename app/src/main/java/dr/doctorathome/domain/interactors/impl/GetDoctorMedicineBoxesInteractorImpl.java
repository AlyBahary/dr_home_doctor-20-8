package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetDoctorMedicineBoxesInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.DoctorMedicineBoxesBody;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;
import timber.log.Timber;

public class GetDoctorMedicineBoxesInteractorImpl extends AbstractInteractor
        implements GetDoctorMedicineBoxesInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private DoctorMedicineBoxesBody doctorMedicineBoxesBody;
    private String token;

    public GetDoctorMedicineBoxesInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, DoctorMedicineBoxesBody doctorMedicineBoxesBody,
                                                String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.doctorMedicineBoxesBody = doctorMedicineBoxesBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetDoctorMedicineBoxesInteractorImpl");
        //make attempt to get doctor medicine boxes
        final RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse;
        restDoctorMedicineBoxesResponse = mDoctorRepository.getDoctorMedicineBoxes(doctorMedicineBoxesBody, token);

        if (restDoctorMedicineBoxesResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorMedicineBoxesSuccess(
                    restDoctorMedicineBoxesResponse));
            return;
        }

        if (!restDoctorMedicineBoxesResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorMedicineBoxesFailed(
                    restDoctorMedicineBoxesResponse.getMessage()));
        }
    }
}