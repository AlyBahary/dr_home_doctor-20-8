package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetDoctorOnlineConsultationRequestsInteractor;
import dr.doctorathome.domain.interactors.GetDoctorVisitRequestsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.OnlineConsultationsBody;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.VisitRequestsBody;
import timber.log.Timber;

public class GetDoctorOnlineConsultationRequestsInteractorImpl extends AbstractInteractor
        implements GetDoctorOnlineConsultationRequestsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private OnlineConsultationsBody onlineConsultationsBody;
    private String token;

    public GetDoctorOnlineConsultationRequestsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, OnlineConsultationsBody onlineConsultationsBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.onlineConsultationsBody = onlineConsultationsBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetDoctorOnlineConsultationRequestsInteractorImpl");
        //make attempt to get doctor online consultation requests
        final RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse;
        restOnlineConsultationsRequestsResponse = mDoctorRepository.getDoctorOnlineConsultations(onlineConsultationsBody, token);

        if (restOnlineConsultationsRequestsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorOnlineConsultationRequestsSuccess(
                    restOnlineConsultationsRequestsResponse));
            return;
        }

        if (!restOnlineConsultationsRequestsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorOnlineConsultationRequestsFailed(
                    restOnlineConsultationsRequestsResponse.getMessage()));
        }
    }
}