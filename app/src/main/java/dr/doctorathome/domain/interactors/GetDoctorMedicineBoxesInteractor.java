package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetDoctorMedicineBoxesInteractor extends Interactor {
    interface Callback{
        void onGettingDoctorMedicineBoxesSuccess(
                RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse);
        void onGettingDoctorMedicineBoxesFailed(String failedMessage);
    }
}
