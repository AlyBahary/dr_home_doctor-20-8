package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetOnlineConsultationDetailsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestOnlineConsultationRequestDetailsResponse;
import timber.log.Timber;

public class GetOnlineConsultationDetailsInteractorImpl extends AbstractInteractor
        implements GetOnlineConsultationDetailsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int id;
    private String token;

    public GetOnlineConsultationDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , int id, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.id = id;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetOnlineConsultationDetailsInteractorImpl");
        //make attempt to get online consultation details
        final RestOnlineConsultationRequestDetailsResponse restOnlineConsultationRequestDetailsResponse;
        restOnlineConsultationRequestDetailsResponse = mDoctorRepository.getDoctorOnlineConsultationDetails(id, token);

        if (restOnlineConsultationRequestDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingOnlineConsultationDetailsSuccess(restOnlineConsultationRequestDetailsResponse));
            return;
        }

        if (!restOnlineConsultationRequestDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingOnlineConsultationDetailsFailed(restOnlineConsultationRequestDetailsResponse.getMessage()));
        }
    }
}