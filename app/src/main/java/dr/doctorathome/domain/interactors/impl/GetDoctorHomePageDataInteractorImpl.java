package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetDoctorHomePageDataInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestHomePageResponse;
import timber.log.Timber;

public class GetDoctorHomePageDataInteractorImpl extends AbstractInteractor
        implements GetDoctorHomePageDataInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int doctorId;
    private String token;

    public GetDoctorHomePageDataInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , int doctorId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.doctorId = doctorId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetDoctorHomePageDataInteractorImpl");
        //make attempt to get doctor home page data
        final RestHomePageResponse restHomePageResponse;
        restHomePageResponse = mDoctorRepository.getDoctorHomePageData(doctorId, token);

        if (restHomePageResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorHomePageDataSuccess(restHomePageResponse));
            return;
        }

        if (!restHomePageResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorHomePageDataFailed(restHomePageResponse.getMessage()));
        }
    }
}