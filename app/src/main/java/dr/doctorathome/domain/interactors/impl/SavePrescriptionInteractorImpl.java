package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.SavePrescriptionInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.SavingPrescreptionBody;
import timber.log.Timber;

public class SavePrescriptionInteractorImpl extends AbstractInteractor
        implements SavePrescriptionInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private SavingPrescreptionBody savingPrescreptionBody;
    private String token;

    public SavePrescriptionInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , SavingPrescreptionBody savingPrescreptionBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.savingPrescreptionBody = savingPrescreptionBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: SavePrescriptionInteractorImpl");
        //make attempt to save prescription
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.savePrescription(savingPrescreptionBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSavePrescriptionSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSavePrescriptionFailed(commonResponse.getMessage()));
        }
    }
}