package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangePasswordInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.ChangePasswordBody;
import dr.doctorathome.network.model.CommonResponse;
import timber.log.Timber;

public class ChangePasswordInteractorImpl extends AbstractInteractor
        implements ChangePasswordInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private ChangePasswordBody changePasswordBody;
    private String token;

    public ChangePasswordInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , ChangePasswordBody changePasswordBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.changePasswordBody = changePasswordBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: ChangePasswordInteractorImpl");
        //make attempt to change password
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.changePassword(changePasswordBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangingPasswordSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangingPasswordFailed(commonResponse.getMessage()));
        }
    }
}