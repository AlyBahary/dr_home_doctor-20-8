package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by AbdallahGaber on 28/07/19.
 */

public interface ChangeConsultationStatusInteractor extends Interactor {
    interface Callback {
        void onChangingConsultationStatusSuccess(CommonResponse commonResponse);

        void onChangingConsultationStatusFailed(String failedMessage);
    }
}
