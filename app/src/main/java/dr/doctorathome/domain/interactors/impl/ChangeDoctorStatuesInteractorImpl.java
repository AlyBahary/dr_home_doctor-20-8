package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangeDoctorStatuesInteractor;
import dr.doctorathome.domain.interactors.UpdateDoctorInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.UserData;
import timber.log.Timber;

public class ChangeDoctorStatuesInteractorImpl extends AbstractInteractor
        implements ChangeDoctorStatuesInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private RestChangeDoctorStatuesBody changeDoctorStatuesBody;
    private String token;

    public ChangeDoctorStatuesInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, RestChangeDoctorStatuesBody changeDoctorStatuesBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.changeDoctorStatuesBody = changeDoctorStatuesBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: ChangeDoctorStatuesInteractorImpl");
        //make attempt to change doctor statues
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.changeDoctorStatues(changeDoctorStatuesBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangeDoctorStatuesSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangeDoctorStatuesFailed(commonResponse.getMessage()));
        }
    }
}