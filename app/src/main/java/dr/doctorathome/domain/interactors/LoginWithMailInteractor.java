package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestLoginResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface LoginWithMailInteractor extends Interactor {
    interface Callback{
        void onLoginSuccess(RestLoginResponse loginResponse);
        void onLoginFailed(String failedMessage);
    }
}
