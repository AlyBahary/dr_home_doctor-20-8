package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.RatePatientInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RatePatientBody;
import timber.log.Timber;

public class RatePatientInteractorImpl extends AbstractInteractor
        implements RatePatientInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private Integer visitId;
    private RatePatientBody ratePatientBody;
    private String token;

    public RatePatientInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , Integer visitId
            , RatePatientBody ratePatientBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.visitId = visitId;
        this.ratePatientBody = ratePatientBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: RatePatientInteractorImpl");
        //make attempt to rate patient
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.ratePatient(visitId, ratePatientBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRatingPatientSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRatingPatientFailed(commonResponse.getMessage()));
        }
    }
}