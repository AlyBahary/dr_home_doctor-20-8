package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetVisitDetailsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestVisitDetailsResponse;
import timber.log.Timber;

public class GetVisitDetailsInteractorImpl extends AbstractInteractor
        implements GetVisitDetailsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int visitId;
    private String token;

    public GetVisitDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , int visitId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.visitId = visitId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetVisitDetailsInteractorImpl");
        //make attempt to get visit details
        final RestVisitDetailsResponse restVisitDetailsResponse;
        restVisitDetailsResponse = mDoctorRepository.getVisitDetails(visitId, token);

        if (restVisitDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingVisitDetailsSuccess(restVisitDetailsResponse));
            return;
        }

        if (!restVisitDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingVisitDetailsFailed(restVisitDetailsResponse.getMessage()));
        }
    }
}