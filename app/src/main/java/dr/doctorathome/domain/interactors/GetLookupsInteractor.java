package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestLookupsResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetLookupsInteractor extends Interactor {
    interface Callback{
        void onGettingLookupsSuccess(RestLookupsResponse restLookupsResponse);
        void onGettingLookupsFailed(String failedMessage);
    }
}
