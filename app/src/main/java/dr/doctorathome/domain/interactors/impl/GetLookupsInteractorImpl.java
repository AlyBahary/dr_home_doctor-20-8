package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetLookupsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.network.model.RestLookupsResponse;
import timber.log.Timber;

public class GetLookupsInteractorImpl extends AbstractInteractor
        implements GetLookupsInteractor {

    private GetLookupsInteractorImpl.Callback mCallback;
    private CommonDataRepository mCommonDataRepository;

    public GetLookupsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, CommonDataRepository mCommonDataRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mCommonDataRepository = mCommonDataRepository;
    }

    @Override
    public void run() {
        Timber.w("run :: GetLookupsInteractorImpl");
        //make attempt to get lookups
        final RestLookupsResponse restLookupsResponse;
        restLookupsResponse = mCommonDataRepository.getLookups();

        if (restLookupsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingLookupsSuccess(restLookupsResponse));
            return;
        }

        if (!restLookupsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingLookupsFailed(restLookupsResponse.getMessage()));
        }
    }
}