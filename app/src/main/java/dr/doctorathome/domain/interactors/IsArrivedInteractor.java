package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.ServerCommonResponse;

/**
 * Created by AbdallahGaber on 03/09/19.
 */

public interface IsArrivedInteractor extends Interactor {
    interface Callback {
        void onDoctorArrivedSuccess(ServerCommonResponse serverCommonResponse);

        void onDoctorArrivedFailed(String failedMessage);
    }
}
