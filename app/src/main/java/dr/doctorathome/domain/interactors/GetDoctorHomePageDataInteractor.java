package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestHomePageResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetDoctorHomePageDataInteractor extends Interactor {
    interface Callback{
        void onGettingDoctorHomePageDataSuccess(RestHomePageResponse restHomePageResponse);
        void onGettingDoctorHomePageDataFailed(String failedMessage);
    }
}
