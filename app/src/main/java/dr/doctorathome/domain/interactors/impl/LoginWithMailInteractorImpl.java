package dr.doctorathome.domain.interactors.impl;

import com.auth0.android.jwt.JWT;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.LoginWithMailInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestLoginWithMailDataModel;
import timber.log.Timber;

public class LoginWithMailInteractorImpl extends AbstractInteractor
        implements LoginWithMailInteractor {

    private LoginWithMailInteractor.Callback mCallback;
    private LoginRepository mLoginRepository;
    private RestLoginWithMailDataModel loginDataModel;

    public LoginWithMailInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, RestLoginWithMailDataModel loginDataModel) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.loginDataModel = loginDataModel;
    }

    @Override
    public void run() {
        Timber.w("run :: LoginWithMailInteractorImpl");
        //make attempt to login with mail
        final RestLoginResponse loginResponseModel;
        loginResponseModel = mLoginRepository.loginWithMail(loginDataModel);

        if (loginResponseModel.isSuccess()) {
            JWT jwt = new JWT(loginResponseModel.getAccessToken());

            if (jwt.getClaim("doctorId").asInt() == null)
                mMainThread.post(() -> mCallback.onLoginFailed(loginResponseModel.getMessage()));

            mMainThread.post(() -> mCallback.onLoginSuccess(loginResponseModel));
            return;
        }

        if (!loginResponseModel.isSuccess()) {
            mMainThread.post(() -> mCallback.onLoginFailed(loginResponseModel.getMessage()));
        }
    }
}
