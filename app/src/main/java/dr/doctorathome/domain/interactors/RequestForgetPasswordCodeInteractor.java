package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestRequestForgetPasswordCodeResponse;
import dr.doctorathome.network.model.RestRequestSignupResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface RequestForgetPasswordCodeInteractor extends Interactor {
    interface Callback{
        void onRequestingForgetPasswordCodeSuccess(RestRequestForgetPasswordCodeResponse restRequestForgetPasswordCodeResponse);
        void onRequestingForgetPasswordCodeFailed(String failedMessage);
    }
}
