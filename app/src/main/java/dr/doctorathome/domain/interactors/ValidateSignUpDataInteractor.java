package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by AbdallahGaber on 12/06/19.
 */

public interface ValidateSignUpDataInteractor extends Interactor {
    interface Callback{
        void onDataValidateSuccess(CommonResponse commonResponse);
        void onDataValidateFailed(String failedMessage);
    }
}
