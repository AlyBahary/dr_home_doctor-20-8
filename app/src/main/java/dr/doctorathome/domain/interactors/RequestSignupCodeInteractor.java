package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestRequestSignupResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface RequestSignupCodeInteractor extends Interactor {
    interface Callback{
        void onRequestingSignupCodeSuccess(RestRequestSignupResponse restRequestSignupResponse);
        void onRequestingSignupCodeFailed(String failedMessage);
    }
}
