package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetPrescriptionDetailsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;
import timber.log.Timber;

public class GetPrescriptionDetailsInteractorImpl extends AbstractInteractor
        implements GetPrescriptionDetailsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int visitId;
    private String token;

    public GetPrescriptionDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , int visitId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.visitId = visitId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetPrescriptionDetailsInteractorImpl");
        //make attempt to get prescription details
        final RestPrescriptionDetailsResponse restVisitDetailsResponse;
        restVisitDetailsResponse = mDoctorRepository.getPrescriptionDetails(visitId, token);

        if (restVisitDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingPrescriptionDetailsSuccess(restVisitDetailsResponse));
            return;
        }

        if (!restVisitDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingPrescriptionDetailsFailed(restVisitDetailsResponse.getMessage()));
        }
    }
}