package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.StartSessionInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.StartEndSessionBody;
import timber.log.Timber;

public class StartSessionInteractorImpl extends AbstractInteractor
        implements StartSessionInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private StartEndSessionBody startEndSessionBody;
    private String token;

    public StartSessionInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , StartEndSessionBody startEndSessionBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.startEndSessionBody = startEndSessionBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: StartSessionInteractorImpl");
        //make attempt to start session
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.startSession(startEndSessionBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onStartingSessionSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onStartingSessionFailed(commonResponse.getMessage()));
        }
    }
}