package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ChangeVisitStatuesInteractor;
import dr.doctorathome.domain.interactors.EnableDisableDoctorServiceInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.EnableDisableServiceBody;
import timber.log.Timber;

public class ChangeVisitStatuesInteractorImpl extends AbstractInteractor
        implements ChangeVisitStatuesInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private ChangeVisitStatuesBody changeVisitStatuesBody;
    private String token;

    public ChangeVisitStatuesInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , ChangeVisitStatuesBody changeVisitStatuesBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.changeVisitStatuesBody = changeVisitStatuesBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: ChangeVisitStatuesInteractorImpl");
        //make attempt to change visit statues
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.changeVisitStatues(changeVisitStatuesBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangingVisitStatuesSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onChangingVisitStatuesFailed(commonResponse.getMessage()));
        }
    }
}