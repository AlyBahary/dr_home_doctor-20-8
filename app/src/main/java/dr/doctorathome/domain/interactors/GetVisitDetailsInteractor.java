package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestVisitDetailsResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetVisitDetailsInteractor extends Interactor {
    interface Callback {
        void onGettingVisitDetailsSuccess(RestVisitDetailsResponse restVisitDetailsResponse);

        void onGettingVisitDetailsFailed(String failedMessage);
    }
}
