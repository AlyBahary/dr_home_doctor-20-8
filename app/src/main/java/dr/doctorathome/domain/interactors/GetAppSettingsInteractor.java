package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestAppSettingsResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetAppSettingsInteractor extends Interactor {
    interface Callback{
        void onGettingAppSettingsSuccess(RestAppSettingsResponse restAppSettingsResponse);
        void onGettingAppSettingsFailed(String failedMessage);
    }
}
