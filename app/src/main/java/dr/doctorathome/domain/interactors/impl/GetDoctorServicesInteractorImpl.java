package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetDoctorServisesInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestDoctorServicesResponse;
import timber.log.Timber;

public class GetDoctorServicesInteractorImpl extends AbstractInteractor
        implements GetDoctorServisesInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int doctorId;
    private String token;

    public GetDoctorServicesInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , int doctorId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.doctorId = doctorId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetDoctorServicesInteractorImpl");
        //make attempt to get doctor services
        final RestDoctorServicesResponse restDoctorServicesResponse;
        restDoctorServicesResponse = mDoctorRepository.getDoctorServices(doctorId, token);

        if (restDoctorServicesResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorServicesSuccess(restDoctorServicesResponse));
            return;
        }

        if (!restDoctorServicesResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingDoctorServicesFailed(restDoctorServicesResponse.getMessage()));
        }
    }
}