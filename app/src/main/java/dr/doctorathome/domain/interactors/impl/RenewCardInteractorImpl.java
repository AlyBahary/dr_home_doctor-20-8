package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetReportInteractor;
import dr.doctorathome.domain.interactors.RenewCardInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.GetReportBody;
import dr.doctorathome.network.model.RenewCardBody;
import dr.doctorathome.network.model.RestReportDetailsResponse;
import timber.log.Timber;

public class RenewCardInteractorImpl extends AbstractInteractor
        implements RenewCardInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private RenewCardBody renewCardBody;
    private String token;

    public RenewCardInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , RenewCardBody renewCardBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.renewCardBody = renewCardBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: RenewCardInteractorImpl");
        //make attempt to renew card
        final CommonResponse commonResponse;
        commonResponse = mDoctorRepository.renewRegistrationCard(renewCardBody, token);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRenewCardSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRenewCardFailed(commonResponse.getMessage()));
        }
    }
}