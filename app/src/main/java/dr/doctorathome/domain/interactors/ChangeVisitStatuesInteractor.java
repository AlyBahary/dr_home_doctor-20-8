package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface ChangeVisitStatuesInteractor extends Interactor {
    interface Callback {
        void onChangingVisitStatuesSuccess(CommonResponse commonResponse);

        void onChangingVisitStatuesFailed(String failedMessage);
    }
}
