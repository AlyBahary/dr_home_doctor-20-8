package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetUserDataInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.RestUserDataResponse;
import timber.log.Timber;

public class GetUserDataInteractorImpl extends AbstractInteractor
        implements GetUserDataInteractor {

    private Callback mCallback;
    private LoginRepository mLoginRepository;
    private Integer id;
    private String token;

    public GetUserDataInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, Integer id, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.id = id;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetUserDataInteractorImpl");
        //make attempt to get user data
        final RestUserDataResponse restUserDataResponse;
        restUserDataResponse = mLoginRepository.getUserData(id, token);

        if (restUserDataResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingUserDataSuccess(restUserDataResponse));
            return;
        }

        if (!restUserDataResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingUserDataFailed(restUserDataResponse.getMessage()));
        }
    }
}