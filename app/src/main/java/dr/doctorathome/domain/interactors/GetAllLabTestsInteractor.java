package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestAllLabTestsResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetAllLabTestsInteractor extends Interactor {
    interface Callback{
        void onGettingAllLabTestsSuccess(
                RestAllLabTestsResponse restAllLabTestsResponse);
        void onGettingAllLabTestsFailed(String failedMessage);
    }
}
