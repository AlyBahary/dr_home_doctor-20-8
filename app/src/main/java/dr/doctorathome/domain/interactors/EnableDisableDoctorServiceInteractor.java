package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestDoctorServicesResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface EnableDisableDoctorServiceInteractor extends Interactor {
    interface Callback {
        void onEnablingDisablingDoctorServiceSuccess(CommonResponse commonResponse, int position);

        void onEnablingDisablingDoctorServiceFailed(String failedMessage, int position);
    }
}
