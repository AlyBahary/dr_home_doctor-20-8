package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.IsArrivedInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.DoctorNewLocationBody;
import dr.doctorathome.network.model.ServerCommonResponse;
import timber.log.Timber;

public class IsArrivedInteractorImpl extends AbstractInteractor
        implements IsArrivedInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private DoctorNewLocationBody doctorNewLocationBody;
    private String token;

    public IsArrivedInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , DoctorNewLocationBody doctorNewLocationBody, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.doctorNewLocationBody = doctorNewLocationBody;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: IsArrivedInteractorImpl");
        //make attempt to check if doctor arrived
        final ServerCommonResponse serverCommonResponse;
        serverCommonResponse = mDoctorRepository.updateDoctorLocation(doctorNewLocationBody, token);

        if (serverCommonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onDoctorArrivedSuccess(serverCommonResponse));
            return;
        }

        if (!serverCommonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onDoctorArrivedFailed(serverCommonResponse.getMessage()));
        }
    }
}