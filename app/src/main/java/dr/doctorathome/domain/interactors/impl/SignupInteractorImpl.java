package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.RequestSignupCodeInteractor;
import dr.doctorathome.domain.interactors.SignupInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestRequestSignupResponse;
import dr.doctorathome.network.model.UserModel;
import timber.log.Timber;

public class SignupInteractorImpl extends AbstractInteractor
        implements SignupInteractor {

    private Callback mCallback;
    private LoginRepository mLoginRepository;
    private UserModel userModel;

    public SignupInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, UserModel userModel) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.userModel = userModel;
    }

    @Override
    public void run() {
        Timber.w("run :: SignupInteractorImpl");
        //make attempt to signup
        final CommonResponse commonResponse;
        commonResponse = mLoginRepository.signup(userModel);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSignupSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onSignupFailed(commonResponse.getMessage()));
        }
    }
}