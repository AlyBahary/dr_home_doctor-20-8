package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.ServerCommonResponse;

/**
 * Created by AbdallahGaber on 19/06/19.
 */

public interface UpdateDoctorLocationInteractor extends Interactor {
    interface Callback {
        void onUpdatingDoctorLocationSuccess(ServerCommonResponse serverCommonResponse);

        void onUpdatingDoctorLocationFailed(String failedMessage);
    }
}
