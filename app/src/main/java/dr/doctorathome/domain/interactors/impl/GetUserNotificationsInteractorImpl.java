package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetUserNotificationsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestNotificationsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import timber.log.Timber;

public class GetUserNotificationsInteractorImpl extends AbstractInteractor
        implements GetUserNotificationsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int userId;
    private int pageNumber;
    private String token;

    public GetUserNotificationsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, int userId, String token, int pageNumber) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.userId = userId;
        this.token = token;
        this.pageNumber = pageNumber;
    }

    @Override
    public void run() {
        Timber.w("run :: GetUserNotificationsInteractorImpl");
        //make attempt to get user notifications
        final RestNotificationsResponse restNotificationsResponse;
        restNotificationsResponse = mDoctorRepository.getUserNotifications(userId, token, pageNumber);

        if (restNotificationsResponse.isSuccess()) {

            mMainThread.post(() -> mCallback.onGettingUserNotificationsSuccess(restNotificationsResponse));
            return;
        }

        if (!restNotificationsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingUserNotificationsFailed(restNotificationsResponse.getMessage()));
        }
    }
}