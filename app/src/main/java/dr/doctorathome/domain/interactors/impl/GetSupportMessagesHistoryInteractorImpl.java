package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetSupportMessagesHistoryInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import timber.log.Timber;

public class GetSupportMessagesHistoryInteractorImpl extends AbstractInteractor
        implements GetSupportMessagesHistoryInteractor {

    private Callback mCallback;
    private CommonDataRepository mCommonDataRepository;
    private int userId;
    private String token;

    public GetSupportMessagesHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, CommonDataRepository mCommonDataRepository, int userId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mCommonDataRepository = mCommonDataRepository;
        this.userId = userId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetSupportMessagesHistoryInteractorImpl");
        //make attempt to get support messages history
        final RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse;
        restSupportMessagesHistoryResponse = mCommonDataRepository.getSupportMessagesHistory(token, userId);

        if (restSupportMessagesHistoryResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingSupportMessagesHistorySuccess(restSupportMessagesHistoryResponse));
            return;
        }

        if (!restSupportMessagesHistoryResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingSupportMessagesHistoryFailed(restSupportMessagesHistoryResponse.getMessage()));
        }
    }
}