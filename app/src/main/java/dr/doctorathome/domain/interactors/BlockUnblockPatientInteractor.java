package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by abdallahgaber on 25/05/19.
 */

public interface BlockUnblockPatientInteractor extends Interactor {
    interface Callback {
        void onBlockUnblockPatientSuccess(CommonResponse commonResponse);

        void onBlockUnblockPatientFailed(String failedMessage);
    }
}
