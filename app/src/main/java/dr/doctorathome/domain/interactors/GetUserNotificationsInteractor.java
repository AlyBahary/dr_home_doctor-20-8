package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestNotificationsResponse;

/**
 * Created by AbdallahGaber on 22/06/19.
 */

public interface GetUserNotificationsInteractor extends Interactor {
    interface Callback{
        void onGettingUserNotificationsSuccess(RestNotificationsResponse restNotificationsResponse);
        void onGettingUserNotificationsFailed(String failedMessage);
    }
}
