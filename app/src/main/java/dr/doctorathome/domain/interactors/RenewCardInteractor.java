package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by AbdallahGaber on 11/06/19.
 */

public interface RenewCardInteractor extends Interactor {
    interface Callback {
        void onRenewCardSuccess(CommonResponse commonResponse);

        void onRenewCardFailed(String failedMessage);
    }
}
