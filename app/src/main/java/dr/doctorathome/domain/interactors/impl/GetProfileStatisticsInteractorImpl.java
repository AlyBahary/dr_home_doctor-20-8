package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetProfileStatisticsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestProfileStatistcsResponse;
import timber.log.Timber;

public class GetProfileStatisticsInteractorImpl extends AbstractInteractor
        implements GetProfileStatisticsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private int doctorId;
    private String token;

    public GetProfileStatisticsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository
            , int doctorId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.doctorId = doctorId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetProfileStatisticsInteractorImpl");
        //make attempt to get profile statistics
        final RestProfileStatistcsResponse restProfileStatistcsResponse;
        restProfileStatistcsResponse = mDoctorRepository.getDoctorProfileStatistics(doctorId, token);

        if (restProfileStatistcsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingProfileStatisticsSuccess(restProfileStatistcsResponse));
            return;
        }

        if (!restProfileStatistcsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingProfileStatisticsFailed(restProfileStatistcsResponse.getMessage()));
        }
    }
}