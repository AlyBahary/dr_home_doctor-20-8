package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.CommonResponse;

/**
 * Created by abdallahgaber on 25/05/19.
 */

public interface SavePrescriptionInteractor extends Interactor {
    interface Callback {
        void onSavePrescriptionSuccess(CommonResponse commonResponse);

        void onSavePrescriptionFailed(String failedMessage);
    }
}
