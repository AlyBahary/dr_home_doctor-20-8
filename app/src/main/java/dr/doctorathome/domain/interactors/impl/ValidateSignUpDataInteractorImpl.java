package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.ValidateSignUpDataInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ValidateSignUpDataBody;
import timber.log.Timber;

public class ValidateSignUpDataInteractorImpl extends AbstractInteractor
        implements ValidateSignUpDataInteractor {

    private Callback mCallback;
    private LoginRepository mLoginRepository;
    private ValidateSignUpDataBody validateSignUpDataBody;

    public ValidateSignUpDataInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, ValidateSignUpDataBody validateSignUpDataBody) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.validateSignUpDataBody = validateSignUpDataBody;
    }

    @Override
    public void run() {
        Timber.w("run :: ValidateSignUpDataInteractorImpl");
        //make attempt to validate sign up data
        final CommonResponse commonResponse;
        commonResponse = mLoginRepository.validateSignUpData(validateSignUpDataBody);

        if (commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onDataValidateSuccess(commonResponse));
            return;
        }

        if (!commonResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onDataValidateFailed(commonResponse.getMessage()));
        }
    }
}