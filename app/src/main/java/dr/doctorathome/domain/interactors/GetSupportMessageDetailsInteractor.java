package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;

/**
 * Created by AbdallahGaber on 18/06/19.
 */

public interface GetSupportMessageDetailsInteractor extends Interactor {
    interface Callback{
        void onGettingSupportMessageDetailsSuccess(RestSupportMessageDetailsResponse restSupportMessageDetailsResponse);
        void onGettingSupportMessageDetailsFailed(String failedMessage);
    }
}
