package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetAppSettingsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import timber.log.Timber;

public class GetAppSettingsInteractorImpl extends AbstractInteractor
        implements GetAppSettingsInteractor {

    private Callback mCallback;
    private CommonDataRepository mCommonDataRepository;

    public GetAppSettingsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, CommonDataRepository mCommonDataRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mCommonDataRepository = mCommonDataRepository;
    }

    @Override
    public void run() {
        Timber.w("run :: GetAppSettingsInteractorImpl");
        //make attempt to get app settings
        final RestAppSettingsResponse restAppSettingsResponse;
        restAppSettingsResponse = mCommonDataRepository.getAppSettings();

        if (restAppSettingsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingAppSettingsSuccess(restAppSettingsResponse));
            return;
        }

        if (!restAppSettingsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingAppSettingsFailed(restAppSettingsResponse.getMessage()));
        }
    }
}