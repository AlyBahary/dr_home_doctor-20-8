package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;

/**
 * Created by abdallahgaber on 21/03/17.
 */

public interface GetPrescriptionDetailsInteractor extends Interactor {
    interface Callback {
        void onGettingPrescriptionDetailsSuccess(RestPrescriptionDetailsResponse restPrescriptionDetailsResponse);

        void onGettingPrescriptionDetailsFailed(String failedMessage);
    }
}
