package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.RequestForgetPasswordCodeInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.RestRequestForgetPasswordCodeResponse;
import dr.doctorathome.network.model.RestRequestSignupResponse;
import timber.log.Timber;

public class RequestForgetPasswordCodeInteractorImpl extends AbstractInteractor
        implements RequestForgetPasswordCodeInteractor {

    private Callback mCallback;
    private LoginRepository mLoginRepository;
    private String emailAddress;

    public RequestForgetPasswordCodeInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, String emailAddress) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.emailAddress = emailAddress;
    }

    @Override
    public void run() {
        Timber.w("run :: RequestForgetPasswordCodeInteractorImpl");
        //make attempt to request forget password code
        final RestRequestForgetPasswordCodeResponse restRequestForgetPasswordCodeResponse;
        restRequestForgetPasswordCodeResponse = mLoginRepository.requestForgetPasswordCode(emailAddress);

        if (restRequestForgetPasswordCodeResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRequestingForgetPasswordCodeSuccess(restRequestForgetPasswordCodeResponse));
            return;
        }

        if (!restRequestForgetPasswordCodeResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRequestingForgetPasswordCodeFailed(restRequestForgetPasswordCodeResponse.getMessage()));
        }
    }
}