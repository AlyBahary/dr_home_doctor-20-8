package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestProfileStatistcsResponse;
import dr.doctorathome.network.model.RestReportDetailsResponse;

/**
 * Created by AbdallahGaber on 17/06/19.
 */

public interface GetProfileStatisticsInteractor extends Interactor {
    interface Callback {
        void onGettingProfileStatisticsSuccess(RestProfileStatistcsResponse restProfileStatistcsResponse);

        void onGettingProfileStatisticsFailed(String failedMessage);
    }
}
