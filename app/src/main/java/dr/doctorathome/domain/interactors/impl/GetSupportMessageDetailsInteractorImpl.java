package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetSupportMessageDetailsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import timber.log.Timber;

public class GetSupportMessageDetailsInteractorImpl extends AbstractInteractor
        implements GetSupportMessageDetailsInteractor {

    private Callback mCallback;
    private CommonDataRepository mCommonDataRepository;
    private int supportMessageId;
    private String token;

    public GetSupportMessageDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, CommonDataRepository mCommonDataRepository, int supportMessageId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mCommonDataRepository = mCommonDataRepository;
        this.supportMessageId = supportMessageId;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetSupportMessageDetailsInteractorImpl");
        //make attempt to get support message details
        final RestSupportMessageDetailsResponse restSupportMessageDetailsResponse;
        restSupportMessageDetailsResponse = mCommonDataRepository.getSupportMessageDetails(token, supportMessageId);

        if (restSupportMessageDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingSupportMessageDetailsSuccess(restSupportMessageDetailsResponse));
            return;
        }

        if (!restSupportMessageDetailsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingSupportMessageDetailsFailed(restSupportMessageDetailsResponse.getMessage()));
        }
    }
}