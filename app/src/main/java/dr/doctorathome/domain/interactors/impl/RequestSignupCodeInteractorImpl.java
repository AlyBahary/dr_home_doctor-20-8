package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.RequestSignupCodeInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.RestRequestSignupResponse;
import timber.log.Timber;

public class RequestSignupCodeInteractorImpl extends AbstractInteractor
        implements RequestSignupCodeInteractor {

    private Callback mCallback;
    private LoginRepository mLoginRepository;
    private String phoneNumber;

    public RequestSignupCodeInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, LoginRepository mLoginRepository, String phoneNumber) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mLoginRepository = mLoginRepository;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void run() {
        Timber.w("run :: RequestSignupCodeInteractorImpl");
        //make attempt to request signup code
        final RestRequestSignupResponse restRequestSignupResponse;
        restRequestSignupResponse = mLoginRepository.requestSignupCode(phoneNumber);

        if (restRequestSignupResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRequestingSignupCodeSuccess(restRequestSignupResponse));
            return;
        }

        if (!restRequestSignupResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onRequestingSignupCodeFailed(restRequestSignupResponse.getMessage()));
        }
    }
}