package dr.doctorathome.domain.interactors.impl;

import dr.doctorathome.domain.executor.Executor;
import dr.doctorathome.domain.executor.MainThread;
import dr.doctorathome.domain.interactors.GetAllLabTestsInteractor;
import dr.doctorathome.domain.interactors.base.AbstractInteractor;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.model.RestAllLabTestsResponse;
import timber.log.Timber;

public class GetAllLabTestsInteractorImpl extends AbstractInteractor
        implements GetAllLabTestsInteractor {

    private Callback mCallback;
    private DoctorRepository mDoctorRepository;
    private String token;

    public GetAllLabTestsInteractorImpl(Executor threadExecutor, MainThread mainThread
            , Callback mCallback, DoctorRepository mDoctorRepository, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mDoctorRepository = mDoctorRepository;
        this.token = token;
    }

    @Override
    public void run() {
        Timber.w("run :: GetAllLabTestsInteractorImpl");
        //make attempt to get all lab tests
        final RestAllLabTestsResponse restAllLabTestsResponse;
        restAllLabTestsResponse = mDoctorRepository.getAllLabTests(token);

        if (restAllLabTestsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingAllLabTestsSuccess(
                    restAllLabTestsResponse));
            return;
        }

        if (!restAllLabTestsResponse.isSuccess()) {
            mMainThread.post(() -> mCallback.onGettingAllLabTestsFailed(
                    restAllLabTestsResponse.getMessage()));
        }
    }
}