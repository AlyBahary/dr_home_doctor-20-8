package dr.doctorathome.domain.interactors;


import dr.doctorathome.domain.interactors.base.Interactor;
import dr.doctorathome.network.model.RestOnlineConsultationRequestDetailsResponse;

/**
 * Created by AbdallahGaber on 28/07/19.
 */

public interface GetOnlineConsultationDetailsInteractor extends Interactor {
    interface Callback {
        void onGettingOnlineConsultationDetailsSuccess(RestOnlineConsultationRequestDetailsResponse
                                                               restOnlineConsultationRequestDetailsResponse);

        void onGettingOnlineConsultationDetailsFailed(String failedMessage);
    }
}
