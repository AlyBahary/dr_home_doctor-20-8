package dr.doctorathome.domain.repositories;

import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.model.SaveOneSignalBody;
import dr.doctorathome.network.model.SendSupportMessageBody;

public interface CommonDataRepository {
    RestLookupsResponse getLookups();
    RestAppSettingsResponse getAppSettings();
    CommonResponse sendSupportMessage(String token
            , SendSupportMessageBody sendSupportMessageBody);
    RestSupportMessagesHistoryResponse getSupportMessagesHistory(String token, int userId);
    RestSupportMessageDetailsResponse getSupportMessageDetails(String token, int supportMessageId);
    CommonResponse saveOneSignalUserId(String token, SaveOneSignalBody saveOneSignalBody);
}
