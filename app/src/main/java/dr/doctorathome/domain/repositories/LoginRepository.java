package dr.doctorathome.domain.repositories;

import android.content.Context;

import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ResetPasswordModel;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestLoginWithMailDataModel;
import dr.doctorathome.network.model.RestRequestForgetPasswordCodeResponse;
import dr.doctorathome.network.model.RestRequestSignupResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.network.model.ValidateSignUpDataBody;

public interface LoginRepository {
    RestLoginResponse loginWithMail(RestLoginWithMailDataModel loginDataModel);
    RestLoginResponse refreshToken(RestLoginWithMailDataModel loginDataModel);
    RestRequestSignupResponse requestSignupCode(String phoneNumber);
    RestRequestForgetPasswordCodeResponse requestForgetPasswordCode(String email);
    CommonResponse signup(UserModel userModel);
    CommonResponse resetPassword(ResetPasswordModel resetPasswordModel);
    RestUserDataResponse getUserData(Integer id, String token);
    CommonResponse updateDoctor(UserDataForUpdateBody userData, String token);
    CommonResponse validateSignUpData(ValidateSignUpDataBody validateSignUpDataBody);
}
