//package dr.doctorathome.utils;
//
//
//import android.content.Context;
//import android.content.res.TypedArray;
//
//import android.graphics.drawable.Drawable;
//import android.util.AttributeSet;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.devlomi.record_view.OnRecordClickListener;
//import com.devlomi.record_view.RecordView;
//import com.devlomi.record_view.ScaleAnim;
//
//import androidx.appcompat.content.res.AppCompatResources;
//import androidx.appcompat.widget.AppCompatImageView;
//
///**
// * Created by Devlomi on 13/12/2017.
// */
//
//public class RecordButtonClass extends AppCompatImageView implements View.OnTouchListener, View.OnClickListener {
//
//    private ScaleAnim scaleAnim;
//    private RecordViewExtendedClass recordView;
//    private boolean listenForRecord = true;
//    private OnRecordClickListener onRecordClickListener;
//
//
//    public void setRecordView(RecordViewExtendedClass recordView) {
//        this.recordView = recordView;
//    }
//
//    public RecordButtonClass(Context context) {
//        super(context);
//        init(context, null);
//    }
//
//    public RecordButtonClass(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        init(context, attrs);
//    }
//
//    public RecordButtonClass(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        init(context, attrs);
//
//
//    }
//
//    private void init(Context context, AttributeSet attrs) {
//        if (attrs != null) {
//            TypedArray typedArray = context.obtainStyledAttributes(attrs, com.devlomi.record_view.R.styleable.RecordButton);
//
//            int imageResource = typedArray.getResourceId(com.devlomi.record_view.R.styleable.RecordButton_mic_icon, -1);
//
//
//            if (imageResource != -1) {
//                setTheImageResource(imageResource);
//            }
//
//            typedArray.recycle();
//        }
//
//
//        scaleAnim = new ScaleAnim(this);
//
//
//        this.setOnTouchListener(this);
//        this.setOnClickListener(this);
//
//
//    }
//
//    @Override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//        setClip(this);
//    }
//
//    public void setClip(View v) {
//        if (v.getParent() == null) {
//            return;
//        }
//
//        if (v instanceof ViewGroup) {
//            ((ViewGroup) v).setClipChildren(false);
//            ((ViewGroup) v).setClipToPadding(false);
//        }
//
//        if (v.getParent() instanceof View) {
//            setClip((View) v.getParent());
//        }
//    }
//
//
//    private void setTheImageResource(int imageResource) {
//        Drawable image = AppCompatResources.getDrawable(getContext(), imageResource);
//        setImageDrawable(image);
//    }
//
//
//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        if (isListenForRecord()) {
//            switch (event.getAction()) {
//
//                case MotionEvent.ACTION_DOWN:
//                    recordView.onActionDown((com.devlomi.record_view.RecordButton) v, event);
//                    break;
//
//
//                case MotionEvent.ACTION_MOVE:
//                    recordView.onActionMove((com.devlomi.record_view.RecordButton) v, event);
//                    break;
//
//                case MotionEvent.ACTION_UP:
//                    recordView.onActionUp((com.devlomi.record_view.RecordButton) v);
//                    break;
//
//            }
//
//        }
//        return isListenForRecord();
//
//
//    }
//
//
//
//    public void setListenForRecord(boolean listenForRecord) {
//        this.listenForRecord = listenForRecord;
//    }
//
//    public boolean isListenForRecord() {
//        return listenForRecord;
//    }
//
//    public void setOnRecordClickListener(OnRecordClickListener onRecordClickListener) {
//        this.onRecordClickListener = onRecordClickListener;
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        if (onRecordClickListener != null)
//            onRecordClickListener.onClick(v);
//    }
//}
//
