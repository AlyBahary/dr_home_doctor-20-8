package dr.doctorathome.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;

public class CommonUtil {

    public static void makeDefaultLocaleToArabic(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Config.PREFS_NAME, 0);
        AndroidApplication.setAppLocale(sharedPref.getString("appLanguage", "ar"));
    }

    public static SharedPreferences commonSharedPref() {
        SharedPreferences pref = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, 0);
        return pref;
    }

    public static void logout() {
        SharedPreferences pref = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("token");
        editor.remove("doctorId");
        editor.remove("userId");
        editor.remove("firstName");
        editor.remove("lastName");
        editor.remove("email");
        editor.remove("specialist");
        editor.remove("profilePicUrl");
        editor.remove("uploadedExpiredCardStatus");
        editor.remove("isExpired");
        editor.remove("oneSignalUserId");
        editor.remove("userIdSaved");
        editor.apply();
        editor.commit();
    }

    public static String readIt(InputStream stream, long len) throws IOException {
        int n = 0;
        char[] buffer = new char[1024 * 4];
        InputStreamReader reader = new InputStreamReader(stream, "UTF8");
        StringWriter writer = new StringWriter();
        while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
        return writer.toString();
    }

    public static void showProgressDialog(ProgressDialog progressDialog, String loadingMessage) {
        try {
            if (progressDialog != null) {
                progressDialog.setTitle(AndroidApplication.getAppContext().getString(R.string.loading_string));
                progressDialog.setMessage(loadingMessage);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static void showSelectionDialog(Context context, String title, int defaultSelected
            , ArrayList<String> items, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder selectionDialogBuilder = new AlertDialog.Builder(context);
        selectionDialogBuilder.setSingleChoiceItems(arrayListToArrayOfCharSequenceItems(items), defaultSelected, onClickListener);
        selectionDialogBuilder.setNegativeButton(context.getString(R.string.cancel), null);
        selectionDialogBuilder.setTitle(title);
        selectionDialogBuilder.show();
    }

    public static void showRequestFilterMenu(Context context, View anchorView
            , PopupMenu.OnMenuItemClickListener onClickListener) {
        PopupMenu popupMenu = new PopupMenu(context, anchorView);
        popupMenu.getMenuInflater().inflate(R.menu.menu_filter, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(onClickListener);
    }

    public static void showOnlineRequestFilterMenu(Context context, View anchorView
            , PopupMenu.OnMenuItemClickListener onClickListener) {
        PopupMenu popupMenu = new PopupMenu(context, anchorView);
        popupMenu.getMenuInflater().inflate(R.menu.menu_filter_consultation, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(onClickListener);
    }

    public static CharSequence[] arrayListToArrayOfCharSequenceItems(ArrayList<String> items) {
        CharSequence[] listOfItems = new CharSequence[items.size()];
        for (int i = 0; i < items.size(); i++) {
            listOfItems[i] = items.get(i);
        }
        return listOfItems;
    }

    public static boolean validateEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static boolean validatePhoneNumber(String phoneNumber) {
        Pattern pattern =
                Pattern.compile("^(009665|9665|\\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$");

        return pattern.matcher(phoneNumber).matches();
    }

    public static String convertFileToBase64(Context context, Uri fileURI) {
        String convertedImage = "";
        try {
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(String.valueOf(fileURI));
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
            Bitmap bm = getBitmapFromUri(context, fileURI);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            bm.compress(Bitmap.CompressFormat.JPEG, 60, baos); //bm is the bitmap object

            byte[] b = baos.toByteArray();

            String encodedImage = android.util.Base64.encodeToString(b, android.util.Base64.NO_WRAP);

            convertedImage = /*"data:" + type + ";base64," + */encodedImage;

            FileLoggingTree fileLoggingTree = new FileLoggingTree();
            fileLoggingTree.log(0, "base64Image", convertedImage, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedImage;
    }

    private static Bitmap getBitmapFromUri(Context context, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static Dialog getInfoDialog(Context context, boolean cancelable, boolean titleVisible, String title
            , boolean contentVisible, String content, int imageResourceId, String btnText
            , CommonCallback commonCallback) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(cancelable);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView dialogTitle = dialog.findViewById(R.id.dialog_title);
        if (titleVisible) {
            dialogTitle.setText(title);
        }
        dialogTitle.setVisibility(titleVisible ? View.VISIBLE : View.INVISIBLE);

        TextView dialogBody = dialog.findViewById(R.id.dialog_message);
        if (contentVisible) {
            dialogBody.setText(content);
        }
        dialogBody.setVisibility(contentVisible ? View.VISIBLE : View.INVISIBLE);

        ImageView dialogImage = dialog.findViewById(R.id.dialog_icon);
        dialogImage.setImageResource(imageResourceId);

        Button neutralButton = dialog.findViewById(R.id.btDialogNeutral);
        neutralButton.setVisibility(View.VISIBLE);
        dialog.findViewById(R.id.yesNoBtnsLayout).setVisibility(View.GONE);
        neutralButton.setText(btnText);
        neutralButton.setOnClickListener(view -> {
            commonCallback.callbackAction();
            dialog.dismiss();
        });

        return dialog;
    }

    public static Dialog getConfirmation2BtnsDialog(Context context, boolean cancelable, boolean titleVisible, String title
            , boolean contentVisible, String content, int imageResourceId, String positiveBtnText, String negativeBtnText
            , CommonCallback positiveBtnCallback) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(cancelable);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView dialogTitle = dialog.findViewById(R.id.dialog_title);
        if (titleVisible) {
            dialogTitle.setText(title);
        }
        dialogTitle.setVisibility(titleVisible ? View.VISIBLE : View.INVISIBLE);

        TextView dialogBody = dialog.findViewById(R.id.dialog_message);
        if (contentVisible) {
            dialogBody.setText(content);
        }
        dialogBody.setVisibility(contentVisible ? View.VISIBLE : View.INVISIBLE);

        ImageView dialogImage = dialog.findViewById(R.id.dialog_icon);
        dialogImage.setImageResource(imageResourceId);

        dialog.findViewById(R.id.yesNoBtnsLayout).setVisibility(View.VISIBLE);
        dialog.findViewById(R.id.btDialogNeutral).setVisibility(View.GONE);


        Button yesButton = dialog.findViewById(R.id.btDialogYes);
        yesButton.setText(positiveBtnText);
        yesButton.setOnClickListener(view -> {
            positiveBtnCallback.callbackAction();
            dialog.dismiss();
        });

        Button noButton = dialog.findViewById(R.id.btDialogNo);
        noButton.setText(negativeBtnText);
        noButton.setOnClickListener(view -> dialog.dismiss());

        return dialog;
    }

    public static String dateFromServer(String date) {
        String dateFormat = "yyyy-MM-dd";
        String toDateFormat = "dd MMMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateFullFromServerWithTime(String date) {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
        String toDateFormat = "dd MMMM yyyy 'at' hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateDaysAndTimeFromServerWithTime(String date) {
        String dateFormat = "yyyy-MM-dd HH:mm";
        String toDateFormat = "dd MMMM yyyy 'at' hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateDaysAndTimeFromServerWithTimeFull(String date) {
        String dateFormat = "yyyy-MM-dd HH:mm a";
        String toDateFormat = "dd MMMM yyyy 'at' hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateFullToServer(Date date) {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        return sdf.format(date);
    }

   
    public static String getCurrentDateTimeZone() {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        return df.format(new Date());

    }
    public static long secondsBetweenNowAndServerDate(String serverDate){
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        Calendar now = Calendar.getInstance();
        Calendar serverTime = Calendar.getInstance();
        try {
            serverTime.setTime(sdf.parse(serverDate));
            long diffInMs = now.getTimeInMillis() - serverTime.getTimeInMillis();
                return TimeUnit.MILLISECONDS.toSeconds(diffInMs);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (30 * 60) - 1;
    }
}
