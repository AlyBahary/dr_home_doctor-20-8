package dr.doctorathome;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;

import com.onesignal.OneSignal;

import java.util.Locale;

import androidx.multidex.MultiDex;
import dr.doctorathome.config.Config;
import dr.doctorathome.presentation.ui.helpers.NotificationOpenedHandler;
import timber.log.Timber;

public class AndroidApplication extends Application {

    //key path  : /Applications/Android Studio.app/Contents/bin/key
    private static Context mContext;

    private static AndroidApplication instance;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();

        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new NotificationOpenedHandler())
                .setNotificationReceivedHandler(new NotificationOpenedHandler())
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        OneSignal.sendTag("userType", "DOCTOR");
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static AndroidApplication getInstance() {
        return instance;
    }

    public static boolean hasNetwork() {
        return getInstance().checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static boolean setAppLocale(String lang) {
        if (lang != null) {
            SharedPreferences sharedPreferences = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor sharedPrefEditor = sharedPreferences.edit();
            sharedPrefEditor.putString("appLanguage", lang);
            sharedPrefEditor.apply();
            sharedPrefEditor.commit();

            Locale myLocale = new Locale(lang);
            Resources res = AndroidApplication.getAppContext().getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                conf.setLocale(myLocale);
            } else {
                conf.locale = myLocale;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                conf.setLayoutDirection(myLocale);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                AndroidApplication.getAppContext().createConfigurationContext(conf);
            } else {
                res.updateConfiguration(conf, dm);
            }

            return true;
        }
        return false;

    }
}