package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JoinSessionResponseModel {

    @SerializedName("rspStatus")
    @Expose
    private String rspStatus;
    @SerializedName("rspBody")
    @Expose
    private String rspBody;
    @SerializedName("patinetuserid")
    @Expose
    private Integer patinetuserid;
    @SerializedName("doctoruserid")
    @Expose
    private Integer doctoruserid;
    @SerializedName("sessionid")
    @Expose
    private String sessionid;
    @SerializedName("doctorplayerid")
    @Expose
    private String doctorplayerid;
    @SerializedName("patientplayerid")
    @Expose
    private String patientplayerid;
    @SerializedName("onllineconsualtionrequestid")
    @Expose
    private Integer onllineconsualtionrequestid;

    public JoinSessionResponseModel(String rspStatus, String rspBody, Integer patinetuserid, Integer doctoruserid, String sessionid, String doctorplayerid, String patientplayerid, Integer onllineconsualtionrequestid) {
        this.rspStatus = rspStatus;
        this.rspBody = rspBody;
        this.patinetuserid = patinetuserid;
        this.doctoruserid = doctoruserid;
        this.sessionid = sessionid;
        this.doctorplayerid = doctorplayerid;
        this.patientplayerid = patientplayerid;
        this.onllineconsualtionrequestid = onllineconsualtionrequestid;
    }

    public String getRspStatus() {
        return rspStatus;
    }

    public String getRspBody() {
        return rspBody;
    }

    public Integer getPatinetuserid() {
        return patinetuserid;
    }

    public Integer getDoctoruserid() {
        return doctoruserid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public String getDoctorplayerid() {
        return doctorplayerid;
    }

    public String getPatientplayerid() {
        return patientplayerid;
    }

    public Integer getOnllineconsualtionrequestid() {
        return onllineconsualtionrequestid;
    }
}
