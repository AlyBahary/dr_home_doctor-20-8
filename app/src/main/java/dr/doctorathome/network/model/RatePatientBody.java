package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RatePatientBody {
    @SerializedName("doctorRating")
    float doctorRating;

    @SerializedName("ratingComment")
    String ratingComment;

    public RatePatientBody() {
    }

    public RatePatientBody(float doctorRating, String ratingComment) {
        this.doctorRating = doctorRating;
        this.ratingComment = ratingComment;
    }

    public float getDoctorRating() {
        return doctorRating;
    }

    public void setDoctorRating(float doctorRating) {
        this.doctorRating = doctorRating;
    }

    public String getRatingComment() {
        return ratingComment;
    }

    public void setRatingComment(String ratingComment) {
        this.ratingComment = ratingComment;
    }
}
