package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class SendDiagnosModel {
    @SerializedName("diagnosise")
    String diagnosise;

    @SerializedName("onlineConsultationId")
    int onlineConsultationId;

    public SendDiagnosModel(String diagnosise, int onlineConsultationId) {
        this.diagnosise = diagnosise;
        this.onlineConsultationId = onlineConsultationId;
    }
}
