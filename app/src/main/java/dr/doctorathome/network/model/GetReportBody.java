package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class GetReportBody {
    @SerializedName("id")
    int doctorId;

    @SerializedName("dateFrom")
    String fromDate;

    @SerializedName("dateTo")
    String toDate;

    public GetReportBody() {
    }

    public GetReportBody(int doctorId, String fromDate, String toDate) {
        this.doctorId = doctorId;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
