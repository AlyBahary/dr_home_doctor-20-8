package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class Service {
    @SerializedName("id")
    Integer id;

    @SerializedName("name")
    String name;

    boolean selected;

    boolean submittingToServer;

    public Service() {
    }

    public Service(Integer id, String name, boolean selected) {
        this.id = id;
        this.name = name;
        this.selected = selected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSubmittingToServer() {
        return submittingToServer;
    }

    public void setSubmittingToServer(boolean submittingToServer) {
        this.submittingToServer = submittingToServer;
    }
}
