package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ChangeOnlineConsultationStatuesBody {
    @SerializedName("onlineConsultationId")
    Integer onlineConsultationId;

    @SerializedName("statusName")
    String statusName;

    public ChangeOnlineConsultationStatuesBody() {
    }

    public ChangeOnlineConsultationStatuesBody(Integer onlineConsultationId, String statusName) {
        this.onlineConsultationId = onlineConsultationId;
        this.statusName = statusName;
    }

    public Integer getOnlineConsultationId() {
        return onlineConsultationId;
    }

    public void setOnlineConsultationId(Integer onlineConsultationId) {
        this.onlineConsultationId = onlineConsultationId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
