package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RestRequestForgetPasswordCodeResponse extends CommonResponse {

    @SerializedName("pwCode")
    private String code;

    public RestRequestForgetPasswordCodeResponse() {
        super(false, "");
    }

    public RestRequestForgetPasswordCodeResponse(boolean success, String message) {
        super(success, message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
