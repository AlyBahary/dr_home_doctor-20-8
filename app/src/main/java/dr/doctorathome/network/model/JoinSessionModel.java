package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JoinSessionModel {
    @SerializedName("connectionId")
    @Expose
    private String connectionId;
    @SerializedName("sessionRequestId")
    @Expose
    private Integer sessionRequestId;

    public JoinSessionModel(String connectionId, Integer sessionRequestId) {
        this.connectionId = connectionId;
        this.sessionRequestId = sessionRequestId;
    }
}
