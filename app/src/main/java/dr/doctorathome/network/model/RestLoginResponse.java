package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RestLoginResponse extends CommonResponse {
    @SerializedName("access_token")
    String accessToken;
    @SerializedName("refresh_token")
    String refresh_token;

    public RestLoginResponse() {
        super(false, "");
    }

    public RestLoginResponse(boolean success, String message) {
        super(success, message);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefresh_token() { return refresh_token; }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
