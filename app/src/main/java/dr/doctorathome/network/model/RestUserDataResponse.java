package dr.doctorathome.network.model;

public class RestUserDataResponse extends CommonResponse {
    UserData userData;

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
