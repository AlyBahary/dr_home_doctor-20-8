package dr.doctorathome.network.model;

public class ErrorModel {
    String message;
    String error;
    Integer status;
    String error_description;


    public String getError_description() {
        return error_description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
