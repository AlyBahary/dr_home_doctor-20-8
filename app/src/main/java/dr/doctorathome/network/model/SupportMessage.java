package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class SupportMessage {
    @SerializedName("id")
    Integer id;

    @SerializedName("subject")
    String subject;

    @SerializedName("message")
    String message;

    @SerializedName("status")
    String status;

    @SerializedName("creationDate")
    String creationDate;

    public SupportMessage() {
    }

    public SupportMessage(Integer id, String subject, String message, String status) {
        this.id = id;
        this.subject = subject;
        this.message = message;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
}
