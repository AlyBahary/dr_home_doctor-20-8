package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ResetPasswordModel {
    @SerializedName("email")
    String email;
    @SerializedName("newPassword")
    String newPassword;
    @SerializedName("pwCode")
    String pwCode;

    public ResetPasswordModel(String email, String newPassword, String pwCode) {
        this.email = email;
        this.newPassword = newPassword;
        this.pwCode = pwCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getPwCode() {
        return pwCode;
    }

    public void setPwCode(String pwCode) {
        this.pwCode = pwCode;
    }
}
