
package dr.doctorathome.network.model.SessionDetailsModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlineConsultationDetailsModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("patient")
    @Expose
    private Patient patient;
    @SerializedName("familyMember")
    @Expose
    private Object familyMember;
    @SerializedName("consultaionDate")
    @Expose
    private String consultaionDate;
    @SerializedName("requestStatus")
    @Expose
    private RequestStatus requestStatus;
    @SerializedName("details")
    @Expose
    private Details details;
    @SerializedName("isJoinBefore")
    @Expose
    private Boolean isJoinBefore;
    @SerializedName("hasEPrescriptions")
    @Expose
    private Boolean hasEPrescriptions;
    @SerializedName("patientPlayerId")
    @Expose
    private String patientPlayerId;
    @SerializedName("type")
    @Expose
    private String type;

    public String getType() {
        return type;
    }

    public void setPatientPlayerId(String patientPlayerId) {
        this.patientPlayerId = patientPlayerId;
    }

    public Boolean getHasEPrescriptions() {
        return hasEPrescriptions;
    }

    public void setHasEPrescriptions(Boolean hasEPrescriptions) {
        this.hasEPrescriptions = hasEPrescriptions;
    }

    public String getPatientPlayerId() {
        return patientPlayerId;
    }

    public Boolean getJoinBefore() {
        return isJoinBefore;
    }

    public void setJoinBefore(Boolean joinBefore) {
        isJoinBefore = joinBefore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Object getFamilyMember() {
        return familyMember;
    }

    public void setFamilyMember(Object familyMember) {
        this.familyMember = familyMember;
    }

    public String getConsultaionDate() {
        return consultaionDate;
    }

    public void setConsultaionDate(String consultaionDate) {
        this.consultaionDate = consultaionDate;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

}
