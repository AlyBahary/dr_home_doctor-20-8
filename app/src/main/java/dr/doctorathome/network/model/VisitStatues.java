package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class VisitStatues {
    @SerializedName("id")
    Integer id;

    @SerializedName("name")
    String name;

    public VisitStatues() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
