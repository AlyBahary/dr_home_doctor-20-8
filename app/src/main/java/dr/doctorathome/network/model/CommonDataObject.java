package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class CommonDataObject {
    @SerializedName("id")
    int id;

    @SerializedName("nameEn")
    String nameEn;

    @SerializedName("name")
    String name;

    @SerializedName("countryId")
    Integer countryId;

    public CommonDataObject() {
    }

    public CommonDataObject(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }
}
