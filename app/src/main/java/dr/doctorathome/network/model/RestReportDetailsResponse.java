package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;


/*
* {
  "totalCompleteVisitRequests": 46,
  "totalCompleteConsultationSession": 0,
  "totalPremiumVisitRequests": 0,
  "totalPremiumConsultationSession": 0,
  "doctorCommissionAmount": 0,
  "totalPaidPremium": 0,
  "totalRemainingPremium": 0,
  "totalCancelledVisits": 58,
  "totalNotDeductions": 0,
  "totalDeductions": 0,
  "totalPendingVisits": 1,
  "totalDistributedMedicine": 0,
  "totalDeliveredMedicine": 0,
  "delayPercentage": 0
}
* */

public class RestReportDetailsResponse extends CommonResponse {
    @SerializedName("totalCompleteVisitRequests")
    Integer totalCompleteVisitRequests;

    @SerializedName("totalCompleteConsultationSession")
    Integer totalCompleteConsultationSession;

    @SerializedName("totalPremiumVisitRequests")
    Integer totalPremiumVisitRequests;

    @SerializedName("doctorCommissionAmount")
    Integer doctorCommissionAmount;

    @SerializedName("totalPremiumConsultationSession")
    Integer totalPremiumConsultationSession;

    @SerializedName("totalPaidPremium")
    Integer totalPaidPremium;

    @SerializedName("totalRemainingPremium")
    Integer totalRemainingPremium;

    @SerializedName("totalCancelledVisits")
    Integer totalCancelledVisits;

    @SerializedName("totalDeductions")
    Integer totalDeductions;

    @SerializedName("totalPendingVisits")
    Integer totalPendingVisits;

    @SerializedName("totalDistributedMedicine")
    Integer totalDistributedMedicine;

    @SerializedName("totalDeliveredMedicine")
    Integer totalDeliveredMedicine;

    @SerializedName("delayPercentage")
    Integer delayPercentage;

    @SerializedName("totalNotDeductions")
    Integer totalNotDeductions;

    @SerializedName("doctorRating")
    Integer rating;
//

    @SerializedName("doctorOnlineSessionRating")
    Integer doctorOnlineSessionRating;

    @SerializedName("totalCancelledSessions")
    Integer totalCancelledSessions;

    @SerializedName("totalCancelledSessionsByPatient")
    Integer totalCancelledSessionsByPatient;


    @SerializedName("totalCancelledSessionsBySystem")
    Integer totalCancelledSessionsBySystem;


    @SerializedName("totalDeductionsOfSessions")
    Integer totalDeductionsOfSessions;


    @SerializedName("totalNotDeductionsOfSessions")
    Integer totalNotDeductionsOfSessions;


    @SerializedName("totalPaidPremiumOfSessions")
    Integer totalPaidPremiumOfSessions;


    @SerializedName("totalPendingSessions")
    Integer totalPendingSessions;


    @SerializedName("totalRemainingPremiumOfSessions")
    Integer totalRemainingPremiumOfSessions;


    @SerializedName("doctorOnlineSessionCommissionAmount")
    Integer doctorOnlineSessionCommissionAmount;
    @SerializedName("totalPremium")
    Integer totalPremium;

    public Integer getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(Integer totalPremium) {
        this.totalPremium = totalPremium;
    }

    public RestReportDetailsResponse() {
    }

    public RestReportDetailsResponse(boolean success, String message) {
        super(success, message);
    }

    public Integer getTotalCompleteVisitRequests() {
        return totalCompleteVisitRequests;
    }

    public void setTotalCompleteVisitRequests(Integer totalCompleteVisitRequests) {
        this.totalCompleteVisitRequests = totalCompleteVisitRequests;
    }

    public Integer getTotalCompleteConsultationSession() {
        return totalCompleteConsultationSession;
    }

    public void setTotalCompleteConsultationSession(Integer totalCompleteConsultationSession) {
        this.totalCompleteConsultationSession = totalCompleteConsultationSession;
    }

    public Integer getTotalPremiumVisitRequests() {
        return totalPremiumVisitRequests;
    }

    public void setTotalPremiumVisitRequests(Integer totalPremiumVisitRequests) {
        this.totalPremiumVisitRequests = totalPremiumVisitRequests;
    }

    public Integer getDoctorCommissionAmount() {
        return doctorCommissionAmount;
    }

    public void setDoctorCommissionAmount(Integer doctorCommissionAmount) {
        this.doctorCommissionAmount = doctorCommissionAmount;
    }

    public Integer getTotalRemainingPremium() {
        return totalRemainingPremium;
    }

    public void setTotalRemainingPremium(Integer totalRemainingPremium) {
        this.totalRemainingPremium = totalRemainingPremium;
    }

    public Integer getTotalCancelledVisits() {
        return totalCancelledVisits;
    }

    public void setTotalCancelledVisits(Integer totalCancelledVisits) {
        this.totalCancelledVisits = totalCancelledVisits;
    }

    public Integer getTotalDeductions() {
        return totalDeductions;
    }

    public void setTotalDeductions(Integer totalDeductions) {
        this.totalDeductions = totalDeductions;
    }

    public Integer getTotalPendingVisits() {
        return totalPendingVisits;
    }

    public void setTotalPendingVisits(Integer totalPendingVisits) {
        this.totalPendingVisits = totalPendingVisits;
    }

    public Integer getTotalDistributedMedicine() {
        return totalDistributedMedicine;
    }

    public void setTotalDistributedMedicine(Integer totalDistributedMedicine) {
        this.totalDistributedMedicine = totalDistributedMedicine;
    }

    public Integer getTotalDeliveredMedicine() {
        return totalDeliveredMedicine;
    }

    public void setTotalDeliveredMedicine(Integer totalDeliveredMedicine) {
        this.totalDeliveredMedicine = totalDeliveredMedicine;
    }

    public Integer getTotalPremiumConsultationSession() {
        return totalPremiumConsultationSession;
    }

    public void setTotalPremiumConsultationSession(Integer totalPremiumConsultationSession) {
        this.totalPremiumConsultationSession = totalPremiumConsultationSession;
    }

    public Integer getTotalPaidPremium() {
        return totalPaidPremium;
    }

    public void setTotalPaidPremium(Integer totalPaidPremium) {
        this.totalPaidPremium = totalPaidPremium;
    }

    public Integer getDelayPercentage() {
        return delayPercentage;
    }

    public void setDelayPercentage(Integer delayPercentage) {
        this.delayPercentage = delayPercentage;
    }

    public Integer getTotalNotDeductions() {
        return totalNotDeductions;
    }

    public void setTotalNotDeductions(Integer totalNotDeductions) {
        this.totalNotDeductions = totalNotDeductions;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
//
    //
        //
    public Integer getDoctorOnlineSessionRating() {
        return doctorOnlineSessionRating;
    }

    public Integer getTotalCancelledSessions() {
        return totalCancelledSessions;
    }

    public Integer getTotalCancelledSessionsByPatient() {
        return totalCancelledSessionsByPatient;
    }

    public Integer getTotalCancelledSessionsBySystem() {
        return totalCancelledSessionsBySystem;
    }

    public Integer getTotalDeductionsOfSessions() {
        return totalDeductionsOfSessions;
    }


    public Integer getTotalNotDeductionsOfSessions() {
        return totalNotDeductionsOfSessions;
    }

    //

    public Integer getTotalPaidPremiumOfSessions() {
        return totalPaidPremiumOfSessions;
    }

    public Integer getTotalPendingSessions() {
        return totalPendingSessions;
    }

    public Integer getTotalRemainingPremiumOfSessions() {
        return totalRemainingPremiumOfSessions;
    }

    public Integer getDoctorOnlineSessionCommissionAmount() {
        return doctorOnlineSessionCommissionAmount;
    }
}
