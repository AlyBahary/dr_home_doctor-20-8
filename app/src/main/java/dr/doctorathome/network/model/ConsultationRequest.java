package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ConsultationRequest {
    @SerializedName("id")
    Integer id;

    public ConsultationRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
