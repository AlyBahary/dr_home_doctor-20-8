package dr.doctorathome.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LabTestInPrescription implements Parcelable {
    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private String price;

    @SerializedName("test")
    private LabTestInPrescription test;

    public LabTestInPrescription getTest() {
        return test;
    }

    public void setTest(LabTestInPrescription test) {
        this.test = test;
    }

    private boolean checked;
    private boolean payable;

    public LabTestInPrescription() {
    }

    public LabTestInPrescription(Integer id, String name, String price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    protected LabTestInPrescription(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        price = in.readString();
    }

    public static final Creator<LabTestInPrescription> CREATOR = new Creator<LabTestInPrescription>() {
        @Override
        public LabTestInPrescription createFromParcel(Parcel in) {
            return new LabTestInPrescription(in);
        }

        @Override
        public LabTestInPrescription[] newArray(int size) {
            return new LabTestInPrescription[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isPayable() {
        return payable;
    }

    public void setPayable(boolean payable) {
        this.payable = payable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(name);
        parcel.writeString(price);
    }
}
