package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlineConsultaionDetails {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("patientRating")
    @Expose
    private Object patientRating;
    @SerializedName("doctorRating")
    @Expose
    private Object doctorRating;
    @SerializedName("sessionId")
    @Expose
    private String sessionId;
    @SerializedName("doctorTokenId")
    @Expose
    private String doctorTokenId;
    @SerializedName("doctorConnectionId")
    @Expose
    private String doctorConnectionId;
    @SerializedName("patientTokenId")
    @Expose
    private String patientTokenId;
    @SerializedName("patientConnectionId")
    @Expose
    private String patientConnectionId;
    @SerializedName("sessionStartDate")
    @Expose
    private String sessionStartDate;
    @SerializedName("sessionEndDate")
    @Expose
    private String sessionEndDate;

    @SerializedName("welcomeMsg")
    @Expose
    private String welcomeMsg;
    @SerializedName("diagnosise")
    @Expose
    private Object diagnosise;

    public Integer getId() {
        return id;
    }

    public Integer getCode() {
        return code;
    }

    public Object getPatientRating() {
        return patientRating;
    }

    public Object getDoctorRating() {
        return doctorRating;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getDoctorTokenId() {
        return doctorTokenId;
    }

    public String getDoctorConnectionId() {
        return doctorConnectionId;
    }

    public String getPatientTokenId() {
        return patientTokenId;
    }

    public String getPatientConnectionId() {
        return patientConnectionId;
    }

    public String getSessionStartDate() {
        return sessionStartDate;
    }

    public String getSessionEndDate() {
        return sessionEndDate;
    }

    public String getWelcomeMsg() {
        return welcomeMsg;
    }

    public Object getDiagnosise() {
        return diagnosise;
    }
}
