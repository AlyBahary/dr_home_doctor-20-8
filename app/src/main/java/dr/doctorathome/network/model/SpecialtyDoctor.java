package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SpecialtyDoctor implements Serializable {
    @SerializedName("id")
    Integer id;
    @SerializedName("name")
    String name;

    public SpecialtyDoctor() {
    }

    public SpecialtyDoctor(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
