package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class EnableDisableServiceBody {
    @SerializedName("doctorId")
    Integer doctorId;

    @SerializedName("serviceId")
    Integer serviceId;

    @SerializedName("enable")
    Boolean enable;

    public EnableDisableServiceBody() {
    }

    public EnableDisableServiceBody(Integer doctorId, Integer serviceId, Boolean enable) {
        this.doctorId = doctorId;
        this.serviceId = serviceId;
        this.enable = enable;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
