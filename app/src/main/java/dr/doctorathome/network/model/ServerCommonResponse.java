package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ServerCommonResponse extends CommonResponse {
    @SerializedName("rspStatus")
    String rspStatues;

    @SerializedName("rspBody")
    String rspBody;

    public ServerCommonResponse() {
    }

    public ServerCommonResponse(boolean success, String message) {
        super(success, message);
    }

    public String getRspStatues() {
        return rspStatues;
    }

    public void setRspStatues(String rspStatues) {
        this.rspStatues = rspStatues;
    }

    public String getRspBody() {
        return rspBody;
    }

    public void setRspBody(String rspBody) {
        this.rspBody = rspBody;
    }
}
