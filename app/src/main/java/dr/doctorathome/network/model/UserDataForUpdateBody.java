package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 *   "bank": "string",
 *   "cityId": 0,
 *   "countryId": 0,
 *   "firstName": "string",
 *   "gender": "MALE",
 *   "iban": "string",
 *   "id": 0,
 *   "insuranceNumber": "string",
 *   "lastName": "string",
 *   "levelId": 0,
 *   "profilePic": "string",
 *   "qualificationDesc": "string",
 *   "regExpiryDate": "yyyy-MM-dd",
 *   "registrationNumber": "string",
 *   "specialtyDoctorId": 0
 *
 * */

public class UserDataForUpdateBody implements Serializable {
    @SerializedName("bank")
    String bank;
    @SerializedName("cityId")
    Integer cityId;
    @SerializedName("countryId")
    Integer countryId;
    @SerializedName("firstName")
    String firstName;
    @SerializedName("gender")
    String gender;
    @SerializedName("iban")
    String iban;
    @SerializedName("id")
    Integer id;
    @SerializedName("insuranceNumber")
    String insuranceNumber;
    @SerializedName("lastName")
    String lastName;
    @SerializedName("levelId")
    Integer levelId;
    @SerializedName("profilePic")
    String profilePicBase64;
    @SerializedName("qualificationDesc")
    String qualificationDesc;
    @SerializedName("regExpiryDate")
    String regExpiryDate;
    @SerializedName("registrationNumber")
    String registrationNumber;
    @SerializedName("specialtyDoctorId")
    Integer specialtyDoctorId;

    public UserDataForUpdateBody() {
    }

    public UserDataForUpdateBody(String bank, Integer cityId, Integer countryId, String firstName,
                                 String gender, String iban, Integer id, String insuranceNumber,
                                 String lastName, Integer levelId, String profilePicBase64, String qualificationDesc,
                                 String regExpiryDate, String registrationNumber, Integer specialtyDoctorId) {
        this.bank = bank;
        this.cityId = cityId;
        this.countryId = countryId;
        this.firstName = firstName;
        this.gender = gender;
        this.iban = iban;
        this.id = id;
        this.insuranceNumber = insuranceNumber;
        this.lastName = lastName;
        this.levelId = levelId;
        this.profilePicBase64 = profilePicBase64;
        this.qualificationDesc = qualificationDesc;
        this.regExpiryDate = regExpiryDate;
        this.registrationNumber = registrationNumber;
        this.specialtyDoctorId = specialtyDoctorId;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getLevelId() {
        return levelId;
    }

    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    public void setProfilePicBase64(String profilePicBase64) {
        this.profilePicBase64 = profilePicBase64;
    }

    public String getQualificationDesc() {
        return qualificationDesc;
    }

    public void setQualificationDesc(String qualificationDesc) {
        this.qualificationDesc = qualificationDesc;
    }

    public String getRegExpiryDate() {
        return regExpiryDate;
    }

    public void setRegExpiryDate(String regExpiryDate) {
        this.regExpiryDate = regExpiryDate;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getSpecialtyDoctorId() {
        return specialtyDoctorId;
    }

    public void setSpecialtyDoctorId(Integer specialtyDoctorId) {
        this.specialtyDoctorId = specialtyDoctorId;
    }
}
