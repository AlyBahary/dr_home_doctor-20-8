package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class UserModel {
    @SerializedName("firstName")
    String firstName;
    @SerializedName("lastName")
    String lastName;
    @SerializedName("mobileNumber")
    String mobileNumber;
    @SerializedName("gender")
    String gender;
    @SerializedName("email")
    String email;
    @SerializedName("specialtyDoctorId")
    Integer specialtyId;
    @SerializedName("clinicId")
    Integer clinicId;
    @SerializedName("qualificationDesc")
    String qualificationDesc;
    @SerializedName("regExpiryDate")
    String regExpiryDate;
    @SerializedName("registartionCard")
    String registartionCard;
    @SerializedName("registrationNumber")
    String registrationNumber;
    @SerializedName("bank")
    String bank;
    @SerializedName("insuranceNumber")
    String insuranceNumber;
    @SerializedName("saudiId")
    String saudiId;
    @SerializedName("iban")
    String iban;
    @SerializedName("levelId")
    Integer level;
    @SerializedName("cityId")
    Integer city;
    @SerializedName("countryId")
    Integer country;
    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;

    public UserModel() {
    }

    public Map<String, String> toMap() {
        Map<String, String> userMap = new HashMap<>();
        userMap.put("firstName", firstName);
        userMap.put("lastName", lastName);
        userMap.put("mobileNumber", mobileNumber);
        userMap.put("gender", gender);
        userMap.put("email", email);
        userMap.put("specialtyDoctorId", specialtyId.toString());
        userMap.put("clinicId", clinicId.toString());
        userMap.put("qualificationDesc", qualificationDesc);
        userMap.put("regExpiryDate", regExpiryDate);
        userMap.put("registartionCard", registartionCard);
        userMap.put("registrationNumber", registrationNumber);
        userMap.put("bank", bank);
        userMap.put("insuranceNumber", insuranceNumber);
        userMap.put("saudiId", saudiId);
        userMap.put("iban", iban);
        userMap.put("levelId", level.toString());
        userMap.put("cityId", city.toString());
        userMap.put("countryId", country.toString());
        userMap.put("username", username);
        userMap.put("password", password);
        return userMap;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getSpecialtyId() {
        return specialtyId;
    }

    public void setSpecialtyId(Integer specialtyId) {
        this.specialtyId = specialtyId;
    }

    public Integer getClinicId() {
        return clinicId;
    }

    public void setClinicId(Integer clinicId) {
        this.clinicId = clinicId;
    }

    public String getQualificationDesc() {
        return qualificationDesc;
    }

    public void setQualificationDesc(String qualificationDesc) {
        this.qualificationDesc = qualificationDesc;
    }

    public String getRegExpiryDate() {
        return regExpiryDate;
    }

    public void setRegExpiryDate(String regExpiryDate) {
        this.regExpiryDate = regExpiryDate;
    }

    public String getRegistartionCard() {
        return registartionCard;
    }

    public void setRegistartionCard(String registartionCard) {
        this.registartionCard = registartionCard;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getSaudiId() {
        return saudiId;
    }

    public void setSaudiId(String saudiId) {
        this.saudiId = saudiId;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
