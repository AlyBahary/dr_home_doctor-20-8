package dr.doctorathome.network.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestModel {
    @SerializedName("id")
    @Expose
    private JsonObject id;
    @SerializedName("test")
    @Expose
    private TestDetailsModel test;
    @SerializedName("testResultUrl")
    @Expose
    private String testResultUrl;
    @SerializedName("notes")
    @Expose
    private String notes;

    public JsonObject getId() {
        return id;
    }

    public void setId(JsonObject id) {
        this.id = id;
    }

    public TestDetailsModel getTest() {
        return test;
    }

    public void setTest(TestDetailsModel test) {
        this.test = test;
    }

    public String getTestResultUrl() {
        return testResultUrl;
    }

    public void setTestResultUrl(String testResultUrl) {
        this.testResultUrl = testResultUrl;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
