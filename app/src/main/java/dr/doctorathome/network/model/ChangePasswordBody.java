package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordBody {
    @SerializedName("id")
    Integer doctorId;

    @SerializedName("newPassword")
    String newPassword;

    @SerializedName("oldPassword")
    String oldPassword;

    public ChangePasswordBody() {
    }

    public ChangePasswordBody(Integer doctorId, String newPassword, String oldPassword) {
        this.doctorId = doctorId;
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
