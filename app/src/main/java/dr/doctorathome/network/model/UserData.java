package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserData implements Serializable {
    @SerializedName("activityStatus")
    String activityStatus;
    @SerializedName("bank")
    String bank;
    @SerializedName("clinic")
    Clinic clinic;
    @SerializedName("description")
    String description;
    @SerializedName("iban")
    String iban;
    @SerializedName("id")
    Integer id;
    @SerializedName("insuranceNumber")
    String insuranceNumber;
    @SerializedName("level")
    Level level;
    @SerializedName("profilePic")
    String profilePicBase64;
    @SerializedName("profilePicUrl")
    String profilePic;
    @SerializedName("qualificationDesc")
    String qualificationDesc;
    @SerializedName("regExpiryDate")
    String regExpiryDate;
    @SerializedName("registartionCard")
    String registartionCard;
    @SerializedName("registrationNumber")
    String registrationNumber;
    @SerializedName("saudiCouncilNumber")
    String saudiCouncilNumber;
    @SerializedName("saudiId")
    String saudiId;
    @SerializedName("specialtyDoctor")
    SpecialtyDoctor specialtyDoctor;
    @SerializedName("user")
    User user;
    @SerializedName("userStatus")
    UserStatus userStatus;

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getQualificationDesc() {
        return qualificationDesc;
    }

    public void setQualificationDesc(String qualificationDesc) {
        this.qualificationDesc = qualificationDesc;
    }

    public String getRegExpiryDate() {
        return regExpiryDate;
    }

    public void setRegExpiryDate(String regExpiryDate) {
        this.regExpiryDate = regExpiryDate;
    }

    public String getRegistartionCard() {
        return registartionCard;
    }

    public void setRegistartionCard(String registartionCard) {
        this.registartionCard = registartionCard;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getSaudiCouncilNumber() {
        return saudiCouncilNumber;
    }

    public void setSaudiCouncilNumber(String saudiCouncilNumber) {
        this.saudiCouncilNumber = saudiCouncilNumber;
    }

    public String getSaudiId() {
        return saudiId;
    }

    public void setSaudiId(String saudiId) {
        this.saudiId = saudiId;
    }

    public SpecialtyDoctor getSpecialtyDoctor() {
        return specialtyDoctor;
    }

    public void setSpecialtyDoctor(SpecialtyDoctor specialtyDoctor) {
        this.specialtyDoctor = specialtyDoctor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public String getProfilePicBase64() {
        return profilePicBase64;
    }

    public void setProfilePicBase64(String profilePicBase64) {
        this.profilePicBase64 = profilePicBase64;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }
}
