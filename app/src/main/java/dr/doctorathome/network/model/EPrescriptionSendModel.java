package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class EPrescriptionSendModel {

    @SerializedName("diagnose")
    @Expose
    private String diagnose;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("labTestIds")
    @Expose
    private ArrayList<Integer> labTestIds = null;
    @SerializedName("medicinIds")
    @Expose
    private ArrayList<Integer> medicinIds = null;
    @SerializedName("medicines")
    @Expose
    private ArrayList<HashMap<String,Integer>> medicines = null;
    @SerializedName("newMedicins")
    @Expose
    private ArrayList<NewMedicinModel> newMedicins = null;
    @SerializedName("requestId")
    @Expose
    private Integer requestId;

    @SerializedName("commonDiseaseId")
    @Expose
    private Integer commonDiseaseId;

    public ArrayList<Integer> getMedicinIds() {
        return medicinIds;
    }

    public void setMedicinIds(ArrayList<Integer> medicinIds) {
        this.medicinIds = medicinIds;
    }

    public Integer getCommonDiseaseId() {
        return commonDiseaseId;
    }

    public void setCommonDiseaseId(Integer commonDiseaseId) {
        this.commonDiseaseId = commonDiseaseId;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<Integer> getLabTestIds() {
        return labTestIds;
    }

    public void setLabTestIds(ArrayList<Integer> labTestIds) {
        this.labTestIds = labTestIds;
    }

    public ArrayList<HashMap<String, Integer>> getMedicines() {
        return medicines;
    }

    public void setMedicines(ArrayList<HashMap<String, Integer>> medicines) {
        this.medicines = medicines;
    }

    public ArrayList<NewMedicinModel> getNewMedicins() {
        return newMedicins;
    }

    public void setNewMedicins(ArrayList<NewMedicinModel> newMedicins) {
        this.newMedicins = newMedicins;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
}
