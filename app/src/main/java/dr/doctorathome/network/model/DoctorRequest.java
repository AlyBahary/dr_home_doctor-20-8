package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorRequest {
    @SerializedName("id")
    Integer id;

    @SerializedName("prescriptionID")
    Integer prescriptionId;

  @SerializedName("prescriptionPaymenetStatus")
    String prescriptionPaymenetStatus;

    @SerializedName("requestdvisitDate")
    String requestdVisitDate;

    @SerializedName("patient")
    Patient patient;

    @SerializedName("visitStatus")
    VisitStatues visitStatus;

    @SerializedName("visitType")
    String visitType;

    @SerializedName("sessionsStartTime")
    String sessionStartTime;

    @SerializedName("complainDescription")
    String complainDescription;
    @SerializedName("navigationStarted")
    String navigationStarted;

    @SerializedName("diagnosise")
    String diagnosise;

    @SerializedName("glucoseMeter")
    @Expose
    private Object glucoseMeter;
    @SerializedName("bloodPressure")
    @Expose
    private Object bloodPressure;
    @SerializedName("pulse")
    @Expose
    private Object pulse;
    @SerializedName("medicalHistory")
    @Expose
    private Object medicalHistory;
    @SerializedName("temperature")
    @Expose
    private Object temperature;
    @SerializedName("warningMessges")
    @Expose
    private Object warningMessges;

    @SerializedName("isBlockedPatient")
    Boolean isBlockedPatient;

    public Object getWarningMessges() {
        return warningMessges;
    }

    public DoctorRequest() {
    }

    public String getPrescriptionPaymenetStatus() {
        return prescriptionPaymenetStatus;
    }

    public String getDiagnosise() {
        return diagnosise;
    }

    public void setDiagnosise(String diagnosise) {
        this.diagnosise = diagnosise;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequestdVisitDate() {
        return requestdVisitDate;
    }

    public void setRequestdVisitDate(String requestdVisitDate) {
        this.requestdVisitDate = requestdVisitDate;
    }

    public String getNavigationStarted() {
        return navigationStarted;
    }

    public void setNavigationStarted(String navigationStarted) {
        this.navigationStarted = navigationStarted;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public VisitStatues getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(VisitStatues visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(String sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public Boolean getBlockedPatient() {
        return isBlockedPatient;
    }

    public void setBlockedPatient(Boolean blockedPatient) {
        isBlockedPatient = blockedPatient;
    }

    public Integer getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(Integer prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public String getComplainDescription() {
        return complainDescription;
    }

    public void setComplainDescription(String complainDescription) {
        this.complainDescription = complainDescription;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public Object getGlucoseMeter() {
        return glucoseMeter;
    }

    public Object getBloodPressure() {
        return bloodPressure;
    }

    public Object getPulse() {
        return pulse;
    }

    public Object getMedicalHistory() {
        return medicalHistory;
    }

    public Object getTemperature() {
        return temperature;
    }
}
