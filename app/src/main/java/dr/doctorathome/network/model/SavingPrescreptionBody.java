package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SavingPrescreptionBody {
    @SerializedName("visitRequestId")
    Integer visitRequestId;

    @SerializedName("extenalMedicines")
    List<String> externalMedicines;

    @SerializedName("medicineBoxs")
    List<Integer> medicineBoxes;

    @SerializedName("tests")
    List<TestToSendModel> tests;

    @SerializedName("recommendedMedicines")
    List<RecommendedMedicineToSent> recommendedMedicines;

    public SavingPrescreptionBody() {
    }

    public SavingPrescreptionBody(Integer visitRequestId, List<String> externalMedicines
            , List<Integer> medicineBoxes, List<TestToSendModel> tests, List<RecommendedMedicineToSent> recommendedMedicines) {
        this.visitRequestId = visitRequestId;
        this.externalMedicines = externalMedicines;
        this.medicineBoxes = medicineBoxes;
        this.tests = tests;
        this.recommendedMedicines = recommendedMedicines;
    }

    public Integer getVisitRequestId() {
        return visitRequestId;
    }

    public void setVisitRequestId(Integer visitRequestId) {
        this.visitRequestId = visitRequestId;
    }

    public List<String> getExternalMedicines() {
        return externalMedicines;
    }

    public void setExtenalMedicines(List<String> externalMedicines) {
        this.externalMedicines = externalMedicines;
    }

    public List<Integer> getMedicineBoxes() {
        return medicineBoxes;
    }

    public void setMedicineBoxes(List<Integer> medicineBoxes) {
        this.medicineBoxes = medicineBoxes;
    }

    public List<TestToSendModel> getTests() {
        return tests;
    }

    public void setTests(List<TestToSendModel> tests) {
        this.tests = tests;
    }

    public List<RecommendedMedicineToSent> getRecommendedMedicines() {
        return recommendedMedicines;
    }

    public void setRecommendedMedicines(List<RecommendedMedicineToSent> recommendedMedicines) {
        this.recommendedMedicines = recommendedMedicines;
    }
}
