package dr.doctorathome.network.model;

import java.util.List;

public class RestAllLabTestsResponse extends CommonResponse{
    List<LabTestInPrescription> labTestInPrescriptionList;

    public RestAllLabTestsResponse() {
    }

    public RestAllLabTestsResponse(boolean success, String message) {
        super(success, message);
    }

    public List<LabTestInPrescription> getLabTestInPrescriptionList() {
        return labTestInPrescriptionList;
    }

    public void setLabTestInPrescriptionList(List<LabTestInPrescription> labTestInPrescriptionList) {
        this.labTestInPrescriptionList = labTestInPrescriptionList;
    }
}
