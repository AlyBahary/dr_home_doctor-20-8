package dr.doctorathome.network.model;

public class RestVisitDetailsResponse extends CommonResponse {
    DoctorRequest doctorRequest;

    public RestVisitDetailsResponse() {
        super(false, "");
    }

    public RestVisitDetailsResponse(boolean success, String message) {
        super(success, message);
    }

    public DoctorRequest getDoctorRequest() {
        return doctorRequest;
    }

    public void setDoctorRequest(DoctorRequest doctorRequest) {
        this.doctorRequest = doctorRequest;
    }
}
