package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestNotificationsResponse extends CommonResponse{
    @SerializedName("notificationResp")
    List<NotificationModel> notificationModels;

    @SerializedName("totalPages")
    Integer totalPages;

    public RestNotificationsResponse() {
    }

    public RestNotificationsResponse(boolean success, String message) {
        super(success, message);
    }

    public List<NotificationModel> getNotificationModels() {
        return notificationModels;
    }

    public void setNotificationModels(List<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
