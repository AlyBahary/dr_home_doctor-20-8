package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestOnlineConsultationsRequestsResponse extends CommonResponse{
    @SerializedName("consultRequests")
    List<ConsultRequest> consultRequests;

    @SerializedName("totalPages")
    Integer totalPages;

    public RestOnlineConsultationsRequestsResponse() {
    }

    public RestOnlineConsultationsRequestsResponse(boolean success, String message) {
        super(success, message);
    }

    public List<ConsultRequest> getConsultRequests() {
        return consultRequests;
    }

    public void setConsultRequests(List<ConsultRequest> consultRequests) {
        this.consultRequests = consultRequests;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
