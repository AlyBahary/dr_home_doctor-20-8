package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestSupportMessagesHistoryResponse extends CommonResponse{
    @SerializedName("supports")
    List<SupportMessage> supportMessages;

    public RestSupportMessagesHistoryResponse() {
    }

    public RestSupportMessagesHistoryResponse(boolean success, String message) {
        super(success, message);
    }

    public List<SupportMessage> getSupportMessages() {
        return supportMessages;
    }

    public void setSupportMessages(List<SupportMessage> supportMessages) {
        this.supportMessages = supportMessages;
    }
}
