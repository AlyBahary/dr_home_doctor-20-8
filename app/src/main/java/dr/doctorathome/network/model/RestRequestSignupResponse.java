package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RestRequestSignupResponse extends CommonResponse {

    @SerializedName("rspBody")
    private String code;

    public RestRequestSignupResponse() {
        super(false, "");
    }

    public RestRequestSignupResponse(boolean success, String message) {
        super(success, message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
