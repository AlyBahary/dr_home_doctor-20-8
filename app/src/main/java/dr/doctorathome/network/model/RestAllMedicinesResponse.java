package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestAllMedicinesResponse extends CommonResponse{
    @SerializedName("medicineDetails")
    List<MedicineInPrescription> medicineInPrescriptionList;

    public RestAllMedicinesResponse() {
    }

    public RestAllMedicinesResponse(boolean success, String message) {
        super(success, message);
    }

    public List<MedicineInPrescription> getMedicineInPrescriptionList() {
        return medicineInPrescriptionList;
    }

    public void setMedicineInPrescriptionList(List<MedicineInPrescription> medicineInPrescriptionList) {
        this.medicineInPrescriptionList = medicineInPrescriptionList;
    }
}
