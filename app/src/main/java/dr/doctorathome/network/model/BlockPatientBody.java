package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class BlockPatientBody {
    @SerializedName("action")
    String action;

    @SerializedName("doctorId")
    Integer doctorId;

    @SerializedName("patientId")
    Integer patientId;

    public BlockPatientBody() {
    }

    public BlockPatientBody(String action, Integer doctorId, Integer patientId) {
        this.action = action;
        this.doctorId = doctorId;
        this.patientId = patientId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }
}
