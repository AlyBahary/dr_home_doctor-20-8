package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class DoctorMedicineBoxesBody {
    @SerializedName("doctorId")
    int doctorId;

    @SerializedName("medicineId")
    int medicineId;

    public DoctorMedicineBoxesBody() {
    }

    public DoctorMedicineBoxesBody(int doctorId, int medicineId) {
        this.doctorId = doctorId;
        this.medicineId = medicineId;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }
}
