package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ChatModel {
    @SerializedName("content")
    String content;
    @SerializedName("format")
    String format;
    @SerializedName("mediaType")
    String mediaType;
    @SerializedName("msgType")
    String msgType;
    @SerializedName("recieverId")
    int recieverId;
    @SerializedName("senderId")
    int senderId;
    @SerializedName("sentAt")
    String sentAt;

    public ChatModel() {
    }


    public ChatModel(String content, String format, String mediaType, String msgType, int recieverId, int senderId, String sentAt) {
        this.content = content;
        this.format = format;
        this.mediaType = mediaType;
        this.msgType = msgType;
        this.recieverId = recieverId;
        this.senderId = senderId;
        this.sentAt = sentAt;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public int getRecieverId() {
        return recieverId;
    }

    public void setRecieverId(int recieverId) {
        this.recieverId = recieverId;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public String getSentAt() {
        return sentAt;
    }

    public void setSentAt(String sentAt) {
        this.sentAt = sentAt;
    }
}
