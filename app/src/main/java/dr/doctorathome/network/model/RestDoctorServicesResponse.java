package dr.doctorathome.network.model;

public class RestDoctorServicesResponse extends CommonResponse {
    DoctorServices doctorServices;

    public RestDoctorServicesResponse() {
    }

    public RestDoctorServicesResponse(boolean success, String message) {
        super(success, message);
    }

    public DoctorServices getDoctorServices() {
        return doctorServices;
    }

    public void setDoctorServices(DoctorServices doctorServices) {
        this.doctorServices = doctorServices;
    }
}
