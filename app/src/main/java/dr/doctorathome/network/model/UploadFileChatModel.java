package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadFileChatModel {

    @SerializedName("rspStatus")
    @Expose
    private String rspStatus;
    @SerializedName("rspBody")
    @Expose
    private String rspBody;

    public String getRspStatus() {
        return rspStatus;
    }

    public void setRspStatus(String rspStatus) {
        this.rspStatus = rspStatus;
    }

    public String getRspBody() {
        return rspBody;
    }

    public void setRspBody(String rspBody) {
        this.rspBody = rspBody;
    }


}
