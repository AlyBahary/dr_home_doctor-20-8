package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardData {
    @SerializedName("cancelledSessions")
    @Expose
    private Integer cancelledSessions;
    @SerializedName("cancelledVisits")
    @Expose
    private Integer cancelledVisits;
    @SerializedName("activeSessions")
    @Expose
    private Integer activeSessions;
    @SerializedName("completedSessions")
    @Expose
    private Integer completedSessions;
    @SerializedName("completedVisits")
    @Expose
    private Integer completedVisits;
    @SerializedName("activeVisits")
    @Expose
    private Integer activeVisits;
    @SerializedName("pendingSessions")
    @Expose
    private Integer pendingSessions;
    @SerializedName("pendingVisits")
    @Expose
    private Integer pendingVisits;
    public DashboardData() {
    }

    public DashboardData(Integer activeVisits, Integer cancelledVisits, Integer completedVisits, Integer pendingVisits) {
        this.activeVisits = activeVisits;
        this.cancelledVisits = cancelledVisits;
        this.completedVisits = completedVisits;
        this.pendingVisits = pendingVisits;
    }

    public Integer getActiveVisits() {
        return activeVisits;
    }

    public void setActiveVisits(Integer activeVisits) {
        this.activeVisits = activeVisits;
    }

    public Integer getCancelledVisits() {
        return cancelledVisits;
    }

    public void setCancelledVisits(Integer cancelledVisits) {
        this.cancelledVisits = cancelledVisits;
    }

    public Integer getCompletedVisits() {
        return completedVisits;
    }

    public void setCompletedVisits(Integer completedVisits) {
        this.completedVisits = completedVisits;
    }

    public Integer getPendingVisits() {
        return pendingVisits;
    }

    public void setPendingVisits(Integer pendingVisits) {
        this.pendingVisits = pendingVisits;
    }

    public Integer getCancelledSessions() {
        return cancelledSessions;
    }

    public Integer getActiveSessions() {
        return activeSessions;
    }

    public Integer getCompletedSessions() {
        return completedSessions;
    }

    public Integer getPendingSessions() {
        return pendingSessions;
    }
}
