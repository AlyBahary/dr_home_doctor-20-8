package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RequestClearanceBody {
    @SerializedName("doctorId")
    Integer doctorId;

    @SerializedName("clearanceReason")
    String clearanceReason;

    @SerializedName("deactivationDate")
    String deactivationDate;

    public RequestClearanceBody() {
    }

    public RequestClearanceBody(Integer doctorId, String clearanceReason, String deactivationDate) {
        this.doctorId = doctorId;
        this.clearanceReason = clearanceReason;
        this.deactivationDate = deactivationDate;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getClearanceReason() {
        return clearanceReason;
    }

    public void setClearanceReason(String clearanceReason) {
        this.clearanceReason = clearanceReason;
    }

    public String getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(String deactivationDate) {
        this.deactivationDate = deactivationDate;
    }
}
