package dr.doctorathome.network.model;


import com.google.gson.annotations.SerializedName;

public class RestChangeDoctorStatuesBody {

    @SerializedName("activityStatusRef")
    String currentStatues;

    @SerializedName("doctorId")
    Integer doctorId;

    @SerializedName("lat")
    Double lat;

    @SerializedName("lng")
    Double lng;

    public RestChangeDoctorStatuesBody() {
    }

    public RestChangeDoctorStatuesBody(String currentStatues, Integer doctorId) {
        this.currentStatues = currentStatues;
        this.doctorId = doctorId;
    }

    public RestChangeDoctorStatuesBody(String currentStatues, Integer doctorId, Double lat, Double lng) {
        this.currentStatues = currentStatues;
        this.doctorId = doctorId;
        this.lat = lat;
        this.lng = lng;
    }

    public String getCurrentStatues() {
        return currentStatues;
    }

    public void setCurrentStatues(String currentStatues) {
        this.currentStatues = currentStatues;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
