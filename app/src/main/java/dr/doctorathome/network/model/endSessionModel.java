package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import dr.doctorathome.network.model.MessagesFromAPIModels.MessageModel;

public class endSessionModel {

    @SerializedName("closeTypeId")
    @Expose
    private Integer closeTypeId = null;

    @SerializedName("onlineConsultationId")
    @Expose
    private Integer onlineConsultationId;

    @SerializedName("diagnosie")
    @Expose
    private String diagnosie;

    public endSessionModel(Integer closeTypeId, Integer onlineConsultationId, String diagnosie) {
        this.closeTypeId = closeTypeId;
        this.onlineConsultationId = onlineConsultationId;
        this.diagnosie = diagnosie;
    }

    public Integer getCloseTypeId() {
        return closeTypeId;
    }

    public void setCloseTypeId(Integer closeTypeId) {
        this.closeTypeId = closeTypeId;
    }

    public Integer getOnlineConsultationId() {
        return onlineConsultationId;
    }

    public void setOnlineConsultationId(Integer onlineConsultationId) {
        this.onlineConsultationId = onlineConsultationId;
    }
}
