package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestToSendModel {
    @SerializedName("id")
    Integer id;

    @SerializedName("payable")
    Boolean payable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getPayable() {
        return payable;
    }

    public void setPayable(Boolean payable) {
        this.payable = payable;
    }

    public TestToSendModel(Integer id, Boolean payable) {
        this.id = id;
        this.payable = payable;
    }
}
