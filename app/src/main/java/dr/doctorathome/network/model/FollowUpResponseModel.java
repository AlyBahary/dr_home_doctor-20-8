package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FollowUpResponseModel {

    @SerializedName("followups")
    @Expose
    private ArrayList<FollowUpModel> followups = null;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;

    public ArrayList<FollowUpModel> getFollowups() {
        return followups;
    }

    public void setFollowups(ArrayList<FollowUpModel> followups) {
        this.followups = followups;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }
}
