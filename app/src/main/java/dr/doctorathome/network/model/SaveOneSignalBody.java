package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class SaveOneSignalBody {
    @SerializedName("userId")
    int userId;

    @SerializedName("deviceID")
    String deviceID;

    @SerializedName("userLocale")
    String userLocale;

    public SaveOneSignalBody() {
    }

    public SaveOneSignalBody(int userId, String deviceID, String userLocale) {
        this.userId = userId;
        this.deviceID = deviceID;
        this.userLocale = userLocale;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getUserLocale() {
        return userLocale;
    }

    public void setUserLocale(String userLocale) {
        this.userLocale = userLocale;
    }
}
