package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RecommendedMedicineToSent {
    @SerializedName("id")
    Integer id;

    @SerializedName("quantity")
    Integer quantity;

    public RecommendedMedicineToSent() {
    }

    public RecommendedMedicineToSent(Integer id, Integer quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
