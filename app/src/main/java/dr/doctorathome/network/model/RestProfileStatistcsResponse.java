package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RestProfileStatistcsResponse extends CommonResponse {
    @SerializedName("completedConsultations")
    Integer completedConsultations;

    @SerializedName("newConsultationsRequests")
    Integer newConsultationsRequests;

    @SerializedName("statisfiedPatients")
    Integer statisfiedPatients;

    @SerializedName("doctorRating")
    Float doctorRating;

    @SerializedName("unreadNotificationsCount")
    Integer unreadNotificationsCount;

    public RestProfileStatistcsResponse() {
    }

    public RestProfileStatistcsResponse(boolean success, String message) {
        super(success, message);
    }

    public Integer getCompletedConsultations() {
        return completedConsultations;
    }

    public void setCompletedConsultations(Integer completedConsultations) {
        this.completedConsultations = completedConsultations;
    }

    public Integer getNewConsultationsRequests() {
        return newConsultationsRequests;
    }

    public void setNewConsultationsRequests(Integer newConsultationsRequests) {
        this.newConsultationsRequests = newConsultationsRequests;
    }

    public Integer getStatisfiedPatients() {
        return statisfiedPatients;
    }

    public void setStatisfiedPatients(Integer statisfiedPatients) {
        this.statisfiedPatients = statisfiedPatients;
    }

    public Float getDoctorRating() {
        return doctorRating;
    }

    public void setDoctorRating(Float doctorRating) {
        this.doctorRating = doctorRating;
    }

    public Integer getUnreadNotificationsCount() {
        return unreadNotificationsCount;
    }

    public void setUnreadNotificationsCount(Integer unreadNotificationsCount) {
        this.unreadNotificationsCount = unreadNotificationsCount;
    }
}
