package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestVisitRequestsResponse extends CommonResponse{
    @SerializedName("visitRequests")
    List<DoctorRequest> visitRequests;

    @SerializedName("totalPages")
    Integer totalPages;

    public RestVisitRequestsResponse() {
    }

    public RestVisitRequestsResponse(boolean success, String message) {
        super(success, message);
    }

    public List<DoctorRequest> getVisitRequests() {
        return visitRequests;
    }

    public void setVisitRequests(List<DoctorRequest> visitRequests) {
        this.visitRequests = visitRequests;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
