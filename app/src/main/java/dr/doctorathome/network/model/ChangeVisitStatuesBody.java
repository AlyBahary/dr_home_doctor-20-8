package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ChangeVisitStatuesBody {
    @SerializedName("visitRequestId")
    Integer visitRequestId;

    @SerializedName("statusName")
    String statusName;

    public ChangeVisitStatuesBody() {
    }

    public ChangeVisitStatuesBody(Integer visitRequestId, String statusName) {
        this.visitRequestId = visitRequestId;
        this.statusName = statusName;
    }

    public Integer getVisitRequestId() {
        return visitRequestId;
    }

    public void setVisitRequestId(Integer visitRequestId) {
        this.visitRequestId = visitRequestId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
