package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ValidateSignUpDataBody {
    @SerializedName("email")
    String email;

    @SerializedName("iban")
    String iban;

    @SerializedName("insuranceNumber")
    String insuranceNumber;

    @SerializedName("mobileNumber")
    String mobileNumber;

    @SerializedName("registrationNumber")
    String registrationNumber;

    @SerializedName("saudiId")
    String saudiId;

    @SerializedName("username")
    String username;

    public ValidateSignUpDataBody() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getSaudiId() {
        return saudiId;
    }

    public void setSaudiId(String saudiId) {
        this.saudiId = saudiId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
