
package dr.doctorathome.network.model.SessionDetailsModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("sessionId")
    @Expose
    private String sessionId;
    @SerializedName("doctorTokenId")
    @Expose
    private String doctorTokenId;
    @SerializedName("doctorConnectionId")
    @Expose
    private Object doctorConnectionId;
    @SerializedName("patientTokenId")
    @Expose
    private String patientTokenId;
    @SerializedName("patientConnectionId")
    @Expose
    private String patientConnectionId;
    @SerializedName("sessionStartDate")
    @Expose
    private String sessionStartDate;
    @SerializedName("sessionEndDate")
    @Expose
    private String sessionEndDate;
    @SerializedName("sessionSettings")
    @Expose
    private SessionSettings sessionSettings;
    @SerializedName("welcomeMsg")
    @Expose
    private String welcomeMsg;
    @SerializedName("diagnosise")
    @Expose
    private Object diagnosise;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getDoctorTokenId() {
        return doctorTokenId;
    }

    public void setDoctorTokenId(String doctorTokenId) {
        this.doctorTokenId = doctorTokenId;
    }

    public Object getDoctorConnectionId() {
        return doctorConnectionId;
    }

    public void setDoctorConnectionId(Object doctorConnectionId) {
        this.doctorConnectionId = doctorConnectionId;
    }

    public String getPatientTokenId() {
        return patientTokenId;
    }

    public void setPatientTokenId(String patientTokenId) {
        this.patientTokenId = patientTokenId;
    }

    public String getPatientConnectionId() {
        return patientConnectionId;
    }

    public void setPatientConnectionId(String patientConnectionId) {
        this.patientConnectionId = patientConnectionId;
    }

    public String getSessionStartDate() {
        return sessionStartDate;
    }

    public void setSessionStartDate(String sessionStartDate) {
        this.sessionStartDate = sessionStartDate;
    }

    public String getSessionEndDate() {
        return sessionEndDate;
    }

    public void setSessionEndDate(String sessionEndDate) {
        this.sessionEndDate = sessionEndDate;
    }

    public SessionSettings getSessionSettings() {
        return sessionSettings;
    }

    public void setSessionSettings(SessionSettings sessionSettings) {
        this.sessionSettings = sessionSettings;
    }

    public String getWelcomeMsg() {
        return welcomeMsg;
    }

    public void setWelcomeMsg(String welcomeMsg) {
        this.welcomeMsg = welcomeMsg;
    }

    public Object getDiagnosise() {
        return diagnosise;
    }

    public void setDiagnosise(Object diagnosise) {
        this.diagnosise = diagnosise;
    }

}
