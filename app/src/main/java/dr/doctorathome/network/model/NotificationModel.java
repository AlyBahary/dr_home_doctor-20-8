package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

/*
* {
    "actionType": "VISIT_ACTION",
    "data": "string",
    "isRead": true,
    "message": "string",
    "senderImageUrl": "string",
    "senderName": "string",
    "sentAt": "2019-06-22T13:29:12.106Z"
  }
* */

public class NotificationModel {
    @SerializedName("id")
    Integer id;

    @SerializedName("actionType")
    String actionType;

    @SerializedName("data")
    String data;

    @SerializedName("isRead")
    Boolean isRead;

    @SerializedName("message")
    String message;

    @SerializedName("senderImageUrl")
    String senderImageUrl;

    @SerializedName("senderName")
    String senderName;

    @SerializedName("sentAt")
    String sentAt;

    public NotificationModel() {
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderImageUrl() {
        return senderImageUrl;
    }

    public void setSenderImageUrl(String senderImageUrl) {
        this.senderImageUrl = senderImageUrl;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSentAt() {
        return sentAt;
    }

    public void setSentAt(String sentAt) {
        this.sentAt = sentAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
