package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class DoctorNewLocationBody {
    @SerializedName("doctorId")
    int doctorId;

    @SerializedName("visitId")
    int visitId;

    @SerializedName("latitude")
    double latitude;

    @SerializedName("longitude")
    double longitude;

    public DoctorNewLocationBody() {
    }

    public DoctorNewLocationBody(int doctorId, int visitId, double latitude, double longitude) {
        this.doctorId = doctorId;
        this.visitId = visitId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
