package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestPrescriptionDetailsResponse extends CommonResponse{
    @SerializedName("externalMedicines")
    List<ExternalMedicine> externalMedicines;

    @SerializedName("medicines")
    List<MedicineInPrescription> medicines;

    @SerializedName("tests")
    List<LabTestInPrescription> tests;

    public RestPrescriptionDetailsResponse() {
    }

    public List<ExternalMedicine> getExternalMedicines() {
        return externalMedicines;
    }

    public void setExternalMedicines(List<ExternalMedicine> externalMedicines) {
        this.externalMedicines = externalMedicines;
    }

    public List<MedicineInPrescription> getMedicines() {
        return medicines;
    }

    public void setMedicines(List<MedicineInPrescription> medicines) {
        this.medicines = medicines;
    }

    public List<LabTestInPrescription> getTests() {
        return tests;
    }

    public void setTests(List<LabTestInPrescription> tests) {
        this.tests = tests;
    }
}
