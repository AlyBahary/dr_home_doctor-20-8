package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RestSupportMessageDetailsResponse extends CommonResponse{
    @SerializedName("id")
    Integer id;

    @SerializedName("creationDate")
    String creationDate;

//    @SerializedName("message")
//    String message;

    @SerializedName("reply")
    String reply;

    @SerializedName("replyDate")
    String replyDate;

    @SerializedName("status")
    String status;

    @SerializedName("subject")
    String subject;

    public RestSupportMessageDetailsResponse() {
    }

    public RestSupportMessageDetailsResponse(boolean success, String message) {
        super(success, message);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

//    @Override
//    public String getMessage() {
//        return message;
//    }
//
//    @Override
//    public void setMessage(String message) {
//        this.message = message;
//    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(String replyDate) {
        this.replyDate = replyDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
