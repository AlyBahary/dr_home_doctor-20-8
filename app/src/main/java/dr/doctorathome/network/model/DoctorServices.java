package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorServices {
    @SerializedName("allServices")
    List<Service> allServices;

    @SerializedName("doctorCurrentServices")
    List<Service> doctorCurrentServices;

    public DoctorServices() {
    }

    public DoctorServices(List<Service> allServices, List<Service> doctorCurrentServices) {
        this.allServices = allServices;
        this.doctorCurrentServices = doctorCurrentServices;
    }

    public List<Service> getAllServices() {
        return allServices;
    }

    public void setAllServices(List<Service> allServices) {
        this.allServices = allServices;
    }

    public List<Service> getDoctorCurrentServices() {
        return doctorCurrentServices;
    }

    public void setDoctorCurrentServices(List<Service> doctorCurrentServices) {
        this.doctorCurrentServices = doctorCurrentServices;
    }
}
