package dr.doctorathome.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MedicineInPrescription implements Parcelable {
    private Integer uniqueViewId;

    @SerializedName("id")
    private Integer id;

    @SerializedName("quantity")
    private Integer count;

    @SerializedName("name")
    private String name;

    @SerializedName("dose")
    private String dose;

    @SerializedName("gross")
    private String price;

    private Integer medicineType;

    private List<Integer> boxesList;

    @SerializedName("medicineBoxs")
    private List<MedicineBox> medicineBoxs;

    public MedicineInPrescription() {
    }

    public MedicineInPrescription(Integer id, Integer count, String name, String dose, String price) {
        this.id = id;
        this.count = count;
        this.name = name;
        this.dose = dose;
        this.price = price;
    }

    public MedicineInPrescription(Integer uniqueViewId, Integer id, Integer count, String name, String dose, String price) {
        this.uniqueViewId = uniqueViewId;
        this.id = id;
        this.count = count;
        this.name = name;
        this.dose = dose;
        this.price = price;
    }

    public MedicineInPrescription(Integer id, Integer count, String name, String dose, String price, Integer medicineType) {
        this.id = id;
        this.count = count;
        this.name = name;
        this.dose = dose;
        this.price = price;

        this.medicineType = medicineType;
    }

    public MedicineInPrescription(Integer uniqueViewId, Integer id, Integer count, String name
            , String dose, String price, Integer medicineType) {
        this.uniqueViewId = uniqueViewId;
        this.id = id;
        this.count = count;
        this.name = name;
        this.dose = dose;
        this.price = price;
        this.medicineType = medicineType;
    }

    protected MedicineInPrescription(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            count = null;
        } else {
            count = in.readInt();
        }
        name = in.readString();
        dose = in.readString();
        price = in.readString();
        if (in.readByte() == 0) {
            medicineType = null;
        } else {
            medicineType = in.readInt();
        }
    }

    public static final Creator<MedicineInPrescription> CREATOR = new Creator<MedicineInPrescription>() {
        @Override
        public MedicineInPrescription createFromParcel(Parcel in) {
            return new MedicineInPrescription(in);
        }

        @Override
        public MedicineInPrescription[] newArray(int size) {
            return new MedicineInPrescription[size];
        }
    };

    public Integer getUniqueViewId() {
        return uniqueViewId;
    }

    public void setUniqueViewId(Integer uniqueViewId) {
        this.uniqueViewId = uniqueViewId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getMedicineType() {
        return medicineType;
    }

    public void setMedicineType(Integer medicineType) {
        this.medicineType = medicineType;
    }

    public List<Integer> getBoxesList() {
        return boxesList;
    }

    public void setBoxesList(List<Integer> boxesList) {
        this.boxesList = boxesList;
    }

    public List<MedicineBox> getMedicineBoxs() {
        return medicineBoxs;
    }

    public void setMedicineBoxs(List<MedicineBox> medicineBoxs) {
        this.medicineBoxs = medicineBoxs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (count == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(count);
        }
        parcel.writeString(name);
        parcel.writeString(dose);
        parcel.writeString(price);
        if (medicineType == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(medicineType);
        }
    }
}
