package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RestLoginWithMailDataModel {
    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;
    @SerializedName("grant_type")
    String grantType;
 @SerializedName("refresh_token")
    String refresh_token;

    public RestLoginWithMailDataModel(String username, String password, String grantType) {
        this.username = username;
        this.password = password;
        this.grantType = grantType;
    }

    public RestLoginWithMailDataModel(String grantType, String refresh_token) {
        this.grantType = grantType;
        this.refresh_token = refresh_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }
}
