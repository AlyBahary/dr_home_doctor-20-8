package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VitalSignsModel {


    @SerializedName("bloodPressure")
    @Expose
    private String bloodPressure;
    @SerializedName("glucoseMeter")
    @Expose
    private String glucoseMeter;
    @SerializedName("medicalHistory")
    @Expose
    private String medicalHistory;
    @SerializedName("pulse")
    @Expose
    private String pulse;
    @SerializedName("temperature")
    @Expose
    private String temperature;
    @SerializedName("visitId")
    @Expose
    private int visitId;

    public VitalSignsModel(String bloodPressure, String glucoseMeter, String medicalHistory, String pulse, String temperature, int visitId) {
        this.bloodPressure = bloodPressure;
        this.glucoseMeter = glucoseMeter;
        this.medicalHistory = medicalHistory;
        this.pulse = pulse;
        this.temperature = temperature;
        this.visitId = visitId;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getGlucoseMeter() {
        return glucoseMeter;
    }

    public void setGlucoseMeter(String glucoseMeter) {
        this.glucoseMeter = glucoseMeter;
    }

    public String getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(String medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }
}
