package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RestAppSettingsResponse extends CommonResponse{
    @SerializedName("id")
    Integer id;

    @SerializedName("adminEmail")
    String adminEmail;

    @SerializedName("facebook")
    String facebook;

    @SerializedName("twitter")
    String twitter;

    @SerializedName("instagram")
    String instagram;

    @SerializedName("linkedIn")
    String linkedIn;

    @SerializedName("youtube")
    String youtube;

    @SerializedName("mobile")
    String mobile;

    @SerializedName("phone")
    String phone;

    @SerializedName("address")
    String address;

    @SerializedName("aboutUS")
    String aboutUS;

    @SerializedName("termsAndConditions")
    String termsAndConditions;

    public RestAppSettingsResponse() {
    }

    public RestAppSettingsResponse(boolean success, String message) {
        super(success, message);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAboutUS() {
        return aboutUS;
    }

    public void setAboutUS(String aboutUS) {
        this.aboutUS = aboutUS;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }
}
