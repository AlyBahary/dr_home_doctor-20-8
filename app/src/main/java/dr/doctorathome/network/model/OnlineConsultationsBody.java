package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class OnlineConsultationsBody {
    @SerializedName("doctorId")
    Integer doctorId;

    @SerializedName("pageNumber")
    Integer pageNumber;

    @SerializedName("statusName")
    String statusName;

    public OnlineConsultationsBody() {
    }

    public OnlineConsultationsBody(Integer doctorId, Integer pageNumber, String statusName) {
        this.doctorId = doctorId;
        this.pageNumber = pageNumber;
        this.statusName = statusName;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
