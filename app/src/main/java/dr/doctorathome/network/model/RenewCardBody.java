package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class RenewCardBody {
    @SerializedName("doctorId")
    int doctorId;

    @SerializedName("regExpiryDate")
    String regExpiryDate;

    @SerializedName("registartionCard")
    String registartionCard;

    public RenewCardBody() {
    }

    public RenewCardBody(int doctorId, String regExpiryDate, String registartionCard) {
        this.doctorId = doctorId;
        this.regExpiryDate = regExpiryDate;
        this.registartionCard = registartionCard;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getRegExpiryDate() {
        return regExpiryDate;
    }

    public void setRegExpiryDate(String regExpiryDate) {
        this.regExpiryDate = regExpiryDate;
    }

    public String getRegistartionCard() {
        return registartionCard;
    }

    public void setRegistartionCard(String registartionCard) {
        this.registartionCard = registartionCard;
    }
}
