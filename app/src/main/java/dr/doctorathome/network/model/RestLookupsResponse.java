package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestLookupsResponse extends CommonResponse{
    @SerializedName("cities")
    List<CommonDataObject> cities;

    @SerializedName("countries")
    List<CommonDataObject> countries;

    @SerializedName("levels")
    List<CommonDataObject> levels;

    @SerializedName("clinics")
    List<CommonDataObject> clinics;

    @SerializedName("services")
    List<CommonDataObject> services;

    @SerializedName("specializations")
    List<CommonDataObject> specializations;

    public RestLookupsResponse() {
        super(false, "");
    }

    public RestLookupsResponse(boolean success, String message) {
        super(success, message);
    }

    public List<CommonDataObject> getCities() {
        return cities;
    }

    public void setCities(List<CommonDataObject> cities) {
        this.cities = cities;
    }

    public List<CommonDataObject> getCountries() {
        return countries;
    }

    public void setCountries(List<CommonDataObject> countries) {
        this.countries = countries;
    }

    public List<CommonDataObject> getLevels() {
        return levels;
    }

    public void setLevels(List<CommonDataObject> levels) {
        this.levels = levels;
    }

    public List<CommonDataObject> getClinics() {
        return clinics;
    }

    public void setClinics(List<CommonDataObject> clinics) {
        this.clinics = clinics;
    }

    public List<CommonDataObject> getServices() {
        return services;
    }

    public void setServices(List<CommonDataObject> services) {
        this.services = services;
    }

    public List<CommonDataObject> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(List<CommonDataObject> specializations) {
        this.specializations = specializations;
    }
}
