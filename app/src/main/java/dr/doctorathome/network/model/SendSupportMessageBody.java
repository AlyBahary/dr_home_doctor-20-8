package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class SendSupportMessageBody {
    @SerializedName("userId")
    Integer userId;

    @SerializedName("subject")
    String subject;

    @SerializedName("message")
    String message;

    public SendSupportMessageBody() {
    }

    public SendSupportMessageBody(Integer userId, String subject, String message) {
        this.userId = userId;
        this.subject = subject;
        this.message = message;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
