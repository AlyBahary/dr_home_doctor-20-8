package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ExternalMedicine {
    @SerializedName("id")
    Integer id;

    @SerializedName("prescription")
    String prescription;

    public ExternalMedicine() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }
}
