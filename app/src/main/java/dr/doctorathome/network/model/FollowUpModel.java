package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FollowUpModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("visitId")
    @Expose
    private Integer visitId;
    @SerializedName("patient")
    @Expose
    private Patient patient;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("status")
    @Expose
    private UserStatus status;

    @SerializedName("familyMember")
    @Expose
    private Object familyMember;
    @SerializedName("visitDiagnoise")
    @Expose
    private String visitDiagnoise;
    @SerializedName("followUpDiagnoise")
    @Expose
    private Object followUpDiagnoise;
    @SerializedName("tests")
    @Expose
    private ArrayList<TestModel> tests = null;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVisitId() {
        return visitId;
    }

    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    //

    public Object getFamilyMember() {
        return familyMember;
    }

    public void setFamilyMember(Object familyMember) {
        this.familyMember = familyMember;
    }

    public String getVisitDiagnoise() {
        return visitDiagnoise;
    }

    public void setVisitDiagnoise(String visitDiagnoise) {
        this.visitDiagnoise = visitDiagnoise;
    }

    public Object getFollowUpDiagnoise() {
        return followUpDiagnoise;
    }

    public void setFollowUpDiagnoise(Object followUpDiagnoise) {
        this.followUpDiagnoise = followUpDiagnoise;
    }

    public ArrayList<TestModel> getTests() {
        return tests;
    }

    public void setTests(ArrayList<TestModel> tests) {
        this.tests = tests;
    }
}
