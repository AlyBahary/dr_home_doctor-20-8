package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewMedicinModel implements Serializable {
    @SerializedName("adminCommission")
    @Expose
    private Integer adminCommission;
    @SerializedName("category")
    @Expose
    private CategoryModel category;
    @SerializedName("cost")
    @Expose
    private Integer cost;
    @SerializedName("doctorCommission")
    @Expose
    private Integer doctorCommission;
    @SerializedName("dose")
    @Expose
    private String dose;
    @SerializedName("durationUnit")
    @Expose
    private String durationUnit;
    @SerializedName("frequency")
    @Expose
    private String frequency;
    @SerializedName("gross")
    @Expose
    private Integer gross;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("indications")
    @Expose
    private String indications;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("prn")
    @Expose
    private Boolean prn;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("root")
    @Expose
    private String root;
    @SerializedName("sellingPrice")
    @Expose
    private Integer sellingPrice;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("unit")
    @Expose
    private String unit;

    public Integer getAdminCommission() {
        return adminCommission;
    }

    public void setAdminCommission(Integer adminCommission) {
        this.adminCommission = adminCommission;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getDoctorCommission() {
        return doctorCommission;
    }

    public void setDoctorCommission(Integer doctorCommission) {
        this.doctorCommission = doctorCommission;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public Integer getGross() {
        return gross;
    }

    public void setGross(Integer gross) {
        this.gross = gross;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIndications() {
        return indications;
    }

    public void setIndications(String indications) {
        this.indications = indications;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPrn() {
        return prn;
    }

    public void setPrn(Boolean prn) {
        this.prn = prn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Integer sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public static class CategoryModel implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id=1;
        @SerializedName("name")
        @Expose
        private String name;

        public CategoryModel(Integer id) {
            this.id = id;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

}