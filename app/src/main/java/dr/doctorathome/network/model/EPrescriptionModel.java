package dr.doctorathome.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EPrescriptionModel {
    @SerializedName("diagnose")
    @Expose
    private String diagnose;
    @SerializedName("labTests")
    @Expose
    private ArrayList<LabTestModel> labTests = null;
    @SerializedName("medicines")
    @Expose
    private ArrayList<MedicineModel> medicines = null;

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public ArrayList<LabTestModel> getLabTests() {
        return labTests;
    }

    public void setLabTests(ArrayList<LabTestModel> labTests) {
        this.labTests = labTests;
    }

    public ArrayList<MedicineModel> getMedicines() {
        return medicines;
    }

    public void setMedicines(ArrayList<MedicineModel> medicines) {
        this.medicines = medicines;
    }
}
