package dr.doctorathome.network.model;

public class DashboardModel {
    String type,Number,status;

    public DashboardModel(String type, String number, String status) {
        this.type = type;
        Number = number;
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
