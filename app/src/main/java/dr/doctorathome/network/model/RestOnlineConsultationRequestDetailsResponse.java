package dr.doctorathome.network.model;

public class RestOnlineConsultationRequestDetailsResponse extends CommonResponse{
    ConsultRequest consultRequest;

    public RestOnlineConsultationRequestDetailsResponse() {
    }

    public RestOnlineConsultationRequestDetailsResponse(boolean success, String message) {
        super(success, message);
    }

    public ConsultRequest getConsultRequest() {
        return consultRequest;
    }

    public void setConsultRequest(ConsultRequest consultRequest) {
        this.consultRequest = consultRequest;
    }
}
