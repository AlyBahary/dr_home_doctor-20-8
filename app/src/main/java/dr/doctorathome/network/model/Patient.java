package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class Patient {
    @SerializedName("id")
    Integer id;

    @SerializedName("significantPastDisease")
    String significantPastDisease;

    @SerializedName("surgeryAndComplications")
    String surgeryAndComplications;

    @SerializedName("drugHistory")
    String drugHistory;

    @SerializedName("familyHistory")
    String familyHistory;

    @SerializedName("user")
    User user;

    @SerializedName("profilePicUrl")
    String profilePicUrl;

    @SerializedName("address")
    String address;

    @SerializedName("location")
    LocationPoint location;

    @SerializedName("diabetes")
    private String diabetes;

    @SerializedName("bloodPressure")
    private String bloodPressure;

    @SerializedName("heartDisease")
    private String heartDisease;

    @SerializedName("rheumatism")
    private String rheumatism;

    @SerializedName("cholestrol")
    private String cholestrol;

    @SerializedName("otherDiseases")
    private String otherDiseases;
    @SerializedName("dateOfBirth")
    private String dateOfBirth;

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Patient() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSignificantPastDisease() {
        return significantPastDisease;
    }

    public void setSignificantPastDisease(String significantPastDisease) {
        this.significantPastDisease = significantPastDisease;
    }

    public String getSurgeryAndComplications() {
        return surgeryAndComplications;
    }

    public void setSurgeryAndComplications(String surgeryAndComplications) {
        this.surgeryAndComplications = surgeryAndComplications;
    }

    public String getDrugHistory() {
        return drugHistory;
    }

    public void setDrugHistory(String drugHistory) {
        this.drugHistory = drugHistory;
    }

    public String getFamilyHistory() {
        return familyHistory;
    }

    public void setFamilyHistory(String familyHistory) {
        this.familyHistory = familyHistory;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocationPoint getLocation() {
        return location;
    }

    public void setLocation(LocationPoint location) {
        this.location = location;
    }

    public String getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(String diabetes) {
        this.diabetes = diabetes;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getHeartDisease() {
        return heartDisease;
    }

    public void setHeartDisease(String heartDisease) {
        this.heartDisease = heartDisease;
    }

    public String getRheumatism() {
        return rheumatism;
    }

    public void setRheumatism(String rheumatism) {
        this.rheumatism = rheumatism;
    }

    public String getCholestrol() {
        return cholestrol;
    }

    public void setCholestrol(String cholestrol) {
        this.cholestrol = cholestrol;
    }

    public String getOtherDiseases() {
        return otherDiseases;
    }

    public void setOtherDiseases(String otherDiseases) {
        this.otherDiseases = otherDiseases;
    }
}
