package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ConsultRequest {
    @SerializedName("id")
    Integer id;

    @SerializedName("consultaionDate")
    String requestdDate;

    @SerializedName("code")
    String code;

    @SerializedName("age")
    Integer age;

    @SerializedName("weight")
    Integer weight;

    @SerializedName("fees")
    Double fees;

    @SerializedName("isForYou")
    String isForYou;

    @SerializedName("complain")
    String complain;

    @SerializedName("allergy")
    String allergy;

    @SerializedName("diabetes")
    String diabetes;

    @SerializedName("bloodPressure")
    String bloodPressure;

    @SerializedName("heartDisease")
    String heartDisease;

    @SerializedName("rheumatism")
    String rheumatism;

    @SerializedName("significantPastDisease")
    String significantPastDisease;

    @SerializedName("cholestrol")
    String cholestrol;

    @SerializedName("patient")
    Patient patient;

    @SerializedName("requestStatus")
    VisitStatues requestStatus;

    @SerializedName("details")

    private OnlineConsultaionDetails details;


    public ConsultRequest() {
    }

    public OnlineConsultaionDetails getDetails() {
        return details;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequestdDate() {
        return requestdDate;
    }

    public void setRequestdDate(String requestdDate) {
        this.requestdDate = requestdDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public VisitStatues getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(VisitStatues requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Double getFees() {
        return fees;
    }

    public void setFees(Double fees) {
        this.fees = fees;
    }

    public String getIsForYou() {
        return isForYou;
    }

    public void setIsForYou(String isForYou) {
        this.isForYou = isForYou;
    }

    public String getComplain() {
        return complain;
    }

    public void setComplain(String complain) {
        this.complain = complain;
    }

    public String getAllergy() {
        return allergy;
    }

    public void setAllergy(String allergy) {
        this.allergy = allergy;
    }

    public String getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(String diabetes) {
        this.diabetes = diabetes;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getHeartDisease() {
        return heartDisease;
    }

    public void setHeartDisease(String heartDisease) {
        this.heartDisease = heartDisease;
    }
//
    public String getRheumatism() {
        return rheumatism;
    }

    public void setRheumatism(String rheumatism) {
        this.rheumatism = rheumatism;
    }

    public String getSignificantPastDisease() {
        return significantPastDisease;
    }

    public void setSignificantPastDisease(String significantPastDisease) {
        this.significantPastDisease = significantPastDisease;
    }

    public String getCholestrol() {
        return cholestrol;
    }

    public void setCholestrol(String cholestrol) {
        this.cholestrol = cholestrol;
    }
}
