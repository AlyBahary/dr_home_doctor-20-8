package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Role implements Serializable {
    @SerializedName("createdAt")
    String createdAt;
    @SerializedName("id")
    Integer id;
    @SerializedName("name")
    String name;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
