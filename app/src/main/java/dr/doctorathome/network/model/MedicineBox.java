package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class MedicineBox {
    @SerializedName("id")
    Integer id;

    @SerializedName("name")
    String name;

    @SerializedName("code")
    String code;

    private boolean checked;

    public MedicineBox() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
