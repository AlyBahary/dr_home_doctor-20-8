package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class ImmediateRequestModel {
    @SerializedName("id")
    Integer id;
    @SerializedName("patient")
    Patient patient;

    public Integer getId() {
        return id;
    }

    public Patient getPatient() {
        return patient;
    }
}
