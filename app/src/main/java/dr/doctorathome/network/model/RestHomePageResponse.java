package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestHomePageResponse extends CommonResponse {
    @SerializedName("activityStatus")
    String activityStatus;

    @SerializedName("dashboard")
    DashboardData dashboardData;

    @SerializedName("consultrequests")
    List<ConsultRequest> consultationRequests;

    @SerializedName("immediateRequests")
    List<ImmediateRequestModel> immediateRequests;

    @SerializedName("requests")
    List<DoctorRequest> requests;

    @SerializedName("warningMessage")
    String warningMessage;

    @SerializedName("totalUnreadMessages")
    Integer totalUnreadMessages;

    public List<ImmediateRequestModel> getImmediateRequests() {
        return immediateRequests;
    }

    public Integer getTotalUnreadMessages() {
        return totalUnreadMessages;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public RestHomePageResponse() {
        super(false, "");
    }

    public RestHomePageResponse(boolean success, String message) {
        super(success, message);
    }

    public DashboardData getDashboardData() {
        return dashboardData;
    }

    public void setDashboardData(DashboardData dashboardData) {
        this.dashboardData = dashboardData;
    }

    public List<ConsultRequest> getConsultationRequests() {
        return consultationRequests;
    }

    public void setConsultationRequests(List<ConsultRequest> consultationRequests) {
        this.consultationRequests = consultationRequests;
    }

    public List<DoctorRequest> getRequests() {
        return requests;
    }

    public void setRequests(List<DoctorRequest> requests) {
        this.requests = requests;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }
}
