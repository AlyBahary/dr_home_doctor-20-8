package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestDoctorMedicineBoxesResponse extends CommonResponse{
    @SerializedName("boxs")
    List<MedicineBox> medicineBoxes;

    public RestDoctorMedicineBoxesResponse() {
    }

    public RestDoctorMedicineBoxesResponse(boolean success, String message) {
        super(success, message);
    }

    public List<MedicineBox> getMedicineBoxes() {
        return medicineBoxes;
    }

    public void setMedicineBoxes(List<MedicineBox> medicineBoxes) {
        this.medicineBoxes = medicineBoxes;
    }
}
