package dr.doctorathome.network.model.MessagesFromAPIModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("onlineConsultationRequestId")
    @Expose
    private Integer onlineConsultationRequestId;
    @SerializedName("senderId")
    @Expose
    private Integer senderId;
    @SerializedName("senderPicUrl")
    @Expose
    private String senderPicUrl;
    @SerializedName("recieverId")
    @Expose
    private Integer recieverId;
    @SerializedName("recieverPicUrl")
    @Expose
    private String recieverPicUrl;
    @SerializedName("msgType")
    @Expose
    private String msgType;
    @SerializedName("sentAt")
    @Expose
    private String sentAt;
    @SerializedName("seenAt")
    @Expose
    private Object seenAt;
    @SerializedName("mediaType")
    @Expose
    private Object mediaType;
    @SerializedName("content")
    @Expose
    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOnlineConsultationRequestId() {
        return onlineConsultationRequestId;
    }

    public void setOnlineConsultationRequestId(Integer onlineConsultationRequestId) {
        this.onlineConsultationRequestId = onlineConsultationRequestId;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public String getSenderPicUrl() {
        return senderPicUrl;
    }

    public void setSenderPicUrl(String senderPicUrl) {
        this.senderPicUrl = senderPicUrl;
    }

    public Integer getRecieverId() {
        return recieverId;
    }

    public void setRecieverId(Integer recieverId) {
        this.recieverId = recieverId;
    }

    public String getRecieverPicUrl() {
        return recieverPicUrl;
    }

    public void setRecieverPicUrl(String recieverPicUrl) {
        this.recieverPicUrl = recieverPicUrl;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getSentAt() {
        return sentAt;
    }

    public void setSentAt(String sentAt) {
        this.sentAt = sentAt;
    }

    public Object getSeenAt() {
        return seenAt;
    }

    public void setSeenAt(Object seenAt) {
        this.seenAt = seenAt;
    }

    public Object getMediaType() {
        return mediaType;
    }

    public void setMediaType(Object mediaType) {
        this.mediaType = mediaType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
