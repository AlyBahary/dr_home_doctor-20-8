package dr.doctorathome.network.model;

import com.google.gson.annotations.SerializedName;

public class StartEndSessionBody {
    @SerializedName("visitId")
    Integer visitId;

    @SerializedName("diagnosis")
    String diagnosis;

    @SerializedName("actionDate")
    String actionDate;

    public StartEndSessionBody() {
    }

    public StartEndSessionBody(Integer visitId, String actionDate) {
        this.visitId = visitId;
        this.actionDate = actionDate;
    }

    public StartEndSessionBody(Integer visitId, String diagnosis, String actionDate) {
        this.visitId = visitId;
        this.diagnosis = diagnosis;
        this.actionDate = actionDate;
    }

    public Integer getVisitId() {
        return visitId;
    }

    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getActionDate() {
        return actionDate;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }
}
