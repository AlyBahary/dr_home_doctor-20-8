
package dr.doctorathome.network.model.SessionDetailsModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionSettings {

    @SerializedName("doctorId")
    @Expose
    private Integer doctorId;
    @SerializedName("doctorIdleTime")
    @Expose
    private Integer doctorIdleTime;
    @SerializedName("patientIdleTime")
    @Expose
    private Integer patientIdleTime;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("remainingTime")
    @Expose
    private Long remainingTime;

    public Long getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(Long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Integer getDoctorIdleTime() {
        return doctorIdleTime;
    }

    public void setDoctorIdleTime(Integer doctorIdleTime) {
        this.doctorIdleTime = doctorIdleTime;
    }

    public Integer getPatientIdleTime() {
        return patientIdleTime;
    }

    public void setPatientIdleTime(Integer patientIdleTime) {
        this.patientIdleTime = patientIdleTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

}
