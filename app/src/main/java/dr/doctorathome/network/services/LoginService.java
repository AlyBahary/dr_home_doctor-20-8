package dr.doctorathome.network.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Map;

import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ResetPasswordModel;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestRequestForgetPasswordCodeResponse;
import dr.doctorathome.network.model.RestRequestSignupResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.network.model.ValidateSignUpDataBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LoginService {

    @FormUrlEncoded
    @POST("oauth/token")
    Call<RestLoginResponse> loginWithEmail(@Field("username") String username,
                                           @Field("password") String password, @Field("grant_type") String grantType
            , @HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("oauth/token")
    Call<RestLoginResponse> refreshToken(@Field("refresh_token") String refresh_token,
                                         @Field("grant_type") String grantType
            , @HeaderMap Map<String, String> headers);

    @GET("api/doctor/doctorinterface/generateotpcode")
    Call<RestRequestSignupResponse> requestSignupCode(@Query("mobileNumber") String phone);

    @GET("api/doctor/doctorinterface/pwverificationcode")
    Call<RestRequestForgetPasswordCodeResponse> requestForgetPasswordCode(@Query("mail") String email);

    @POST("api/doctor/doctorinterface/registerDoctor")
    Call<CommonResponse> signup(@Body UserModel userModel);

    @POST("api/doctor/doctorinterface/forgetpassword")
    Call<CommonResponse> resetPassword(@Body ResetPasswordModel resetPasswordModel);

    @GET("api/doctor/doctorinterface/viewDoctorProfile/{id}")
    Call<UserData> getUserData(@Path("id") Integer id, @Header("Authorization") String token);

    @PUT("api/doctor/doctorinterface/v2/updateDoctor")
    Call<UserData> updateDoctor(@Body UserDataForUpdateBody userData, @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/validateData")
    Call<CommonResponse> validateSignUpData(@Body ValidateSignUpDataBody validateSignUpDataBody);

    @POST("api/user/{userID}/logout")
    Call<JsonObject> logOut(@Header("Authorization") String token, @Path("userID") String userID);

}

//error_description -> Full authentication is required to access this resource

//todo: -> warningMessges (in ->visit details , Home)
//todo: -> Time Zone
//todo: -> case rejected on all visits (hide buttons)