package dr.doctorathome.network.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.model.SaveOneSignalBody;
import dr.doctorathome.network.model.SendSupportMessageBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommonDataService {

    @GET("api/doctor/retreiveLookUps")
    Call<RestLookupsResponse> getLookups();

    @GET("api/settingmanagement/setting")
    Call<RestAppSettingsResponse> getAppSettings();

    @POST("api/support/addSupport")
    Call<CommonResponse> sendSupportMessage(@Header("Authorization") String token
            ,@Body SendSupportMessageBody sendSupportMessageBody);

    @GET("api/support/supports/userid/{user_id}")
    Call<RestSupportMessagesHistoryResponse> getSupportMessagesHistory(@Header("Authorization") String token
            , @Path("user_id") int userId);

    @GET("api/support/supports/supportid/{support_id}")
    Call<RestSupportMessageDetailsResponse> getSupportMessageDetails(@Header("Authorization") String token
            , @Path("support_id") int supportMessageId);

    @POST("api/user/saveuseridentification")
    Call<CommonResponse> saveOneSignalUserId(@Header("Authorization") String token
            ,@Body SaveOneSignalBody saveOneSignalBody);


    @GET("api/settingmanagement/versions")
    Call<JsonObject> getVersions(@Header("Authorization") String token, @Query("appType") String appType, @Query("platformType")String platformType, @Query("version") String version);

}
