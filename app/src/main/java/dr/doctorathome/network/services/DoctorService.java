package dr.doctorathome.network.services;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dr.doctorathome.network.model.BlockPatientBody;
import dr.doctorathome.network.model.ChangeOnlineConsultationStatuesBody;
import dr.doctorathome.network.model.ChangePasswordBody;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ConsultRequest;
import dr.doctorathome.network.model.DoctorMedicineBoxesBody;
import dr.doctorathome.network.model.DoctorNewLocationBody;
import dr.doctorathome.network.model.DoctorRequest;
import dr.doctorathome.network.model.DoctorServices;
import dr.doctorathome.network.model.EPrescriptionModel;
import dr.doctorathome.network.model.EPrescriptionSendModel;
import dr.doctorathome.network.model.EnableDisableServiceBody;
import dr.doctorathome.network.model.FollowUpModel;
import dr.doctorathome.network.model.FollowUpResponseModel;
import dr.doctorathome.network.model.GetReportBody;
import dr.doctorathome.network.model.JoinSessionModel;
import dr.doctorathome.network.model.JoinSessionResponseModel;
import dr.doctorathome.network.model.LabTestInPrescription;
import dr.doctorathome.network.model.LabTestModel;
import dr.doctorathome.network.model.MedicineModel;
import dr.doctorathome.network.model.MessagesFromAPIModels.ChatAPIModel;
import dr.doctorathome.network.model.OnlineConsultationsBody;
import dr.doctorathome.network.model.RatePatientBody;
import dr.doctorathome.network.model.RenewCardBody;
import dr.doctorathome.network.model.RequestClearanceBody;
import dr.doctorathome.network.model.RestAllMedicinesResponse;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestNotificationsResponse;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;
import dr.doctorathome.network.model.RestProfileStatistcsResponse;
import dr.doctorathome.network.model.RestReportDetailsResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.SavingPrescreptionBody;
import dr.doctorathome.network.model.SendDiagnosModel;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.network.model.SessionDetailsModels.OnlineConsultationDetailsModel;
import dr.doctorathome.network.model.StartEndSessionBody;
import dr.doctorathome.network.model.UploadFileChatModel;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.VisitRequestsBody;
import dr.doctorathome.network.model.VitalSignsModel;
import dr.doctorathome.network.model.endSessionModel;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DoctorService {

    @PUT("api/doctor/doctorinterface/updatedoctorstatus")
    Call<UserData> changeDoctorStatues(@Body RestChangeDoctorStatuesBody changeDoctorStatuesBody
            , @Header("Authorization") String token);

    @GET("api/doctor/doctorinterface/viewdoctorhomepage/{doctorId}")
    Call<RestHomePageResponse> getDoctorHomePageData(@Path("doctorId") int doctorId
            , @Header("Authorization") String token);

    @GET("api/doctor/doctorinterface/doctorservices/{doctorId}")
    Call<DoctorServices> getDoctorServices(@Path("doctorId") int doctorId
            , @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/managedoctorservices")
    Call<CommonResponse> enableDisableDoctorService(@Body EnableDisableServiceBody enableDisableServiceBody
            , @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/visitrequests")
    Call<RestVisitRequestsResponse> getDoctorVisits(@Body VisitRequestsBody visitRequestsBody
            , @Header("Authorization") String token);

    @POST("api/onlineconsultation/doctorinterface/sessionrequests")
    Call<RestOnlineConsultationsRequestsResponse> getDoctorOnlineConsultations(@Body OnlineConsultationsBody onlineConsultationsBody
            , @Header("Authorization") String token);

    @GET("api/onlineconsultation/doctorinterface/sessionrequests/{id}")
    Call<ConsultRequest> getDoctorOnlineConsultationDetails(@Path("id") int id, @Header("Authorization") String token);

    @PUT("api/onlineconsultation/doctorinterface/handleonlineconsult")
    Call<CommonResponse> changeOnlineConsultationStatus(@Body ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody,
                                                        @Header("Authorization") String token);

    @GET("api/doctor/doctorinterface/visitrequests/{visitId}")
    Call<DoctorRequest> getVisitDetails(@Path("visitId") int visitId
            , @Header("Authorization") String token);

    @GET("api/doctor/doctorinterface/viewPrescription/{visitId}")
    Call<RestPrescriptionDetailsResponse> getPrescriptionDetails(@Path("visitId") int visitId
            , @Header("Authorization") String token);

    @PUT("api/doctor/doctorinterface/handlevisitrequest")
    Call<CommonResponse> changeVisitStatues(@Body ChangeVisitStatuesBody changeVisitStatuesBody
            , @Header("Authorization") String token);

    @PUT("api/doctor/doctorinterface/startsession")
    Call<CommonResponse> startSession(@Body StartEndSessionBody startEndSessionBody
            , @Header("Authorization") String token);

    @PUT("api/doctor/doctorinterface/endsession")
    Call<CommonResponse> endSession(@Body StartEndSessionBody startEndSessionBody
            , @Header("Authorization") String token);

    @POST("api/patient/visits/{visitId}/rate")
    Call<CommonResponse> ratePatient(@Path("visitId") int visitId, @Body RatePatientBody ratePatientBody
            , @Header("Authorization") String token);

    @GET("api/pharmacymedicineinventory/doctorinterface/activeMedicine")
    Call<RestAllMedicinesResponse> getAllSystemMedicines(@Header("Authorization") String token);

    @GET("api/lab/doctorinterface/labtests")
    Call<List<LabTestInPrescription>> getAllSystemLabTests(@Header("Authorization") String token);

    @POST("api/pharmacymedicineinventory/doctorinterface/doctorMedicineBoxs")
    Call<RestDoctorMedicineBoxesResponse> getDoctorMedicinesBoxes(@Body DoctorMedicineBoxesBody doctorMedicineBoxesBody,
                                                                  @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/saveprescription")
    Call<CommonResponse> savePrescription(@Body SavingPrescreptionBody savingPrescreptionBody,
                                          @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/blockpatient")
    Call<CommonResponse> blockPatient(@Body BlockPatientBody blockPatientBody,
                                      @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/requestclearance")
    Call<CommonResponse> requestClearance(@Body RequestClearanceBody requestClearanceBody,
                                          @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/changepassword")
    Call<CommonResponse> changePassword(@Body ChangePasswordBody changePasswordBody,
                                        @Header("Authorization") String token);

    @POST("api/reports/doctorinterface/reports")
    Call<RestReportDetailsResponse> getReport(@Body GetReportBody getReportBody,
                                              @Header("Authorization") String token);

    @POST("api/doctor/doctorinterface/renewregistrationcard")
    Call<ServerCommonResponse> renewRegistrationCard(@Body RenewCardBody renewCardBody,
                                                     @Header("Authorization") String token);

    @GET("api/doctor/doctorinterface/profilestatistcs/{doctorId}")
    Call<RestProfileStatistcsResponse> getDoctorProfileStatistics(@Path("doctorId") int doctorId,
                                                                  @Header("Authorization") String token);

    @PUT("api/doctor/doctorinterface/updatedoctorlocation")
    Call<ServerCommonResponse> updateDoctorLocation(@Body DoctorNewLocationBody doctorNewLocationBody,
                                                    @Header("Authorization") String token);

    @PUT("api/doctor/doctorinterface/isarrive")
    Call<ServerCommonResponse> isArrived(@Body DoctorNewLocationBody doctorNewLocationBody,
                                         @Header("Authorization") String token);

    @GET("api/notification/user/{userId}/{pageNumber}")
    Call<RestNotificationsResponse> getUserNotifications(@Path("userId") int userId,
                                                         @Header("Authorization") String token, @Path("pageNumber") int pageNumber);

    @PUT("api/notification/{notificationId}/read")
    Call<CommonResponse> markNotificationAsRead
            (@Path("notificationId") int notificationId,
             @Header("Authorization") String token);

    @Multipart
    @POST("api/onlineconsultation/doctorinterface/sessions/{sessionId}/messages/uploadfile")
    Call<UploadFileChatModel> Upload_Img(
            @Part MultipartBody.Part file,
            @Path("sessionId") int sessionId,
            @Query("senderId") int senderId,
            @Header("Authorization") String token
    );

    @Multipart
    @POST("api/followup/doctorinterface/sessions/{sessionId}/messages/uploadfile")
    Call<UploadFileChatModel> Upload_Img_follow_Up(
            @Part MultipartBody.Part file,
            @Path("sessionId") String sessionId,
            @Query("senderId") int senderId,
            @Header("Authorization") String token
    );

    @GET("api/onlineconsultation/doctorinterface/sessionrequests/details/{sessionId}")
    Call<OnlineConsultationDetailsModel> getSessionDetails(
            @Path("sessionId") int sessionId
            , @Header("Authorization") String token
    );

    @GET("api/onlineconsultation/doctorinterface/sessions/{sessionId}/messages/{pageNumber}")
    Call<ChatAPIModel> getMessages(
            @Path("pageNumber") int pageNumber,
            @Path("sessionId") int sessionId
            , @Header("Authorization") String token
    );

    @GET("api/followup/doctorinterface/sessions/{sessionId}/messages/{pageNumber}")
    Call<ChatAPIModel> getMessages_followUp(
            @Path("pageNumber") int pageNumber,
            @Path("sessionId") String sessionId
            , @Header("Authorization") String token
    );

    @PUT("api/onlineconsultation/doctorinterface/endsession")
    Call<ServerCommonResponse> endSession(@Body endSessionModel endSessionBody
            , @Header("Authorization") String token);

    @PUT("api/onlineconsultation/doctorinterface/joinsession")
    Call<JoinSessionResponseModel> joinSession(@Body JoinSessionModel joinSessionModel
            , @Header("Authorization") String token);

    @PUT("api/onlineconsultation/doctorinterface/sessiondiagnosie")
    Call<ServerCommonResponse> addSessionDiagnose(@Body SendDiagnosModel sendDiagnosModel
            , @Header("Authorization") String token);

    @PUT("api/doctor/doctorinterface/visits/addVitalSigns")
    Call<ServerCommonResponse> addVitalSigns(@Body VitalSignsModel vitalSignsModel
            , @Header("Authorization") String token);

    @GET("api/followup/doctorinterface/sessions/visit/{visitId}")
    Call<JsonObject> getFollowUp(@Path("visitId") String visitId
            , @Header("Authorization") String token);

    @POST("api/followup/doctorinterface/sessions")
    Call<FollowUpResponseModel> getFollowUp(@Body HashMap<String,String> body
            , @Header("Authorization") String token);

    @GET("api/followup/doctorinterface/sessions/{FollowUpId}")
    Call<FollowUpModel> getFollowUpbyId(@Path("FollowUpId")  String FollowUpId
            , @Header("Authorization") String token);

    @PUT("api/followup/doctorinterface/followdiagnosie")
    Call<JsonObject> addDiagnose(@Body HashMap<String,String> body
            , @Header("Authorization") String token);

    @PUT("api/followup/doctorinterface/endfollowUp")
    Call<JsonObject> endFollowUp(@Body HashMap<String, String> body
            , @Header("Authorization") String token);
    @GET("api/pharmacymedicineinventory/doctorinterface/searchForMedicine/{name}")
    Call<ArrayList<MedicineModel>> getMedicins(@Path("name")String name, @Header("Authorization") String token);

    @GET("api/lab/doctorinterface/searchForLabTest/{name}")
    Call<ArrayList<LabTestModel>> getLabTest(@Path("name")String name, @Header("Authorization") String token);

    @POST("api/onlineconsultation/doctorinterface/eprescriptions/{seesionId}")
    Call<EPrescriptionModel> getEPrescription(@Path("seesionId")String seesionId, @Header("Authorization") String token);

    @POST("api/onlineconsultation/doctorinterface/eprescriptions/save")
    Call<JsonObject> savePrescription(@Body EPrescriptionSendModel model, @Header("Authorization") String token);

    @PUT("api/pharmacy/doctor-interface/quick-sells/approve-quick-sell")
    Call<JsonObject> approveImmediateCons(@Body HashMap dto, @Header("Authorization") String token);

    @POST("api/pharmacy/admin/common-diseases/{pageNumber}")
    Call<JsonObject> getCommonDisease(@Path("pageNumber") String pageNumber,@Body HashMap<String,String> dto, @Header("Authorization") String token);



//
//    @GET("current")
//    Call<your_model>getWeather(
//            @Query("access_Key") String key,
//            @Query("query") String city
//    );
//
//



}
