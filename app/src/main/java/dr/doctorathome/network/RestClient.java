package dr.doctorathome.network;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.model.ErrorModel;
import dr.doctorathome.network.model.RestLoginWithMailDataModel;
import dr.doctorathome.network.repositoryImpl.LoginRepositoryImpl;
import dr.doctorathome.utils.CommonUtil;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class RestClient {
    /**
     * This is our main backend/server URL.
     ****/

    //drhome-1544575261424-38c20
    //drhome-1544575261424-fa675
    //http://drhometst.beetleware.com/doctorhome/
    //https://api.drathome.com.sa/doctorhome/
    public static String REST_API_URL = "http://drhometst.beetleware.com/doctorhome/";
    static String TAG = "intercept";

    private static final String CACHE_CONTROL = "Cache-Control";


    private static Retrofit s_retrofit;


    public static Retrofit getClient() {
        if (s_retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            s_retrofit = new Retrofit.Builder()
                    .baseUrl(REST_API_URL)
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(provideOkHttpClient())
                    .build();
        }
        return s_retrofit;
    }

    public static void setS_retrofit(Retrofit s_retrofit) {
        RestClient.s_retrofit = s_retrofit;
    }

    private static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(provideSentLanguageInterceptor())
                .addInterceptor(provideLogInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request request = chain.request();
                        Response response = chain.proceed(request);

                        if(request.url().equals(RestClient.REST_API_URL+"api/doctor/doctorinterface/updatedoctorstatus ")){
                            return response;
                        }

                        if (response.code() == 401) {
                            if (!response.isSuccessful()) {
                                String parsedError = CommonUtil.readIt(response.body().byteStream(), response.body().contentLength());
                                Timber.w("%s", parsedError);
                                try {
                                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                                    Log.d(TAG, "intercept: error 401 " + error.getError_description() + error.getError());
                                    if (error.getError_description().contains(" is required to access this resource")
                                            || error.getError_description().contains("Invalid access token")) {
                                        LoginRepository loginRepository = new LoginRepositoryImpl();
                                        loginRepository
                                                .refreshToken(new RestLoginWithMailDataModel("refresh_token"
                                                        , CommonUtil.commonSharedPref().getString("refresh_token", "")));
                                        Toast toast = new Toast(AndroidApplication.getAppContext());
                                        toast.makeText(AndroidApplication.getAppContext(), "" + AndroidApplication.getAppContext().getString(R.string.please_try_again), Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.TOP, 0, 0);
                                        toast.show();

                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();

                                }
                            }
                        }

                        return response;
                    }
                })
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .callTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();
    }

    public static Interceptor provideSentLanguageInterceptor() {
        return chain -> {
            Request request = chain.request().newBuilder().addHeader("Accept-Language"
                    , CommonUtil.commonSharedPref().getString("appLanguage", "en")).build();
            Timber.d("Retrofit :: " + request.headers().toString());
            return chain.proceed(request);
        };


//                chain -> {
//            Request.Builder requestBuilder = chain.request().newBuilder();
//            requestBuilder.removeHeader("Accept-Language");
//            requestBuilder.addHeader("Accept-Language", CommonUtil.commonSharedPref().getString("appLanguage", "en"));
//            return chain.proceed(requestBuilder.build());
//        };
    }

    public static class NullOnEmptyConverterFactory extends Converter.Factory {

        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return (Converter<ResponseBody, Object>) body -> {
                if (body.contentLength() == 0) return null;
                return delegate.convert(body);
            };
        }
    }

    public static Interceptor provideLogInterceptor() {
        return chain -> {
            Request originalRequest = chain.request(); //Current Request

            Response response = chain.proceed(originalRequest); //Get response of the request

            /** DEBUG STUFF */
            //                if (BuildConfig.DEBUG) {
            //I am logging the response body in debug mode. When I do this I consume the response (OKHttp only lets you do this once) so i have re-build a new one using the cached body
            String bodyString = response.body().string();
            Timber.d(String.format("Sending request %s \n\n with body %s \n\n with headers %s ", originalRequest.url()
                    , (originalRequest.body() != null ? bodyToString(originalRequest) : originalRequest.body()), originalRequest.headers()));
            Timber.d(String.format("Got response HTTP %s %s \n\n with body %s \n\n with headers %s ", response.code(), response.message(), bodyString, response.headers()));
            response = response.newBuilder().body(ResponseBody.create(response.body().contentType(), bodyString)).build();
            //                }
            Log.d(TAG, "provideLogInterceptor: "+CommonUtil.commonSharedPref().getString("token", ""));

            return response;
        };
    }

    private static String bodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
