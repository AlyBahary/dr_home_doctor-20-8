package dr.doctorathome.network.repositoryImpl;

import android.util.Log;

import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.domain.repositories.CommonDataRepository;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ErrorModel;
import dr.doctorathome.network.model.RestAppSettingsResponse;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestLoginWithMailDataModel;
import dr.doctorathome.network.model.RestLookupsResponse;
import dr.doctorathome.network.model.RestSupportMessageDetailsResponse;
import dr.doctorathome.network.model.RestSupportMessagesHistoryResponse;
import dr.doctorathome.network.model.SaveOneSignalBody;
import dr.doctorathome.network.model.SendSupportMessageBody;
import dr.doctorathome.network.services.CommonDataService;
import dr.doctorathome.network.services.LoginService;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Response;
import timber.log.Timber;


public class CommonDataRepositoryImpl implements CommonDataRepository {

    @Override
    public RestLookupsResponse getLookups() {
        CommonDataService commonDataService = RestClient.getClient().create(CommonDataService.class);
        try {

            Response<RestLookupsResponse> lookupsResponse = commonDataService.getLookups().execute();

            Timber.i("GET LOOKUPS SUCCESS: %d", lookupsResponse.code());

            RestLookupsResponse restLookupsResponse = new RestLookupsResponse();
            if (!lookupsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(lookupsResponse.errorBody().byteStream(), lookupsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restLookupsResponse.setMessage(error.getMessage());
                    restLookupsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restLookupsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restLookupsResponse.setSuccess(false);
                }
            } else {
                restLookupsResponse = lookupsResponse.body();
                restLookupsResponse.setSuccess(true);
            }

            return restLookupsResponse;
        } catch (IOException e) {
            Timber.e("GET LOOKUPS");
            e.printStackTrace();

            RestLookupsResponse restLookupsResponse = new RestLookupsResponse();
            restLookupsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restLookupsResponse.setSuccess(false);
            return restLookupsResponse;
        } catch (Exception e) {
            RestLookupsResponse restLookupsResponse = new RestLookupsResponse();
            restLookupsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restLookupsResponse.setSuccess(false);

            return restLookupsResponse;
        }
    }

    @Override
    public RestAppSettingsResponse getAppSettings() {
        CommonDataService commonDataService = RestClient.getClient().create(CommonDataService.class);
        try {

            Response<RestAppSettingsResponse> appSettingsResponse = commonDataService.getAppSettings().execute();

            Timber.i("getAppSettings SUCCESS: %d", appSettingsResponse.code());

            RestAppSettingsResponse restAppSettingsResponse = new RestAppSettingsResponse();
            if (!appSettingsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(appSettingsResponse.errorBody().byteStream(), appSettingsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restAppSettingsResponse.setMessage(error.getMessage());
                    restAppSettingsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restAppSettingsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restAppSettingsResponse.setSuccess(false);
                }
            } else {
                restAppSettingsResponse = appSettingsResponse.body();
                restAppSettingsResponse.setSuccess(true);
            }

            return restAppSettingsResponse;
        } catch (IOException e) {

            Timber.e("getAppSettings Failed");
            e.printStackTrace();

            RestAppSettingsResponse restAppSettingsResponse = new RestAppSettingsResponse();
            restAppSettingsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restAppSettingsResponse.setSuccess(false);
            return restAppSettingsResponse;
        } catch (Exception e) {
            Log.d("getAppSettings", "catch2: ");

            RestAppSettingsResponse restAppSettingsResponse = new RestAppSettingsResponse();
            restAppSettingsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restAppSettingsResponse.setSuccess(false);

            return restAppSettingsResponse;
        }
    }

    @Override
    public CommonResponse sendSupportMessage(String token, SendSupportMessageBody sendSupportMessageBody) {
        CommonDataService commonDataService = RestClient.getClient().create(CommonDataService.class);
        try {

            Response appSettingsResponse = commonDataService.sendSupportMessage(token, sendSupportMessageBody).execute();

            Timber.i("sendSupportMessage SUCCESS: %d", appSettingsResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!appSettingsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(appSettingsResponse.errorBody().byteStream(), appSettingsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("sendSupportMessage Failed");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public RestSupportMessagesHistoryResponse getSupportMessagesHistory(String token, int userId) {
        CommonDataService commonDataService = RestClient.getClient().create(CommonDataService.class);
        try {

            Response<RestSupportMessagesHistoryResponse> supportMessageHistoryResponse = commonDataService
                    .getSupportMessagesHistory(token, userId).execute();

            Timber.i("getSupportMessagesHistory SUCCESS: %d", supportMessageHistoryResponse.code());

            RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse = new RestSupportMessagesHistoryResponse();
            if (!supportMessageHistoryResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(supportMessageHistoryResponse.errorBody().byteStream(), supportMessageHistoryResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restSupportMessagesHistoryResponse.setMessage(error.getMessage());
                    restSupportMessagesHistoryResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restSupportMessagesHistoryResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restSupportMessagesHistoryResponse.setSuccess(false);
                }
            } else {
                restSupportMessagesHistoryResponse = supportMessageHistoryResponse.body();
                restSupportMessagesHistoryResponse.setSuccess(true);
            }

            return restSupportMessagesHistoryResponse;
        } catch (IOException e) {
            Timber.e("getSupportMessagesHistory Failed");
            e.printStackTrace();

            RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse = new RestSupportMessagesHistoryResponse();
            restSupportMessagesHistoryResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restSupportMessagesHistoryResponse.setSuccess(false);
            return restSupportMessagesHistoryResponse;
        } catch (Exception e) {
            RestSupportMessagesHistoryResponse restSupportMessagesHistoryResponse = new RestSupportMessagesHistoryResponse();
            restSupportMessagesHistoryResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restSupportMessagesHistoryResponse.setSuccess(false);

            return restSupportMessagesHistoryResponse;
        }
    }

    @Override
    public RestSupportMessageDetailsResponse getSupportMessageDetails(String token, int supportMessageId) {
        CommonDataService commonDataService = RestClient.getClient().create(CommonDataService.class);
        try {

            Response<RestSupportMessageDetailsResponse> supportMessageDetailsResponse = commonDataService
                    .getSupportMessageDetails(token, supportMessageId).execute();

            Timber.i("getSupportMessageDetails SUCCESS: %d", supportMessageDetailsResponse.code());

            RestSupportMessageDetailsResponse restSupportMessagesHistoryResponse = new RestSupportMessageDetailsResponse();
            if (!supportMessageDetailsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(supportMessageDetailsResponse.errorBody().byteStream(), supportMessageDetailsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restSupportMessagesHistoryResponse.setMessage(error.getMessage());
                    restSupportMessagesHistoryResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restSupportMessagesHistoryResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restSupportMessagesHistoryResponse.setSuccess(false);
                }
            } else {
                restSupportMessagesHistoryResponse = supportMessageDetailsResponse.body();
                restSupportMessagesHistoryResponse.setSuccess(true);
            }

            return restSupportMessagesHistoryResponse;
        } catch (IOException e) {
            Timber.e("getSupportMessageDetails Failed");
            e.printStackTrace();

            RestSupportMessageDetailsResponse restSupportMessageDetailsResponse = new RestSupportMessageDetailsResponse();
            restSupportMessageDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restSupportMessageDetailsResponse.setSuccess(false);
            return restSupportMessageDetailsResponse;
        } catch (Exception e) {
            RestSupportMessageDetailsResponse restSupportMessageDetailsResponse = new RestSupportMessageDetailsResponse();
            restSupportMessageDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restSupportMessageDetailsResponse.setSuccess(false);

            return restSupportMessageDetailsResponse;
        }
    }

    @Override
    public CommonResponse saveOneSignalUserId(String token, SaveOneSignalBody saveOneSignalBody) {
        CommonDataService commonDataService = RestClient.getClient().create(CommonDataService.class);
        try {

            Response<CommonResponse> supportMessageDetailsResponse = commonDataService
                    .saveOneSignalUserId(token, saveOneSignalBody).execute();

            Timber.i("saveOneSignalUserId SUCCESS: %d", supportMessageDetailsResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!supportMessageDetailsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(supportMessageDetailsResponse.errorBody().byteStream(), supportMessageDetailsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("saveOneSignalUserId Failed");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }
}
