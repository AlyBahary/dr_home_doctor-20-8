package dr.doctorathome.network.repositoryImpl;

import android.util.Log;

import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.domain.repositories.DoctorRepository;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.BlockPatientBody;
import dr.doctorathome.network.model.ChangeOnlineConsultationStatuesBody;
import dr.doctorathome.network.model.ChangePasswordBody;
import dr.doctorathome.network.model.ChangeVisitStatuesBody;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ConsultRequest;
import dr.doctorathome.network.model.DoctorMedicineBoxesBody;
import dr.doctorathome.network.model.DoctorNewLocationBody;
import dr.doctorathome.network.model.DoctorRequest;
import dr.doctorathome.network.model.DoctorServices;
import dr.doctorathome.network.model.EnableDisableServiceBody;
import dr.doctorathome.network.model.ErrorModel;
import dr.doctorathome.network.model.GetReportBody;
import dr.doctorathome.network.model.LabTestInPrescription;
import dr.doctorathome.network.model.OnlineConsultationsBody;
import dr.doctorathome.network.model.RatePatientBody;
import dr.doctorathome.network.model.RenewCardBody;
import dr.doctorathome.network.model.RequestClearanceBody;
import dr.doctorathome.network.model.RestAllLabTestsResponse;
import dr.doctorathome.network.model.RestAllMedicinesResponse;
import dr.doctorathome.network.model.RestChangeDoctorStatuesBody;
import dr.doctorathome.network.model.RestDoctorMedicineBoxesResponse;
import dr.doctorathome.network.model.RestDoctorServicesResponse;
import dr.doctorathome.network.model.RestHomePageResponse;
import dr.doctorathome.network.model.RestNotificationsResponse;
import dr.doctorathome.network.model.RestOnlineConsultationRequestDetailsResponse;
import dr.doctorathome.network.model.RestOnlineConsultationsRequestsResponse;
import dr.doctorathome.network.model.RestPrescriptionDetailsResponse;
import dr.doctorathome.network.model.RestProfileStatistcsResponse;
import dr.doctorathome.network.model.RestReportDetailsResponse;
import dr.doctorathome.network.model.RestVisitDetailsResponse;
import dr.doctorathome.network.model.RestVisitRequestsResponse;
import dr.doctorathome.network.model.SavingPrescreptionBody;
import dr.doctorathome.network.model.ServerCommonResponse;
import dr.doctorathome.network.model.StartEndSessionBody;
import dr.doctorathome.network.model.VisitRequestsBody;
import dr.doctorathome.network.services.DoctorService;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Response;
import timber.log.Timber;

public class DoctorRepositoryImpl implements DoctorRepository {
    @Override
    public CommonResponse changeDoctorStatues(RestChangeDoctorStatuesBody changeDoctorStatuesBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response changeDoctorStatuesResponse = doctorService.changeDoctorStatues(changeDoctorStatuesBody, token).execute();

            Timber.i("changeDoctorStatues SUCCESS: %d", changeDoctorStatuesResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!changeDoctorStatuesResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(changeDoctorStatuesResponse.errorBody().byteStream(), changeDoctorStatuesResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("changeDoctorStatues FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public RestHomePageResponse getDoctorHomePageData(int doctorId, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestHomePageResponse> doctorHomePageDataResponse = doctorService.getDoctorHomePageData(
                    doctorId, token).execute();

            Timber.i("getDoctorHomePageData SUCCESS: %d", doctorHomePageDataResponse.code());
            Timber.i("getDoctorHomePageData SUCCESS: %d", doctorId);

            RestHomePageResponse restHomePageResponse = new RestHomePageResponse();
            if (!doctorHomePageDataResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(doctorHomePageDataResponse.errorBody().byteStream(), doctorHomePageDataResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restHomePageResponse.setMessage(error.getMessage());
                    restHomePageResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restHomePageResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restHomePageResponse.setSuccess(false);
                }
            } else {
                restHomePageResponse = doctorHomePageDataResponse.body();
                restHomePageResponse.setSuccess(true);
            }

            return restHomePageResponse;
        } catch (IOException e) {
            Timber.e("getDoctorHomePageData FAIL");
            e.printStackTrace();

            RestHomePageResponse restHomePageResponse = new RestHomePageResponse();
            restHomePageResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restHomePageResponse.setSuccess(false);
            return restHomePageResponse;
        } catch (Exception e) {
            RestHomePageResponse restHomePageResponse = new RestHomePageResponse();
            restHomePageResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restHomePageResponse.setSuccess(false);

            return restHomePageResponse;
        }
    }

    @Override
    public RestDoctorServicesResponse getDoctorServices(int doctorId, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<DoctorServices> doctorServicesResponse = doctorService.getDoctorServices(
                    doctorId, token).execute();

            Timber.i("getDoctorServices SUCCESS: %d", doctorServicesResponse.code());

            RestDoctorServicesResponse restDoctorServicesResponse = new RestDoctorServicesResponse();
            if (!doctorServicesResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(doctorServicesResponse.errorBody().byteStream(), doctorServicesResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restDoctorServicesResponse.setMessage(error.getMessage());
                    restDoctorServicesResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restDoctorServicesResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restDoctorServicesResponse.setSuccess(false);
                }
            } else {
                DoctorServices doctorServices = doctorServicesResponse.body();
                restDoctorServicesResponse.setDoctorServices(doctorServices);
                restDoctorServicesResponse.setSuccess(true);
            }

            return restDoctorServicesResponse;
        } catch (IOException e) {
            Timber.e("getDoctorServices FAIL");
            e.printStackTrace();

            RestDoctorServicesResponse restDoctorServicesResponse = new RestDoctorServicesResponse();
            restDoctorServicesResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restDoctorServicesResponse.setSuccess(false);
            return restDoctorServicesResponse;
        } catch (Exception e) {
            RestDoctorServicesResponse restDoctorServicesResponse = new RestDoctorServicesResponse();
            restDoctorServicesResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restDoctorServicesResponse.setSuccess(false);

            return restDoctorServicesResponse;
        }
    }

    @Override
    public CommonResponse enableDisableDoctorService(EnableDisableServiceBody enableDisableServiceBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response enableDisableDoctorServiceResponse = doctorService.enableDisableDoctorService(
                    enableDisableServiceBody, token).execute();

            Timber.i("enableDisableDoctorService SUCCESS: %d", enableDisableDoctorServiceResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!enableDisableDoctorServiceResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(enableDisableDoctorServiceResponse.errorBody().byteStream(), enableDisableDoctorServiceResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("enableDisableDoctorService FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public RestVisitRequestsResponse getDoctorVisits(VisitRequestsBody visitRequestsBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestVisitRequestsResponse> doctorVisitRequestsResponse = doctorService.getDoctorVisits(
                    visitRequestsBody, token).execute();

            Timber.i("getDoctorVisits SUCCESS: %d", doctorVisitRequestsResponse.code());

            RestVisitRequestsResponse restVisitRequestsResponse = new RestVisitRequestsResponse();
            if (!doctorVisitRequestsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(doctorVisitRequestsResponse.errorBody().byteStream(), doctorVisitRequestsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restVisitRequestsResponse.setMessage(error.getMessage());
                    restVisitRequestsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restVisitRequestsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restVisitRequestsResponse.setSuccess(false);
                }
            } else {
                restVisitRequestsResponse = doctorVisitRequestsResponse.body();
                restVisitRequestsResponse.setSuccess(true);
            }

            return restVisitRequestsResponse;
        } catch (IOException e) {
            Timber.e("getDoctorVisits FAIL");
            e.printStackTrace();

            RestVisitRequestsResponse restVisitRequestsResponse = new RestVisitRequestsResponse();
            restVisitRequestsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restVisitRequestsResponse.setSuccess(false);
            return restVisitRequestsResponse;
        } catch (Exception e) {
            RestVisitRequestsResponse restVisitRequestsResponse = new RestVisitRequestsResponse();
            restVisitRequestsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restVisitRequestsResponse.setSuccess(false);

            return restVisitRequestsResponse;
        }
    }

    @Override
    public RestOnlineConsultationsRequestsResponse getDoctorOnlineConsultations(OnlineConsultationsBody onlineConsultationsBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestOnlineConsultationsRequestsResponse> doctorOnlineConsultationRequestsResponse
                    = doctorService.getDoctorOnlineConsultations(
                    onlineConsultationsBody, token).execute();

            Timber.i("getDoctorOnlineConsultations SUCCESS: %d", doctorOnlineConsultationRequestsResponse.code());

            RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse
                    = new RestOnlineConsultationsRequestsResponse();
            if (!doctorOnlineConsultationRequestsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(doctorOnlineConsultationRequestsResponse.errorBody()
                        .byteStream(), doctorOnlineConsultationRequestsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restOnlineConsultationsRequestsResponse.setMessage(error.getMessage());
                    restOnlineConsultationsRequestsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restOnlineConsultationsRequestsResponse.setMessage(AndroidApplication.getAppContext()
                            .getString(R.string.parsing_error_message));
                    restOnlineConsultationsRequestsResponse.setSuccess(false);
                }
            } else {
                restOnlineConsultationsRequestsResponse = doctorOnlineConsultationRequestsResponse.body();
                restOnlineConsultationsRequestsResponse.setSuccess(true);
            }

            return restOnlineConsultationsRequestsResponse;
        } catch (IOException e) {
            Timber.e("getDoctorOnlineConsultations FAIL");
            e.printStackTrace();

            RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse
                    = new RestOnlineConsultationsRequestsResponse();
            restOnlineConsultationsRequestsResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restOnlineConsultationsRequestsResponse.setSuccess(false);
            return restOnlineConsultationsRequestsResponse;
        } catch (Exception e) {
            e.printStackTrace();
            RestOnlineConsultationsRequestsResponse restOnlineConsultationsRequestsResponse
                    = new RestOnlineConsultationsRequestsResponse();
            restOnlineConsultationsRequestsResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restOnlineConsultationsRequestsResponse.setSuccess(false);

            return restOnlineConsultationsRequestsResponse;
        }
    }

    @Override
    public RestOnlineConsultationRequestDetailsResponse getDoctorOnlineConsultationDetails(int id, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<ConsultRequest> consultRequestDetailsResponse = doctorService.getDoctorOnlineConsultationDetails(id, token).execute();

            Timber.i("getDoctorOnlineConsultationDetails SUCCESS: %d", consultRequestDetailsResponse.code());

            RestOnlineConsultationRequestDetailsResponse restOnlineConsultDetailsResponse = new RestOnlineConsultationRequestDetailsResponse();
            if (!consultRequestDetailsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(consultRequestDetailsResponse.errorBody().byteStream(), consultRequestDetailsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restOnlineConsultDetailsResponse.setMessage(error.getMessage());
                    restOnlineConsultDetailsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restOnlineConsultDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restOnlineConsultDetailsResponse.setSuccess(false);
                }
            } else {
                ConsultRequest doctorRequest = consultRequestDetailsResponse.body();
                restOnlineConsultDetailsResponse.setConsultRequest(doctorRequest);
                restOnlineConsultDetailsResponse.setSuccess(true);
            }

            return restOnlineConsultDetailsResponse;
        } catch (IOException e) {
            Timber.e("getDoctorOnlineConsultationDetails FAIL");
            e.printStackTrace();

            RestOnlineConsultationRequestDetailsResponse restOnlineConsultDetailsResponse = new RestOnlineConsultationRequestDetailsResponse();
            restOnlineConsultDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restOnlineConsultDetailsResponse.setSuccess(false);
            return restOnlineConsultDetailsResponse;
        } catch (Exception e) {
            RestOnlineConsultationRequestDetailsResponse restOnlineConsultDetailsResponse = new RestOnlineConsultationRequestDetailsResponse();
            restOnlineConsultDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restOnlineConsultDetailsResponse.setSuccess(false);

            return restOnlineConsultDetailsResponse;
        }
    }

    @Override
    public CommonResponse changeOnlineConsultationStatus(ChangeOnlineConsultationStatuesBody changeOnlineConsultationStatuesBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response changeConsultationStatusResponse = doctorService.changeOnlineConsultationStatus(
                    changeOnlineConsultationStatuesBody, token).execute();

            Timber.i("changeOnlineConsultationStatus SUCCESS: %d", changeConsultationStatusResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!changeConsultationStatusResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(changeConsultationStatusResponse.errorBody().byteStream(), changeConsultationStatusResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("changeOnlineConsultationStatus FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public RestVisitDetailsResponse getVisitDetails(Integer visitId, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<DoctorRequest> visitDetailsResponse = doctorService.getVisitDetails(
                    visitId, token).execute();

            Timber.i("getVisitDetails SUCCESS: %d", visitDetailsResponse.code());

            RestVisitDetailsResponse restVisitDetailsResponse = new RestVisitDetailsResponse();
            if (!visitDetailsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(visitDetailsResponse.errorBody().byteStream(), visitDetailsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restVisitDetailsResponse.setMessage(error.getMessage());
                    restVisitDetailsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restVisitDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restVisitDetailsResponse.setSuccess(false);
                }
            } else {
                DoctorRequest doctorRequest = visitDetailsResponse.body();
                restVisitDetailsResponse.setDoctorRequest(doctorRequest);
                restVisitDetailsResponse.setSuccess(true);
            }

            return restVisitDetailsResponse;
        } catch (IOException e) {
            Timber.e("getVisitDetails FAIL");
            e.printStackTrace();

            RestVisitDetailsResponse restVisitDetailsResponse = new RestVisitDetailsResponse();
            restVisitDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restVisitDetailsResponse.setSuccess(false);
            return restVisitDetailsResponse;
        } catch (Exception e) {
            Timber.e("getVisitDetails FAIL2"+e.getMessage());

            RestVisitDetailsResponse restVisitDetailsResponse = new RestVisitDetailsResponse();
            restVisitDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restVisitDetailsResponse.setSuccess(false);

            return restVisitDetailsResponse;
        }
    }

    @Override
    public RestPrescriptionDetailsResponse getPrescriptionDetails(Integer visitId, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestPrescriptionDetailsResponse> prescriptionDetailsResponse = doctorService.getPrescriptionDetails(
                    visitId, token).execute();

            Timber.i("getPrescriptionDetails SUCCESS: %d", prescriptionDetailsResponse.code());

            RestPrescriptionDetailsResponse restPrescriptionDetailsResponse = new RestPrescriptionDetailsResponse();
            if (!prescriptionDetailsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(prescriptionDetailsResponse.errorBody().byteStream(), prescriptionDetailsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restPrescriptionDetailsResponse.setMessage(error.getMessage());
                    restPrescriptionDetailsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restPrescriptionDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restPrescriptionDetailsResponse.setSuccess(false);
                }
            } else {
                restPrescriptionDetailsResponse = prescriptionDetailsResponse.body();
                restPrescriptionDetailsResponse.setSuccess(true);
            }

            return restPrescriptionDetailsResponse;
        } catch (IOException e) {
            Timber.e("getPrescriptionDetails FAIL");
            e.printStackTrace();

            RestPrescriptionDetailsResponse restPrescriptionDetailsResponse = new RestPrescriptionDetailsResponse();
            restPrescriptionDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restPrescriptionDetailsResponse.setSuccess(false);
            return restPrescriptionDetailsResponse;
        } catch (Exception e) {
            RestPrescriptionDetailsResponse restPrescriptionDetailsResponse = new RestPrescriptionDetailsResponse();
            restPrescriptionDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restPrescriptionDetailsResponse.setSuccess(false);

            return restPrescriptionDetailsResponse;
        }
    }

    @Override
    public CommonResponse changeVisitStatues(ChangeVisitStatuesBody changeVisitStatuesBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response changeVisitStatuesResponse = doctorService.changeVisitStatues(
                    changeVisitStatuesBody, token).execute();

            Timber.i("changeVisitStatues SUCCESS: %d", changeVisitStatuesResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!changeVisitStatuesResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(changeVisitStatuesResponse.errorBody().byteStream(), changeVisitStatuesResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("changeVisitStatues FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public CommonResponse startSession(StartEndSessionBody startEndSessionBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response startSessionResponse = doctorService.startSession(
                    startEndSessionBody, token).execute();

            Timber.i("startSession SUCCESS: %d", startSessionResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!startSessionResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(startSessionResponse.errorBody().byteStream(), startSessionResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("startSession FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public CommonResponse endSession(StartEndSessionBody startEndSessionBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response endSessionResponse = doctorService.endSession(
                    startEndSessionBody, token).execute();

            Timber.i("endSession SUCCESS: %d", endSessionResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!endSessionResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(endSessionResponse.errorBody().byteStream(), endSessionResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("endSession FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public CommonResponse ratePatient(Integer visitId, RatePatientBody ratePatientBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response ratePatientResponse = doctorService.ratePatient(visitId,
                    ratePatientBody, token).execute();

            Timber.i("ratePatient SUCCESS: %d", ratePatientResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!ratePatientResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(ratePatientResponse.errorBody().byteStream(), ratePatientResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("ratePatient FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public RestAllMedicinesResponse getAllMedicines(String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestAllMedicinesResponse> allMedicinesResponse
                    = doctorService.getAllSystemMedicines(token).execute();

            Timber.i("getAllMedicines SUCCESS: %d", allMedicinesResponse.code());

            RestAllMedicinesResponse restAllMedicinesResponse
                    = new RestAllMedicinesResponse();
            if (!allMedicinesResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(allMedicinesResponse.errorBody()
                        .byteStream(), allMedicinesResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restAllMedicinesResponse.setMessage(error.getMessage());
                    restAllMedicinesResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restAllMedicinesResponse.setMessage(AndroidApplication.getAppContext()
                            .getString(R.string.parsing_error_message));
                    restAllMedicinesResponse.setSuccess(false);
                }
            } else {
                restAllMedicinesResponse = allMedicinesResponse.body();
                restAllMedicinesResponse.setSuccess(true);
            }

            return restAllMedicinesResponse;
        } catch (IOException e) {
            Timber.e("getAllMedicines FAIL");
            e.printStackTrace();

            RestAllMedicinesResponse restAllMedicinesResponse
                    = new RestAllMedicinesResponse();
            restAllMedicinesResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restAllMedicinesResponse.setSuccess(false);
            return restAllMedicinesResponse;
        } catch (Exception e) {
            RestAllMedicinesResponse restAllMedicinesResponse
                    = new RestAllMedicinesResponse();
            restAllMedicinesResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restAllMedicinesResponse.setSuccess(false);

            return restAllMedicinesResponse;
        }
    }

    @Override
    public RestAllLabTestsResponse getAllLabTests(String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<List<LabTestInPrescription>> allLabTestsResponse
                    = doctorService.getAllSystemLabTests(token).execute();

            Timber.i("getDoctorMedicineBoxes SUCCESS: %d", allLabTestsResponse.code());

            RestAllLabTestsResponse restAllLabTestsResponse
                    = new RestAllLabTestsResponse();
            if (!allLabTestsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(allLabTestsResponse.errorBody()
                        .byteStream(), allLabTestsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restAllLabTestsResponse.setMessage(error.getMessage());
                    restAllLabTestsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restAllLabTestsResponse.setMessage(AndroidApplication.getAppContext()
                            .getString(R.string.parsing_error_message));
                    restAllLabTestsResponse.setSuccess(false);
                }
            } else {
                List<LabTestInPrescription> labTestInPrescriptionList = allLabTestsResponse.body();
                restAllLabTestsResponse.setLabTestInPrescriptionList(labTestInPrescriptionList);
                restAllLabTestsResponse.setSuccess(true);
            }

            return restAllLabTestsResponse;
        } catch (IOException e) {
            Timber.e("getDoctorMedicineBoxes FAIL");
            e.printStackTrace();

            RestAllLabTestsResponse restAllLabTestsResponse
                    = new RestAllLabTestsResponse();
            restAllLabTestsResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restAllLabTestsResponse.setSuccess(false);
            return restAllLabTestsResponse;
        } catch (Exception e) {
            RestAllLabTestsResponse restAllLabTestsResponse
                    = new RestAllLabTestsResponse();
            restAllLabTestsResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restAllLabTestsResponse.setSuccess(false);

            return restAllLabTestsResponse;
        }
    }

    @Override
    public RestDoctorMedicineBoxesResponse getDoctorMedicineBoxes(DoctorMedicineBoxesBody medicineBoxesBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestDoctorMedicineBoxesResponse> doctorMedicineBoxesResponse
                    = doctorService.getDoctorMedicinesBoxes(medicineBoxesBody, token).execute();

            Timber.i("getDoctorMedicineBoxes SUCCESS: %d", doctorMedicineBoxesResponse.code());

            RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse
                    = new RestDoctorMedicineBoxesResponse();
            if (!doctorMedicineBoxesResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(doctorMedicineBoxesResponse.errorBody()
                        .byteStream(), doctorMedicineBoxesResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restDoctorMedicineBoxesResponse.setMessage(error.getMessage());
                    restDoctorMedicineBoxesResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restDoctorMedicineBoxesResponse.setMessage(AndroidApplication.getAppContext()
                            .getString(R.string.parsing_error_message));
                    restDoctorMedicineBoxesResponse.setSuccess(false);
                }
            } else {
                restDoctorMedicineBoxesResponse = doctorMedicineBoxesResponse.body();
                restDoctorMedicineBoxesResponse.setSuccess(true);
            }

            return restDoctorMedicineBoxesResponse;
        } catch (IOException e) {
            Timber.e("getDoctorMedicineBoxes FAIL");
            e.printStackTrace();

            RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse
                    = new RestDoctorMedicineBoxesResponse();
            restDoctorMedicineBoxesResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restDoctorMedicineBoxesResponse.setSuccess(false);
            return restDoctorMedicineBoxesResponse;
        } catch (Exception e) {
            RestDoctorMedicineBoxesResponse restDoctorMedicineBoxesResponse
                    = new RestDoctorMedicineBoxesResponse();
            restDoctorMedicineBoxesResponse.setMessage(AndroidApplication.getAppContext()
                    .getString(R.string.unknown_errror_message));
            restDoctorMedicineBoxesResponse.setSuccess(false);

            return restDoctorMedicineBoxesResponse;
        }
    }

    @Override
    public CommonResponse savePrescription(SavingPrescreptionBody savingPrescreptionBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response savePrescriptionResponse = doctorService.savePrescription(savingPrescreptionBody, token).execute();

            Timber.i("savePrescription SUCCESS: %d", savePrescriptionResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!savePrescriptionResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(savePrescriptionResponse.errorBody().byteStream(), savePrescriptionResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("savePrescription FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public CommonResponse blockUnblockPatient(BlockPatientBody blockPatientBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response blockUnblockPatientResponse = doctorService.blockPatient(blockPatientBody, token).execute();

            Timber.i("blockUnblockPatient SUCCESS: %d", blockUnblockPatientResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!blockUnblockPatientResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(blockUnblockPatientResponse.errorBody().byteStream(), blockUnblockPatientResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("blockUnblockPatient FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public CommonResponse requestClearance(RequestClearanceBody requestClearanceBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response requestClearanceResponse = doctorService.requestClearance(requestClearanceBody, token).execute();

            Timber.i("requestClearance SUCCESS: %d", requestClearanceResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!requestClearanceResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(requestClearanceResponse.errorBody().byteStream(), requestClearanceResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("requestClearance FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public CommonResponse changePassword(ChangePasswordBody changePasswordBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response changePasswordResponse = doctorService.changePassword(changePasswordBody, token).execute();

            Timber.i("changePassword SUCCESS: %d", changePasswordResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!changePasswordResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(changePasswordResponse.errorBody().byteStream(), changePasswordResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("changePassword FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public RestReportDetailsResponse getReport(GetReportBody getReportBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestReportDetailsResponse> getReportResponse = doctorService.getReport(getReportBody, token).execute();

            Timber.i("getReport SUCCESS: %d", getReportResponse.code());


            RestReportDetailsResponse restReportDetailsResponse = new RestReportDetailsResponse();
            if (!getReportResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(getReportResponse.errorBody().byteStream(), getReportResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restReportDetailsResponse.setMessage(error.getMessage());
                    restReportDetailsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restReportDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restReportDetailsResponse.setSuccess(false);
                }
            } else {
                restReportDetailsResponse = getReportResponse.body();
                restReportDetailsResponse.setSuccess(true);
            }

            return restReportDetailsResponse;
        } catch (IOException e) {
            Timber.e("getReport FAIL");
            e.printStackTrace();

            RestReportDetailsResponse restReportDetailsResponse = new RestReportDetailsResponse();
            restReportDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restReportDetailsResponse.setSuccess(false);
            return restReportDetailsResponse;
        } catch (Exception e) {
            RestReportDetailsResponse restReportDetailsResponse = new RestReportDetailsResponse();
            restReportDetailsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restReportDetailsResponse.setSuccess(false);

            return restReportDetailsResponse;
        }
    }

    @Override
    public CommonResponse renewRegistrationCard(RenewCardBody renewCardBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<ServerCommonResponse> renewRegistrationCardResponse = doctorService.renewRegistrationCard(renewCardBody, token).execute();

            Timber.i("renewRegistrationCard SUCCESS: %d", renewRegistrationCardResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!renewRegistrationCardResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(renewRegistrationCardResponse.errorBody().byteStream(), renewRegistrationCardResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                ServerCommonResponse serverCommonResponse = renewRegistrationCardResponse.body();
                commonResponse.setSuccess(serverCommonResponse.getRspStatues() != null
                        && serverCommonResponse.getRspStatues().equals("Success"));
                commonResponse.setMessage(serverCommonResponse.getRspBody());
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("renewRegistrationCard FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public RestProfileStatistcsResponse getDoctorProfileStatistics(int doctorId, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestProfileStatistcsResponse> getDoctorProfileStatisticsResponse = doctorService.getDoctorProfileStatistics(doctorId, token).execute();

            Timber.i("getDoctorProfileStatistics SUCCESS: %d", getDoctorProfileStatisticsResponse.code());

            RestProfileStatistcsResponse restProfileStatistcsResponse = new RestProfileStatistcsResponse();
            if (!getDoctorProfileStatisticsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(getDoctorProfileStatisticsResponse.errorBody().byteStream(), getDoctorProfileStatisticsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restProfileStatistcsResponse.setMessage(error.getMessage());
                    restProfileStatistcsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restProfileStatistcsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restProfileStatistcsResponse.setSuccess(false);
                }
            } else {
                restProfileStatistcsResponse = getDoctorProfileStatisticsResponse.body();
                restProfileStatistcsResponse.setSuccess(true);
            }

            return restProfileStatistcsResponse;
        } catch (IOException e) {
            Timber.e("getDoctorProfileStatistics FAIL");
            e.printStackTrace();

            RestProfileStatistcsResponse restProfileStatistcsResponse = new RestProfileStatistcsResponse();
            restProfileStatistcsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restProfileStatistcsResponse.setSuccess(false);
            return restProfileStatistcsResponse;
        } catch (Exception e) {
            RestProfileStatistcsResponse restProfileStatistcsResponse = new RestProfileStatistcsResponse();
            restProfileStatistcsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restProfileStatistcsResponse.setSuccess(false);

            return restProfileStatistcsResponse;
        }
    }

    @Override
    public ServerCommonResponse updateDoctorLocation(DoctorNewLocationBody doctorNewLocationBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<ServerCommonResponse> updateDoctorLocationResponse = doctorService.updateDoctorLocation(
                    doctorNewLocationBody, token).execute();

            Timber.i("updateDoctorLocation SUCCESS: %d", updateDoctorLocationResponse.code());

            ServerCommonResponse serverCommonResponse = new ServerCommonResponse();
            if (!updateDoctorLocationResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(updateDoctorLocationResponse.errorBody().byteStream(), updateDoctorLocationResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    serverCommonResponse.setMessage(error.getMessage());
                    serverCommonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    serverCommonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    serverCommonResponse.setSuccess(false);
                }
            } else {
                serverCommonResponse = updateDoctorLocationResponse.body();
                serverCommonResponse.setSuccess(true);
            }

            return serverCommonResponse;
        } catch (IOException e) {
            Timber.e("updateDoctorLocation FAIL");
            e.printStackTrace();

            ServerCommonResponse serverCommonResponse = new ServerCommonResponse();
            serverCommonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            serverCommonResponse.setSuccess(false);
            return serverCommonResponse;
        } catch (Exception e) {
            ServerCommonResponse serverCommonResponse = new ServerCommonResponse();
            serverCommonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            serverCommonResponse.setSuccess(false);

            return serverCommonResponse;
        }
    }

    @Override
    public ServerCommonResponse isArrived(DoctorNewLocationBody doctorNewLocationBody, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<ServerCommonResponse> updateDoctorLocationResponse = doctorService.isArrived(
                    doctorNewLocationBody, token).execute();

            Timber.i("isArrived SUCCESS: %d", updateDoctorLocationResponse.code());

            ServerCommonResponse serverCommonResponse = new ServerCommonResponse();
            if (!updateDoctorLocationResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(updateDoctorLocationResponse.errorBody().byteStream(), updateDoctorLocationResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    serverCommonResponse.setMessage(error.getMessage());
                    serverCommonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    serverCommonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    serverCommonResponse.setSuccess(false);
                }
            } else {
                serverCommonResponse = updateDoctorLocationResponse.body();
                serverCommonResponse.setSuccess(true);
            }

            return serverCommonResponse;
        } catch (IOException e) {
            Timber.e("isArrived FAIL");
            e.printStackTrace();

            ServerCommonResponse serverCommonResponse = new ServerCommonResponse();
            serverCommonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            serverCommonResponse.setSuccess(false);
            return serverCommonResponse;
        } catch (Exception e) {
            ServerCommonResponse serverCommonResponse = new ServerCommonResponse();
            serverCommonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            serverCommonResponse.setSuccess(false);

            return serverCommonResponse;
        }
    }

    @Override
    public RestNotificationsResponse getUserNotifications(int userId, String token, int pageNumber) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<RestNotificationsResponse> getUserNotificationsResponse = doctorService.getUserNotifications(userId, token, pageNumber).execute();

            Timber.i("getUserNotifications SUCCESS: %d", getUserNotificationsResponse.code());
            Log.d("TTTTTT", "getUserNotifications: "+getUserNotificationsResponse.raw().request().url().toString());

            RestNotificationsResponse restNotificationsResponse = new RestNotificationsResponse();
            if (!getUserNotificationsResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(getUserNotificationsResponse.errorBody().byteStream(), getUserNotificationsResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restNotificationsResponse.setMessage(error.getMessage());
                    restNotificationsResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restNotificationsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restNotificationsResponse.setSuccess(false);
                }
            } else {
                restNotificationsResponse = getUserNotificationsResponse.body();
                restNotificationsResponse.setSuccess(true);
            }

            return restNotificationsResponse;
        } catch (IOException e) {
            Timber.e("getUserNotifications FAIL");
            e.printStackTrace();

            RestNotificationsResponse restNotificationsResponse = new RestNotificationsResponse();
            restNotificationsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restNotificationsResponse.setSuccess(false);
            return restNotificationsResponse;
        } catch (Exception e) {
            RestNotificationsResponse restNotificationsResponse = new RestNotificationsResponse();
            restNotificationsResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restNotificationsResponse.setSuccess(false);

            return restNotificationsResponse;
        }
    }

    @Override
    public CommonResponse markNotificationAsRead(int notificationId, String token) {
        DoctorService doctorService = RestClient.getClient().create(DoctorService.class);
        try {
            Response<CommonResponse> markNotificationAsReadResponse = doctorService.markNotificationAsRead(notificationId, token).execute();

            Timber.i("markNotificationAsRead SUCCESS: %d", markNotificationAsReadResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!markNotificationAsReadResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(markNotificationAsReadResponse.errorBody().byteStream(), markNotificationAsReadResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("markNotificationAsRead FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }
}
