package dr.doctorathome.network.repositoryImpl;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import dr.doctorathome.AndroidApplication;
import dr.doctorathome.R;
import dr.doctorathome.config.Config;
import dr.doctorathome.domain.repositories.LoginRepository;
import dr.doctorathome.network.RestClient;
import dr.doctorathome.network.model.CommonResponse;
import dr.doctorathome.network.model.ErrorModel;
import dr.doctorathome.network.model.ResetPasswordModel;
import dr.doctorathome.network.model.RestLoginResponse;
import dr.doctorathome.network.model.RestLoginWithMailDataModel;
import dr.doctorathome.network.model.RestRequestForgetPasswordCodeResponse;
import dr.doctorathome.network.model.RestRequestSignupResponse;
import dr.doctorathome.network.model.RestUserDataResponse;
import dr.doctorathome.network.model.UserData;
import dr.doctorathome.network.model.UserDataForUpdateBody;
import dr.doctorathome.network.model.UserModel;
import dr.doctorathome.network.model.ValidateSignUpDataBody;
import dr.doctorathome.network.services.LoginService;
import dr.doctorathome.presentation.ui.activities.LoginActivity;
import dr.doctorathome.presentation.ui.activities.SplashActivity;
import dr.doctorathome.utils.CommonUtil;
import retrofit2.Response;
import timber.log.Timber;


public class LoginRepositoryImpl implements LoginRepository {
    SharedPreferences mSharedPreferences;

    @Override
    public RestLoginResponse loginWithMail(RestLoginWithMailDataModel loginDataModel) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Map<String, String> headers = new HashMap<>();
            //            headers.put("Content-Type", Config.CONTENT_TYPE);
            headers.put("Authorization", Config.BASIC_AUTH);
            //            headers.put("Accept", "application/json");
            Response<RestLoginResponse> loginResponse = loginService.loginWithEmail(loginDataModel.getUsername(),
                    loginDataModel.getPassword(), loginDataModel.getGrantType(), headers).execute();

            Timber.i("LOGIN SUCCESS: %d", loginResponse.code());
            Log.i("LOGIN SUCCESS: %d", loginResponse.raw().request().url() + "");

            RestLoginResponse restUserResponse = new RestLoginResponse();
            if (!loginResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(loginResponse.errorBody().byteStream(), loginResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restUserResponse.setMessage(error.getError_description());
                    restUserResponse.setSuccess(false);
                    //                    if(error.isSuccess() == null){
                    //                        restUserResponse.setTokenExpired(false);
                    //                    }else{
                    //                        restUserResponse.setTokenExpired(!error.isSuccess());
                    //                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    restUserResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restUserResponse.setSuccess(false);
                }
            } else {
                Log.d("TTTT", "loginWithMail: "+loginResponse.body().getMessage());
                Log.d("TTTT", "loginWithMail: "+loginResponse.body().getAccessToken());
                Log.d("TTTT", "loginWithMail: "+loginResponse.body().getRefresh_token());
                restUserResponse = loginResponse.body();
                restUserResponse.setSuccess(true);
            }

            return restUserResponse;
        } catch (IOException e) {
            Timber.e("LOGIN FAIL");
            e.printStackTrace();

            RestLoginResponse restUserResponse = new RestLoginResponse();
            restUserResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restUserResponse.setSuccess(false);
            return restUserResponse;
        } catch (Exception e) {
            RestLoginResponse restUserResponse = new RestLoginResponse();
            restUserResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restUserResponse.setSuccess(false);

            return restUserResponse;
        }
    }

    @Override
    public RestLoginResponse refreshToken(RestLoginWithMailDataModel loginDataModel) {
        Log.d("intercept", "refreshToken: "+loginDataModel.getRefresh_token());

        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Map<String, String> headers = new HashMap<>();
            //            headers.put("Content-Type", Config.CONTENT_TYPE);
            headers.put("Authorization", Config.BASIC_AUTH);
            //            headers.put("Accept", "application/json");
            Response<RestLoginResponse> loginResponse = loginService.refreshToken(
                    loginDataModel.getRefresh_token(), loginDataModel.getGrantType(),headers).execute();


            RestLoginResponse restUserResponse = new RestLoginResponse();
            Log.d("intercept", "refreshToken:code "+loginResponse.isSuccessful()+loginResponse.raw().code());
            if (!loginResponse.isSuccessful()) {

                String parsedError = CommonUtil.readIt(loginResponse.errorBody().byteStream(), loginResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                CommonUtil.logout();

                Intent i = AndroidApplication.getAppContext().getPackageManager()
                        .getLaunchIntentForPackage( AndroidApplication.getAppContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                AndroidApplication.getAppContext().startActivity(i);


                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restUserResponse.setMessage(error.getError_description());
                    restUserResponse.setSuccess(false);
                    //                    if(error.isSuccess() == null){
                    //                        restUserResponse.setTokenExpired(false);
                    //                    }else{
                    //                        restUserResponse.setTokenExpired(!error.isSuccess());
                    //                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    restUserResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.your_are_not_allowed_to_login));
                    restUserResponse.setSuccess(false);
                }
            } else {

                JWT jwt = new JWT(loginResponse.body().getAccessToken());
                mSharedPreferences = CommonUtil.commonSharedPref();
                SharedPreferences.Editor editor = mSharedPreferences.edit();

                editor.putString("token", "Bearer " + loginResponse.body().getAccessToken());
                editor.putString("refresh_token", loginResponse.body().getRefresh_token());
                editor.putInt("doctorId", jwt.getClaim("doctorId").asInt());
                editor.putInt("userId", jwt.getClaim("userId").asInt());
                editor.putString("firstName", jwt.getClaim("firstName").asString());
                editor.putString("lastName", jwt.getClaim("lastName").asString());
                editor.putString("email", jwt.getClaim("email").asString());
                editor.putString("specialist", jwt.getClaim("specialist").asString());
                editor.putString("profilePicUrl", jwt.getClaim("profilePicUrl").asString());
                editor.putString("uploadedExpiredCardStatus", jwt.getClaim("uploadedExpiredCardStatus").asString());
                editor.putBoolean("isExpired", jwt.getClaim("isExpired").asBoolean());
                editor.putString("accountStatus", jwt.getClaim("accountStatus").asString());
                editor.apply();
                editor.commit();
                restUserResponse = loginResponse.body();
                restUserResponse.setSuccess(true);
            }

            return restUserResponse;
        } catch (Exception e) {
            Log.d("intercept11", "refreshToken: "+e.getMessage());

        }

        return null;
    }


    @Override
    public RestRequestSignupResponse requestSignupCode(String phoneNumber) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Response<RestRequestSignupResponse> requestSignupCodeResponse = loginService.requestSignupCode(phoneNumber).execute();

            Timber.i("Signup Code SUCCESS: %d", requestSignupCodeResponse.code());

            RestRequestSignupResponse restRequestSignupResponse = new RestRequestSignupResponse();
            if (!requestSignupCodeResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(requestSignupCodeResponse.errorBody().byteStream(), requestSignupCodeResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restRequestSignupResponse.setMessage(error.getMessage());
                    restRequestSignupResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restRequestSignupResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restRequestSignupResponse.setSuccess(false);
                }
            } else {
                restRequestSignupResponse = requestSignupCodeResponse.body();
                restRequestSignupResponse.setSuccess(true);
            }

            return restRequestSignupResponse;
        } catch (IOException e) {
            Timber.e("Signup Code FAIL");
            e.printStackTrace();

            RestRequestSignupResponse restRequestSignupResponse = new RestRequestSignupResponse();
            restRequestSignupResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restRequestSignupResponse.setSuccess(false);
            return restRequestSignupResponse;
        } catch (Exception e) {
            RestRequestSignupResponse restRequestSignupResponse = new RestRequestSignupResponse();
            restRequestSignupResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restRequestSignupResponse.setSuccess(false);

            return restRequestSignupResponse;
        }
    }

    @Override
    public RestRequestForgetPasswordCodeResponse requestForgetPasswordCode(String email) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Response<RestRequestForgetPasswordCodeResponse> requestForgetPasswordCodeResponse
                    = loginService.requestForgetPasswordCode(email).execute();

            Timber.i("Forget Password Code SUCCESS: %d", requestForgetPasswordCodeResponse.code());

            RestRequestForgetPasswordCodeResponse restRequestForgetPasswordResponse = new RestRequestForgetPasswordCodeResponse();
            if (!requestForgetPasswordCodeResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(requestForgetPasswordCodeResponse.errorBody().byteStream(), requestForgetPasswordCodeResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restRequestForgetPasswordResponse.setMessage(error.getMessage());
                    restRequestForgetPasswordResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restRequestForgetPasswordResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restRequestForgetPasswordResponse.setSuccess(false);
                }
            } else {
                restRequestForgetPasswordResponse = requestForgetPasswordCodeResponse.body();
                restRequestForgetPasswordResponse.setSuccess(true);
            }

            return restRequestForgetPasswordResponse;
        } catch (IOException e) {
            Timber.e("Signup Code FAIL");
            e.printStackTrace();

            RestRequestForgetPasswordCodeResponse restRequestForgetPasswordResponse = new RestRequestForgetPasswordCodeResponse();
            restRequestForgetPasswordResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restRequestForgetPasswordResponse.setSuccess(false);
            return restRequestForgetPasswordResponse;
        } catch (Exception e) {
            RestRequestForgetPasswordCodeResponse restRequestForgetPasswordResponse = new RestRequestForgetPasswordCodeResponse();
            restRequestForgetPasswordResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restRequestForgetPasswordResponse.setSuccess(false);

            return restRequestForgetPasswordResponse;
        }
    }

    @Override
    public CommonResponse signup(UserModel userModel) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Response<CommonResponse> signupResponse = loginService.signup(userModel).execute();

            Timber.i("Signup SUCCESS: %d", signupResponse.code());

            CommonResponse restSignupResponse = new CommonResponse();
            if (!signupResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(signupResponse.errorBody().byteStream(), signupResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restSignupResponse.setMessage(error.getMessage());
                    restSignupResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restSignupResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restSignupResponse.setSuccess(false);
                }
            } else {
//                restSignupResponse = signupResponse.body();
                restSignupResponse.setSuccess(true);
            }

            return restSignupResponse;
        } catch (IOException e) {
            Timber.e("Signup FAIL");
            e.printStackTrace();

            CommonResponse restSignupResponse = new CommonResponse();
            restSignupResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restSignupResponse.setSuccess(false);
            return restSignupResponse;
        } catch (Exception e) {
            CommonResponse restSignupResponse = new CommonResponse();
            restSignupResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restSignupResponse.setSuccess(false);

            return restSignupResponse;
        }
    }

    @Override
    public CommonResponse resetPassword(ResetPasswordModel resetPasswordModel) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Response<CommonResponse> resetPasswordResponse = loginService.resetPassword(resetPasswordModel).execute();

            Timber.i("Reset Password SUCCESS: %d", resetPasswordResponse.code());

            CommonResponse restResetPasswordResponse = new CommonResponse();
            if (!resetPasswordResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(resetPasswordResponse.errorBody().byteStream(), resetPasswordResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restResetPasswordResponse.setMessage(error.getMessage());
                    restResetPasswordResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restResetPasswordResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restResetPasswordResponse.setSuccess(false);
                }
            } else {
                //                restResetPasswordResponse = resetPasswordResponse.body();
                restResetPasswordResponse.setSuccess(true);
            }

            return restResetPasswordResponse;
        } catch (IOException e) {
            Timber.e("Reset Password FAIL");
            e.printStackTrace();

            CommonResponse restResetPasswordResponse = new CommonResponse();
            restResetPasswordResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restResetPasswordResponse.setSuccess(false);
            return restResetPasswordResponse;
        } catch (Exception e) {
            CommonResponse restResetPasswordResponse = new CommonResponse();
            restResetPasswordResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restResetPasswordResponse.setSuccess(false);

            return restResetPasswordResponse;
        }
    }

    @Override
    public RestUserDataResponse getUserData(Integer id, String token) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Response<UserData> userDataResponse = loginService.getUserData(id, token).execute();

            Timber.i("getUserData SUCCESS: %d", userDataResponse.code());

            RestUserDataResponse restUserDataResponse = new RestUserDataResponse();
            if (!userDataResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(userDataResponse.errorBody().byteStream(), userDataResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    restUserDataResponse.setMessage(error.getMessage());
                    restUserDataResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    restUserDataResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    restUserDataResponse.setSuccess(false);
                }
            } else {
                UserData userData = userDataResponse.body();
                restUserDataResponse.setUserData(userData);
                restUserDataResponse.setSuccess(true);
            }

            return restUserDataResponse;
        } catch (IOException e) {
            Timber.e("getUserData FAIL");
            e.printStackTrace();

            RestUserDataResponse restUserDataResponse = new RestUserDataResponse();
            restUserDataResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restUserDataResponse.setSuccess(false);
            return restUserDataResponse;
        } catch (Exception e) {
            RestUserDataResponse restUserDataResponse = new RestUserDataResponse();
            restUserDataResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            restUserDataResponse.setSuccess(false);

            return restUserDataResponse;
        }
    }

    @Override
    public CommonResponse updateDoctor(UserDataForUpdateBody userData, String token) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Response updateDoctorResponse = loginService.updateDoctor(userData, token).execute();

            Timber.i("updateDoctor SUCCESS: %d", updateDoctorResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!updateDoctorResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(updateDoctorResponse.errorBody().byteStream(), updateDoctorResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
//                UserData userData =  updateDoctorResponse.body();
//                commonResponse.setUserData(userData);
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("updateDoctor FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }

    @Override
    public CommonResponse validateSignUpData(ValidateSignUpDataBody validateSignUpDataBody) {
        LoginService loginService = RestClient.getClient().create(LoginService.class);
        try {
            Response validateSignUpDataResponse = loginService.validateSignUpData(validateSignUpDataBody).execute();

            Timber.i("validateSignUpData SUCCESS: %d", validateSignUpDataResponse.code());

            CommonResponse commonResponse = new CommonResponse();
            if (!validateSignUpDataResponse.isSuccessful()) {
                String parsedError = CommonUtil.readIt(validateSignUpDataResponse.errorBody().byteStream(), validateSignUpDataResponse.errorBody().contentLength());
                Timber.w("%s", parsedError);
                try {
                    ErrorModel error = new GsonBuilder().create().fromJson(parsedError, ErrorModel.class);
                    commonResponse.setMessage(error.getMessage());
                    commonResponse.setSuccess(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.parsing_error_message));
                    commonResponse.setSuccess(false);
                }
            } else {
                commonResponse.setSuccess(true);
            }

            return commonResponse;
        } catch (IOException e) {
            Timber.e("validateSignUpData FAIL");
            e.printStackTrace();

            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);
            return commonResponse;
        } catch (Exception e) {
            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setMessage(AndroidApplication.getAppContext().getString(R.string.unknown_errror_message));
            commonResponse.setSuccess(false);

            return commonResponse;
        }
    }
}
