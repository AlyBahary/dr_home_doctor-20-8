package dr.doctorathome.config;

public class RequestStatues {
    public static final String PENDING = "PENDING";
    public static final String CONFIRMED = "CONFIRMED";
    public static final String CANCELLED = "CANCELLED";
    public static final String COMPLETED = "COMPLETED";
    public static final String REJECTED = "REJECTED";
    public static final String RUNNING = "RUNNING";
    public static final String PAID = "PAID";
}
