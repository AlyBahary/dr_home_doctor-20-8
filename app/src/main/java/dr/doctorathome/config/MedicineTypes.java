package dr.doctorathome.config;

public class MedicineTypes {
    public static final Integer RECOMENDED_MEDICINE = 0;
    public static final Integer MEDICINE_BOX = 1;
    public static final Integer OUT_OF_THE_SYSTEM = 2;
}
